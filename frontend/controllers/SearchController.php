<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use frontend\models\Search;

/**
 * Search controller
 */
class SearchController extends Controller
{
    
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays login page.
     *
     * @return mixed
     */
    public function actionIndex(){
		$model = new Search();
		$blogsData = '';
		$awardsData = '';
		$newsroomData = '';
		$boardData = '';
		$careerCampusData = '';
		$careerLearnData = '';
		$careerThoughtData = '';
		$careerWeMakeData = '';
		$caseStudyData = '';
		$contactUsData = '';
		$contentsData = '';
		$governanceData = '';
		$disclosureData = '';
		$focusData = '';
		$investorData = '';
		$leadershipData = '';
		$managementData = '';
		$pageImagesData = '';
		if(!empty($_GET['q'])){
			$q = $_GET['q'];
			$q = preg_replace('#[^a-z0-9_]#', '-', $q);
			$blogsData = $model->getSearchBlogsData($q);
			$newsroomData = $model->getSearchNewsroomData($q);
			$awardsData = $model->getSearchAwardsData($q);
			$boardData = $model->getSearchBoardData($q);
			$careerCampusData = $model->getSearchCareerCampusData($q);
			$careerLearnData = $model->getSearchCareerLearnData($q);
			$careerThoughtData = $model->getSearchCareerThoughtData($q);
			$careerWeMakeData = $model->getSearchCareerWeMakeData($q);
			$caseStudyData = $model->getSearchCaseStudyData($q);
			$contactUsData = $model->getSearchContactUsData($q);
			$contentsData = $model->getSearchContentsData($q);
			$governanceData = $model->getSearchCorporateGovernanceData($q);
			$disclosureData = $model->getSearchDisclosuresData($q);
			$focusData = $model->getSearchFocusAreaData($q);
			$investorData = $model->getSearchInvestorRelationsData($q);
			$leadershipData = $model->getSearchLeadershipData($q);
			$managementData = $model->getSearchManagementTeamData($q);
			$pageImagesData = $model->getSearchPageImagesData($q);
		}else{
			return $this->redirect('index');
		}
		
		return $this->render('index', [
						'blogsData' => $blogsData, 
						'newsroomData' => $newsroomData,
						'awardsData' => $awardsData,
						'boardData' => $boardData,
						'careerCampusData' => $careerCampusData,
						'careerLearnData' => $careerLearnData,
						'careerThoughtData' => $careerThoughtData,
						'careerWeMakeData' => $careerWeMakeData,
						'caseStudyData' => $caseStudyData,
						'contactUsData' => $contactUsData,
						'contentsData' => $contentsData,
						'governanceData' => $governanceData,
						'disclosureData' => $disclosureData,
						'focusData' => $focusData,
						'investorData' => $investorData,
						'leadershipData' => $leadershipData,
						'managementData' => $managementData,
						'pageImagesData' => $pageImagesData,
				]);
    }
	
}

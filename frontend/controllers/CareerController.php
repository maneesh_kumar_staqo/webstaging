<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use frontend\models\Career;
use frontend\models\Careerquery;

/**
 * Career controller
 */
class CareerController extends Controller
{
    
	/**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays career pages.
     *
     * @return mixed
     */
    public function actionIndex(){
		$model = new Career();
		$modelQuery = new Careerquery();
		
		// For get banner data
		$bannerData = $model->getBannerData();
		
		// For get content data
		$contentData = $model->getContentData();
		
		// For get we make data
		$wemakeData = $model->getWemakeData();
		
		// For get slider data
		$sliderData = $model->getSliderData();
		
		// For get thought data
		$thoughtData = $model->getThoughtData();
		
		// For get learn data
		$learnData = $model->getLearnData();
		
		// For get campus data
		$campusData = $model->getCampusData();
		
		// For get vacancies data
		$vacanciesData = $model->getVacanciesData();
		
		// For save career page query
		if(!empty($_POST['apply_for']) && !empty($_POST['name']) && !empty($_POST['email_id']) && !empty($_POST['contact_no']) && !empty($_POST['current_organisation']) && !empty($_POST['current_designation']) && !empty($_POST['experience'])){
			$modelQuery->apply_for = strip_tags($_POST['apply_for']);
			$modelQuery->name = strip_tags($_POST['name']);
			$modelQuery->email_id = strip_tags($_POST['email_id']);
			$modelQuery->contact_no = strip_tags($_POST['contact_no']);
			$modelQuery->current_organisation = strip_tags($_POST['current_organisation']);
			$modelQuery->current_designation = strip_tags($_POST['current_designation']);
			$modelQuery->experience = strip_tags($_POST['experience']);
			$msg = "Your details has been submitted successfully, We will contact you shortly.";
			if(!filter_var($modelQuery->email_id, FILTER_VALIDATE_EMAIL)) {
			  $msg = "Invalid email format.";
			}else{
				if(!empty($_FILES['image1']['name'])){
					$image_name = time().$_FILES['image1']['name'];
					$image_tmp_name = $_FILES['image1']['tmp_name'];
					$image_path = IMG_UPLOAD_URL.'career/';
					move_uploaded_file($image_tmp_name, $image_path. $image_name);
					$modelQuery->resume = $image_name;
				}
				$modelQuery->save();	
			}
			return $this->render('index', ['bannerData' => $bannerData, 'contentData' => $contentData, 'wemakeData' => $wemakeData, 'sliderData' => $sliderData, 'thoughtData' => $thoughtData, 'learnData' => $learnData, 'campusData' => $campusData, 'vacanciesData' => $vacanciesData, 'msg' => $msg]);
		}
		
		return $this->render('index', ['bannerData' => $bannerData, 'contentData' => $contentData, 'wemakeData' => $wemakeData, 'sliderData' => $sliderData, 'thoughtData' => $thoughtData, 'learnData' => $learnData, 'campusData' => $campusData, 'vacanciesData' => $vacanciesData]);
    }
	
}

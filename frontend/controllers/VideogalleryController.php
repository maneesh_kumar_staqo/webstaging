<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use frontend\models\Videogallery;
use backend\models\Category;

/**
 * Videogallery controller
 */
class VideogalleryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays video gallery pages.
     *
     * @return mixed
     */
    public function actionIndex(){
		$model = new Videogallery();
		$bannerData = $model->getBannerData();
		$category = ArrayHelper::map(Category::find()->where(['and', "type = 'video_gallery'"])->orderBy(['priority'=>SORT_ASC])->all(),'id','title');
		
		return $this->render('index', ['bannerData' => $bannerData, 'category' => $category]);
    }

}

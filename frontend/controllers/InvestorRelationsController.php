<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use frontend\models\InvestorRelation;
use backend\models\Category;

/**
 * Bgel controller
 */
class InvestorRelationsController extends Controller
{
    
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays contact us pages.
     *
     * @return mixed
     */
    public function actionIndex(){
        
        $page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $pageu= basename($page_url);


		$model = new InvestorRelation();
        $pageData = $model->getPageData($pageu);
          // echo "<pre>";print_r($pageData);die;
		return $this->render('index', ['pageData' => $pageData]);
    }
	
}

<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\Projects;

/**
 * Projects controller
 */
class ProjectsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                // 'actions' => [
                    // 'logout' => ['post'],
                // ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays all pages.
     *
     * @return mixed
     */
    public function actionIndex(){
        $model = new Projects();
        $pageData = $model->getPageData('utility-scale-projects');
        $state = $model->getStateData();

        return $this->render('index', ['pageData' => $pageData,'state'=>$state]);
    }
    
    public function actionStatedata(){
        $model = new Projects();
        $data = '';
        if(!empty($_GET['state_id'])){
            $data = $model->getStateData($_GET['state_id']);
        }
        return $this->renderPartial('statedata', ['data' => $data]);
    }
    
    public function actionStatedatamobile(){
        $model = new Projects();
        $data = '';
        if(!empty($_GET['state_id'])){
            $data = $model->getStateData();
        }
        return $this->renderPartial('statedatamobile', ['data' => $data]);
    }


           public function actionFilterData(){
          
        if (Yii::$app->request->isAjax) {
        Yii::$app->request->enableCsrfValidation = false; 
                $data = Yii::$app->request->get();
                $model = new Projects();
                $result = $model->getFilterProject($data['type'],$data['location']);
                
                $html="";
                foreach ($result as $key => $value) {
                     $html.='<div class="col-md-4">
                    <div class="c_and_i_feature_product_item">
                        <div class="card_white">
                            <img src="'.BASE_BE_URL.'uploads/project/'.$value['image'].' ">
                            <div class="c_and_i_feature_product_item_content">
                                <h4>'.$value['title'].'</h4>
                                <div>
                                    '.$value['description'].'
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
                }
             
            }
     
              return json_encode(array(
                                    "data"=>$html
                                    ));
       

    }
   

}

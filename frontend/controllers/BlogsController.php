<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use common\models\LoginForm;
use backend\models\Category;
use frontend\models\Blogs;

/**
 * Blogs controller
 */
class BlogsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                // 'actions' => [
                    // 'logout' => ['post'],
                // ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays all pages.
     *
     * @return mixed
     */
    public function actionIndex(){
		$model = new Blogs();
		$category = ArrayHelper::map(Category::find()->where(['and', "type = 'blogs'", "status = 'Active'"])->orderBy(['priority'=>SORT_ASC])->all(),'id','title');
		  $banner = $model->getMediaBanner('blog');
        return $this->render('index', ['category' => $category,'banner'=>$banner]);
    }
	
	public function actionSearch(){
		$model = new Blogs();
		if(empty($_GET['q'])){
			return $this->redirect('blogs');
		}
		$category = ArrayHelper::map(Category::find()->where(['and', "type = 'blogs'", "status = 'Active'"])->orderBy(['priority'=>SORT_ASC])->all(),'id','title');
		
        return $this->render('search', ['category' => $category]);
    }

	public function actionDetails($page_url=''){
		$model = new Blogs();
 
		$blogsData = $model->getBlogsData($page_url);
		
        return $this->render('details', ['blogsData' => $blogsData, 'page_url' => $page_url]);
    }

}

<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use common\models\LoginForm;
use backend\models\Category;
use frontend\models\Csr;

/**
 * Csr controller
 */
class CsrController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                // 'actions' => [
                    // 'logout' => ['post'],
                // ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays all pages.
     *
     * @return mixed
     */
    public function actionIndex(){
		$model = new Csr();
		$pageData = $model->getPageData('csr');
		$category = ArrayHelper::map(Category::find()->where(['and', "type = 'focus_areas'"])->orderBy(['priority'=>SORT_ASC])->all(),'id','title');
		// echo "<pre>"; print_r($pageData); die;
		
        return $this->render('index', ['pageData' => $pageData, 'category' => $category]);
    }
	
	public function actionGetfocusareas(){
		$model = new Csr();
		if(!empty($_GET['category_id'])){
			$category_id = $_GET['category_id'];
			$data = $model->getFocusData($category_id);
			return $this->renderPartial('focus', ['data' => $data]);
			// echo "<pre>"; print_r($data); die;	
		}
		
    }

}

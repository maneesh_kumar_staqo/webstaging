<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use frontend\models\Policies;

/**
 * Policies controller
 */
class PoliciesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                // 'actions' => [
                    // 'logout' => ['post'],
                // ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays all pages.
     *
     * @return mixed
     */
    public function actionIndex($page_name){
   
		$model = new Policies();
		$pageData = $model->getPageData($page_name);
        $page_id=$pageData[0]['id'];
        
        $policy = $model->getPolicyData($page_name);
        $disclouser = $model->getDisclouserData($page_id);  
        //   echo "<pre>";print_r($discl0user);die;
		$bannerData = $model->getBannerData($page_name);
		// For get content data
		$contentData = $model->getContentData($page_name);
        $address = $model->getAddressData($page_name);
        // echo "<pre>";print_r($address);die;
        return $this->render('index', ['policy' => $policy, 'bannerData' => $bannerData, 'contentData' => $contentData,'disclouser'=>$disclouser,'page_id'=>$page_id,'address'=>$address]);
    }

}

<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use frontend\models\Contact;
use frontend\models\Contactquery;
use frontend\models\Utility;
use backend\models\Category;

/**
 * Contact controller
 */
class ContactController extends Controller
{
    
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays contact us pages.
     *
     * @return mixed
     */
    public function actionIndex(){
		$model = new Contact();
		$bannerData = $model->getBannerData();
		$contactAddData = $model->getContactAddressData();
		$session = Yii::$app->session;
		$captcha_code_val = $session->get('captcha_code_val');
		
		$category = ArrayHelper::map(Category::find()->where(['and', "type = 'contact_us'"])->orderBy(['priority'=>SORT_ASC])->all(),'id','title');
		
		return $this->render('index', ['category' => $category, 'bannerData' => $bannerData, 'contactAddData' => $contactAddData, 'captcha_code_val' => $captcha_code_val]);
    }
	
	// For rooftop query
	public function actionContactquery(){
			$this->enableCsrfValidation = false; 
		$this->layout = false;
		$model = new Contactquery();
		$utility = new Utility();
		if(!empty($_POST['cname']) && !empty($_POST['email_id']) && !empty($_POST['message']) && !empty($_POST['type'])){
			$model->name = !empty($_POST['cname']) ? strip_tags($_POST['cname']) : '';
			$model->phone = !empty($_POST['phone']) ? strip_tags($_POST['phone']) : '';
			$model->email_id = !empty($_POST['email_id']) ? strip_tags($_POST['email_id']) : '';
			$model->message = !empty($_POST['message']) ? strip_tags($_POST['message']) : '';
			$model->type = !empty($_POST['type']) ? strip_tags($_POST['type']) : '';
			if (!filter_var($model->email_id, FILTER_VALIDATE_EMAIL)) {
			  echo "<script> alert('Invalid email format.'); location.href = 'contact-us'; </script>";
			  exit();
			}
			$model->save();
			// For send email
			$subject = 'Contact form query - HFE';
			$message = 'Dear Team, <br> You have received an query from Contact Us page with below details:';
			$message .= '<br><b>Name : </b> '.$model->name.'<br> <b>Email Id : </b>'.$model->email_id.'<br> <b>Message : </b>'.$model->message.'<br> <b>Type : </b>'.$model->type;
			$message .= '<br>Thanks & Regards<br>HFE Team';
			if($model->type == "General Information"){
				$to = 'info@herofutureenergies.com';
			}else if($model->type == "C&I Enquires"){
				$to = 'solarrooftop@herofutureenergies.com';	
			}else if($model->type == "International Business"){
				$to = 'HFE_IBD@herofutureenergies.com';	
			}
			$bcc = 'sarita@uncindia.in';
			//$bcc = array("sarita@uncindia.in" => "Sarita", "aman@uncindia.in" => "Aman");
			$utility->sendEmail($to,$subject,$message,$bcc);
			//$utility->sendEmail($to,$subject,$message);
			
			echo "<script> alert('Your details has been submitted successfully, We will contact you shortly.'); location.href = 'contact-us'; </script>";
		}else{
			echo "<script> alert('*All fields are required.'); location.href = 'contact-us'; </script>";
		}
	}
	
	public function actionTestemail(){
		$utility = new Utility();
		$to = 'sarita@asearchonline.com';
		$subject = 'Test Subject';
		$message = 'Test Message';
		$utility->sendEmail($to,$subject,$message);
		echo "send email."; die;
	}
	
	public function actionCaptcha(){
		// session_start(); // Staring Session
		$captchanumber = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz'; // Initializing PHP variable with string
		$captchanumber = substr(str_shuffle($captchanumber), 0, 6); // Getting first 6 word after shuffle.
		$session = Yii::$app->session;
		$session->set('captcha_code_val', $captchanumber);
		$image = imagecreatefromjpeg("captcha.jpg"); // Generating CAPTCHA
		$foreground = imagecolorallocate($image, 175, 199, 200); // Font Color
		imagestring($image, 5, 45, 8, $captchanumber, $foreground);
		header('Content-type: image/png');
		imagepng($image);
	}
	
}

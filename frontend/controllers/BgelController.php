<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use frontend\models\Bgel;
use backend\models\Category;

/**
 * Bgel controller
 */
class BgelController extends Controller
{
    
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays contact us pages.
     *
     * @return mixed
     */
    public function actionIndex(){
  
        $page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $pageu= basename($page_url);
        // echo $pageu;die;

		$model = new Bgel();
        $pageData = $model->getPageData($pageu);
        $page_id=$pageData['page_data']['id'];
		$bannerData = $model->getBannerData($page_id);
		$contentData = $model->getContentData($page_id);
        $discoulser = $model->getDisclouserData($page_id);

		$category = ArrayHelper::map(Category::find()->where(['and', "type = 'disclosures'", "status = 'Active'"])->orderBy(['priority'=>SORT_ASC])->all(),'id','title');
		
		return $this->render('index', ['category' => $category, 'bannerData' => $bannerData, 'contentData' => $contentData,'discoulser'=>$discoulser,'pageData' => $pageData,'page_id'=>$page_id]);
    }
	
}

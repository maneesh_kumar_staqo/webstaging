<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\Rooftop;
use frontend\models\Rooftopquery;

/**
 * Rooftop controller
 */
class RooftopController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                // 'actions' => [
                    // 'logout' => ['post'],
                // ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays all pages.
     *
     * @return mixed
     */
    public function actionIndex(){
		$model = new Rooftop();
		$pageData = $model->getPageData('ci-solution');

        $offerdata = $model->getOfferData();
        return $this->render('index', ['pageData' => $pageData,'offerdata'=>$offerdata]);
    }
	
	// For rooftop query
	public function actionQuery(){
        $this->enableCsrfValidation = false; 
		$this->layout = false;
		$model = new Rooftopquery();
		if(!empty($_POST)){
			$model->name = !empty($_POST['name']) ? $_POST['name'] : '';
			$model->designation = !empty($_POST['designation']) ? $_POST['designation'] : '';
			$model->annual_turnover = !empty($_POST['annual_turnover']) ? $_POST['annual_turnover'] : '';
			$model->monthly_electricity_consuption = !empty($_POST['monthly_electricity_consuption']) ? $_POST['monthly_electricity_consuption'] : '';
			$model->email = !empty($_POST['email']) ? $_POST['email'] : '';
			$model->company_name = !empty($_POST['company_name']) ? $_POST['company_name'] : '';
			$model->average_monthly_bill = !empty($_POST['average_monthly_bill']) ? $_POST['average_monthly_bill'] : '';
			$model->phone = !empty($_POST['phone']) ? $_POST['phone'] : '';
			$model->city = !empty($_POST['city']) ? $_POST['city'] : '';
			$model->message = !empty($_POST['message']) ? $_POST['message'] : '';
			$model->approximate_space_available = !empty($_POST['approximate_space_available']) ? $_POST['approximate_space_available'] : '';
			$model->save();
			echo "<script> alert('Your details has been submitted successfully, We will contact you shortly.'); location.href = 'utility-scale-projects'; </script>";
			// return $this->redirect('rooftop-solar');
		}

	}
	
	// For rooftop calculator
	public function actionCalculator(){
		$this->layout = false;
           if (Yii::$app->request->isAjax) {
            Yii::$app->request->enableCsrfValidation = false; 
            $data = Yii::$app->request->get();
            
            if(!empty($data['rooftop_area'])){
                		$model = new Rooftopquery();
				$model->saveCalculatorQuery($data);
				$rooftop_area = $data['rooftop_area'];
				$area = $rooftop_area;
				$capacity = ($rooftop_area/10);
				$cost = ($capacity*50000);
				$energy = ($capacity*1350);
				$co2 = ($capacity*1.2);
				$saving = ($energy*8);
				$payback = "Within 4 years";
				echo $area."@@".$capacity."@@".$cost."@@".$energy."@@".$co2."@@".$saving."@@".$payback;
				exit();
			}
           }

		$model = new Rooftopquery();
		// if(!empty($_POST)){
		// 	if(!empty($_POST['rooftop_area'])){
		// 		$model->saveCalculatorQuery($_POST);
		// 		$rooftop_area = $_POST['rooftop_area'];
		// 		$area = $rooftop_area;
		// 		$capacity = ($rooftop_area/10);
		// 		$cost = ($capacity*50000);
		// 		$energy = ($capacity*1350);
		// 		$co2 = ($capacity*1.2);
		// 		$saving = ($energy*8);
		// 		$payback = "Within 4 years";
		// 		echo $area."@@".$capacity."@@".$cost."@@".$energy."@@".$co2."@@".$saving."@@".$payback;
		// 		exit();
		// 	}
		// }
	}

}

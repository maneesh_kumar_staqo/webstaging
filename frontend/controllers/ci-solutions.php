<!DOCTYPE html>
<html>
   <head>
      <?php include 'common/css-url.php';?>
      <title></title>
   </head>
   <body>
      <?php include 'common/header.php';?>
      <div class="about-us-page" id="wrapper">
         <section>
            <div class="n_home_slider">
               <div class="n_home_slider_item" style="background-image: url(images/c-and-i-n2.jpg);">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-left">Green Powered Businesses </h4>
                        <p class="text-left">
                           Offering Green Energy Solutions for Industries  since 2012 <br>

175 locations across 20+ Indian cities
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- <section class="sec_pd_normal bg_sec">   
            <div class="container">
            
               <div class="c_and_i_praise">
                  <p><span>175 +</span> locations  across <span>20+</span> Indian cities
                  </p>
               </div>
            
            
            </div>
            </section> -->
         <section class="n_video_section mrb_0 " id="Solutions">
            <div class="container">
               <div class="row v_center">
                  <div class="col-md-4 v_center">
                     <h2 class="section-title text-center mrb_0" > BUSINESS OFFERINGS</h2>
                     <!--  <div class="solution_img">
                        <img src="images/15698202214.jpg">
                        </div>
                        -->                  
                  </div>
                  <div class="col-md-6">
                     <div class="c_and_i_sec_n1">
                        <ul class="theme-accordian-list">
                           <li class="theme-accordian-list-item ">
                              <div class="c_and_i_sec_card_n1"><img src="images/bullet-2-white.png">  Behind the Meter – Distributed Solar & Ground-mounted Solar <span class="collapse_postion"></span></div>
                              <div class="solution_item_container answer ">
                                 <ul class="icon_list">
                                    <li>
                                       Engineering, Procurement and Construction scope
                                    </li>
                                    <li>
                                       OPEX/CAPEX & DEFERRED CAPEX model
                                    </li>
                                 </ul>
                              </div>
                           </li>
                           <li class="theme-accordian-list-item">
                              <div class="c_and_i_sec_card_n1"><img src="images/bullet-2-white.png"> Open Access - Group Captive <span class="collapse_postion"></span></div>
                              <div class="solution_item_container answer">
                                 <ul class="icon_list">
                                    <li>
                                       In the Group Captive model, solar PV plant generates power for collective use of one or many corporate buyers
                                    </li>
                                    <li>
                                       Your investment is 26% of the equity amount
                                    </li>
                                 </ul>
                              </div>
                           </li>
                           <li class="theme-accordian-list-item">
                              <div class="c_and_i_sec_card_n1"><img src="images/bullet-2-white.png">Open Access - Third-party PPA <span class="collapse_postion"></span></div>
                              <div class="solution_item_container answer">
                                 <ul class="icon_list">
                                    <li>
                                       Power Purchase Agreement [PPA] is a contract between power producer and customer which includes terms, conditions of energy supply, and clearly states electricity purchase tariffs
                                    </li>
                                    <li>
                                       For third-party PPAs, your investment is 100% equity amount
                                    </li>
                                 </ul>
                              </div>
                           </li>
                           <li class="theme-accordian-list-item">
                              <div class="c_and_i_sec_card_n1"><img src="images/bullet-2-white.png">Behind the Meter – Energy Storage Solutions <span class="collapse_postion"></span></div>
                              <div class="solution_item_container answer">
                                 <ul class="icon_list">
                                    <li>
                                       Customised turnkey projects
                                    </li>
                                    <li>
                                       OPEX/CAPEX & DEFERRED CAPEX model
                                    </li>
                                 </ul>
                              </div>
                           </li>
                           <li class="theme-accordian-list-item">
                              <div class="c_and_i_sec_card_n1"><img src="images/bullet-2-white.png">Futuristic Solutions <span class="collapse_postion"></span></div>
                              <div class="solution_item_container answer">
                                 <ul class="icon_list">
                                    <li>
                                       Greenfield development
                                    </li>
                                    <li>
                                       Customised turnkey projects
                                    </li>
                                    <li>
                                       Operational Infrastructure Platforms
                                    </li>
                                    <li>
                                       Build own & operate [BOO] green H2/green NH3 model
                                    </li>
                                    <li>
                                       Integration of Hybrid [wind-solar/ solar-energy storage] model
                                    </li>
                                 </ul>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="n_video_section theme_sec     pdt_0 ">
            <div class="container">
               <div class="offering_below_list">
                  <div class="slider_item_list_nd1">
                     <div class="highlights_card_nd1">
                        <div>
                           <img src="images/we-offer.png" class="highlights_nd1_img">
                        <h4>We offer flexible business models </h4>
                        </div>
                     </div>
                  </div>
                  <div class="slider_item_list_nd1 lineseperatot">
                     <div class="highlights_card_nd1">
                       <div>
                           <img src="images/Piggi-Bank.png" class="highlights_nd1_img">
                        <h4>Our green business solutions saves your cost of operation</h4>
                       </div>
                     </div>
                  </div>
                  <div class="slider_item_list_nd1">
                     <div class="highlights_card_nd1">
                        <div>
                            <img src="images/Tree-in-bulb.png" class="highlights_nd1_img">
                        <h4>We offer every solution for your ‘net zero’ journey</h4>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="n_video_section theme_sec mrb_0 mrt_0 pdt_0 " id="feature-product" style="padding-bottom: 0px;">
            <div class="container">
               <div class="bg_sec sec_padding_n2">
                  <div class="row">
                     <div class="col-md-12">
                        <h2 class="section-title text-left" style="padding: 0px 20px;">FEATURED PROJECTS  </h2>
                     </div>
                     <div class="col-md-12">
                        <div class="c_and_i_feature_product_list ">
                           <div class="c_and_i_feature_product_item">
                              <div class="card_white">
                                 <img src="images/rooftop-13.jpg">
                                 <div class="c_and_i_feature_product_item_content">
                                    <h4>Hero MotoCorp</h4>
                                    <ul class="icon_list">
                                       <li>
                                          Capacity  – 1980 kW
                                       </li>
                                       <li>
                                          Location  - Neemrana, India
                                       </li>
                                       <li>
                                          Industry – Automobile
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <div class="c_and_i_feature_product_item">
                              <div class="card_white">
                                 <img src="images/15698208815.jpg">
                                 <div class="c_and_i_feature_product_item_content">
                                    <h4>Delhi Metro Rail </h4>
                                    <ul class="icon_list">
                                       <li>
                                          Project Capacity – 7140 kW
                                       </li>
                                       <li>
                                          Location – All across Delhi, Noida & Ghaziabad , India
                                       </li>
                                       <li>
                                          Industry- Government
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <div class="c_and_i_feature_product_item">
                              <div class="card_white">
                                 <img src="images/car-port.jpg">
                                 <div class="c_and_i_feature_product_item_content">
                                    <h4>GD Goenka</h4>
                                    <ul class="icon_list">
                                       <li>
                                          Project Capacity – 234 kW
                                       </li>
                                       <li>
                                          Location  - Haryana, India
                                       </li>
                                       <li>
                                          Industry- Education Institution
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <div class="c_and_i_feature_product_item">
                              <div class="card_white">
                                 <img src="images/motorcop.jpg">
                                 <div class="c_and_i_feature_product_item_content">
                                    <h4>Hero MotoCorp</h4>
                                    <ul class="icon_list">
                                       <li>
                                          Project Capacity – 2500 kW
                                       </li>
                                       <li>
                                          Location  -  Andhra Pradesh,India
                                       </li>
                                       <li>
                                          Industry- Automobiles
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <div class="c_and_i_feature_product_item">
                              <div class="card_white">
                                 <img src="images/Panchratna-Fasteners.jpg">
                                 <div class="c_and_i_feature_product_item_content">
                                    <h4>Panchratna Fasteners</h4>
                                    <ul class="icon_list">
                                       <li>
                                          Project Capacity – 620.1 kW
                                       </li>
                                       <li>
                                          Location  -  Rohtak, Haryana , India
                                       </li>
                                       <li>
                                          Industry- Automotive components
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <div class="c_and_i_feature_product_item">
                              <div class="card_white">
                                 <img src="images/Beri-Udhyog.jpg">
                                 <div class="c_and_i_feature_product_item_content">
                                    <h4>Beri Udhyog</h4>
                                    <ul class="icon_list">
                                       <li>
                                          Project Capacity – 620.1 kW
                                       </li>
                                       <li>
                                          Location  -  Karnal, Haryana , India
                                       </li>
                                       <li>
                                          Industry- Agriculture Machinery
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <div class="c_and_i_feature_product_item">
                              <div class="card_white">
                                 <img src="images/nit.jpg">
                                 <div class="c_and_i_feature_product_item_content">
                                    <h4>National Institute of Solar Energy</h4>
                                    <ul class="icon_list">
                                       <li>
                                          Project Capacity – 150 kW
                                       </li>
                                       <li>
                                          Location  -  Haryana, India
                                       </li>
                                       <li>
                                          Industry- Education Institution
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <div class="c_and_i_feature_product_item">
                              <div class="card_white">
                                 <img src="images/Delhi-Technical-University.jpg">
                                 <div class="c_and_i_feature_product_item_content">
                                    <h4>Delhi Technical University</h4>
                                    <ul class="icon_list">
                                       <li>
                                          Project Capacity – 432 kW
                                       </li>
                                       <li>
                                          Location  -  Delhi, India
                                       </li>
                                       <li>
                                          Industry- Education Institution
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <div class="c_and_i_feature_product_item">
                              <div class="card_white">
                                 <img src="images/Kochi-Metro-Rail.JPG">
                                 <div class="c_and_i_feature_product_item_content">
                                    <h4>Kochi Metro Rail</h4>
                                    <ul class="icon_list">
                                       <li>
                                          Project Capacity – 2670 kW
                                       </li>
                                       <li>
                                          Location  -  Kochi, India
                                       </li>
                                       <li>
                                          Industry- Government
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>
      </section>
      <section class="n_video_section mrb_0" id="our-client">
         <div class="container">
            <div class="row row_align">
               <div class="col-md-4">
                  <div class="">
                     <div class="">
                        <h2 class="section-title text-center mrb_0" style="padding: 0px 20px;">From  Our  Clients</h2>
                     </div>
                  </div>
               </div>
               <div class="col-md-8">
                  <div class="testimoniial_sllider_container_n2">
                     <div class="testimonial_slider_btn_n2">
                        <ul>
                           <li  id="pastnext">
                              <i class="fa fa-angle-left" aria-hidden="true"></i>
                           </li>
                           <li id="pastprevious">
                              <i class="fa fa-angle-right" aria-hidden="true"></i>
                           </li>
                        </ul>
                     </div>
                     <div class="testimoniial_sllider_n2_list">
                        <div class="testimoniial_sllider_n2_item">
                           <div class="row align_center">
                              <div class="col-md-8">
                                 <div class="testimoniial_sllider_n2_card">
                                    <div class="Client_speak_detail_n1">
                                       <p>
                                          <b>Surenderjeet , Chief Engineer</b>
                                       </p>
                                       <p class="color_grey">
                                          G.D. Goenka University Sohna, Haryana
                                       </p>
                                    </div>
                                    <!-- <h4></h4> -->
                                    <div class="Client_speak_detail_dsc_n1">
                                       <p>
                                          "We are saving both money and the environment with solar energy installed by Hero Future Energies. We are very happy with the services of the company."
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 mar_l_minus">
                                 <div class="testimoniial_sllider_n2_image">
                                    <img src="images/GD-Goenka-University.jpg">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="testimoniial_sllider_n2_item">
                           <div class="row align_center">
                              <div class="col-md-8">
                                 <div class="testimoniial_sllider_n2_card">
                                    <div class="Client_speak_detail_n1">
                                       <p>
                                          <b>Ashok Malik , General Manager (Operations)</b>
                                       </p>
                                       <p class="color_grey">
                                          Centex International Pvt. Ltd.
                                       </p>
                                    </div>
                                    <!-- <h4></h4> -->
                                    <div class="Client_speak_detail_dsc_n1">
                                       <p>
                                          "We've been using the power from 2 rooftop solar plants installed by Hero Future Energies in our premises for months now. We are very happy with the services provided by HFE."
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 mar_l_minus">
                                 <div class="testimoniial_sllider_n2_image">
                                    <img src="images/Centex-International-Pvt-Ltd.jpeg">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="testimoniial_sllider_n2_item">
                           <div class="row align_center">
                              <div class="col-md-8">
                                 <div class="testimoniial_sllider_n2_card">
                                    <div class="Client_speak_detail_n1">
                                       <p>
                                          <b>Elias George ,Managing Director,</b>
                                       </p>
                                       <p class="color_grey">
                                          KMRL
                                       </p>
                                    </div>
                                    <!-- <h4></h4> -->
                                    <div class="Client_speak_detail_dsc_n1">
                                       <p>
                                          "Kochi Metro Rail Limited partnered with Hero Future Energies to commission rooftop solar plants to reduce their carbon footprints. Our experience with Hero Future Energies has been very good."
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 mar_l_minus">
                                 <div class="testimoniial_sllider_n2_image">
                                    <img src="images/KMRL.png">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="testimoniial_sllider_n2_item">
                           <div class="row align_center">
                              <div class="col-md-8">
                                 <div class="testimoniial_sllider_n2_card">
                                    <div class="Client_speak_detail_n1">
                                       <p>
                                          <b>Ajai ShahHead of Maintenance </b>
                                       </p>
                                       <p class="color_grey">
                                          HiTech Gears LTD
                                       </p>
                                    </div>
                                    <!-- <h4></h4> -->
                                    <div class="Client_speak_detail_dsc_n1">
                                       <p>
                                          "Hero Future Energies partnered with Hitech Gears LTD. to install rooftop solar to reduce their regu lar power consumption."
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 mar_l_minus">
                                 <div class="testimoniial_sllider_n2_image">
                                    <img src="images/HiTech-Gears-LTD.jpeg">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="testimoniial_sllider_n2_item">
                           <div class="row align_center">
                              <div class="col-md-8">
                                 <div class="testimoniial_sllider_n2_card">
                                    <div class="Client_speak_detail_n1">
                                       <p>
                                          <b>Ajith Kumar Reddy  </b>
                                       </p>
                                       <p class="color_grey">
                                          Omni Auto Limited
                                       </p>
                                    </div>
                                    <!-- <h4></h4> -->
                                    <div class="Client_speak_detail_dsc_n1">
                                       <p>
                                          "Hero Future Energies was a great choice for us to go solar. Everything from site assessment to final project commissioning was done in a thoroughly professional manner. HFE overcame all challenges to commission the project during the COVID lockdown. We are extremely happy that our factory roof is now a source of clean power."
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 mar_l_minus">
                                 <div class="testimoniial_sllider_n2_image">
                                    <img src="images/Omni-Auto-Limited.png">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="testimoniial_sllider_n2_item">
                           <div class="row align_center">
                              <div class="col-md-8">
                                 <div class="testimoniial_sllider_n2_card">
                                    <div class="Client_speak_detail_n1">
                                       <p>
                                          <b>RAVINDER DHAKA  </b>
                                       </p>
                                       <p class="color_grey">
                                          GMAX AUTO LTD, BAWAL
                                       </p>
                                    </div>
                                    <!-- <h4></h4> -->
                                    <div class="Client_speak_detail_dsc_n1">
                                       <p>
                                          "A 100 KW PLANT WITH 350 PANELS HAS BEEN INSTALLED BY HERO FUTURE ENERGIES AT GMAX AUTO LTD. WHICH HAS FLAWLESSLY SOLVED THE POWER TRIPPING ISSUE AT OUR FACTORY."
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 mar_l_minus">
                                 <div class="testimoniial_sllider_n2_image">
                                    <img src="images/GMAX-AUTO-LTD.jpeg">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="testimoniial_sllider_n2_item">
                           <div class="row align_center">
                              <div class="col-md-8">
                                 <div class="testimoniial_sllider_n2_card">
                                    <div class="Client_speak_detail_n1">
                                       <p>
                                          <b>YASHPAL SARDANA  </b>
                                       </p>
                                       <p class="color_grey">
                                          HERO MOTORCORP LTD
                                       </p>
                                    </div>
                                    <!-- <h4></h4> -->
                                    <div class="Client_speak_detail_dsc_n1">
                                       <p>
                                          "HERO FUTURE ENERGIES USED INVERTED TRUSS STRUCTURES AND HIGH-EFFICIENCY MODULES, TO SAFEGUARD THE GREEN COVER ON THE ROOF WHILE OFFSETTING THE CARBON FOOTPRINT OF HERO MOTOCORP."
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 mar_l_minus">
                                 <div class="testimoniial_sllider_n2_image">
                                    <img src="images/logo-21.png">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="testimoniial_sllider_n2_item">
                           <div class="row align_center">
                              <div class="col-md-8">
                                 <div class="testimoniial_sllider_n2_card">
                                    <div class="Client_speak_detail_n1">
                                       <p>
                                          <b>Manjul Singhal (Chief Electrical Engineer)  </b>
                                       </p>
                                       <p class="color_grey">
                                          DMRC
                                       </p>
                                    </div>
                                    <!-- <h4></h4> -->
                                    <div class="Client_speak_detail_dsc_n1">
                                       <p>
                                          "The Delhi Metro Rail Corporation is powering their lifts, escalators and station lighting from the energy generated by solar plants installed by Hero Future Energies"
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 mar_l_minus" >
                                 <div class="testimoniial_sllider_n2_image">
                                    <img src="images/DMRC_logo.png">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- why choose us  -->
      <section class="n_video_section bg_sec mrb_0 rts-sec10bottom desktop-view" id="why-us">
         <div class="container">
            <div class="row">
               <div class="col-lg-8 col-md-8 col-sm-8">
                  <div class="row divcustom2">
                     <div class="col-container-4 lefttop mh-275 current">
                        <div class="reason-img colaction">
                           <div class="reasonhead">
                              <img src="images/1570078795r1.png">
                              <div class="reasonhead-right mb-5">
                                 <p>Inhouse Talent pool</p>
                              </div>
                              <p>Experienced team catering to design & engineering needs</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-container-4 mr-bottom70">
                        <div class="reason-img colaction">
                           <div class="reasonhead">
                              <img src="https://www.herofutureenergies.com/backend/web/uploads/reasons/1570078833r2.png">
                              <div class="reasonhead-right mb-5">
                                 <p>Customised Solutions for C&I businesses</p>
                              </div>
                              <p>HFE offers end to end customised renewable energy solutions with state-of-the art performance monitoring</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-container-4 lefttop mh-275">
                        <div class="reason-img colaction">
                           <div class="reasonhead">
                              <img src="https://www.herofutureenergies.com/backend/web/uploads/reasons/1570078879r3.png">
                              <div class="reasonhead-right mb-5">
                                 <p>Project Development </p>
                              </div>
                              <p>Timely completion and strong focus on asset quality standards</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-container-4 mh342">
                        <div class="reason-img colaction">
                           <div class="reasonhead">
                              <img src="https://www.herofutureenergies.com/backend/web/uploads/reasons/1570078956r4.png">
                              <div class="reasonhead-right mb-5">
                                 <p>Cleantech Solutions Expert </p>
                              </div>
                              <p>HFE is continuously adopting to latest technology trends using renewable energy sources from Energy Storage to Green Hydrogen
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-container-4 neg32 mh-250">
                        <div class="reason-img colaction">
                           <div class="reasonhead">
                              <img src="https://www.herofutureenergies.com/backend/web/uploads/reasons/1570078985r5.png">
                              <div class="reasonhead-right mb-5">
                                 <p>O&M</p>
                              </div>
                              <p>O&M support throughout Project lifecycle
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-container-4">
                        <div class="reason-img colaction">
                           <div class="reasonhead">
                              <img src="https://www.herofutureenergies.com/backend/web/uploads/reasons/1570078923r6.png">
                              <div class="reasonhead-right mb-5">
                                 <p>Makes Business sense</p>
                              </div>
                              <p>HFE offers flexible business models. Our solutions saves your cost of operation. We help you in smooth transition to green mandate</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-container-4 mx-auto neg90">
                        <div class="reason-img colaction">
                           <div class="reasonhead">
                              <img src="https://www.herofutureenergies.com/backend/web/uploads/reasons/1570079019r7.png">
                              <div class="reasonhead-right mb-5">
                                 <p>HSE  </p>
                              </div>
                              <p>Strict adherence to safety protocols</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="reasons-heading">
                     <h2 class="section-title text-left">Why <br> Partner <br> with <br> HFE <br> 
                     </h2>
                     <!--   <p>The name stands for trust, quality and timeliness</p>    -->
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- new sec  -->
      <section class="rts-sec2bottom hfe-reg-vender-middle form-label n_video_section  mrb_0 n_video_section ">
         <div class="container">
            <div class="row rts-assessment-left">
               <div class="rts-assessment-bgcolor row">
                  
                  <div class="col-md-9 col-lg-9 col-sm-9 assessment-form" id="calc">
                     <div class="sec_padding_n2 bg_sec">
                        <div class="investor_relation_heading_nd1 left">Get your free Solar Assessment done now</div>
                        <p class="wow fadeInRight">Fill up the form to get your solar assessment or use the energy savings calculator below.</p>
                     <div>
                       
                        <button type="button" class="btnallred btn btn-sm btn-theme-danger waves-effect waves-light"  data-toggle="modal" data-target="#modalcalc"><span>ENERGY SAVING CALCULATOR &nbsp; ></span> </button>
                     </div>

                     <form action="">
                        <input type="hidden" name="_csrf-frontend" id="csrf_frontend" value="" />   
                        <div class="row">
                           <div class="col-sm-4 col-lg-4 col-md-4">
                              <div class="form-group ">
                                 <label for="name" class="">Name</label>
                                 <input type="text" id="name" name="name" class="form-control" maxlength="50">
                                 
                              </div>
                              <div class="form-group ">
                                 <label for="designation" class="">Designation</label>
                                 <input type="text" id="designation" name="designation" class="form-control" maxlength="50">
                                 
                              </div>
                              <div class="form-group ">
                                 <label for="annual_turnover" class="">Annual Turnover</label>
                                 <input type="text" id="annual_turnover" name="annual_turnover" class="form-control" maxlength="20">
                                 
                              </div>
                              <div class="form-group ">
                                 <label for="monthly_electricity_consuption" class="">Monthly Electricity Consuption</label>
                                 <input type="text" id="monthly_electricity_consuption" name="monthly_electricity_consuption" class="form-control" maxlength="30">
                                 
                              </div>
                           </div>
                           <div class="col-sm-4 col-lg-4 col-md-4">
                              <div class="form-group ">
                                 <label for="email" class="">Email</label>
                                 <input type="text" id="email" name="email" class="form-control" maxlength="40">
                                 
                              </div>
                              <div class="form-group">
                                 <label for="company_name" class="">Company Name</label>
                                 <input type="text" id="company_name" name="company_name" class="form-control" maxlength="50">
                                 
                              </div>
                              <div class="form-group ">
                                 <label for="average_monthly_bill" class="">Average Monthly Bill</label>
                                 <input type="text" id="average_monthly_bill" name="average_monthly_bill" class="form-control" maxlength="20">
                                 
                              </div>
                              <div class="form-group ">
                                  <label for="approximate_space_available" class="">Approximate Space Available</label>
                                 <input type="text" id="approximate_space_available" name="approximate_space_available" class="form-control" maxlength="30">
                                
                              </div>
                           </div>
                           <div class="col-sm-4 col-lg-4 col-md-4">
                              <div class="form-group ">
                                 <label for="Phone" class="">Phone</label>
                                 <input type="text" id="phone" name="phone" class="form-control" maxlength="10">
                                 
                              </div>
                              <div class="form-group ">
                                 <label for="city" class="">City</label>
                                 <input type="text" id="city" name="city" class="form-control" maxlength="30">
                                 
                              </div>
                              <div class="form-group ">
                                 <label for="message" class="">Message</label>
                                 <textarea id="message" name="message" rows="3" cols="50" class="form-control"></textarea>
                                 
                              </div>
                             
                           </div>
                           <div class="col-sm-12 col-lg-12 col-md-12">
                               <button type="submit" class="btnallred btn btn-sm btn-theme-danger waves-effect waves-light" id="rooftop_form" onclick="return rooftopFrm();"><span>Submit &nbsp; ></span> </button>
                           </div>
                        </div>
                     </form>
                     </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-sm-3">
                     <div class="cal_video">
                        <div class="investor_relation_heading_nd1 left">Video Gallery
   
                        </div>

                           <iframe width="100%" height="180" src="https://www.youtube.com/embed/oa_NI4-0P1U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                     </div>
                     <div class="cal_knowmore">
                        <div class="investor_relation_heading_nd1 left">Know More
   
                        </div>

                           <div>
               <ul class="video-gallery lightgallery" id="gallery">
                <li>
                  <a href="images/modal_image.jpg"  class="gallery-single-item lg-trigger" data-exthumbnail="images/modal_image.jpg">
                  <img src="images/photo.png"> How does Rooftop Solar work                    </a>    
                </li>
                     
                <li>
                    <a href="images/modal_image.jpg"  class="gallery-single-item lg-trigger" data-exthumbnail="images/modal_image.jpg">
                  <img src="images/photo.png"> Models in Rooftop Solar                    </a>    
                </li>
                     
                <li>
                    <a href="images/modal_image.jpg"  class="gallery-single-item lg-trigger" data-exthumbnail="images/modal_image.jpg">
                  <img src="images/photo.png"> 6 ways to go solar                    </a>    
                </li>
                     
                <li>
                    <a href="images/modal_image.jpg"  class="gallery-single-item lg-trigger" data-exthumbnail="images/modal_image.jpg">
                  <img src="images/photo.png"> A green school setting                    </a>    
                </li>
                     
                <li>
                    <a href="images/modal_image.jpg"  class="gallery-single-item lg-trigger" data-exthumbnail="images/modal_image.jpg">
                  <img src="images/photo.png"> Climate change                    </a>    
                </li>
                     
                <li>
                    <a href="images/modal_image.jpg"  class="gallery-single-item lg-trigger" data-exthumbnail="images/modal_image.jpg">
                  <img src="images/photo.png"> Rooftop solar quality protocol                    </a>    
                </li>
                     
                <li>
                    <a href="images/modal_image.jpg"  class="gallery-single-item lg-trigger" data-exthumbnail="images/modal_image.jpg">
                  <img src="images/photo.png"> Powering your Rooftops 2019 Report - CII Report                    </a>    
                </li>
                       
            </ul>
                           </div>
                     </div>
                  </div>
               </div>
               <div class="width20 desktop-view">
                 
               </div>
            </div>
         </div>
      </section>
      <!-- end new sec / -->
      <!-- end why chooose us -->
      <section class="n_video_section bg_sec  mrb_0 contact-sec2bottom desktop-view" id="for-business-queries">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="visit-heading">
                     <h2 class="section-title text-left">For Business Queries</h2>
                  </div>
                  <div class="contact-visit">
                     <ul class="nav nav-pills mb-5" id="pills-tab" role="tablist">
                        <li class="nav-item">
                           <a class="nav-link contact-visit-link active" id="pills-home-tab1" data-toggle="pill" href="#UK" role="tab" aria-controls="pills-home" aria-selected="true">UK <span>|</span> Europe <span>|</span> MENA  </a>
                        </li>
                        <span class="video-pipe-small"></span>
                        <li class="nav-item">
                           <a class="nav-link contact-visit-link" id="pills-home-tab2" data-toggle="pill" href="#India" role="tab" aria-controls="pills-home" aria-selected="false">India</a>
                        </li>
                        <span class="video-pipe-small"></span>
                        <li class="nav-item">
                           <a class="nav-link contact-visit-link" id="pills-home-tab3" data-toggle="pill" href="#SouthEastAsia" role="tab" aria-controls="pills-home" aria-selected="false">Southeast Asia</a>
                        </li>
                        <!--    <span class="video-pipe-small"></span>
                           <li class="nav-item">
                              <a class="nav-link contact-visit-link" id="pills-home-tab4" data-toggle="pill" href="#us" role="tab" aria-controls="pills-home" aria-selected="false">US</a>
                           </li> -->
                        <span class="video-pipe-small"></span>
                        <li class="nav-item">
                           <a class="nav-link contact-visit-link" id="pills-home-tab5" data-toggle="pill" href="#south-asia" role="tab" aria-controls="pills-home" aria-selected="false"> South Asia <span>|</span> United States </a>
                        </li>
                        <span class="video-pipe-small"></span>
                     </ul>
                     <div class="tab-content cust-tab-visit pt-2 pl-1" id="pills-tabContent">
                        <div class="tab-pane fade visit visit-border-bottom active show" id="UK" role="tabpanel" aria-labelledby="pills-home-tab">
                           <div class="row ">
                              <div class="col-lg-3 col-md-3 col-sm-3">
                                 <img src="https://www.herofutureenergies.com/backend/web/uploads/contact/1571417821202.png" class="img-fluid address-img">
                              </div>
                              <div class="col-lg-9 col-md-9 col-sm-9">
                                 <div>
                                    <div class="row">
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <div class="visit-pra">
                                             
                                             <!-- <p> Phone: <a href="">0785 088 2679</a></p> -->
                                             <p> Email: <a href="mailto:hfe.emena@herofutureenergies.com">hfe.emena@herofutureenergies.com</a></p>
                                          </div>
                                          <!--  <p><a href="https://www.google.com/maps/dir//Kensington+Pavilion,+96+Kensington+High+St,+Kensington,+London+W8+4SG,+United+Kingdom/@51.5016128,-0.2623031,12z/data=!4m8!4m7!1m0!1m5!1m1!1s0x48760ff6e197b971:0xd1c2aa78f1b666e0!2m2!1d-0.1922634!2d51.5016337" target="_blank">Directions</a></p> -->
                                       </div>
                                       <div class="border-bottom mobile-view"></div>
                                       <div class="border-bottom"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane fade visit visit-border-bottom" id="India" role="tabpanel" aria-labelledby="pills-home-tab">
                           <div class="row ">
                              <div class="col-lg-3 col-md-3 col-sm-3">
                                 <img src="https://www.herofutureenergies.com/backend/web/uploads/contact/1570946746india.png" class="img-fluid address-img">
                              </div>
                              <div class="col-lg-9 col-md-9 col-sm-9">
                                 <div>
                                    <div class="row">
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <div class="visit-pra">
                                             <p>Email: <a href="mailto:hfe.cni.india@herofutureenergies.com">hfe.cni.india@herofutureenergies.com</a></p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane fade visit visit-border-bottom" id="SouthEastAsia" role="tabpanel" aria-labelledby="pills-home-tab">
                           <div class="row ">
                              <div class="col-lg-3 col-md-3 col-sm-3">
                                 <img src="https://www.herofutureenergies.com/backend/web/uploads/contact/1570946903singapore.png" class="img-fluid address-img">
                              </div>
                              <div class="col-lg-9 col-md-9 col-sm-9">
                                 <div>
                                    <div class="row">
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <!-- <div class="visit-pra">
                                             Oman Singh
                                          </div> -->
                                          <!--  <p> 
                                             Phone: <a href="tel:+65-90036502">+65-90036502</a>
                                             </p> -->
                                          <p> 
                                             Email: <a href="mailto:hfe.asia@herofutureenergies.com">hfe.asia@herofutureenergies.com</a>
                                          </p>
                                       </div>
                                       <div class="border-bottom mobile-view"></div>
                                       <div class="border-bottom"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane fade visit visit-border-bottom  " id="us" role="tabpanel" aria-labelledby="pills-home-tab">
                           <div class="row ">
                              <div class="col-lg-3 col-md-3 col-sm-3">
                                 <img src="https://www.herofutureenergies.com/backend/web/uploads/contact/1571417821202.png" class="img-fluid address-img">
                              </div>
                              <div class="col-lg-9 col-md-9 col-sm-9">
                                 <div>
                                    <div class="row">
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <div class="visit-pra">
                                             <!-- <p>Robert Ayres</p> -->
                                             <p> Email: <a href="mailto:hfe.cni.ib@herofutureenergies.com">hfe.cni.ib@herofutureenergies.com</a></p>
                                          </div>
                                          
                                       </div>
                                       <div class="border-bottom mobile-view"></div>
                                       <div class="border-bottom"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane fade visit visit-border-bottom  " id="south-asia" role="tabpanel" aria-labelledby="pills-home-tab">
                           <div class="row ">
                              <div class="col-lg-3 col-md-3 col-sm-3">
                                 <img src="https://www.herofutureenergies.com/backend/web/uploads/contact/1571417821202.png" class="img-fluid address-img">
                              </div>
                              <div class="col-lg-9 col-md-9 col-sm-9">
                                 <div>
                                    <div class="row">
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <div class="visit-pra">
                                            <!--  <p>Robert Ayres</p> -->
                                             <!-- <p> Phone: <a href="">0785 088 2679</a></p> -->
                                             <p> Email: <a href="mailto:hfe.cni.ib@herofutureenergies.com">hfe.cni.ib@herofutureenergies.com</a></p>
                                          </div>
                                         
                                       </div>
                                       <div class="border-bottom mobile-view"></div>
                                       <div class="border-bottom"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      </div>
      <?php include 'common/footer.php';?>
      <?php include 'common/modal.php';?>
      <?php include 'common/js-url.php';?>
   </body>
</html>
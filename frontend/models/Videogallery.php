<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Videogallery model.
 */
class Videogallery extends \yii\db\ActiveRecord {
		
	// For get Videogallery page data
	public function getPageData($category_id){
		$connection = \Yii::$app->db;	
		$page_id = 10;
		$sql = "select * from hfe_youtube where status = 'Active' and category_id = :category_id";
		$command = $connection->createCommand($sql);
		$command->bindParam(':category_id', $category_id);
		$pageData = $command->queryAll();
		return $pageData;
	}
	
	// For get banner data
	public function getBannerData(){
		$connection = \Yii::$app->db;	
		$page_id = 10;
		$sql = "select * from hfe_banners where status = 'Active' and page_id = :page_id";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_id', $page_id);
		$pageData = $command->queryAll();
		return $pageData;
	}
	
}

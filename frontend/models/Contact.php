<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Contact model.
 */
class Contact extends \yii\db\ActiveRecord {
		
	// For get contact page data
	public function getPageData($category_id){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_contact_us where status = 'Active' and category_id = :category_id";
		$command = $connection->createCommand($sql);
		$command->bindParam(':category_id', $category_id);
		$pageData = $command->queryAll();
		return $pageData;
	}
	
	// For get banner data
	public function getBannerData(){
		$connection = \Yii::$app->db;	
		$page_id = 8;
		$sql = "select * from hfe_banners where status = 'Active' and page_id = :page_id";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_id', $page_id);
		$pageData = $command->queryAll();
		return $pageData;
	}
	
	// For get contact address
	public function getContactAddressData(){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_contact_us_address where status = 'Active' order by priority Asc";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		return $data;
	}
	
}

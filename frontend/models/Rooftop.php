<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Rooftop model.
 */
class Rooftop extends \yii\db\ActiveRecord {
		
	// For get page data from page name
	public function getPageData($page_name){
		$connection = \Yii::$app->db;	
		$sql = "select id, page_name,page_url,page_type,meta_title,meta_keywords,meta_description from hfe_page_layout where page_url = :page_url and status = 'Active'";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_url', $page_name);
		$pageData = $command->queryAll();
		$pageArray = array();
		if(!empty($pageData[0])){
			// for page data
			$pageArray['page_data'] = $pageData[0];
			$page_id = $pageData[0]['id'];
			$sql = "select table_name,page_type,priority from hfe_page_priority where page_id = :page_id and priority != '' order by priority ASC";
			$command = $connection->createCommand($sql);
			$command->bindParam(':page_id', $page_id);
			$pagePriorityData = $command->queryAll();
			if(!empty($pagePriorityData)){
				// for page priority
				$pageArray['page_priority_data'] = $pagePriorityData;
				
				foreach($pagePriorityData as $priority){
						
					$cond = '';
					if($priority['page_type'] == "rooftop_reasons"){
						$cond = ' and type = "Reasons_Choose_Us"';
					}
					
					if($priority['page_type'] == "rooftop_benefits"){
						$cond = ' and type = "Benefits_Rooftop_Solar"';
					}
					if($priority['page_type'] == "rooftop_appreciation"){
						$cond = ' and type = "words_of_appreciation"';
					}
					if($priority['page_type'] == "rooftop_clients"){
						$cond = ' and type = "our_client"';
					}
					if($priority['page_type'] == "rooftop_knowmore"){
						$cond = ' and type = "know_more"';
					}
					
					
					if($priority['page_type'] == "rooftop_gallery"){ 
					
						$sql = "select * from ".$priority['table_name']." where  status = 'Active' order by priority ASC";
						$command = $connection->createCommand($sql);
					}else if($priority['page_type'] == "contact"){
			
						$sql = "select * from ".$priority['table_name']." where status = 'Active' ";
						$command = $connection->createCommand($sql);
					}else{
						$sql = "select * from ".$priority['table_name']." where page_id = :page_id and status = 'Active' $cond order by priority ASC";
						$command = $connection->createCommand($sql);
						$command->bindParam(':page_id', $page_id);
					}
					$data = $command->queryAll();
		
					if(!empty($data)){
						// for all page data
						if($priority['page_type'] == "rooftop_knowmore"){
							$pageArray['rooftop_gallery']['knowmore'][$priority['page_type']] = $data;
						}else{
							if($priority['page_type'] != "rooftop_tracking"){
								$pageArray[$priority['page_type']] = $data;
							}
						}
						
					}
				}
				
			}	
		}
		return $pageArray;
	}
	
	// For get online tracking data
	public function getTrackingData($type){
		$connection = \Yii::$app->db;
		$sql = "select * from hfe_rooftop_online_tracking where status = 'Active' and type = :type order by priority ASC";
		$command = $connection->createCommand($sql);
		$command->bindParam(':type', $type);
		$data = $command->queryAll();
		return $data;
	}

		public function getOfferData(){
		$connection = \Yii::$app->db;
		$sql = "select * from hfe_business_offer_tabdata where status = 'Active' order by priority ASC";
		$command = $connection->createCommand($sql);
		$command->bindParam(':type', $type);
		$data = $command->queryAll();
		return $data;
	}


		public function getQueryData($category_id){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_contact_us where status = 'Active' and category_id = :category_id";
		$command = $connection->createCommand($sql);
		$command->bindParam(':category_id', $category_id);
		$pageData = $command->queryAll();
		return $pageData;
	}
	
}

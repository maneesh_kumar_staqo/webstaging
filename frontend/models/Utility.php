<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Utility model.
 */
class Utility extends \yii\db\ActiveRecord {
		
	// For send email
	public function sendEmail($to,$subject,$message,$bcc=''){
		Yii::$app
		->mailer
		->compose()
		->setFrom(["communication@herofutureenergies.com" => "HFE Team"])
		->setTo($to)
		->setBcc($bcc)
		->setSubject($subject)
		->setHtmlBody($message)
		->send();
	}
	
	// For get page tittle
	public function getPageTitle($type){
		$connection = \Yii::$app->db;	
		$sql = "select heading from hfe_page_heading where status = 'Active' and type = :type";
		$command = $connection->createCommand($sql);
		$command->bindParam(':type', $type);
		$pageData = $command->queryAll();
		if(!empty($pageData[0]['heading'])){
			return $pageData[0]['heading'];
		}
	}
	
	// For get page meta data
	public function getPageMetaData($page_url){
		$connection = \Yii::$app->db;	
		$sql = "select meta_title,meta_keywords,meta_description from hfe_page_layout where status = 'Active' and page_url = :page_url";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_url', $page_url);
		$pageData = $command->queryAll();
		if(!empty($pageData[0])){
			return $pageData[0];
		}
	}
	
	// For get page header and footer data
	public static function getPageHeaderFooter($type){
		// echo $type;die;
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_menu where status = 'Active' and type = :type";
		$command = $connection->createCommand($sql);
		$command->bindParam(':type', $type);
		$pageData = $command->queryAll();
		return $pageData;
	}
	public static function getPageMenuNewsroom($type){
		// echo $type;die;
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_menu where status = 'Active' and type = :type";
		$command = $connection->createCommand($sql);
		$command->bindParam(':type', $type);
		$pageData = $command->queryAll();
		return $pageData;
	}

		public function getSubPageHeader($id){
		
	
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_submenu where status = 'Active' and main_menu_id = :id";
		$command = $connection->createCommand($sql);
		$command->bindParam(':id', $id);
		$pageData = $command->queryAll();
	
		return $pageData;
	}
	
}

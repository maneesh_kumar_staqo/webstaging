<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Csr model.
 */
class Csr extends \yii\db\ActiveRecord {
		
	// For get page data from page name
	public function getPageData($page_name){
		$connection = \Yii::$app->db;	
		$sql = "select id, page_name,page_url,page_type,meta_title,meta_keywords,meta_description from hfe_page_layout where page_url = :page_url and status = 'Active'";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_url', $page_name);
		$pageData = $command->queryAll();
		$pageArray = array();
		if(!empty($pageData[0])){
			// for page data
			$pageArray['page_data'] = $pageData[0];
			$page_id = $pageData[0]['id'];
			$sql = "select table_name,page_type,priority from hfe_page_priority where page_id = :page_id and priority != '' order by priority ASC";
			$command = $connection->createCommand($sql);
			$command->bindParam(':page_id', $page_id);
			$pagePriorityData = $command->queryAll();
			if(!empty($pagePriorityData)){
				// for page priority
				$pageArray['page_priority_data'] = $pagePriorityData;
				// for category
				$focus_areas = 'focus_areas';
				$sql = "select title,id,priority from hfe_category where type = :type and status = 'Active' order by priority ASC";
				$command = $connection->createCommand($sql);
				$command->bindParam(':type', $focus_areas);
				$pageCategoryData = $command->queryAll();
				$pageArray['page_category_data'] = $pageCategoryData;
				foreach($pagePriorityData as $priority){
					$cond = '';
					if($priority['table_name'] == "hfe_focus_areas" && !empty($pageCategoryData[0]['id'])){
						$cond = ' and category_id = '.$pageCategoryData[0]['id'];	
					}
					$sql = "select * from ".$priority['table_name']." where page_id = :page_id and status = 'Active' $cond order by priority ASC";
					$command = $connection->createCommand($sql);
					$command->bindParam(':page_id', $page_id);
					$data = $command->queryAll();
					if(!empty($data)){
						// for all page data
						$pageArray[$priority['page_type']] = $data;
					}
				}
			}	
		}
		return $pageArray;
	}
	
	// for get focus areas data
	public function getFocusData($category_id){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_focus_areas where category_id = :category_id and status = 'Active' order by priority Asc";
		$command = $connection->createCommand($sql);
		$command->bindParam(':category_id', $category_id);
		$data = $command->queryAll();	
		return $data;
	}
	
}

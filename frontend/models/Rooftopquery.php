<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Rooftopquery model.
 */
class Rooftopquery extends \yii\db\ActiveRecord {
	
	public static function tableName() {
        return '{{hfe_rooftop_solar_query}}';
    }	
		
	// Insert calculator query details	
	public function saveCalculatorQuery($postData){
		$connection = \Yii::$app->db;	
		$name = !empty($postData['name']) ? $postData['name'] : '';
		$email_id = !empty($postData['email']) ? $postData['email'] : '';
		$phone = !empty($postData['phone']) ? $postData['phone'] : '';
		$rooftop_area = !empty($postData['rooftop_area']) ? $postData['rooftop_area'] : '';
		$created_on = date("Y-m-d H:i:s");
		$sql = "insert into hfe_calculator (name, email_id, phone, rooftop_area, created_on) values (:name, :email_id, :phone, :rooftop_area, :created_on)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':name', $name);
		$command->bindParam(':email_id', $email_id);
		$command->bindParam(':phone', $phone);
		$command->bindParam(':rooftop_area', $rooftop_area);
		$command->bindParam(':created_on', $created_on);
		$command->execute();
	}	
}

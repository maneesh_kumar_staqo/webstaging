<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Blogs model.
 */
class Blogs extends \yii\db\ActiveRecord {
		
	// For get page data
	public function getPageData($category_id){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_blogs where status = 'Active' and category_id = :category_id order by publish_date desc";
		$command = $connection->createCommand($sql);
		$command->bindParam(':category_id', $category_id);
		$pageData = $command->queryAll();
		return $pageData;
	}
	
	// For get search page data
	public function getPageSearchData($q){
		$connection = \Yii::$app->db;	
		$q = "%".$q."%";
		$sql = "select * from hfe_blogs where status = 'Active' and tags like :tags order by publish_date desc";
		$command = $connection->createCommand($sql);
		$command->bindParam(':tags', $q);
		$pageData = $command->queryAll();	
		return $pageData;
	}
	
	// For get blogs details
	public function getBlogsData($page_url){
		$connection = \Yii::$app->db;
		$sql = "select category.id from hfe_blogs blogs join hfe_category category on (category.id = blogs.category_id) where blogs.status = 'Active' and blogs.page_url = :page_url and category.type = 'blogs' order by publish_date desc";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_url', $page_url);
		$data = $command->queryAll();
		$category_id = !empty($data[0]['id']) ? $data[0]['id'] : '';
			
		$sql = "select category.title as categoryTitle, blogs.* from hfe_blogs blogs join hfe_category category on (category.id = blogs.category_id) where blogs.status = 'Active' and blogs.category_id = :category_id order by publish_date desc";
		$command = $connection->createCommand($sql);
		$command->bindParam(':category_id', $category_id);
		$pageData = $command->queryAll();
		return $pageData;
	}

	public function getMediaBanner($page){
	
			$connection = \Yii::$app->db;
		$sql = "select * from other_hfe_banners where status = 'Active' and type = :type order by priority desc";
			$command = $connection->createCommand($sql);
			$command->bindParam(':type', $page);
			$pageData = $command->queryAll();	
			return $pageData;
	}
	
}

<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Pages model.
 */
class Pages extends \yii\db\ActiveRecord {
		
	public function getLeadershipVisionData(){
				$connection = \Yii::$app->db;	
		$sql = "select * from hfe_leadership where type = 'Leadership Vision' and status = 'Active' order by priority ASC";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_url', $page_name);
		$data = $command->queryAll();
	
		return $data;
	}
	// For get page data from page name
	public function getPageData($page_name="about-us"){
		$connection = \Yii::$app->db;	
		$sql = "select id, page_name,page_url,page_type,meta_title,meta_keywords,meta_description from hfe_page_layout where page_url = :page_url and status = 'Active'";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_url', $page_name);
		$pageData = $command->queryAll();
		$pageArray = array();
		
		if(!empty($pageData[0])){
			// for page data
			$pageArray['page_data'] = $pageData[0];
			$page_id = $pageData[0]['id'];
			$sql = "select table_name,page_type,priority from hfe_page_priority where page_id = :page_id and priority != '' order by priority ASC";
            
			$command = $connection->createCommand($sql);
			$command->bindParam(':page_id', $page_id);
			$pagePriorityData = $command->queryAll();
				
	


			if(!empty($pagePriorityData)){
				// for page priority
				$pageArray['page_priority_data'] = $pagePriorityData;
				foreach($pagePriorityData as $priority){
					if($priority['table_name'] == "hfe_leadership"){
						$sql = "select * from ".$priority['table_name']." where page_id = :page_id and type = 'Leadership' and status = 'Active' order by priority ASC";
					}else if($priority['table_name'] == "hfe_board_directors"){
						$sql = "select * from ".$priority['table_name']." where page_id = :page_id and status = 'Active' order by priority ASC";
					}else if($priority['table_name'] == "hfe_management_team"){
						$sql = "select * from ".$priority['table_name']." where page_id = :page_id and status = 'Active' order by priority ASC";
					}else{
						$sql = "select * from ".$priority['table_name']." where page_id = :page_id and status = 'Active'";
					}
					$command = $connection->createCommand($sql);
					$command->bindParam(':page_id', $page_id);
					$data = $command->queryAll();
				
					if(!empty($data)){
						// for all page data
						$pageArray[$priority['page_type']] = $data;
					}

				}
			}	
		}

		return $pageArray;
	}
}

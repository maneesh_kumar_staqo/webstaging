<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Search model.
 */
class Search extends \yii\db\ActiveRecord {
		
	// For get blogs search data
	public function getSearchBlogsData($q){
		$connection = \Yii::$app->db;
		$q = str_replace("-", " ", $q);
		$q = "%".$q."%";	
		$sql = "select * from hfe_blogs where status = 'Active' and (tags like :tags OR description like :description OR title like :title OR short_description like :short_description)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':title', $q);
		$command->bindParam(':tags', $q);
		$command->bindParam(':description', $q);
		$command->bindParam(':short_description', $q);
		$data = $command->queryAll();
		return $data;
	}
	
	// For get newsroom search data
	public function getSearchNewsroomData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select * from hfe_news_room where status = 'Active' and (tags like :tags OR description like :description OR title like :title OR short_description like :short_description)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':title', $q);
		$command->bindParam(':tags', $q);
		$command->bindParam(':description', $q);
		$command->bindParam(':short_description', $q);
		$data = $command->queryAll();
		return $data;
	}
	
	// For get awards search data
	public function getSearchAwardsData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select layout.* from hfe_awards aw join hfe_page_layout layout on (layout.id = aw.page_id) where aw.status = 'Active' and (aw.description like :description OR aw.title like :title)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':title', $q);
		$command->bindParam(':description', $q);
		$data = $command->queryAll();
		return $data;
	}
	
	// For get board of director search data
	public function getSearchBoardData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select layout.* from hfe_board_directors bd join hfe_page_layout layout on (layout.id = bd.page_id) where bd.status = 'Active' and (bd.description like :description OR bd.name like :name OR bd.designation like :designation)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':name', $q);
		$command->bindParam(':designation', $q);
		$command->bindParam(':description', $q);
		$data = $command->queryAll();
		return $data;
	}
	
	// For get career campus placement search data
	public function getSearchCareerCampusData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select * from hfe_career_campus_placements where status = 'Active' and (description like :description OR title like :title OR short_description like :short_description)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':title', $q);
		$command->bindParam(':short_description', $q);
		$command->bindParam(':description', $q);
		$data = $command->queryAll();
		if(!empty($data)){
			$sql = "select * from hfe_page_layout where status = 'Active' and page_url = 'career'";
			$command = $connection->createCommand($sql);
			$data = $command->queryAll();
			return $data;
		}
	}
	
	// For get career learn to win search data
	public function getSearchCareerLearnData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select * from hfe_career_learn_to_win where status = 'Active' and (description like :description OR title like :title OR image_short_description like :image_short_description OR image2_short_description like :image2_short_description OR image3_short_description like :image3_short_description)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':title', $q);
		$command->bindParam(':image_short_description', $q);
		$command->bindParam(':image2_short_description', $q);
		$command->bindParam(':image3_short_description', $q);
		$command->bindParam(':description', $q);
		$data = $command->queryAll();
		if(!empty($data)){
			$sql = "select * from hfe_page_layout where status = 'Active' and page_url = 'career'";
			$command = $connection->createCommand($sql);
			$data = $command->queryAll();
			return $data;
		}
	}
	
	// For get career thought search data
	public function getSearchCareerThoughtData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select * from hfe_career_thought_leadership where status = 'Active' and (description like :description OR title like :title)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':title', $q);
		$command->bindParam(':description', $q);
		$data = $command->queryAll();
		if(!empty($data)){
			$sql = "select * from hfe_page_layout where status = 'Active' and page_url = 'career'";
			$command = $connection->createCommand($sql);
			$data = $command->queryAll();
			return $data;
		}
	}
	
	// For get career we make search data
	public function getSearchCareerWeMakeData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select * from hfe_career_we_make where status = 'Active' and (title like :title)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':title', $q);
		$data = $command->queryAll();
		if(!empty($data)){
			$sql = "select * from hfe_page_layout where status = 'Active' and page_url = 'career'";
			$command = $connection->createCommand($sql);
			$data = $command->queryAll();
			return $data;
		}
	}
	
	// For get case study search data
	public function getSearchCaseStudyData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select layout.* from hfe_case_studies study join hfe_page_layout layout on (layout.id = study.page_id) where study.status = 'Active' and (study.title like :title)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':title', $q);
		$data = $command->queryAll();
		return $data;
	}
	
	// For get contact us search data
	public function getSearchContactUsData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select layout.* from hfe_contact_us contact join hfe_page_layout layout on (layout.id = contact.page_id) where contact.status = 'Active' and (contact.title like :title OR contact.address1 like :address1 OR contact.address2 like :address2)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':title', $q);
		$command->bindParam(':address1', $q);
		$command->bindParam(':address2', $q);
		$data = $command->queryAll();
		return $data;
	}
	
	// For get content search data
	public function getSearchContentsData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select layout.* from hfe_contents contents join hfe_page_layout layout on (layout.id = contents.page_id) where contents.status = 'Active' and (contents.title like :title OR contents.sub_title like :sub_title OR contents.description like :description)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':title', $q);
		$command->bindParam(':sub_title', $q);
		$command->bindParam(':description', $q);
		$data = $command->queryAll();
		return $data;
	}
	
	// For get corporate governance search data
	public function getSearchCorporateGovernanceData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select layout.* from hfe_corporate_governance governance join hfe_page_layout layout on (layout.id = governance.page_id) where governance.status = 'Active' and (governance.title like :title OR governance.sub_title like :sub_title OR governance.description like :description)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':title', $q);
		$command->bindParam(':sub_title', $q);
		$command->bindParam(':description', $q);
		$data = $command->queryAll();
		return $data;
	}
	
	// For get disclosures search data
	public function getSearchDisclosuresData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select layout.* from hfe_disclosures disclosures join hfe_page_layout layout on (layout.id = disclosures.page_id) where disclosures.status = 'Active' and (disclosures.title like :title)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':title', $q);
		$data = $command->queryAll();
		return $data;
	}
	
	// For get focus area search data
	public function getSearchFocusAreaData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select layout.* from hfe_focus_areas focus join hfe_page_layout layout on (layout.id = focus.page_id) where focus.status = 'Active' and (focus.state_name like :state_name OR focus.operational_beneficiary like :operational_beneficiary OR focus.description like :description)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':state_name', $q);
		$command->bindParam(':operational_beneficiary', $q);
		$command->bindParam(':description', $q);
		$data = $command->queryAll();
		return $data;
	}
	
	// For get investor relations search data
	public function getSearchInvestorRelationsData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select layout.* from hfe_investor_relations relations join hfe_page_layout layout on (layout.id = relations.page_id) where relations.status = 'Active' and (relations.title like :title OR relations.description like :description)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':title', $q);
		$command->bindParam(':description', $q);
		$data = $command->queryAll();
		return $data;
	}
	
	// For get leadership search data
	public function getSearchLeadershipData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select layout.* from hfe_leadership leadership join hfe_page_layout layout on (layout.id = leadership.page_id) where leadership.status = 'Active' and (leadership.name like :name OR leadership.designation like :designation OR leadership.description like :description)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':name', $q);
		$command->bindParam(':designation', $q);
		$command->bindParam(':description', $q);
		$data = $command->queryAll();
		return $data;
	}
	
	// For get management team search data
	public function getSearchManagementTeamData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select layout.* from hfe_management_team team join hfe_page_layout layout on (layout.id = team.page_id) where team.status = 'Active' and (team.name like :name OR team.designation like :designation OR team.description like :description)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':name', $q);
		$command->bindParam(':designation', $q);
		$command->bindParam(':description', $q);
		$data = $command->queryAll();
		return $data;
	}
	
	// For get page images search data
	public function getSearchPageImagesData($q){
		$connection = \Yii::$app->db;
		$q = "%".$q."%";	
		$sql = "select layout.* from hfe_page_images images join hfe_page_layout layout on (layout.id = images.page_id) where images.status = 'Active' and (images.title like :title OR images.short_description like :short_description OR images.description like :description)";
		$command = $connection->createCommand($sql);
		$command->bindParam(':title', $q);
		$command->bindParam(':short_description', $q);
		$command->bindParam(':description', $q);
		$data = $command->queryAll();
		return $data;
	}
	
}

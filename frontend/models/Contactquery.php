<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Contactquery model.
 */
class Contactquery extends \yii\db\ActiveRecord {
		
	public static function tableName() {
        return 'hfe_contact_us_query';
    }	
}

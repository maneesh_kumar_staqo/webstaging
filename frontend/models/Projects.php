<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Projects model.
 */
class Projects extends \yii\db\ActiveRecord {
		
	// For get page data from page name
	public function getPageData($page_name){
		$connection = \Yii::$app->db;	
		$sql = "select id, page_name,page_url,page_type,meta_title,meta_keywords,meta_description from hfe_page_layout where page_url = :page_url and status = 'Active'";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_url', $page_name);
		$pageData = $command->queryAll();
		$pageArray = array();
	
		if(!empty($pageData[0])){
			// for page data
			$pageArray['page_data'] = $pageData[0];
			$page_id = $pageData[0]['id'];
			$sql = "select table_name,page_type,priority from hfe_page_priority where page_id = :page_id and priority != '' order by priority ASC";
			$command = $connection->createCommand($sql);
			$command->bindParam(':page_id', $page_id);
			$pagePriorityData = $command->queryAll();
			if(!empty($pagePriorityData)){
				// for page priority
				$pageArray['page_priority_data'] = $pagePriorityData;
				foreach($pagePriorityData as $priority){
					// echo $priority['page_type']."</br>";
			
						$imgType = '';
						if($priority['page_type'] == 'commissioned_projects'){
							$imgType = ' and type = "Commissioned Projects" ';	
						}
						$sql = "select * from ".$priority['table_name']." where page_id = :page_id and status = 'Active' $imgType order by priority ASC";
						$command = $connection->createCommand($sql);
						$command->bindParam(':page_id', $page_id);
						$data = $command->queryAll();
						if(!empty($data)){
							// for all page data
							$pageArray[$priority['page_type']] = $data;
						}
					
				}
			}	
		}

		return $pageArray;
	}
	
	// Get state data
	public function getStateData(){
		$connection = \Yii::$app->db;
		$sql = "select location from commissioned_projects where status = 'Active' AND type = 'Commissioned Projects' GROUP BY location";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		return $data;
	}
	
	// Get investor images data
	public function getInvestorImgData(){
		$connection = \Yii::$app->db;
		$sql = "select * from hfe_page_images where status = 'Active' and type = 'investor_image' order by priority Asc";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		return $data;
	}

		public function getFilterProject($type,$location){
		$connection = \Yii::$app->db;
		
		if(!empty($type) && !empty($location)){
		$sql = "select * from commissioned_projects where installation_type= :type and location= :location and type = 'Commissioned Projects' and status = 'Active' order by priority ASC ";
		$command = $connection->createCommand($sql);
		$command->bindParam(':type', $type);
		$command->bindParam(':location', $location);
		}
		else if(empty($location) && empty($type)){
			$sql = "select * from commissioned_projects where type = 'Commissioned Projects' and status = 'Active' order by priority ASC ";
		$command = $connection->createCommand($sql);

		}
		else if(empty($type)){
			$sql = "select * from commissioned_projects where location= :location and type = 'Commissioned Projects' and status = 'Active' order by priority ASC ";
		$command = $connection->createCommand($sql);
		$command->bindParam(':location', $location);
		}
		else if(empty($location)){
			$sql = "select * from commissioned_projects where installation_type= :type and type = 'Commissioned Projects' and status = 'Active' order by priority ASC ";
		$command = $connection->createCommand($sql);
		$command->bindParam(':type', $type);
		}
		
	
	
	
		$data = $command->queryAll();
	
		return $data;
	}
	
}

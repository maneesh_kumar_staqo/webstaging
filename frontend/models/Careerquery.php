<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Careerquery model.
 */
class Careerquery extends \yii\db\ActiveRecord {
		
	public static function tableName() {
        return 'hfe_career_query';
    }	
	
}

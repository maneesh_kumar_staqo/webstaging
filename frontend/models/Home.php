<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Home model.
 */
class Home extends \yii\db\ActiveRecord {
		
	// For get page data from page name
	public function getPageData($page_name){
		$connection = \Yii::$app->db;	
		$sql = "select id, page_name,page_url,page_type,meta_title,meta_keywords,meta_description from hfe_page_layout where page_url = :page_url and status = 'Active'";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_url', $page_name);
		$pageData = $command->queryAll();
		$pageArray = array();
		if(!empty($pageData[0])){
			$pageArray['page_data'] = $pageData[0];
			$page_id = $pageData[0]['id'];
			$sql = "select table_name,page_type,priority from hfe_page_priority where page_id = :page_id and priority != '' order by priority ASC";
			$command = $connection->createCommand($sql);
			$command->bindParam(':page_id', $page_id);
			$pagePriorityData = $command->queryAll();
			if(!empty($pagePriorityData)){
				// for page priority

				$pageArray['page_priority_data'] = $pagePriorityData;
				// for category
				$focus_areas = 'focus_areas';
				$sql = "select title,id,priority from hfe_category where type = :type and status = 'Active' order by priority ASC";
				$command = $connection->createCommand($sql);
				$command->bindParam(':type', $focus_areas);
				$pageCategoryData = $command->queryAll();
				$pageArray['page_category_data'] = $pageCategoryData;
				// echo "<pre>";print_r($pagePriorityData);die;
				foreach($pagePriorityData as $priority){
					
					if($priority['page_type'] == "esg-highlights"){
						$sql = "select * from ".$priority['table_name']." where status = 'Active' ";
						$command = $connection->createCommand($sql);
						$data = $command->queryAll();

					}else if($priority['page_type'] == "social-action"){
			
						$sql = "select * from ".$priority['table_name']." where status = 'Active' ";
						$command = $connection->createCommand($sql);
					}else if($priority['page_type'] == "blogs"){
			
						$sql = "select * from ".$priority['table_name']." where status = 'Active' ";
						$command = $connection->createCommand($sql);
					}
					else{

						$sql = "select * from ".$priority['table_name']." where page_id = :page_id and status = 'Active' order by priority ASC";
						$command = $connection->createCommand($sql);
						$command->bindParam(':page_id', $page_id);
					}
					$data = $command->queryAll();
				
					if(!empty($data)){
						$pageArray[$priority['page_type']] = $data;
					}
				}
			
			}	
		}
		return $pageArray;
	}
	
	// For get blogs data for home page
	public function getBlogsData(){
		$connection = \Yii::$app->db;	
		//$sql = "select id,title,image_link,page_url,image from hfe_blogs where status = 'Active' and is_home = 'Yes' order by id Asc";	
		$sql = "select id,title,image_link,page_url,image,publish_date, created_on, updated_on  from hfe_blogs where status = 'Active' order by publish_date  Desc LIMIT 3";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		return $data;
	}
	
	// For get youtube data for home page
public function getYoutubeData(){
		$connection = \Yii::$app->db;	
		$sql = "select id, title, video_thumb_image, video_link from hfe_youtube where status = 'Active' and is_home = 'Yes' order by id ASC";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		if(!empty($data[0])){
			return $data[0];
		}
	}
	public function getStaticsData(){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_statics where status = 'Active' order by priority ASC";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		return $data;
	}
		public function getInvestorLogoData(){
		$connection = \Yii::$app->db;	
		$sql = "select * from investor_relation_logo where status = 'Active' order by priority ASC";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		return $data;
	}
	public function getHighlightsData(){
		$connection = \Yii::$app->db;	
		$sql = "select * from esg_highlights where status = 'Active' order by priority ASC";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		return $data;
	}
	public function getSocialData(){
		$connection = \Yii::$app->db;	
			
		$sql = "select *  from social_action where status = 'Active' order by id ASC LIMIT 3";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		return $data;
	}

		public function getVlogData($id){
		$connection = \Yii::$app->db;	
			
		$sql = "select * from hfe_youtube where status = 'Active' AND category_id = :category_id order by priority  ASC LIMIT 3";
		$command = $connection->createCommand($sql);
		$command->bindParam(':category_id', $id);
		$data = $command->queryAll();
		return $data;
	}
	public function CorporateVideo($id){
		$connection = \Yii::$app->db;	
			
		$sql = "select * from hfe_youtube where status = 'Active' AND category_id = :category_id order by priority  ASC LIMIT 1";
		$command = $connection->createCommand($sql);
		$command->bindParam(':category_id', $id);
		$data = $command->queryOne();
		return $data;
	}

	
	
}

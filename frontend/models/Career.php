<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Career model.
 */
class Career extends \yii\db\ActiveRecord {
		
	// For get banner data
	public function getBannerData(){
		$connection = \Yii::$app->db;	
		$page_id = 11;
		$sql = "select * from hfe_banners where page_id = :page_id and status = 'Active' order by priority Asc";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_id', $page_id);
		$data = $command->queryAll();
		if(!empty($data)){
			return $data;
		}
	}
	
	// For get content
	public function getContentData(){
		$connection = \Yii::$app->db;	
		$page_id = 11;
		$sql = "select * from hfe_contents where page_id = :page_id and status = 'Active'";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_id', $page_id);
		$data = $command->queryAll();
		if(!empty($data)){
			return $data;
		}
	}
	
	// For get we make
	public function getWemakeData(){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_career_we_make where status = 'Active' order by priority Asc";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		if(!empty($data)){
			return $data;
		}
	}
	
	// For get slider
	public function getSliderData(){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_career_slider where status = 'Active' order by priority Asc";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		if(!empty($data)){
			return $data;
		}
	}
	
	// For get thought
	public function getThoughtData(){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_career_thought_leadership where status = 'Active'";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		if(!empty($data)){
			return $data;
		}
	}
	
	// For get learn
	public function getLearnData(){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_career_learn_to_win where status = 'Active'";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		if(!empty($data)){
			return $data;
		}
	}
	
	// For get campus
	public function getCampusData(){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_career_campus_placements where status = 'Active' order by priority Asc";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		if(!empty($data)){
			return $data;
		}
	}
	
	// For get vacancies
	public function getVacanciesData(){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_career_vacancies where status = 'Active'";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		if(!empty($data)){
			return $data;
		}
	}
	
}

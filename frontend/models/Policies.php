<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Policies model.
 */
class Policies extends \yii\db\ActiveRecord {
		
	// For get page data
	public function getPageData($page_name){
		$connection = \Yii::$app->db;	
		// $sql = "select policies.* from hfe_policies policies join hfe_page_layout layout on (layout.id = policies.page_id) where policies.status = 'Active' and layout.page_url = :page_url";

		$sql = "select id, page_name,page_url,page_type,meta_title,meta_keywords,meta_description from hfe_page_layout where page_url = :page_url and status = 'Active'";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_url', $page_name);
		$pageData = $command->queryAll();
	
		return $pageData;
	}
		public function getPolicyData($page_name){
		$connection = \Yii::$app->db;	
		$sql = "select policies.* from hfe_policies policies join hfe_page_layout layout on (layout.id = policies.page_id) where policies.status = 'Active' and layout.page_url = :page_url";

		$command = $connection->createCommand($sql);
		$command->bindParam(':page_url', $page_name);
		$pageData = $command->queryAll();
	
		return $pageData;
	}
	

	// For get banner data
	public function getBannerData($page_name){
		$connection = \Yii::$app->db;	
		$sql = "select banner.* from hfe_banners banner join hfe_page_layout layout on (layout.id = banner.page_id) where banner.status = 'Active' and layout.page_url = :page_url order by banner.priority Asc";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_url', $page_name);
		$bannerData = $command->queryAll();
		// echo "<pre>";print_r($bannerData);die;
		return $bannerData;
	}
	
	// For get content
	public function getContentData($page_url){
		$connection = \Yii::$app->db;	
		$page_id = 6;
		$sql = "select content.* from hfe_contents content join hfe_page_layout layout on (layout.id = content.page_id) where layout.page_url = :page_url and content.status = 'Active'";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_url', $page_url);
		$data = $command->queryAll();
		if(!empty($data)){
			return $data;
		}
	}

	public function getDisclouserData($page_id){

		$connection = \Yii::$app->db;	
		// $sql = "select * from  hfe_disclosures where status = 'Active'  order by priority Asc";
		$sql="select years, month from hfe_disclosures  where status = 'Active' and page_id = :page_id group by years,month";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_id', $page_id);
		$data = $command->queryAll();
		return $data;
	}

	public function getAnnualQuarterly($years,$page_id){
	
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_disclosures where years = :years AND type= 'Annual / Quarterly' AND  status = 'Active' and page_id = :page_id ORDER BY month DESC ";
		$command = $connection->createCommand($sql);
		$command->bindParam(':years', $years);
		$command->bindParam(':page_id', $page_id);
		$pageData = $command->queryAll();
		return $pageData;
	}
	public function getMonthly($years,$page_id){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_disclosures where years = :years AND type= 'Monthly' AND  status = 'Active' and page_id = :page_id ORDER BY month DESC ";
		$command = $connection->createCommand($sql);
		$command->bindParam(':years', $years);
		$command->bindParam(':page_id', $page_id);
		$pageData = $command->queryAll();
		return $pageData;
	}	
	
	public function getDisclouserDataitem($page_id){
		$connection = \Yii::$app->db;	
		$sql = "select distinct(years) from hfe_disclosures where status = 'Active' and page_id = :page_id ORDER BY years DESC";
		$command = $connection->createCommand($sql);
				$command->bindParam(':page_id', $page_id);
		$pageData = $command->queryAll();
		return $pageData;
	}
	public function getAddressData($page_id){
	
		$connection = \Yii::$app->db;	
		$sql = "select hfe_bgel_address.* from hfe_bgel_address join hfe_page_layout layout on (layout.id = hfe_bgel_address.page_id) where hfe_bgel_address.status = 'Active' and layout.page_url = :page_url";

		// $sql = "select * from hfe_bgel_address where status = 'Active' and page_id = :page_id ORDER BY priority DESC ";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_url', $page_id);
		$addressData = $command->queryAll();
		return $addressData;
	}
}

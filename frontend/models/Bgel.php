<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Bgel model.
 */
class Bgel extends \yii\db\ActiveRecord {
		
	// For get contact page data
	public function getPageData($page_name){
		
		$connection = \Yii::$app->db;	
		$sql = "select id, page_name,page_url,page_type,meta_title,meta_keywords,meta_description from hfe_page_layout where page_url = :page_url and status = 'Active'";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_url', $page_name);
		$pageData = $command->queryAll();
;
		$pageArray = array();
		if(!empty($pageData[0])){
			// for page data
			$pageArray['page_data'] = $pageData[0];
			$page_id = $pageData[0]['id'];
			$sql = "select table_name,page_type,priority from hfe_page_priority where page_id = :page_id and priority != '' order by priority ASC";
			$command = $connection->createCommand($sql);
			$command->bindParam(':page_id', $page_id);
			$pagePriorityData = $command->queryAll();
			//    echo "<pre>";print_r($pagePriorityData);die;
			if(!empty($pagePriorityData)){
				// for page priority
				$pageArray['page_priority_data'] = $pagePriorityData;
				
				foreach($pagePriorityData as $priority){
						
					$cond = '';
					 if($priority['page_type'] == "disclosures"){
			
							$sql="select years,page_id, month from hfe_disclosures where page_id = :page_id AND  status = 'Active' group by years,month";
					$command = $connection->createCommand($sql);
					$command->bindParam(':page_id', $page_id);
					}else{

						$sql = "select * from ".$priority['table_name']." where page_id = :page_id and status = 'Active' $cond order by priority ASC";
						$command = $connection->createCommand($sql);
						$command->bindParam(':page_id', $page_id);
					}
					$data = $command->queryAll();
		
					if(!empty($data)){
						// for all page data
						if($priority['page_type'] == "rooftop_knowmore"){
							$pageArray['rooftop_gallery']['knowmore'][$priority['page_type']] = $data;
						}else{
							if($priority['page_type'] != "rooftop_tracking"){
								$pageArray[$priority['page_type']] = $data;
							}
						}
						
					}
				}
				
			}	
		}
		return $pageArray;
	}



	public function getDisclouserDataitem($page_id){
		$connection = \Yii::$app->db;	
		$sql = "select distinct(years) from hfe_disclosures where status = 'Active' and page_id = :page_id ORDER BY years DESC";
		$command = $connection->createCommand($sql);
				$command->bindParam(':page_id', $page_id);
		$pageData = $command->queryAll();
		return $pageData;
	}

	public function getAnnualQuarterly($years,$page_id){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_disclosures where years = :years AND type= 'Annual / Quarterly' AND  status = 'Active' and page_id = :page_id ORDER BY month DESC ";
		$command = $connection->createCommand($sql);
		$command->bindParam(':years', $years);
		$command->bindParam(':page_id', $page_id);
		$pageData = $command->queryAll();
		return $pageData;
	}
	public function getMonthly($years,$page_id){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_disclosures where years = :years AND type= 'Monthly' AND  status = 'Active' and page_id = :page_id ORDER BY month DESC ";
		$command = $connection->createCommand($sql);
		$command->bindParam(':years', $years);
		$command->bindParam(':page_id', $page_id);
		$pageData = $command->queryAll();
		return $pageData;
	}
	
	// For get banner data
	public function getBannerData($page_id){
		$connection = \Yii::$app->db;	
	
		$sql = "select * from hfe_banners where status = 'Active' and page_id = :page_id";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_id', $page_id);
		$pageData = $command->queryAll();
		return $pageData;
	}
	
	// For get content data
	public function getContentData($page_id){
		$connection = \Yii::$app->db;	
	
		$sql = "select * from hfe_contents where status = 'Active' and page_id = :page_id";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_id', $page_id);
		$pageData = $command->queryAll();
		return $pageData;
	}
	
	// For get bgel address data
	public function getBgelAddrData(){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_bgel_address where status = 'Active' order by priority Asc";
		$command = $connection->createCommand($sql);
		$data = $command->queryAll();
		return $data;
	}
	public function getDisclouserData($page_id){

		$connection = \Yii::$app->db;	
		// $sql = "select * from  hfe_disclosures where status = 'Active'  order by priority Asc";
		$sql="select years, month from hfe_disclosures  where status = 'Active' and page_id = :page_id group by years,month";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_id', $page_id);
		$data = $command->queryAll();
	
		// $allmonth;
		// foreach ($data as $key => $value) {
		// 	// echo $value['years']."<br>";
		// 	$connection = \Yii::$app->db;	
		// $sql1 = "select * from hfe_disclosures where status = 'Active' and years = :years GROUP BY month";
		// 	$years=$value['years'];
		
		// 	$command1 = $connection->createCommand($sql1);
		// 	$command1->bindParam(':years',$years);
			
		// 	$data1 = $command1->queryAll();
		// 	$allmonth[]=$data1;
		
		// }
			// echo "<pre>";print_r($allmonth);die;
	

		return $data;
	}
	
}

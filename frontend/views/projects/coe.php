<?php

if(!empty($data)){ ?>
	   <section class="n_video_section theme_sec bg_sec mrb_0 " id="COI">
      <div class="container" style="display: block;">
         <div class="row">
            <div class="col-md-12">
               <h2 class="section-title text-center"><?= $data[0]['title'] ?></h2>
            </div>
            <div class="col-md-12">
               <div class="about_data_content mrb-20" id="about-hero-group">
                  <p class="text-center">
                    <?= $data[0]['sub_title'] ?>
                  </p>
                  <div class="coe_images_bg mobile-hidden">
                     <img src="<?= BASE_BE_URL; ?>uploads/contents/<?= $data[0]['image']; ?>">
                  </div>
                    <div class="coe_images_bg desktop-hidden">
                     <p><br></p>
                     <img src="<?= BASE_BE_URL; ?>uploads/contents/<?= $data[0]['mobile_image']; ?>">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
<?php }

?>
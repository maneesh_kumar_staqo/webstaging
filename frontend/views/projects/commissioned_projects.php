<section class="blue_sec">
	<div class="container">
		<div class="projects_filter">
			<div class="row pl-filter">
				<div class="col-sm-12 pl">
					<h3><span style="color: #fff">HFE’s Utility Scale Wind and Solar Project Portfolio</span></h3>
					<div class="filter_list">
						<form id="">
							<ul>
							<li class="">
								<label for="inst" style="color: #fff;">Select Installation Type</label>
								<select name="installation" id="insttype" class="form-control">
									<option value=""> All</option>
									<option value="Utility Scale Solar"> Utility Scale Solar</option>
									<option value="Hybrid"> Hybrid</option>
									<option value="Utility Scale Wind">Utility Scale Wind</option>
								</select>
							</li>
							<li class="">
								<label for="inst" style="color: #fff;">Select Location</label>
								<select name="installation" id="location" class="form-control">
									<option value=""> All</option>
										<?php foreach ($state as $key => $value) { ?>
										<option value="<?= $value['location'] ?>"><?= $value['location'] ?></option>
										<?php } ?>
								</select>
							</li>
							<li class="">
								<button type="button"
									class="btnallred btn btn-sm btn-theme-danger hard_button waves-effect waves-light" id="commissionedfilter"><span>View
										&nbsp; &gt;</span> </button>
							</li>
						</ul>
						</form>
					</div>
				</div>
			</div>
		</div>
</section>
<?php 
// echo "<pre>";print_r($data);die;

if(!empty($data)){  ?>
<section class="n_video_section  mrb_0 mrt_0 pdt_0 " id="project-commissioned" style="padding-bottom: 0px;">
	<div class="container">
		<div class=" sec_padding_n2 project_listing">
			<div class="row">
				<div class="col-md-12">
					<h2 class="section-title text-center" style="padding: 0px 20px;" id="upcoming-project_heading">Commissioned Projects </h2>
				</div>
			</div>
			<div class="row" id="filterdata">
				<?php foreach ($data as $key => $value) { ?>
				<div class="col-md-4">
					<div class="c_and_i_feature_product_item">
						<div class="card_white">
							<img src="<?= BASE_BE_URL; ?>uploads/project/<?= $value['image']; ?> ">
							<div class="c_and_i_feature_product_item_content">
								<h4><?= $value['title']; ?></h4>
								<div>
									<?= $value['description']; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
			
			
			</div>	
			<div class="pagination" style="justify-content: center;">
					</div>
		</div>
	</div>

</section>
<?php } ?>
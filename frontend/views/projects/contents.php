<?php
if(!empty($data)){ ?>
  <section class="n_video_section bg_sec theme_sec mrb_0 " id="Health-Safety-and-Environment">
      <div class="container">
         <div class=" sec_padding_n2">
            <div class="row">
               <div class="col-md-12">

                  <div class="about_data_content" id="about-us">
                     <div class="row">
                        <div class="col-md-12">
                           <h2 class="section-title text-center"><?= $data[0]['title'] ?>
                           </h2>
                        </div>
                        <div class="col-md-8">
                          
                              <?= $data[0]['sub_title'] ?>
                           <div class="download_esia_report">
                              <a href="<?= BASE_BE_URL; ?>uploads/contents/<?= $data[0]['report']; ?>"
                                 target="_blank">Download ESIA Report</a>
                           </div>

                           <span id="deskdots"></span>
                        
                        </div>
                        <div class="col-md-4">
                           <div class="health_ims">
                              <img src="<?= BASE_BE_URL; ?>uploads/contents/<?= $data[0]['image']; ?>">
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div style="display: none;" id="deskmore">
                             
								    <?= $data[0]['description'] ?> <br>
                           </div>
                        </div>
                     </div>



                     <div>
                        <a class="btnallred btn btn-sm btn-theme-danger waves-effect waves-light"
                           href="javascript:void(0);" id="deskmyBtn" onclick="deskdivtoggle()"><span> Read More &nbsp;
                              ></span> </a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
<?php }

?>
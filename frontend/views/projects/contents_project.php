<?php 
if($data){
	foreach($data as $value){
		$style = "";
		if(!empty($value['bg_color'])){
			$style = "background-color:".$value['bg_color'];
		}
?>
<section class="top_banner_bottom ml-1 desktop-view" style="<?= $style; ?>">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-md-10 col-sm-12">
			  	<div class="top_banner_bottom-content text">
				  	<div class="mb-4 pl-3 wow fadeInRight"><?= $value['description']; ?></div>
			  	</div>
			</div>
		</div>
	</div>
</section>
</div>
<?php  } } ?>
<!-- mobile view section 1 start-->
<?php 
if($data){
	foreach($data as $value){
		$style = "";
		if(!empty($value['bg_color'])){
			$style = "background-color:".$value['bg_color'];
		}
?>
<section class="top_banner_bottom ml-3 mobile-view" style="<?= $style; ?>">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-md-10 col-sm-12">
  				<div class="slider-bottom-content text">
	  				<div class="pl-3 wow fadeInRight"><?= substr($value['description'],0,150); ?><span class="dots2">...		</span><span class="more2"><?= substr($value['description'],150,strlen($value['description'])); ?></span></div>
  				<button type="button" class="mt-2 btnallred btn-outline-danger float-left ml-3 mb-4 myproBtn" onclick="mobprodivtoggle()"><span>READ MORE &nbsp; ></span> </button>
  			</div>   				
			</div>
		</div>
	</div>
</section>
<?php  } } ?>

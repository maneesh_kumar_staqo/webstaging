<?php 
if(!empty($data)){

    ?>
       <section class="n_video_section theme_sec  mrb_0" id="awards">
      <div class="container" style="display: block;">
         <div class="">
            <h2 class="section-title text-center">Awards</h2>
         </div>
         <div class="theme_blue">
            <div class="row">
               <div class="col-md-12">
                  <div class="sec_box_blue">
                     <div class="property-item horizontal expertise-slide">
                        <div class="thumb">
                           <img src="<?= BASE_BE_URL; ?>uploads/awards/<?= $data[0]['image']; ?>" alt="">
                        </div>
                        <div class="content">
                           <h3 class="post-title para_title"><?= $data[0]['title'] ?>
                           </h3>
                           <div class="excerpt">
                              <div class="award-data">
                                <?= $data[0]['description'] ?>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
<?php }

?>
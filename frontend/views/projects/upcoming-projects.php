<?php if(!empty($data)){
	?>

	   <section class="n_video_section  mrb_0  " id="upcoming-project">
      <div class="container">
         <div class="">
            <h2 class="section-title text-center" style="padding: 0px 20px;" id="">UPCOMING PROJECTS </h2>
         </div>
         <div class="upcoming_event_slider">
			 <?php foreach ($data as $key => $value) { ?>
				 <div class="upcoming_event_slider_item">
               <div class="row no-gutters">
                  <div class="col-lg-12">
                     <div class="plant-desc bg-gradient h-100 p-4">
                        <div class="row">
                           <div class="col-md-4">
                              <div class="align_n_center">
                                 <div class="plant-placeholder">
                                    <div class="upcoming-cart_overlay">
                                       <?= $value['heading']; ?>
                                    </div>
                                    <img src="<?= BASE_BE_URL; ?>uploads/project/<?= $value['image']; ?>" class="img-fluid" alt="" disablewebedit="False">
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-8">
                              <div class="align_n_center">
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="upcoming_project_card_transparent">
                                          <div>
                                             <h4>
                                                <img src="<?= BASE_BE_URL; ?>uploads/project/<?= $value['box_image1']; ?>">
                                                <?= $value['box_heading1']; ?>
                                             </h4>
                                             <p>
                                                <?= $value['box_description1']; ?>
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="upcoming_project_card_transparent">
                                         <div>
                                             <h4>
                                                <img src="<?= BASE_BE_URL; ?>uploads/project/<?= $value['box_image2']; ?>">
                                                <?= $value['box_heading2']; ?>
                                             </h4>
                                             <p>
                                                <?= $value['box_description2']; ?>
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="upcoming_project_card_transparent">
                                           <div>
                                             <h4>
                                                <img src="<?= BASE_BE_URL; ?>uploads/project/<?= $value['box_image3']; ?>">
                                                <?= $value['box_heading3']; ?>
                                             </h4>
                                             <p>
                                                <?= $value['box_description3']; ?>
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="upcoming_project_card_transparent">
                                          <div>
                                             <h4>
                                                <img src="<?= BASE_BE_URL; ?>uploads/project/<?= $value['box_image4']; ?>">
                                                <?= $value['box_heading4']; ?>
                                             </h4>
                                             <p>
                                                <?= $value['box_description4']; ?>
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--  <p class="mb-3">
                              <button type="button" class="btnallred white_btn btn btn-sm btn-theme-danger hard_button waves-effect waves-light"><span>Explore more &nbsp; &gt;</span> </button>
                              
                              </p> -->
                     </div>
                  </div>
               </div>
            </div>	 
			<?php } ?>
           
            
         </div>
      </div>
   </section>

<?php } ?>
<?php
// echo "<pre>";print_r($data);die;

?>


<section class="mobile-hidden">
            <div class="n_home_slider">
               <?php 
                    if(!empty($data)){ 
                        foreach($data as $banners){
                    ?>
               <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $banners['image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= !empty($banners['title']) ? $banners['title'] : ''; ?> </h4>
                        <p class="text-right">
                           <?= !empty($banners['sub_title']) ? $banners['sub_title'] : ''; ?>
                        </p>
                     </div>
                  </div>
               </div>
               <?php } } ?>
            </div>
         </section>



<section class="desktop-hidden">
            <div class="n_home_slider">
               <?php 
                    if(!empty($data)){ 
                        foreach($data as $banners){
                    ?>
               <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $banners['mobile_image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= !empty($banners['title']) ? $banners['title'] : ''; ?> </h4>
                        <p class="text-right">
                           <?= !empty($banners['sub_title']) ? $banners['sub_title'] : ''; ?>
                        </p>
                     </div>
                  </div>
               </div>
               <?php } } ?>
            </div>
         </section>

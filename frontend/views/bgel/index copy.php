<?php
$this->title = 'Bhilwara Green Energy Limited | Hero Future Energies';
use frontend\models\Bgel;
$model = new Bgel();
$bgelData = $model->getBgelAddrData();
?>


<section>
            <div class="n_home_slider">
               <?php 
                    if(!empty($bannerData)){ 
                        foreach($bannerData as $banners){
                    ?>
               <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $banners['image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-left"><?= !empty($banners['title']) ? $banners['title'] : ''; ?> </h4>
                        <p class="text-left">
                           <?= !empty($banners['sub_title']) ? $banners['sub_title'] : ''; ?>
                        </p>
                     </div>
                  </div>
               </div>
               <?php } } ?>
            </div>
         </section>


<?php if(!empty($contentData[0]['title'])){ ?>
  <section class="bg_sec  theme_sec mrb_40 " style="padding-bottom: 0px;">
            <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class=" mrb_40 text-center">
            <div class="investor_relation_heading_nd1"><?= !empty($contentData[0]['title']) ? $contentData[0]['title'] : ''; ?></div>
            <div class="text-center">
                <?= !empty($contentData[0]['description']) ? $contentData[0]['description'] : ''; ?>
          </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php } ?>

<section class="n_video_section bgel-sec-3 ">
  <div class="container">

    	<div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
          <h2 class="section-title text-left">DISCLOSURES </h2>
        </div>
      		<div class="col-md-12 col-lg-12 col-sm-12">
        		<div class="accordion md-accordion" id="accordionfirst" role="tablist" aria-multiselectable="true">
          			<div class="row">
<?php 
	if(!empty($discoulser)){
		$count = 1;
		$count2 = 0;
		foreach($discoulser as $key => $cat){
      $month_name = date("F", mktime(0, 0, 0, $cat['month'], 10));

			if($count2 == 0){
		
			}
?>
      <div class="col-lg-6 col-md-6 col-sm-6">
			 <!-- Accordion card -->
          <div class="card box-shadow-none">
          <!-- Card header -->
            <div class="card-header" role="tab" id="headingOne<?= $count; ?>">
              <a data-toggle="collapse" data-parent="#accordionfirst" href="#collapseOne1<?= $count; ?>" aria-expanded="true"
                aria-controls="collapseOne11">
                <p class="mb-0">
                 <i class="fas fa-plus"></i> &nbsp; <?= $month_name."  ".$cat['years']; ?> 
                </p>
              </a>
            </div>

            <!-- Card body -->
            <div id="collapseOne1<?= $count; ?>" class="collapse" role="tabpanel" aria-labelledby="headingOne<?= $count; ?>" data-parent="#accordionfirst">
              <div class="card-body">
                <p class="disc_tbl_head"><b>
                    <font style="vertical-align: inherit;">
                      <font style="vertical-align: inherit;">Annual / Quarterly</font>
                    </font>
                  </b></p>
<?php 
	$pageData = $model->getDisclouserDataitem($cat['years'],$cat['month']);
	if(!empty($pageData)){
		foreach(array_reverse($pageData) as $value){
?>			  
          <a href="<?= !empty($value['pdf']) ? BASE_BE_URL.'uploads/disclosures/'.$value['pdf'] : ''; ?>#view=fitV" target="_blank">
					<p class=""><?= $value['title']; ?></p>
				</a>
<?php 
		}
	}
?>	
              </div>
            </div>
          </div>
          </div>
          <!-- Accordion card -->
<?php 
				$count2++;
				if((ceil(count($category) / 2)) == $count2){
				
					$count2 = 0;
				}
				$count++;
			}
		}
?>			  

       
          </div>
      
      </div>
     
      </div>

  </div>
  </div>
</section>

<?php
$this->title = 'Blogs';
use frontend\models\Blogs;
$model = new Blogs();
?>
 <?php if(!empty($banner)){ ?>
             <section class="mobile-hidden">
            <div class="n_home_slider">
               <?php foreach ($banner as $key => $value) { ?>
                     <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $value['image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= $value['title'] ?></h4>
                        <p class="text-right">
                           <?= $value['sub_title'] ?>
                        </p>
                     </div>
                  </div>
               </div>
              <?php  } ?>
            
            </div>
         </section>
         <?php } ?>

           <?php if(!empty($banner)){ ?>
             <section class="desktop-hidden">
            <div class="n_home_slider">
               <?php foreach ($banner as $key => $value) { ?>
                     <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $value['image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= $value['title'] ?></h4>
                        <p class="text-right">
                           <?= $value['sub_title'] ?>
                        </p>
                     </div>
                  </div>
               </div>
              <?php  } ?>
            
            </div>
         </section>
         <?php } ?>
        
   <section class="n_video_section newsroom-sec2bottom ">
            <div class="container">
	<div class="row justify-content-center">
  		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="blog-container">
	  			<div class="blog-container-heading mb-4">
				<h2 class="section-title">Blogs</h2>  
				 <div class="searhcnews">
					<form action="" onsubmit="return searchVal();">
	  			<input type="text" placeholder="Search Here" class="blog-search" id="blog-search" name="q">
	  			<button type="submit" class="blog-btn search_inline_btn"><i class="fa fa-search"></i></button>
			</form>
					
					</div>


	  			</div>
	  			<div class="blog-tab">
					<div class="row">
		  				<div class="col-md-3 col-lg-3 col-sm-3">
							<div class="blog-vertical-tab">
		  						<ul class="nav nav-pills mb-5" id="pills-tab" role="tablist">
								<?php if(!empty($category)){
									$count = 1;
									foreach($category as $cat){
										$class = '';
										if($count == 1){ $class = 'active'; } ?>
                                    
                                    <?php
                                    if ($cat == "Power2Think")
                                    {  ?>   
									<li class="nav-item">
										<a class="nav-link contact-visit-link <?= $class; ?>" id="blog-tab<?= $count; ?>" data-toggle="pill" href="#<?= str_replace(" ", "", $cat); ?>" role="tab" aria-controls="pills-home" aria-selected="true"><?= "#".$cat; ?></a>
									</li>
                                    <?php
                                    } else
                                    {  ?>
                                    <li class="nav-item">
										<a class="nav-link contact-visit-link <?= $class; ?>" id="blog-tab<?= $count; ?>" data-toggle="pill" href="#<?= str_replace(" ", "", $cat); ?>" role="tab" aria-controls="pills-home" aria-selected="true"><?= $cat; ?></a>
									</li>
                                    <?php
                                    } ?>
                                    
								<?php $count++; } } ?>		
		  						</ul>
							</div>
							<span class="blog-pipe-small"></span>
		  				</div>
						<div class="col-md-9 col-lg-9 col-sm-9">
							<div class="tab-content cust-tab-visit pt-2 pl-1" id="pills-tabContent">
								<?php 
									if(!empty($category)){
										$count = 1;
										foreach($category as $key => $cat){
											$class = "";
											if($count == 1){
												$class = "show active";
											}
											$pageData = $model->getPageData($key);
								?>			
								<div class="tab-pane fade <?= $class; ?> blog-padd" id="<?= str_replace(" ", "", $cat); ?>" role="tabpanel" aria-labelledby="blog-tab<?= $count; ?>">
			  						<div class="row mb-4">
										<?php 
											if(!empty($pageData)){
												foreach($pageData as $value){
										?>				
										<div class="col-lg-4 col-md-4 col-sm-4 mb-5">
					  						<div class="card blog-card">
					  							<?php if(!empty($value['image'])){ ?>
												<img class="card-img-top" src="<?= BASE_BE_URL; ?>uploads/blogs/<?= $value['image']; ?>" alt="Card image cap">
					  							<?php } ?>
												<div class="card-body">
						  							<p class="color-black"><?= $value['publish_date']; ?> / <?= $value['team_name']; ?></p>
						  							<h4 class="color-black"><?= $value['title']; ?></h4>
						  							<p class="card-text color-black"><?= $value['short_description']; ?>...<a href="<?= BASE_URL.'blogs/'.$value['page_url']; ?>">Read More</a></p>
												</div>
											</div>
										</div>
										<?php } } ?>		
				  					</div>
								</div>
								<?php  $count++; } } ?>
							</div>
		  				</div>
					</div>	  
	  			</div>
			</div>
  		</div>
	</div>
												  </div>
</section>

<script>
function searchVal(){
	if($("#blog-search").val() == ""){
		alert("Please enter search keyword.");
		$("#blog-search").focus();
		return false;
	}else{
		var str = $("#blog-search").val();
		str = str.toLowerCase().replace(/[_\W]+/g, "-");
		window.location.href = BASE_URL + 'blog-search?q=' + str;
		return false;
	}
}
</script>
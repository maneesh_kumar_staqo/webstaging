<?php
$this->title = 'Blogs Search';
use frontend\models\Blogs;
$model = new Blogs();
$q = '';
if(!empty($_GET['q'])){
	$q = $_GET['q'];
	$q = preg_replace('#[^a-z0-9_]#', ' ', $q);
}
?>

<section class="n_video_section newsroom-sec2bottom  ">
	<div class="container">
		<div class="row justify-content-center">
  		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="blog-container">

	  			<div class="blog-container-heading mb-4">
					<h2 class="section-title">BLOGS</h2>    
						 <div class="searhcnews">
					      	<form action="" onsubmit="return searchVal();">
	  			<input type="text" placeholder="Search Here" class="blog-search" id="blog-search" name="q" value="<?= $q; ?>">
	  			<button type="submit" class="blog-btn"><i class="fa fa-search"></i></button>
			</form>
		</div>
	  			</div>
	  			<div class="blog-tab">
					<div class="row">
		  				<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="tab-content cust-tab-visit pt-2 pl-1" id="pills-tabContent">

								<?php $pageData = $model->getPageSearchData($q); ?>		
								
								<div class="tab-pane fade show active " role="tabpanel" aria-labelledby="blog-tab1">
			  						<div class="row mb-4">
										<?php 
											if(!empty($pageData)){
												foreach($pageData as $value){
										?>				
										<div class="col-lg-3 col-md-3 col-sm-4 mb-5">
					  						<div class="card blog-card">
					  							<?php if(!empty($value['image'])){ ?>
												<img class="card-img-top" src="<?= BASE_BE_URL; ?>uploads/blogs/<?= $value['image']; ?>" alt="Card image cap">
					  							<?php } ?>
												<div class="card-body">
						  							<p class="color-black"><?= $value['publish_date']; ?> / <?= $value['team_name']; ?></p>
						  							<h4 class="color-black"><?= $value['title']; ?></h4>
						  							<p class="card-text color-black"><?= $value['short_description']; ?>...<a href="<?= BASE_URL.'blogs/'.$value['page_url']; ?>">Read More</a></p>
												</div>
											</div>
										</div>
										<?php } }else{
											echo "<div>No data found.</div>";
										} ?>		
				  					</div>
								</div>
							</div>
		  				</div>
					</div>	  
	  			</div>
			</div>
  		</div>
	</div>
	</div>
</section>

<script>
function searchVal(){
	if($("#blog-search").val() == ""){
		alert("Please enter search keyword.");
		$("#blog-search").focus();
		return false;
	}else{
		var str = $("#blog-search").val();
		str = str.toLowerCase().replace(/[_\W]+/g, "-");
		window.location.href = BASE_URL + 'blog-search?q=' + str;
		return false;
	}
}
</script>
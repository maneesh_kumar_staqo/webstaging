<?php
$this->title = 'Blogs';
use frontend\models\Blogs;
$model = new Blogs();
?>
<section class="blog-sec1bottom">
	<div class="row justify-content-center">
  		<div class="col-lg-8 col-md-8 col-sm-8">
			<form action="#">
	  			<input type="text" placeholder="Search Here" class="blog-search" name="search">
	  			<button type="submit" class="blog-btn"><i class="fa fa-search"></i></button>
			</form>
  		</div>
	</div>
</section>

<section class="blog-sec2bottom desktop-view">
	<div class="row justify-content-center">
  		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="blog-container">
	  			<div class="blog-container-heading mb-4">
					<h1>BLOGS</h1>          
	  			</div>
	  			<div class="blog-tab">
					<div class="row">
		  				<div class="col-md-3 col-lg-3 col-sm-3">
							<div class="blog-vertical-tab">
		  						<ul class="nav nav-pills mb-5" id="pills-tab" role="tablist">
								<?php if(!empty($category)){
									$count = 1;
									foreach($category as $cat){
										$class = '';
										if($count == 1){ $class = 'active'; } ?>				
									<li class="nav-item">
										<a class="nav-link contact-visit-link <?= $class; ?>" id="blog-tab<?= $count; ?>" data-toggle="pill" href="#<?= str_replace(" ", "", $cat); ?>" role="tab" aria-controls="pills-home" aria-selected="true"><?= $cat; ?></a>
									</li>
                                    
                                    <?php 
                                        if(strlen($cat) < 4)
                                        { ?>
                                            <br>
                                        <?php } ?>
                                    
								<?php $count++; } } ?>		
		  						</ul>
							</div>
							<span class="blog-pipe-small"></span>
		  				</div>
						<div class="col-md-9 col-lg-9 col-sm-9">
							<div class="tab-content cust-tab-visit pt-2 pl-1" id="pills-tabContent">
								<?php 
									if(!empty($category)){
										$count = 1;
										foreach($category as $key => $cat){
											$class = "";
											if($count == 1){
												$class = "show active";
											}
											$pageData = $model->getPageData($key);
											// echo "<pre>"; print_r($pageData); die;
								?>			
								<div class="tab-pane fade <?= $class; ?> blog-padd" id="<?= str_replace(" ", "", $cat); ?>" role="tabpanel" aria-labelledby="blog-tab<?= $count; ?>">
			  						<div class="row mb-4">
										<?php 
											if(!empty($pageData)){
												foreach($pageData as $value){
										?>				
										<div class="col-lg-4 col-md-4 col-sm-4 mb-5">
					  						<div class="card blog-card">
					  							<?php if(!empty($value['image'])){ ?>
												<img class="card-img-top" src="<?= BASE_BE_URL; ?>uploads/blogs/<?= $value['image']; ?>" alt="Card image cap">
					  							<?php } ?>
												<div class="card-body">
						  							<p class="color-black"><?= $value['publish_date']; ?> / <?= $value['team_name']; ?></p>
						  							<h4 class="color-black"><?= $value['title']; ?></h4>
						  							<p class="card-text color-black"><?= $value['short_description']; ?>...<a href="<?= BASE_URL.'blogs/'.$value['page_url']; ?>">Read More</a></p>
												</div>
											</div>
										</div>
										<?php } } ?>		
				  					</div>
								</div>
								<?php  $count++; } } ?>
							</div>
		  				</div>
					</div>	  
	  			</div>
			</div>
  		</div>
	</div>
</section>
<section class="newsroom-sec2bottom mobile-view">
	<div class="row justify-content-center">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="newsroom-container blog-select">
			  	<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-4 text-center">
						<h1 class="mob-blog-heading">BLOGS</h1> 
						<br>						
						<select class="test" multiple="multiple" id="select">
						<?php if(!empty($category)){
							$count = 1;
							foreach($category as $cat){
								$class = '';
								if($count == 1){ $class = 'selected'; } ?>
						  	<option value="<?= $count; ?>" <?= $class; ?> ><?= $cat; ?></option>
						<?php $count++; } } ?>
						</select>
					</div>
			  	</div><br>				
  				<?php 
					if(!empty($category)){
						$count = 1;
						foreach($category as $key => $cat){
							$class = "";
							if($count == 1){
								$class = "show active";
							}
							$pageData = $model->getPageData($key);
							// echo "<pre>"; print_r($pageData); die;
				?>	
				<div class="gallery-sec-content pr-price d<?= $count; ?>">
  					<div class="row mb-4">						
						<?php 
							if(!empty($pageData)){
								foreach($pageData as $value){
						?>				
						<div class="col-lg-4 col-md-4 col-sm-4 mb-5">
	  						<div class="card blog-card">
	  							<?php if(!empty($value['image'])){ ?>
								<img class="card-img-top" src="<?= BASE_BE_URL; ?>uploads/blogs/<?= $value['image']; ?>" alt="Card image cap">
	  							<?php } ?>
								<div class="card-body">
		  							<p class="color-black"><?= $value['publish_date']; ?> / <?= $value['team_name']; ?></p>
		  							<h4 class="color-black"><?= $value['title']; ?></h4>
		  							<p class="card-text color-black"><?= $value['short_description']; ?>...<a href="<?= BASE_URL.'blogs/'.$value['page_url']; ?>">Read More</a></p>
								</div>
							</div>
						</div>
						<?php } } ?>		
  					</div>
				</div>
				<?php  $count++; } } ?>
			</div>
		</div>
	</div>
</section>
<?php
$this->title = 'Blogs';

?>
   <section class="n_video_section newsroom-sec2bottom ">
            <div class="container">
<div class="row justify-content-center">
  <div class="col-lg-12 col-md-12 col-sm-12">
	<div class="blog-container">	  
	  <div class="blog-tab">
		<div class="row">
		  <div class="col-md-3 col-lg-3 desktop-view col-sm-3">
			<div class="retelling-container-heading mb-4">
				<h2 class="section-title">Recent <?= !empty($blogsData[0]['categoryTitle']) ? $blogsData[0]['categoryTitle'] : ''; ?></h2>          
			</div>
			<div class="retelling-vertical-tab mt-4">
			<ul class="nav nav-pills mb-5" id="pills-tab" role="tablist">
<?php 
		if(!empty($blogsData)){
			$count = 1;
			foreach($blogsData as $value){
				$class = "";
				if($value['page_url'] == $page_url){
					$class = "active";
				}
?>				
				<li class="nav-item">
					<span><?= $value['publish_date']; ?></span> / <span><?= $value['team_name']; ?></span>
					<a class="nav-link retelling-visit-link <?= $class; ?> p-0 mb-4 mt-1" id="blog-tab<?= $count; ?>"  href="<?= $value['page_url']; ?>" role="tab" aria-controls="pills-home" aria-selected="true"><?= $value['title']; ?></a>
				</li>
<?php 
					$count++;
			}
		}
?>		
	
			</ul>
		</div>
		<span class="blog-pipe-small"></span>
		  </div>

		  <div class="col-md-12 col-lg-9 col-sm-12">
			<div class="tab-content cust-tab-visit pt-2 pl-1" id="pills-tabContent">
			
			
<?php 
		if(!empty($blogsData)){
			$count = 1;
			foreach($blogsData as $value){
				$class = "";
				if($value['page_url'] == $page_url){
					$class = "show active"; ?>	

			  <div class="tab-pane fade <?= $class; ?> blog-padd" id="<?= $value['page_url']; ?>" role="tabpanel" aria-labelledby="blog-tab<?= $count; ?>">
				  <div class="row mb-4">
					<div class="col-lg-12 col-md-12 col-sm-12 mb-5">
					<?php if(!empty($value['image'])){ ?>
						<img class="img-fluid" src="<?= BASE_BE_URL; ?>uploads/blogs/<?= $value['image']; ?>" alt="Card image cap">
					  <?php } ?>
					  <p class="date-retelling text-uppercase">
						<span><?= $value['publish_date']; ?></span> / <span><?= $value['team_name']; ?></span>
					  </p>
					  <h3 class="retelling-heading"><?= $value['title']; ?></h3>
					  <?= $value['description']; ?>
					</div>
			</div>
		</div> 		
			
<?php 
                }
                    $count++;
			}
		}
?>		
			  

			</div>
		  </div>
		</div>	  
	  </div>
	</div>
  </div>
</div>
</div>

</section>

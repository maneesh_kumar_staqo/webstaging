<?php
/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
AppAsset::register($this);

use frontend\models\Utility;
use frontend\models\Blogs;

$blog = new Blogs();
$utility = new Utility();
$page_url = "";
$page_name = ""; 
$pageUrlVal = str_replace("/", "", $_SERVER['REQUEST_URI']);
if(empty($pageUrlVal)){
	$pageUrlVal = 'index';
}
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";


$pageUrlVal= basename($actual_link);



$metaData = $utility->getPageMetaData($pageUrlVal);
// print_r($metaData);die;
if (strpos( $_SERVER['REQUEST_URI'], 'blogs/') !== false) {
    $page_name = "Blogs_Detail";
    $page_url = str_replace("blogs","",$pageUrlVal);
}

if($page_name == "Blogs_Detail"){
$blogsData = $blog->getBlogsData($page_url);


}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="keywords">
<?php 

    if(!empty($blogsData)){
    $count = 1;
    foreach($blogsData as $value){
	
	if($value['page_url'] == $page_url){
	$class = "show active"; ?>	
	<title><?= $value['title'].' - Hero Future Energies'; ?></title>
        <meta name="keywords" content="<?= $value['meta_keywords'];  ?>" />
	<meta name="description" content="<?= $value['meta_description'];  ?>" />			
   <?php 
     }
        $count++;
     }
     }
     else{
   ?>		

<title><?= !empty($metaData['meta_title']) ? $metaData['meta_title'] : Html::encode($this->title).' - Hero Future Energies'; ?></title>
	<meta name="keywords" content="<?= !empty($metaData['meta_keywords']) ? $metaData['meta_keywords'] : ''; ?> <?= $pageUrlVal ?>" />
	<meta name="description" content="<?= !empty($metaData['meta_description']) ? $metaData['meta_description'] : ''; ?>" />
<?php } ?>
	<?php $this->registerCsrfMetaTags() ?>

	
<?php
$this->beginBody();
include(dirname(__FILE__) . "/../common/css-url.php");
?> 

</head>
<body id="body" >
 <?php
$this->beginBody();
include(dirname(__FILE__) . "/../common/header.php");
?> 

<?= $content ?>
<?php $this->beginBody();
include(dirname(__FILE__) . "/../common/footer.php");
?>
<?php
$this->beginBody();
include(dirname(__FILE__) . "/../common/modal.php");
?>
<?php
$this->beginBody();
include(dirname(__FILE__) . "/../common/js-url.php");
?>
<!-- map api -->
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTzDMr5csTrmlNzj0B-KoRpRtgTIbCO_A&callback=initMap"
async defer></script>
<!-- map api ends -->



<script type="text/javascript">
$(document).ready(function() {
$('#show-hidden-menu').click(function() {
$('.hidden-menu').slideToggle("slow");
// Alternative animation for example
slideToggle("fast");
});
});
</script>



</body>
</html>
<?php $this->endPage() ?>

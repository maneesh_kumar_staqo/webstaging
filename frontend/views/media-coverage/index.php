<?php
$this->title = 'Newsroom';
use frontend\models\Newsroom;
$model = new Newsroom();
$q = '';
if(!empty($_GET['q'])){
	$q = $_GET['q'];
}
?>
      <?php if(!empty($banner)){ ?>
             <section class="mobile-hidden">
            <div class="n_home_slider">
               <?php foreach ($banner as $key => $value) { ?>
                     <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $value['image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= $value['title'] ?></h4>
                        <p class="text-right">
                           <?= $value['sub_title'] ?>
                        </p>
                     </div>
                  </div>
               </div>
              <?php  } ?>
            
            </div>
         </section>
         <?php } ?>

           <?php if(!empty($banner)){ ?>
             <section class="desktop-hidden">
            <div class="n_home_slider">
               <?php foreach ($banner as $key => $value) { ?>
                     <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $value['image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= $value['title'] ?></h4>
                        <p class="text-right">
                           <?= $value['sub_title'] ?>
                        </p>
                     </div>
                  </div>
               </div>
              <?php  } ?>
            
            </div>
         </section>
         <?php } ?>
        
<section class="n_video_section newsroom-sec2bottom ">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="newsroom-container">
					<div class="newsroom-container-heading mb-4">
						<h2 class="section-title">HFE NEWSROOM </h2>
			<!-- 			<div class="searhcnews"><input type="text " placeholder="Search here ....." name=""> <button
								type="submit" class="search_inline_btn"><i class="fa fa-search"
									aria-hidden="true"></i></button></div> -->
					</div>
					<div class="newsroom-tab">
						<?php 
						if(!empty($category)){
							$count = 1;
							foreach($category as $key => $cat){
								$class = "";
								$q = '';
								if($count == 1){
									$class = "show active";
								}
								if(!empty($_GET['q'])){
									$q = $_GET['q'];
								}
								$pageData = $model->getPageData($cat['id'], $q);
					?>
						<?php  $count++; } } ?>

						<div id="columns">
							<?php 
																if(!empty($pageData)){
																	foreach($pageData as $value){
															?>
							<figure>
								<div class="card newsrom-card">
									<?php if(!empty($value['image'])){ ?>
									<img class="card-img-top"
										src="<?= BASE_BE_URL; ?>uploads/newsroom/<?= $value['image']; ?>"
										alt="Card image cap">
									<?php } ?>
									<div class="card-body">
										<p>
											<?= $value['publish_date']; ?> /
											<?= $value['team_name']; ?>
										</p>
										<h4>
											<?= $value['title']; ?>
										</h4>
										<p class="card-text">
											<?= $value['short_description']; ?>...<a
												href="<?= BASE_URL; ?>media-coverage/<?= $value['page_url']; ?>">Read More</a>
										</p>
									</div>
								</div>
							</figure>
							<?php  } }else{
															echo "<div>No data found.</div>";
															} ?>
						</div>
					
					</div>
				</div>
			</div>
		</div>
</section>
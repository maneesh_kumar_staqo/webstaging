	<?php
$this->title = 'Search';
$q = '';
if(!empty($_GET['q'])){
	$q = $_GET['q'];
	
}
?>

<style type="text/css">
	#columns figure {
		transition: opacity .4s ease-in-out;
		box-shadow: none;
	}

	#columns {
		column-width: 320px;
		column-gap: 15px;
		width: 90%;
		max-width: 1100px;
		margin: 50px auto;
	}

	figure {
		background: #fefefe;
		/* border: 2px solid #fcfcfc; */
		box-shadow: 0 1px 2px rgba(34, 25, 25, 0.4);
		margin: 0 2px 15px;
		/* padding: 15px; */
		/* padding-bottom: 10px; */
		transition: opacity .4s ease-in-out;
		display: inline-block;
		column-break-inside: avoid;
	}

	#columns figure {
		width: 100% !important;
	}

	@media screen and (max-width: 750px) {
		#columns {
			column-gap: 0px;
		}

		#columns figure {
			width: 100%;
		}
	}

	.newsroom-sec1bottom {
		margin-bottom: 31px;
	}
</style>

<!-- <section class="newsroom-sec1bottom">
	<div class="row justify-content-center">
		<div class="col-lg-8 col-md-8 col-sm-8">
			<form action="<?= BASE_URL ?>search">
				<input type="text" placeholder="Search Here" class="blog-search" name="q" value="<?= $q; ?>">
				<button type="submit" class="blog-btn"><i class="fa fa-search"></i></button>
		
		</div>
	</div>
</section> -->

<section class="n_video_section">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="newsroom-container">
					<div class="newsroom-container-heading mb-4">
						<form action="<?= BASE_URL ?>search">
						<h2 class="section-title">Search Here </h2>
						<div class="searhcnews"><input type="text " placeholder="Search here ....." name="q"
								value="<?= $q; ?>"> <button type="submit" class="search_inline_btn"><i
									class="fa fa-search" aria-hidden="true"></i></button>
								
						</div>
							</form>
					</div>

					<div class="newsroom-tab">
						<div id="columns">
							<?php 
$pageArray = array();		
if(!empty($blogsData) || !empty($newsroomData) || !empty($awardsData) || !empty($boardData) || !empty($careerCampusData) || !empty($careerLearnData) || !empty($careerThoughtData) || !empty($careerWeMakeData) || !empty($caseStudyData) || !empty($contactUsData) || !empty($contentsData) || !empty($governanceData) || !empty($disclosureData) || !empty($focusData) || !empty($investorData)){
		if(!empty($blogsData)){
			foreach($blogsData as $value){
				if(!empty($value['meta_title'])){
?>
							<figure>
								<div class="card newsrom-card">
									<div class="card-body">
										<h4><a href="<?= BASE_URL; ?>blogs/<?= $value['page_url']; ?>">
												<?= $value['meta_title']; ?>
											</a></h4>
										<p class="card-text">
											<?= $value['meta_description']; ?>
										</p>
									</div>
								</div>
							</figure>
							<?php				
				}
			}
		}if(!empty($newsroomData)){
			foreach($newsroomData as $value){
				if(!empty($value['meta_title'])){
?>
							<figure>
								<div class="card newsrom-card">
									<div class="card-body">
										<h4><a href="<?= BASE_URL; ?>blogs/<?= $value['page_url']; ?>">
												<?= $value['meta_title']; ?>
											</a></h4>
										<p class="card-text">
											<?= $value['meta_description']; ?>
										</p>
									</div>
								</div>
							</figure>
							<?php 
				}
			}
		}if(!empty($awardsData)){
			foreach($awardsData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					 	</div> </figure>';
				}
			}
		}if(!empty($boardData)){
			foreach($boardData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					</div> </figure>';
				}
			}
		}if(!empty($careerCampusData)){
			foreach($careerCampusData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					</div> </figure>';
				}
			}
		}if(!empty($careerLearnData)){
			foreach($careerLearnData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					</div> </figure>';
				}
			}
		}if(!empty($careerThoughtData)){
			foreach($careerThoughtData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					</div> </figure>';
				}
			}
		}if(!empty($careerWeMakeData)){	
			foreach($careerWeMakeData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					</div> </figure>';
				}
			}
		}if(!empty($caseStudyData)){	
			foreach($caseStudyData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					</div> </figure>';
				}
			}
		}if(!empty($contactUsData)){	
			foreach($contactUsData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					</div> </figure>';
				}
			}
		}if(!empty($contentsData)){	
			foreach($contentsData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					</div> </figure>';
				}
			}
		}if(!empty($governanceData)){	
			foreach($governanceData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					</div> </figure>';
				}
			}
		}if(!empty($disclosureData)){	
			foreach($disclosureData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					</div> </figure>';
				}
			}
		}if(!empty($focusData)){	
			foreach($focusData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					</div> </figure>';
				}
			}
		}if(!empty($investorData)){	
			foreach($investorData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					</div> </figure>';
				}
			}
		}if(!empty($leadershipData)){	
			foreach($leadershipData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					</div> </figure>';
				}
			}
		}if(!empty($managementData)){	
			foreach($managementData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					</div> </figure>';
				}
			}
		}if(!empty($pageImagesData)){	
			foreach($pageImagesData as $value){
				if(!empty($value['meta_title']) && !in_array($value['page_url'], $pageArray)){
					$pageArray[] = $value['page_url'];
					echo '<figure> <div class="card newsrom-card"> <div class="card-body">
						<h4><a href="'.BASE_URL.$value['page_url'].'">'.$value['meta_title'].'</a></h4>
						<p class="card-text">'.$value['meta_description'].'</p>
					</div> </figure>';
				}
			}
		}
}else{
	echo "<div style='margin-top:80px;text-align:center;width:100%;font-size:20px;'>No data found.</div>";
}
?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

      <div class="about-us-page" id="wrapper">
         <?php if(!empty($banner)){ ?>
             <section class="mobile-hidden">
            <div class="n_home_slider">
               <?php foreach ($banner as $key => $value) { ?>
                     <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $value['image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= $value['title'] ?></h4>
                        <p class="text-right">
                           <?= $value['sub_title'] ?>
                        </p>
                     </div>
                  </div>
               </div>
              <?php  } ?>
            
            </div>
         </section>
         <?php } ?>

           <?php if(!empty($banner)){ ?>
             <section class="desktop-hidden">
            <div class="n_home_slider">
               <?php foreach ($banner as $key => $value) { ?>
                     <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $value['image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= $value['title'] ?></h4>
                        <p class="text-right">
                           <?= $value['sub_title'] ?>
                        </p>
                     </div>
                  </div>
               </div>
              <?php  } ?>
            
            </div>
         </section>
         <?php } ?>
        
         <section class="n_video_section blog-sec2bottom">
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                     <div class="blog-container">
                        <div class="blog-tab">
                           <div class="row">
                              <div class="col-md-12">
                                  <div class="retelling-container-heading mb-4">
                                    <h2 class="section-title text-uppercase">Media Release</h2>
                                 </div>
                              </div>
                              <div class="col-md-3 col-lg-3 m-hidden col-sm-3 col-push-3">
                                
                                 <div class="retelling-vertical-tab mt-4">
                                    <ul class="nav nav-pills mb-5" id="pills-tab" role="tablist"> 
                                 
                                      <?php 
                                 if(!empty($newsData)){
                                    $count = 1;
                                    foreach($newsData as $value){
                                       $class = "";
                                       if($value['page_url'] == $page_url){
                                          $class = "active";
                                       }
                           ?>				
                                       <li class="nav-item">
                                          <span><?= $value['publish_date']; ?></span> / <span><?= $value['team_name']; ?></span>
                                          <a class="nav-link retelling-visit-link <?= $class; ?> p-0 mb-4 mt-1" id="blog-tab<?= $count; ?>"  href="<?= BASE_URL ?>media-release/<?= $value['page_url']; ?>" role="tab" aria-controls="pills-home" aria-selected="true"><?= $value['title']; ?></a>
                                       </li>
                           <?php 
                                          $count++;
                                    }
                                 }
                           ?>
                                    </ul>
                                 </div>
                                 <span class="blog-pipe-small"></span>
                              </div>
                              <div class="col-md-9 col-lg-9 col-sm-9 col-push-3">
                                 <div class="tab-content cust-tab-visit pt-2 pl-1" id="pills-tabContent">
                                    <div class="tab-pane fade show active blog-padd" id="strategic-investment-of-us-150-million-funding-growth-portfolio-for-hero-future-energies" role="tabpanel" aria-labelledby="blog-tab3">
                                       <div class="row ">
                                          	<div class="col-lg-12 col-md-12 col-sm-12 mb-5">
					<?php if(!empty($value['image'])){ ?>
						<img class="img-fluid" src="<?= BASE_BE_URL; ?>uploads/newsroom/<?= $value['image']; ?>" alt="Card image cap">
					  <?php } ?>
					  <p class="date-retelling text-uppercase">
						<span><?= $value['publish_date']; ?></span> / <span><?= $value['team_name']; ?></span>
					  </p>
					  <h3 class="retelling-heading"><?= $value['title']; ?></h3>
					  <?= $value['description']; ?>
					</div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-3 col-lg-3 d-hidden col-sm-3 col-push-3">
                                 <h2 class="section-title text-uppercase">Latest Media Release</h2>
                                 <div class="retelling-vertical-tab mt-4">
                                    <ul class="nav nav-pills mb-5" id="pills-tab" role="tablist">
                                             <?php 
                                                                              if(!empty($newsData)){
                                                                                 $count = 1;
                                                                                 foreach($newsData as $value){
                                                                                    $class = "";
                                                                                    if($value['page_url'] == $page_url){
                                                                                       $class = "active";
                                                                                    }
                                                                        ?>
                                             <li class="nav-item">
                                                <span>
                                                   <?= $value['publish_date']; ?>
                                                </span> / <span>
                                                   <?= $value['team_name']; ?>
                                                </span>
                                                <a class="nav-link retelling-visit-link <?= $class; ?> p-0 mb-4 mt-1" id="blog-tab<?= $count; ?>"
                                                   href="<?= BASE_URL ?>media-release/<?= $value['page_url']; ?>" role="tab" aria-controls="pills-home"
                                                   aria-selected="true">
                                                   <?= $value['title']; ?>
                                                </a>
                                             </li>
                                             <?php 
                                                                                       $count++;
                                                                                 }
                                                                              }
                                                                        ?>
                                    </ul>
                                 </div>
                                 <span class="blog-pipe-small"></span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      
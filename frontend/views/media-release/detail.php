<?php    ?>
      <div class="about-us-page" id="wrapper">
         <section>
            <div class="n_home_slider">
               <div class="n_home_slider_item" style="background-image: url(images/contact-banner.jpg);">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-left">Media  <br> Release</h4>
                        <p class="text-left">
                           Lorem ipsum dolor sit amet,  consectetur adipisicing
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="n_video_section blog-sec2bottom">
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                     <div class="blog-container">
                        <div class="blog-tab">
                           <div class="row">
                              <div class="col-md-12">
                                  <div class="retelling-container-heading mb-4">
                                    <h2 class="section-title text-uppercase">Media Release</h2>
                                 </div>
                              </div>
                              <div class="col-md-3 col-lg-3 m-hidden col-sm-3 col-push-3">
                                
                                 <div class="retelling-vertical-tab mt-4">
                                    <ul class="nav nav-pills mb-5" id="pills-tab" role="tablist"> 
                                 
                                      <?php  
                                     
                                 if(!empty($newsData)){
                                    $count = 1;
                                    foreach($newsData as $value){
                                       $class = "";
                                    
                                       if($value['page_url'] == $page_url1){
                                          $class = "active";
                                       }
                           ?>				
                                       <li class="nav-item">
                                          <span><?= $value['publish_date']; ?></span> / <span><?= $value['team_name']; ?></span>
                                          <a class="nav-link retelling-visit-link <?= $class; ?> p-0 mb-4 mt-1" id="blog-tab<?= $count; ?>"  href="<?= BASE_URL ?>media-release/<?= $value['page_url']; ?>" role="tab" aria-controls="pills-home" aria-selected="true"><?= $value['title']; ?></a>
                                       </li>
                           <?php 
                                          $count++;
                                    }
                              
                                 }
                           ?>
                                    </ul>
                                 </div>
                                 <span class="blog-pipe-small"></span>
                              </div>
                              <div class="col-md-9 col-lg-9 col-sm-9 col-push-3">
                                 <div class="tab-content cust-tab-visit pt-2 pl-1" id="pills-tabContent">
                                    <?php
                                     foreach($newsData as $value){
                                        if($value['page_url'] == $page_url1){ ?>
                                          <div class="tab-pane fade show active blog-padd"
                                             id="strategic-investment-of-us-150-million-funding-growth-portfolio-for-hero-future-energies" role="tabpanel"
                                             aria-labelledby="blog-tab3">
                                             <div class="row ">
                                                <div class="col-lg-12 col-md-12 col-sm-12 mb-5">
                                                   <?php if(!empty($value['image'])){ ?>
                                                   <img class="img-fluid" src="<?= BASE_BE_URL; ?>uploads/newsroom/<?= $value['image']; ?>" alt="Card image cap">
                                                   <?php } ?>
                                                   <p class="date-retelling text-uppercase">
                                                      <span>
                                                         <?= $value['publish_date']; ?>
                                                      </span> / <span>
                                                         <?= $value['team_name']; ?>
                                                      </span>
                                                   </p>
                                                   <h3 class="retelling-heading">
                                                      <?= $value['title']; ?>
                                                   </h3>
                                                   <?= $value['description']; ?>
                                                </div>
                                             </div>
                                          </div>
                                       <?php }

                                     }
                                    ?>
                                    
                                 </div>
                              </div>
                              <div class="col-md-3 col-lg-3 d-hidden col-sm-3 col-push-3">
                                 <h2 class="section-title text-uppercase">Latest Media Release</h2>
                                 <div class="retelling-vertical-tab mt-4">
                                    <ul class="nav nav-pills mb-5" id="pills-tab" role="tablist">
                                       <li class="nav-item">
                                          <span>2021-03-19</span> / <span></span>
                                          <a class="nav-link retelling-visit-link  p-0 mb-4 mt-1" id="blog-tab1" href="" role="tab" aria-controls="pills-home" aria-selected="true">USD Green bond offering by Hero Future Energies was successfully oversubscribed by 8.5 times</a>
                                       </li>
                                       <li class="nav-item">
                                          <span>2021-01-05</span> / <span>HFE Team</span>
                                          <a class="nav-link retelling-visit-link  p-0 mb-4 mt-1" id="blog-tab2" href="" role="tab" aria-controls="pills-home" aria-selected="true">Hero Future Energies appoints Srivatsan Iyer as global CEO</a>
                                       </li>
                                       <li class="nav-item">
                                          <span>2019-11-11</span> / <span>HFE</span>
                                          <a class="nav-link retelling-visit-link active p-0 mb-4 mt-1" id="blog-tab3" href="" role="tab" aria-controls="pills-home" aria-selected="true">Strategic investment of US$150 million funding growth portfolio for Hero Future Energies</a>
                                       </li>
                                       <li class="nav-item">
                                          <span>2019-10-26</span> / <span>HFE Team</span>
                                          <a class="nav-link retelling-visit-link  p-0 mb-4 mt-1" id="blog-tab4" href="" role="tab" aria-controls="pills-home" aria-selected="true">Hero Future Energies appoints Chief Financial Officer</a>
                                       </li>
                                       <li class="nav-item">
                                          <span>2017-01-09</span> / <span>HFE Team</span>
                                          <a class="nav-link retelling-visit-link  p-0 mb-4 mt-1" id="blog-tab5" href="" role="tab" aria-controls="pills-home" aria-selected="true">Hero Future Energies raises fresh equity of USD 125 million to fund growth plans</a>
                                       </li>
                                       <li class="nav-item">
                                          <span>2016-10-21</span> / <span>HFE Team</span>
                                          <a class="nav-link retelling-visit-link  p-0 mb-4 mt-1" id="blog-tab6" href="" role="tab" aria-controls="pills-home" aria-selected="true">Premier educational institutes in Delhi gets solarised</a>
                                       </li>
                                       <li class="nav-item">
                                          <span>2016-08-31</span> / <span>HFE Team</span>
                                          <a class="nav-link retelling-visit-link  p-0 mb-4 mt-1" id="blog-tab7" href="" role="tab" aria-controls="pills-home" aria-selected="true">Hero Future Energies commissions 10 MW solar plant in Karnataka</a>
                                       </li>
                                       <li class="nav-item">
                                          <span>2016-02-04</span> / <span>Webmaster</span>
                                          <a class="nav-link retelling-visit-link  p-0 mb-4 mt-1" id="blog-tab8" href="" role="tab" aria-controls="pills-home" aria-selected="true">Hero Future Energies gives Indian RE market its first certified climate bond</a>
                                       </li>
                                    </ul>
                                 </div>
                                 <span class="blog-pipe-small"></span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      
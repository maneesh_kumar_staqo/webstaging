
<?php if(!empty($data)){ ?>
<section class="n_video_section theme_sec  mrb_0" id="our-value">
	<div class="container" style="display: block;">
		<div class="">
			<h2 class="section-title text-center"><?= !empty($data[0]['title']) ? $data[0]['title'] : ''; ?></h2>
		</div>
		<div class="bg_sec">
			<div class="row">

				<div class="col-md-12">
					<div class="sec_box_blue">
						<div class="property-item horizontal expertise-slide">
							<div class="thumb">
								<img src="<?= BASE_BE_URL ?>uploads/images/<?= $data[0]['image'] ?>"
									alt="">
							</div>
							<div class="content">
								<!-- <p class="post-type">Our Areas of Expertise</p> -->
								<h3 class="post-title"><?= !empty($data[0]['short_description']) ? $data[0]['short_description'] : ''; ?>
								</h3>
								<div class="excerpt">
									<?= $data[0]['description'] ?>

								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php } ?>
<section class="about-sec-7 desktop-view">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="mt-3 heading-title">
<h3 class="text-left mb-3 color-red wow fadeInLeft" style="text-transform: uppercase;">Corporate <br> Governance</h3>
<p class="wow fadeInLeft mb-0">
We at HFE acknowledge that with great power, comes great responsibility! HFE breathes into its core a well-defined corporate governance structure, the roles and responsibilities of which are exhaled into each of its constituents, thus ensuring balanced contribution by all constituents. We envision a mutually beneficial relationship by delivering congruency between the best interest of all our stakeholders and business functionality and are religiously dedicated towards that vision. As a result, we are able to attract the confidence and trust of our capital providers, both human and financial – a trust that is of utmost value to us and forms a primary building block of our success.
</p> 
<br>
<p class="wow fadeInLeft">
With immense pride we share that our employees are constantly motivated and empowered to contribute their best efforts towards the performance of our corporate entity by doing business with humanity in mind. We thrive to create an environment where our highly valuable workforce makes conscious and constant efforts for environment sustainability and overall community development.
</p>
</div>
</div>
</div>
</div>
</section>


<!-- mobile corporate governance -->

<section class="about-sec-7 mobile-view">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="mt-3 heading-title">
<h3 class="text-left mb-3 color-red wow fadeInLeft" style="text-transform: uppercase;">Corporate <br> Governance</h3>
<p class="wow fadeInLeft mb-0">
We at HFE acknowledge that with great power, comes great responsibility! HFE breathes into its core a well-defined corporate governance structure, the roles and responsibilities of which are exhaled into each of its constituents, thus ensuring balanced contribution by all constituents.<span id="corpdots">...</span>
<span id="corpmore">We envision a mutually beneficial relationship by delivering congruency between the best interest of all our stakeholders and business functionality and are religiously dedicated towards that vision. As a result, we are able to attract the confidence and trust of our capital providers, both human and financial – a trust that is of utmost value to us and forms a primary building block of our success.
<br>
 With immense pride we share that our employees are constantly motivated and empowered to contribute their best efforts towards the performance of our corporate entity by doing business with humanity in mind. We thrive to create an environment where our highly valuable workforce makes conscious and constant efforts for environment sustainability and overall community development. </span>
</p>  

<button type="button" class="mt-3 btnallred btn-outline-danger float-left ml-1 mb-4"onclick="corpdivtoggle()" id="corpmyBtn"><span>READ MORE &nbsp; ></span> </button>
</div>

</div>
</div>
</div>
</section>


<!-- mobile corporate governance -->
<?php if(!empty($data)){ 

	 ?>
<section class="n_video_section theme_sec mrb_40 " style="padding-bottom: 0px;">
	<div class="container">
		<div class="bg_sec sec_padding_n2">
			<div class="row">

				<div class="col-md-12">
					<?php foreach ($data as $key => $value) { ?>
					<div class="about_data_content <?php if($key==0){echo 'mrb_40'; } ?>" id="about-hero-group">
						<h2 class="section-title text-left">
							<?= $value['title'] ?>
						</h2>
						
						<?php if(!empty($value['description'])){ ?>

						<div>
							
							<?= $value['description'] ?>
						</div>
						

						<?php	} ?>
					</div>
					<?php	} ?>


				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>





<?php

if(!empty($visondata)){?>
<section class="about-us-area pb-0 n_video_section mrb_0 pdb_0 " id="leadership-vision">
	<div class="container">
		<div class="row align-items-center mrb_80">
			<div class="col-lg-12">
				<h2 class="section-title text-center">Leadership Vision</h2>
			</div>
		</div>
		<?php foreach ($visondata as $key => $value) {
			if($key % 2 == 0){ ?>
				<div class="row">
			<div class="col-lg-4">
				<div class="about-img-three">
					<img src="<?= BASE_BE_URL ?>uploads/leadership/<?= $value['image'] ?>" alt="Image">
				</div>
			</div>
			<div class="col-lg-8">
				<div class="about-content about-content-style-two style_2">
					<!-- <span>About us</span> -->
					<h3><?= $value['name'] ?></h3>
					<span><?= $value['designation'] ?></span>
					<p>
						<?= $value['description'] ?>
					</p>
					<span class="team_quotes"><i class="fa fa-quote-left" aria-hidden="true"></i> </span>
					<span class="team_quotes team_quotes_bottom"><i class="fa fa-quote-right" aria-hidden="true"></i>
					</span>
				</div>
			</div>
		</div>
			<?php }
			else{ ?>
				<div class="row">
					<div class="col-lg-4  d-hidden">
						<div class="about-img-three">
							<img src="<?= BASE_BE_URL ?>uploads/leadership/<?= $value['image'] ?>" alt="Image">
						</div>
					</div>
					<div class="col-lg-8 opposit_leadership">
						<div class="about-content about-content-style-two last_d">
							<!-- <span>About us</span> -->
							<h3><?= $value['name'] ?></h3>
					<span><?= $value['designation'] ?></span>
					<p>
						<?= $value['description'] ?>
					</p>
							<span class="team_quotes team_quotes_right_top"><i class="fa fa-quote-right" aria-hidden="true"></i> </span>
							<span class="team_quotes team_quotes_left_bottom"><i class="fa fa-quote-left" aria-hidden="true"></i> </span>
						</div>
					</div>
					<div class="col-lg-4 m-hidden">
						<div class="about-img-three">
							<img src="<?= BASE_BE_URL ?>uploads/leadership/<?= $value['image'] ?>" alt="Image">
						</div>
					</div>
				</div>
				
				<?php
			}
		} ?>
	</div>
</section>
<?php }
	

?>
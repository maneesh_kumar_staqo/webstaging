 <section class="csr-sec2BottomHeading desktop-hide" id="divscroll">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-3">
         <h3 class="wow fadeInLeft">Focus Areas</h3>
      </div>
      <div class="col-lg-9 col-md-9 col-sm-9">
        <div class="csr-gallery-sec-heading float-right">
          <button type="button" class="btn csr-tab-btn btn-pad-1 select-color sel1" id="csr-btn1">Water ATM</button>
          <button type="button" class="btn csr-tab-btn btn-pad-2 select-active sel2" id="csr-btn2">Asha Education Center</button>
          <button type="button" class="btn csr-tab-btn btn-pad-2 select-active2 sel3" id="csr-btn3">Water Check Dam</button>
          <button type="button" class="btn csr-tab-btn btn-pad-2 select-active3 sel4 border-right-none" id="csr-btn4">Ladakh Project</button>
        </div> 
      </div>
    </div>
  </section>

 
  <section class="csr-sec2bottom pr-5 pl-5 desktop-hide">
    
    <div class="row">
      <div class="col-lg-10 col-md-10 col-sm-10 sec2order">

        <div class="csr-future-left-img">
          <div class="slider slider-for csr-leftside-slide-change" id="csr-slide-img">
            <div>
              <img src="img/csr/csr-slide-bg.jpg" class="img-fluid">
            </div>
            <div>
              <img src="img/csr/csr-slide-bg2.jpg" class="img-fluid">
            </div>
            <div>
              <img src="img/csr/csr-slide-bg3.jpg" class="img-fluid">
            </div>
           <div>
              <img src="img/csr/csr-slide-bg4.jpg" class="img-fluid">
            </div>
          </div>
          <!-- background slider ends -->
          <!-- card slider start here -->
          <div class="csr-rightside-slide-change">
            <div class="slider slider-nav" id="csr-slide">
            <div class="card">
              <div class="card-body">
                <div class="card-body-content wow fadeInLeft">
                  <h4 class="h5">Pratapgarh,</h4>
                  <p class="h5">Rajasthan</p>
                  <hr class="hr-border"></hr>
                  <p class="h5">Operational</p>
                  <p class="h5">Beneficiary:</p>
                  <p class="h2" style="">60-70 Families</p>
                </div>                            
              </div>
            </div>
            <div class="card">
                <div class="card-body">
                  <div class="card-body-content wow fadeInRight">
                 <h4 class="h5">Devgarh,</h4>
                  <p class="h5">Rajasthan</p>
                  <hr class="hr-border"></hr>
                  <p class="h5">Operational</p>
                  <p class="h5">Beneficiary:</p>
                  <p class="h2">100 Families</p>
                </div>
                </div>
              </div>
            <div class="card">
                <div class="card-body">
                  <div class="card-body-content wow fadeInLeft">
                 <h4 class="h5">Alote,</h4>
                  <p class="h5">Madhya Pradesh</p>
                  <hr class="hr-border"></hr>
                  <p class="h5">Operational</p>
                  <p class="h5">Beneficiary:</p>
                  <p class="h2">110 Families</p>
                </div>
              </div>
              </div>
              <div class="card">
                <div class="card-body">
                  <div class="card-body-content wow fadeInRight">
                 <h4 class="h5">Alote,</h4>
                  <p class="h5">Madhya Pradesh</p>
                  <hr class="hr-border"></hr>
                  <p class="h5">Operational</p>
                  <p class="h5">Beneficiary:</p>
                  <p class="h2">110 Families</p>
                </div>
              </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </section>


  <!-- mobile view focus section -->

  <section class="csr-sec2BottomHeading mob-gallery-hide desktop-hide focus-mob" id="divscroll">
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-4 text-center">
         <h2 class="h1 mob-pad01">Focus Areas</h2>
          <select class="test" multiple="multiple" id="select">
            <option value="1" selected>Water ATM</option>
            <option value="2">Asha Education Center</option>
            <option value="3">Water Check Dam</option>
            <option value="3">Ladakh Project</option>
          </select>
      </div>   
    </div>
  </section>

 
  <!-- mobile gallery -->

  <section class="sec7CsrBottom-mob mob-gallery-hide">
    <div class="row">
          <div class="col-lg-9"> 
            <div class="gallery-sec-content pr-price d1">
            <!-- slider start -->
            <div class="owl-carousel owl-theme" id="focus-slider01">
              <div class="item">
                <div class="card">
                   <img src="img/csr/csr-slide-bg.jpg">
                    <div class="card-body">
                      <div class="">
                       <h4 class="h5">Pratapgarh, Rajasthan</h4>                       
                        <p class="h5">Operational Beneficiary:</p>                       
                        <p class="h4 csr-h4-font">60-70 Families</p>
                    </div>                            
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="card">
                   <img src="img/csr/csr-slide-bg2.jpg">
                    <div class="card-body">
                      <div class="">
                        <h4 class="h5">Devgarh, Rajasthan</h4>
                        <p class="h5">Operational Beneficiary:</p>
                        <p class="h4 csr-h4-font">100 Families</p>
                    </div>                            
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="card">
                   <img src="img/csr/csr-slide-bg3.jpg">
                    <div class="card-body">
                      <div class="">
                       <h4 class="h4">Alote, Madhya Pradesh</h4>
                        <p class="h4">Operational Beneficiary:</p>                     
                        <p class="h4 csr-h4-font">110 Families</p>
                    </div>                            
                  </div>
                </div>
              </div>
            </div>
            <!-- slider ends here -->
        
            </div> <!-- india tab ends -->
            </div> <!-- india tab ends -->
            </div> <!-- india tab ends -->

            <!-- Tab panels -->
            

        <!-- second selection -->

          <div class="row">
          <div class="col-lg-9"> 
            <div class="gallery-sec-content pr-price d2">
               
             <!-- slider start -->
            <div class="owl-carousel owl-theme" id="focus-slider02">
              <div class="item">
                <div class="card">
                   <img src="img/csr/csr-slide-bg.jpg">
                    <div class="card-body">
                      <div class="">
                       <h4 class="h5">Pratapgarh, Rajasthan</h4>                       
                        <p class="h5">Operational Beneficiary:</p>                       
                        <p class="h4 csr-h4-font">60-70 Families</p>
                    </div>                            
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="card">
                   <img src="img/csr/csr-slide-bg2.jpg">
                    <div class="card-body">
                      <div class="">
                        <h4 class="h5">Devgarh, Rajasthan</h4>
                        <p class="h5">Operational Beneficiary:</p>
                        <p class="h4 csr-h4-font">100 Families</p>
                    </div>                            
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="card">
                   <img src="img/csr/csr-slide-bg3.jpg">
                    <div class="card-body">
                      <div class="">
                       <h4 class="h4">Alote, Madhya Pradesh</h4>
                        <p class="h4">Operational Beneficiary:</p>                     
                        <p class="h4 csr-h4-font">110 Families</p>
                    </div>                            
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="card">
                   <img src="img/csr/csr-slide-bg4.jpg">
                    <div class="card-body">
                      <div class="">
                       <h4 class="h4">Alote, Madhya Pradesh</h4>
                        <p class="h4">Operational Beneficiary:</p>                     
                        <p class="h4 csr-h4-font">110 Families</p>
                    </div>                            
                  </div>
                </div>
              </div>
            </div>
            <!-- slider ends here -->
          <!-- tab 2 slider -->
          <!-- tab 2 slider ends here -->            
            </div>
            <!-- right side global-tab list 1 start-->
            <!-- right side list 1 ends -->
          </div> <!-- col-9 -->
        </div>  
        <!-- second seletion ends here -->

         <!-- third selection -->

          <div class="row">
          <div class="col-lg-9"> 
            <div class="gallery-sec-content pr-price d3">
            <!-- slider start -->
            <div class="owl-carousel owl-theme" id="focus-slider03">
              <div class="item">
                <div class="card">
                   <img src="img/csr/csr-slide-bg3.jpg">
                    <div class="card-body">
                      <div class="">
                       <h4 class="h4">Alote, Madhya Pradesh</h4>
                        <p class="h4">Operational Beneficiary:</p>                     
                        <p class="h4 csr-h4-font">110 Families</p>
                    </div>                            
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="card">
                   <img src="img/csr/csr-slide-bg2.jpg">
                    <div class="card-body">
                      <div class="">
                        <h4 class="h5">Devgarh, Rajasthan</h4>
                        <p class="h5">Operational Beneficiary:</p>
                        <p class="h4 csr-h4-font">100 Families</p>
                    </div>                            
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="card">
                   <img src="img/csr/csr-slide-bg.jpg">
                    <div class="card-body">
                      <div class="">
                       <h4 class="h5">Pratapgarh, Rajasthan</h4>                       
                        <p class="h5">Operational Beneficiary:</p>                       
                        <p class="h4 csr-h4-font">60-70 Families</p>
                    </div>                            
                  </div>
                </div>
              </div>
            </div>
            <!-- slider ends here --> 
          <!-- tab 2 slider -->          

          <!-- tab 2 slider ends here -->            
            </div>
            <!-- right side global-tab list 1 start-->
            <!-- right side list 1 ends -->
          </div> <!-- col-9 -->
        </div>  
        <!-- third seletion ends here -->

          <div class="row">
          <div class="col-lg-9"> 
            <div class="gallery-sec-content pr-price d4">
            <!-- slider start -->
            <div class="owl-carousel owl-theme" id="focus-slider04">
              <div class="item">
                <div class="card">
                   <img src="img/csr/csr-slide-bg4.jpg">
                    <div class="card-body">
                      <div class="">
                       <h4 class="h4">Alote, Madhya Pradesh</h4>
                        <p class="h4">Operational Beneficiary:</p>                     
                        <p class="h4 csr-h4-font">110 Families</p>
                    </div>                            
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="card">
                   <img src="img/csr/csr-slide-bg2.jpg">
                    <div class="card-body">
                      <div class="">
                        <h4 class="h5">Devgarh, Rajasthan</h4>
                        <p class="h5">Operational Beneficiary:</p>
                        <p class="h4 csr-h4-font">100 Families</p>
                    </div>                            
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="card">
                   <img src="img/csr/csr-slide-bg.jpg">
                    <div class="card-body">
                      <div class="">
                       <h4 class="h5">Pratapgarh, Rajasthan</h4>                       
                        <p class="h5">Operational Beneficiary:</p>                       
                        <p class="h4 csr-h4-font">60-70 Families</p>
                    </div>                            
                  </div>
                </div>
              </div>
            </div>
            <!-- slider ends here --> 
          <!-- tab 2 slider -->            

          <!-- tab 2 slider ends here -->
             
            </div>

            <!-- right side global-tab list 1 start-->

            <!-- right side list 1 ends -->

          </div> <!-- col-9 -->

        </div>  


         <!-- Tab panels -->
            <div class="row">
             <div class="col-lg-9"> 
            <!-- Categorty selection -->
            <div class="gallery-sec-content pr-price1 dd5">
              <div class="gallery cat wow fadeInRight">
                  <p>1 The company has created an asset base of ~ 1 GW of operational and under construction utility scale wind projects. In our journey from an Independent Power Producer in renewable energy to becoming a cleantech entity, we have invested extensively on the state-of-the-art central monitoring system which aces our performance management capabilities.  Our strong sense of design, pool of talented engineering professionals and adherence to HSE norms contribute majorly to this success.</p>
                  <p class="font-weight">State wise power capacity of projects (MW)</p>
                 <ul class="ulfont">
                        <li>Madhya Pradesh</li>
                        <li>Andhra Pradesh</li>
                        <li>Karnataka</li>
                        <li>Telangana </li>
                      </ul>  
              </div>
            </div>

            <!-- Categorty selection ends here-->
            
          </div>  <!-- col-9 ends-->
        </div>
  </section>  <!-- mobile gallery ends here -->

  <!-- mobile view focus section -->

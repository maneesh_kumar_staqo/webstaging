


<?php if(!empty($data)){ ?>
<section class="about-us-area pt-0 n_video_section mrb_0 " id="leadership-vision">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 ">
				<div class="the_team_sec">
					<h2 class="section-title text-center">Leadership</h2>
					<div class="other_management_team mrt_40" id="leadership">
						<!-- <h3 class="section-title text-center" style="font-size: 28px;">The Team</h3> -->
						<div class="">
							<div class="row other_manament_slider_n" id="">
								<?php foreach ($data as $key => $value) { ?>
									<div class="  member inviewport animated delay1" data-target="#myModalManagement<?= $key ?>" data-toggle="modal">
									<div class="pic">
										<img alt="member-image" class="img-responsive" src="<?= BASE_BE_URL; ?>uploads/leadership/<?= $value['image']; ?>">
										<div class="social">
											<?= $value['designation']; ?>
										</div>
									</div>
									<div class="info">
										<h3><?= $value['name']; ?></h3>
									</div>
								</div>
								<?php } ?>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>
<?php } ?>


<?php if(!empty($data)){ foreach ($data as $key => $value) {  ?>

<div class="modal centermodel" id="myModalManagement<?= $key ?>"
  style="background-color: rgba(255, 255, 255, 0.94); display: none;" aria-hidden="true">
  <div class="modal-dialog modal-fluid fluid-modal-width">
    <div class="modal-content">
      <div class="modal-body cust-modal-body">
        <button type="button" class="close float-right close-overflow" data-dismiss="modal"
          style="color:#fff">×</button>
        <h3 class="float-left text-uppercase"><?= $value['name']; ?></h3> <br> <br>
        <h5 class="float-left text-capitalize" style="line-height: 0; font-size: 1.1rem!important;"><?= $value['designation']; ?></h5>
        <hr>
        <div class="row">
          <div class="col-md-4">
            <img src="<?= BASE_BE_URL; ?>uploads/leadership/<?= $value['image']; ?>"
              class="img-fluid">
          </div>
          <div class="col-md-8">
            <div>
				<?= $value['description']; ?>
			</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } } ?>

<!-- Yxw2yp43 -->
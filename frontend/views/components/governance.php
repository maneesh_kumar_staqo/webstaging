<section class="about-sec-7 desktop-view">
	<div class="container">
		<div class="row">
		<?php 
		if(!empty($data)){
			foreach($data as $governance){
		?>
  			<div class="col-lg-12">
  				<div class="mt-3 heading-title">
					<h3 class="text-left mb-3 color-red wow fadeInLeft" style="text-transform: uppercase;"><?= $governance['title']; ?></h3>
					<?= $governance['description']; ?>
  				</div>
  			</div>
		<?php } } ?>	
		</div>
	</div>
</section>
<!-- mobile corporate governance -->
<section class="about-sec-7 mobile-view">
	<div class="container">
		<div class="row">
		<?php 
		if(!empty($data)){
			foreach($data as $governance){
		?>
		  	<div class="col-lg-12">
		  		<div class="mt-3 heading-title">
					<h3 class="text-left mb-3 color-red wow fadeInLeft text-uppercase"><?= $governance['title']; ?></h3>
					<div class="wow fadeInLeft mb-0">
						<?= substr($governance['description'], 0, 289); ?><span id="mobdots123"></span>
						<span id="mobmore123"><?= substr($governance['description'], 289, strlen($governance['description'])); ?></span>
					</div>
					<button type="button" class="mt-3 btnallred btn-outline-danger float-left ml-1 mb-4" onclick="mobdivtoggle1()" id="govBtn"><span>READ MORE</span> </button>
		  		</div>
		  	</div>
		<?php } } ?>	
		</div>
	</div>
</section>
<!-- mobile corporate governance -->
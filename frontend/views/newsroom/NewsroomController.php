<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use common\models\LoginForm;
use backend\models\Category;
use frontend\models\Newsroom;

/**
 * Newsroom controller
 */
class NewsroomController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                // 'actions' => [
                    // 'logout' => ['post'],
                // ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays all pages.
     *
     * @return mixed
     */
    public function actionIndex(){
		$model = new Newsroom();
		$category = Category::find()->where(['and', "type = 'news_room'"])->orderBy(['publish_date'=>SORT_ASC])->asArray()->all();
		return $this->render('index', ['category' => $category]);
    }
	
	public function actionDetails($page_url=''){
		$model = new Newsroom();
		$newsData = $model->getNewsroomData($page_url);
				
        return $this->render('details', ['newsData' => $newsData, 'page_url' => $page_url]);
    }
	
}

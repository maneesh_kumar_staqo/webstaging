<?php
$this->title = 'Newsroom';
use frontend\models\Newsroom;
$model = new Newsroom();
$q = '';
if(!empty($_GET['q'])){
	$q = $_GET['q'];
}
?>

<section class="n_video_section newsroom-sec2bottom   ">
	<div class="container">
	<div class="row justify-content-center">
  		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="newsroom-container">
	  			<div class="newsroom-container-heading mb-4">
					<h2 class="section-title">HFE NEWSROOM </h2> 
							 <div class="searhcnews">  
					<form action="<?= BASE_URL; ?>newsroom">
	  			<input type="text" placeholder="Search Here" class="blog-search" name="q" value="<?= $q; ?>">
	  			<button type="submit" class="blog-btn"><i class="fa fa-search"></i></button>
			</form>
		</div>
	  			</div>
	  			<div class="newsroom-tab">
					<ul class="nav nav-pills mb-5" id="pills-tab" role="tablist">
					<?php 
						if(!empty($category)){
							$count = 1;
							foreach($category as $cat){
								$class = '';
								if($count == 1){
									$class = 'active';
								}
								if(!empty($cat['link'])){
					?>			
						<li class="nav-item">
							<a class="nav-link contact-visit-link <?= $class; ?>" id="pills-home-tab" href="<?= $cat['link']; ?>"><?= $cat['title']; ?></a>
						</li>
						<?php }else{ ?>
						<li class="nav-item">
							<a class="nav-link contact-visit-link <?= $class; ?>" id="pills-home-tab" data-toggle="pill" href="#<?= str_replace(" ", "", $cat['title']); ?>" role="tab" aria-controls="pills-home" aria-selected="true"><?= $cat['title']; ?></a>
						</li>	
						<?php } if(count($category) != $count){ ?>
						<span class="video-pipe-small"></span>
					<?php } $count++; } } ?>		  
					</ul>
					<div class="tab-content cust-tab-visit pt-2 pl-1" id="pills-tabContent">
						<?php 
						// echo "<pre>";print_r($category);die;
							if(!empty($category)){
								$count = 1;
								foreach($category as $key => $cat){
									$class = "";
									$q = '';
									if($count == 1){
										$class = "show active";
									}
									if(!empty($_GET['q'])){
										$q = $_GET['q'];
									}
									$pageData = $model->getPageData($cat['id'], $q);
						?>					
						<div class="tab-pane fade <?= $class; ?>" id="<?= str_replace(" ", "", $cat['title']); ?>" role="tabpanel" aria-labelledby="pills-home-tab">
		  					<div id="<?php if($key==0){ echo 'columns'; } ?>">
								<?php 
									if(!empty($pageData)){
										foreach($pageData as $value){
								?>	
								<figure>
			  						<div class="card newsrom-card">
									  	<?php if(!empty($value['image'])){ ?>
											<img class="card-img-top" src="<?= BASE_BE_URL; ?>uploads/newsroom/<?= $value['image']; ?>" alt="Card image cap">
									  	<?php } ?>
										<div class="card-body">
					  						<p><?= $value['publish_date']; ?> / <?= $value['team_name']; ?></p>
					  						<h4><?= $value['title']; ?></h4>
					  						<p class="card-text"><?= $value['short_description']; ?>...<a href="<?= BASE_URL; ?>newsroom/<?= $value['page_url']; ?>">Read More</a></p>
										</div>
			  						</div>
								</figure>
								<?php  } }else{
								echo "<div>No data found.</div>";
								} ?>			
		  					</div>
						</div>
						<?php  $count++; } } ?>		
	  				</div>
	  			</div>
			</div>
  		</div>
	</div>
</section>
<section class="newsroom-sec2bottom mobile-view">
	<div class="row justify-content-center">
  		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="newsroom-container">
	  			<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-4 text-center">
						<h1>HFE NEWSROOM</h1>
						<br>
						<select class="test redirectpage_select" multiple="multiple" id="select">
						<?php 
							if(!empty($category)){
								$count = 1;
								foreach($category as $cat){
									$class = '';
									if($count == 1){
										$class = 'selected';
									}
									if(!empty($cat['link'])){
						?>			
										<option value="" linkVal="<?= $cat['link']; ?>" <?= $class; ?>><?= $cat['title']; ?></option>
						<?php  
									}else{
						?>
										<option value="<?= $count; ?>" <?= $class; ?>><?= $cat['title']; ?></option>
						<?php	
									}
						
						$count++; } } ?>
						</select>
					</div>
	  			</div>
	  			<br>	  
				<?php 
					if(!empty($category)){
						$count2 = 1;
						foreach($category as $cat){
							$q = '';
							if(!empty($_GET['q'])){
								$q = $_GET['q'];
							}
							if($count2 == 1){
								$class = "show active";
							}
							$pageData = $model->getPageData($cat['id'], $q);
							
				?>	  
	  			<div class="gallery-sec-content pr-price d<?= $count2; ?> <?= $class; ?>">
					<div class="row mb-4">
					<?php 
						if(!empty($pageData)){
							$count = 1;
							foreach($pageData as $value){
								$class = '';
								if($count%2 == 1){
									$class = 'news-content-card';
								}
					?>			
						<div class="col-lg-4 col-md-4 col-sm-4 mt-4">
			  				<div class="card newsrom-card <?= $class; ?>">
								<div class="card-body">
					  				<br>
					  				<p><?= $value['publish_date']; ?> / <?= $value['team_name']; ?></p>
					  				<h4><?= $value['title']; ?></h4>
					  				<p class="card-text"><?= $value['short_description']; ?>...<a href="<?= BASE_URL; ?>newsroom/<?= $value['page_url']; ?>">Read More</a></p>
								</div>
							</div>
						</div>
					<?php  $count++; } }else{
						echo "<div>No data found.</div>";
					} ?>			
		  			</div>  
	  			</div>
			<?php  $count2++; } } ?>		
	  		</div>
		</div>
  	</div>
  	</div>
</section>

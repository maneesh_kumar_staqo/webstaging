<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

/**
 * Newsroom model.
 */
class Newsroom extends \yii\db\ActiveRecord {
		
	// For get page data
	public function getPageData($category_id, $q){
		$connection = \Yii::$app->db;	
		if(!empty($q)){
			$q = "%".$q."%";
			$sql = "select * from hfe_news_room where status = 'Active' and category_id = :category_id and tags like :tags order by publish_date desc";
			$command = $connection->createCommand($sql);
			$command->bindParam(':category_id', $category_id);
			$command->bindParam(':tags', $q);
			$pageData = $command->queryAll();	
		}else{
			$sql = "select * from hfe_news_room where status = 'Active' and category_id = :category_id order by publish_date desc";
			$command = $connection->createCommand($sql);
			$command->bindParam(':category_id', $category_id);
			$pageData = $command->queryAll();	
		}
		return $pageData;
	}

	// For get newsroom details
	public function getNewsroomData($page_url){
		$connection = \Yii::$app->db;	
		$sql = "select category.id from hfe_news_room room join hfe_category category on (category.id = room.category_id) where room.status = 'Active' and page_url = :page_url and category.type = 'news_room'";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_url', $page_url);
		$data = $command->queryAll();
		$category_id = !empty($data[0]['id']) ? $data[0]['id'] : '';
		
		$sql = "select category.title as categoryTitle, room.* from hfe_news_room room join hfe_category category on (category.id = room.category_id) where room.status = 'Active' and category_id = :category_id";
		$command = $connection->createCommand($sql);
		$command->bindParam(':category_id', $category_id);
		$pageData = $command->queryAll();
		return $pageData;
	}	
	
}

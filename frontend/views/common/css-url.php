<head>
  <meta name="robots" content="noindex" />

  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="description">
  <meta content="" name="keywords">
  <link rel="icon" href="imageslogo.png" type="image/gif" >
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap" rel="stylesheet">



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


  <!-- <link href="css/all.css" rel="stylesheet"> -->
   <link href="<?= IMG_URL ?>css/bootstrap.css" rel="stylesheet">  
  <link href="<?= IMG_URL ?>css/mdb.min.css" rel="stylesheet"> 
  <link rel="stylesheet" href="<?= IMG_URL ?>css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?= IMG_URL ?>css/owl.theme.default.min.css">
  <link href="<?= IMG_URL ?>css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= IMG_URL ?>css/animate.css">
  <link rel="stylesheet" href="<?= IMG_URL ?>css/animate.min.css">
    <link rel="stylesheet" href="<?= IMG_URL ?>css/animate.min.css">
       <link rel="stylesheet" href="<?= IMG_URL ?>css/jquery-ui.css">
    <link href="<?= IMG_URL ?>css/form-style.css" rel="stylesheet">
  <link href="<?= IMG_URL ?>css/fSelect.css" rel="stylesheet">
  
   
       <link href="<?= IMG_URL ?>css/slick.css" rel="stylesheet">
    <link href="<?= IMG_URL ?>css/slick-theme.css" rel="stylesheet">
   <link href="<?= IMG_URL ?>css/aos.css" rel="stylesheet">
    <link href="<?= IMG_URL ?>css/lightbox.css" rel="stylesheet">
  
   <link href="<?= IMG_URL ?>css/latest-style.css" rel="stylesheet">
    <link href="<?= IMG_URL ?>css/responsive.css" rel="stylesheet">
</head>
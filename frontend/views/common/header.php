<?php 
use frontend\models\Utility;
$model=new Utility();
$mainmenu=$model->getPageHeaderFooter('Header');

?>
<header class="header">
   <div class="container">
      <div class="c-header-wrap">
         <div class="row">
            <div class="col-md-2 col-12">
               <div class="c-logo-wrap">
                  <div class="c-logo">
                     <a href="<?= BASE_URL ?>"><img src="<?= IMG_URL  ?>images/logo.jpg"></a>
                  </div>
                  <div class="float_header">
                     <a class="search_header_btn d-hidden" href="javascript:void(0);" data-toggle="modal"
                        data-target="#searchmodal">
                        <i class="fa fa-search" aria-hidden="true"></i>
                     </a>
                     <div class="d-hidden mobile_menu_btn " id="btn_menu">
                        <img src="<?= IMG_URL  ?>images/menu.png" class="openmenu_n1">
                        <img src="<?= IMG_URL  ?>images/close-menu.png" class="closemenu_n1" style="display: none;">
                     </div>
                  </div>

               </div>
            </div>
            <div class="col-md-10 col-12 position_intial " id="header-menu">
               <div class="c-nav-wrapper text-right" style="height: auto;">
                  <nav class="js-menu-init">
                     <ul id="parent_menu">
                        <?php 
                        if(!empty($mainmenu)){

                       
                        
                        foreach ($mainmenu as $key => $value) {
                              $submenu=$model->getSubPageHeader($value['id']);
                          
                           ?>
                                 <li class="has-sub-menu js-submenu ">
                                    <a class="desktop-menu main_p_menu" href="javascript:void(0);" data-tag="parentmenu<?= $key ?>">
                                       <?= $value['title'] ?>
                                    </a>
                                    <div class="dropdown_container" id="parentmenu<?= $key ?>">
                                       <div class="row">
                                          <div class="col-md-4">
                                             <div class="dropdown_menu_link">
                                                <div class="text-left">
                                                   <a href="<?= BASE_URL ?><?= $value['page_url'] ?>" style="padding:0px;">
                                                      <div class="dropdown_menu_heading">
                                                         <?= $value['title'] ?> <span><img src="<?= IMG_URL  ?>images/arrow-pointing-to-right-white.png"></span>
                                                      </div>
                                                   </a>
                                                  <!--  <button type="desktop-hidden mobile_menu_dropdown_close">
                                                       <img src="<?= IMG_URL  ?>images/close-menu.png">
                                                   </button> -->
                                                </div>
                                                <ul class="dropdown-list">
                                                <?php foreach ($submenu as $key1 => $value1) { ?>
                                                   <li class="dropchildli" data-tag="menu<?= $key.$key1 ?>">
                                                      <a href="<?= BASE_URL ?><?= $value['page_url'] ?>#<?= $value1['section_id']  ?>"><?= $value1['title'] ?> </a>
                                                   </li>
                                               <?php } ?>
                                                   
                                                   
                                                </ul>
                                             </div>
                                          </div>
                                          <div class="col-md-8">
                                             <!-- lopp data  -->
                                             <div class="text-left menu_container-sub">
                                                <div class="dropdown_menu_link_data">
                                                   <div class="dropdown_menu_content">
                                                      <?= $value['menu_description'] ?>
                                                   </div>
                                                   <div class="close_dropdown" data-tag="parentmenu<?= $key ?>"><i class="fa fa-times" aria-hidden="true"></i>
                                                   </div>
                                                </div>
                                                <!-- end loop data  -->
                                                   <?php foreach ($submenu as $key1 => $value1) { ?>
                                                <div class="dropdown_menu_data_dynamic <?php if($key1==0){ echo 'active'; } ?>" id="menu<?= $key.$key1 ?>">
                                                   <h4> <?= $value1['title'] ?> </h4>
                                                   <div class="dropdown_menu_data_dynamic_content">
                                                    <?= $value1['menu_description'] ?>
                                                   </div>
                                                </div>
                                             <?php } ?>
                                                   
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                       <?php }   } ?>
                        
                       
                        <li class="js-submenu   ">
                           <a href="javascript:void(0);" class="btn_enq_header_n1" data-toggle="modal"
                              data-target="#enquire_now">
                              Enquire Now
                           </a>
                        </li>
                        <li class=" js-submenu  m-hidden">
                           <a class="search_header_btn" href="javascript:void(0);" data-toggle="modal"
                              data-target="#searchmodal">
                              <i class="fa fa-search" aria-hidden="true"></i>
                           </a>
                        </li>
                     </ul>
                  </nav>
               </div>
            </div>
         </div>
      </div>
   </div>
</header>
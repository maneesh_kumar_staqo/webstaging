<div class="home_header2">
  
  <header id="header" class="fixed-top home_header ">
    <div class="container d-flex align-items-center">
      <h1 class="logo mr-auto"><a href="index.php"><img src="assets/img/logo.png"></a></h1>
      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <?php 

          $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
           
          ?>

          <li class="<?php if($url=="/template/home.php"){echo "active";}?>" ><a href="home.php">Home</a></li>
          <!-- drop-down -->
            <li class="drop-down "><a href="#">Notification</a>
              <div class="notification_center">
                    <h4 class="notification_center_heading">Recent Notification</h4>
                    <div class="notification_item">
                      <h4><a href="">You have recived a report from your schedule screening</a></h4>
                      <p>1 min ago</p>
                    </div>
                     <div class="notification_item">
                      <h4><a href="">You have recived a report from your schedule screening</a></h4>
                      <p>1 min ago</p>
                    </div>
                     <div class="notification_item">
                      <h4><a href="">You have recived a report from your schedule screening</a></h4>
                      <p>1 min ago</p>
                    </div>
                     <div class="notification_item">
                      <h4><a href="">You have recived a report from your schedule screening</a></h4>
                      <p>1 min ago</p>
                    </div>
                     <div class="notification_item">
                      <h4><a href="">You have recived a report from your schedule screening</a></h4>
                      <p>1 min ago</p>
                    </div>
              </div>
          </li>
          <li class="<?php if($url=="/template/report.php"){echo "active"; }?>"><a href="report.php" >Reports</a></li>
          <li><a href="profile.php">Profile</a></li>
           <li><a href="#">Blogs</a></li>
          <li><a href="#">Logout</a></li>
        
          
        </ul>
      </nav>
     
    </div>
  </header>
</div>
<!-- end header  -->
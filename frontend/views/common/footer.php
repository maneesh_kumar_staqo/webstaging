
<?php 
use frontend\models\Utility;
$model=new Utility();
$footermenu=$model->getPageHeaderFooter('Footer');
$newsroom=$model->getPageMenuNewsroom('Newsroom');
// echo "<pre>";print_r($mainmenu);die;
?>

<div class="social">
    <ul>
        <li class="insta"><a href="<?= BASE_URL ?>ci-solution#calc"><span>Calculator</span><img src="https://www.herofutureenergies.com/img/cal.gif"></a></li>
        <li class="facebook"><a href="https://www.facebook.com/herofutureenergies"><span>Facebook</span><i class="fa fa-facebook fa-facebook-f"></i></a></li>
        <li class="twitter"><a href="https://twitter.com/HeroFuture_HFE"><span>Twitter</span><i class="fa fa-twitter"></i></a></li>
        <li class="linkedin"><a href="https://www.linkedin.com/company/hero-future-energies"><span>Linkedin</span><i class="fa fa-linkedin fa-linkedin-in"></i></a></li>
        <!--<li class="insta"><a href="https://www.instagram.com/hero_futureenergies/"><span>Instagram</span><p><i class="fab fa-instagram"></i></p></a></li>-->
        
        <li class="youtube"><a href="https://www.youtube.com/channel/UC_i6ZrJLBLhmU1VDZq5wvTw"><span>Youtube</span><i class="fa fa-youtube"></i></a></li>
    </ul>
</div>
<footer class="footer footer-section ">
    <div class="container-fluid">
       <div class="row ml-0">
<div class="col-md-4 pt-5 bg" >
<div class="f-list mx-auto">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12">
<div class="list-conatiner ftleft wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
<ul>
<?php
if(!empty($footermenu)){
foreach ($footermenu as $key => $value) { ?>
    
<li><a href="<?=  BASE_URL ?><?= $value['page_url'] ?>"><?= $value['title'] ?></a></li>

<?php } } ?>


</ul>
<hr></div>
</div>
 <?php
if(!empty($newsroom)){ ?>
<div class="col-lg-12 col-md-12 col-sm-12">
<div class="list-conatiner wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
<ul class="mt-1 commonul">
<li><a href="#">HFE Newsroom</a></li>
</ul>
<ul class="mt-1 ftright" style="list-style-type: disc; padding-left: 4rem!important;">
   <?php
foreach ($newsroom as $key => $value) { ?>
<li><a href="<?= $value['page_url'] ?>"><?= $value['title'] ?></a></li>
<?php }  ?>
</ul>


</div>
</div>
<?php } ?>
</div>
<!-- mobile social
<div class="row">&nbsp;</div>--> <!-- mobile social ends -->
<div class="copy-right col-md-10 ml-4 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
<p>© 2021 Hero Future Energies | All Rights Reserved</p>
</div>
</div>
</div>
<div class="col-md-8 " style="position: relative; padding-left: 0px;">
<div class="footer-map-container"><img class="map-img" src="<?= IMG_URL ?>images/map.jpg" alt="">
<div class="mapdot-7 map-common backgroundRed7">
<div class="dotoneaddress-1 arrow_box"><span style="font-weight: bold;">London Office</span> <br>The Pavilion, Kensington Pavilion 96 Kensington High Street London W8 4SG</div>
</div>
<div class="mapdot-1 map-common backgroundRed1">
<div class="dotoneaddress-1 arrow_box"><span style="font-weight: bold;">Registered Office</span> <br>202, Third Floor, Okhla Industrial Estate, Phase – III, New Delhi – 110 020, India</div>
</div>
<div class="mapdot-6 map-common backgroundRed6">
<div class="dotoneaddress-1 arrow_box"><span style="font-weight: bold;">Corporate Office</span> <br>201, Third Floor,Okhla Industrial Estate, Phase – III, New Delhi – 110 020, India</div>
</div>
<div class="mapdot-2 map-common backgroundRed2">
<div class="dotoneaddress-1 arrow_box"><span style="font-weight: bold;">Bangalore Office</span> <br>Unit no 1002 &amp; 3, 10th floor, Prestige Meridian- Block – I, No. 29, M.G. Road, Bangalore 560001, India</div>
</div>
<div class="mapdot-3 map-common backgroundRed3">
<div class="dotoneaddress-1 arrow_box"><span style="font-weight: bold;">Chennai office</span> <br>No. 45/6, Shri Karthik Bajaj Flats, Ground Floor, B Block, Nerkundram Pathai, Chennai 600026, India</div>
</div>
<div class="mapdot-4 map-common backgroundRed4">
<div class="dotoneaddress-1 arrow_box"><span style="font-weight: bold;">Vietnam office</span> <br>613, 6F Me Linh Point Tower, 02 Ngo Duc Ke, District 1, Ho Chi Minh City, Vietnam</div>
</div>
<div class="mapdot-5 map-common backgroundRed5">
<div class="dotoneaddress-1 arrow_box"><span style="font-weight: bold;">Singapore office</span> <br>1, Fullerton Road, #02-01 One Fullerton, Singapore 049213</div>
</div>
</div>
</div>
</div>
    </div>
   </footer>


   <a href="#" class="back-to-top" ><i class="fa fa-angle-up" aria-hidden="true"></i></a>


   <div class="fixed-bottom MobFixed MobFixed-block desktop-hidden">
    <div class="container" style="display: block;">     
        <div class="row">
            <div class="col-6 col-6 MobLeft"><p><a href="<?= BASE_URL ?>ci-solution#calc">CALCULATOR</a></p></div>          
            <div class="col-6 col-6 MobRight"><p><a href="<?= BASE_URL ?>utility-scale-projects#project-commissioned">CASE STUDIES</a></p></div>
        </div>
    </div>
</div>
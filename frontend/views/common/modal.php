<div class="modal theme_modal fade-scale searchmodal " id="searchmodal">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- Modal body -->
         <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="searchmodal_container">
               <div class="searchmodal_container_heding">
                  <h2 class="section-title fadeInElement text-center"> Search Query</h2>
                  <form action="<?= BASE_URL; ?>search" onSubmit="return searchMainVal();">
                     <div class="searchmodal_container_item">
                        <span><i class="fa fa-search" aria-hidden="true"></i></span>
                        <input type="text" class="form-control" id="main_search"  name="q"  value="<?= !empty($_GET['q']) ? preg_replace('#[^a-z0-9_]#', '-', $_GET['q']) : ''; ?>"  placeholder="Search here ....."> <button
                           class="btn_theme" type="submit">Search</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal theme_modal fade-scale  enquire_now" id="enquire_now">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- Modal body -->
         <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
             <form action="<?= BASE_URL; ?>contactquery" method="post" id="headercontactform">
                <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
                value="<?=Yii::$app->request->csrfToken?>" />

              <div class="row">
               <div class="col-md-12">
                  <h2 class="section-title fadeInElement text-center">
                     FOR BUSINESS QUERIES 
                  </h2>
               </div>
               <div class="col-md-12">
                  <div class="form-group">
                     <select class="test validatesel form-control" id="enqtype" name="type">
                        <option value="" class="sel" selected>Select</option>
                        <option value="General Information">General Information</option>
                        <!-- <option value="HR">HR</option> -->
                        <option value="C&I Enquires">C&I Enquires</option>
                        <option value="International Business">International Business</option>
                     </select>
                  </div>
               </div>
               <div class="col-md-12">
                  <div class="form-group">
                     <input type="text" id="enqcname" name="cname" class="form-control"
                        placeholder=" Your Company Name">
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" id="enqemail_id" name="email_id" class="form-control"
                        placeholder=" Your Email Id">
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" id="enqphone" name="phone" class="form-control"
                        placeholder=" Your Contact No.">
                  </div>
               </div>
               <div class="col-md-12">
                  <div class="form-group">
                     <textarea  id="enqmessage" name="message" class="form-control"> Write Here 
                     </textarea>
                  </div>
               </div>
               <div class="col-md-12  text-center">
                   <div id="RecaptchaField1" class="g-recaptcha" style=" "></div>
               </div>

               <div class="col-md-12  ">
                  <button type="submit" class="btnallred btn btn-sm btn-theme-danger waves-effect waves-light"
                     tabindex="0" onclick="return contactQuery('headercontactform');"><span>Submit &nbsp; &gt;</span> </button>
               </div>
            </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- calculatior modal  -->
<div class="modal fade " id="modalcalc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
   aria-hidden="true">
   <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content mob_modal_scroll">
         <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <form action="">
               <div class="row" id="calcform">
                  <div class="col-lg-6 col-md-6 col-sm-6">
                     <h3>Solar PV Calculator</h3>
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                           <div class="form-group">
                              <label for="cal_name" class="">Name</label>
                              <input type="text" id="cal_name" name="cal_name" class="form-control">
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                           <div class="form-group">
                              <label for="cal_email" class="">Email</label>
                              <input type="text" id="cal_email" name="cal_email" class="form-control">
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="cal_phone" class="">Phone</label>
                        <input type="text" id="cal_phone" name="cal_phone" class="form-control" maxlength="10">
                     </div>
                     <div class=" form-group">
                        <label class="" for="customRadioInline1"> <input type="radio" id="customRadioInline1" name="customRadioInline1" class=""
                           value="rooftop" checked="checked">Rooftop Area</label>
                       
                     </div>
                     <div class="row" id="rooftopDiv">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                           <div class="form-group">
                              <label>Rooftop Area*</label>
                              <input type="text" id="rooftop_area" name="rooftop_area" class="form-control" placeholder="">
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                           <div class="form-group">
                              <label>Sq. Meter</label>
                              <select class="form-control">
                                 <option value="Sq. Meter">Sq. Meter</option>
                              </select>
                           </div>
                        </div>
                     </div>
                    <!--  <div class="form-group hideContents" id="capacityDiv">
                        <input type="text" id="cal_phone" name="cal_phone" class="form-control" placeholder="Capacity(in KW)*"
                           maxlength="10">
                     </div>
                     <div class="form-group hideContents" id="costDiv">
                        <input type="text" id="cal_phone" name="cal_phone" class="form-control" placeholder="Cost(INR)*"
                           maxlength="10">
                     </div> -->
                     <button type="button" class="btnallred btn btn-sm btn-theme-danger waves-effect waves-light"
                        onclick="return calculators();"><span>GO</span> </button>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6">
                     <h3>Results</h3>
                     <!--div class="md-form mb-0">
                        <select class="test" multiple="multiple" id="select">
                          <option value="1" selected>Polycrystalline</option>
                          <option value="2">Thinfilm</option>
                          <option value="3">Monocrystalline</option>
                        </select>
                        </div-->
                     <div class="form-group">
                        <label for="Area" class="fillValues">Area (sq mtr)</label>
                        <input type="text" id="Area" name="Area" class="form-control fillValuesInput" readonly>
                     </div>
                     <div class="form-group">
                        <label for="Capacity" class="fillValues">Capacity(kw)</label>
                        <input type="text" id="Capacity" name="Capacity" class="form-control fillValuesInput" readonly>
                     </div>
                     <div class="form-group">
                        <label for="System_Cost" class="fillValues">System Cost(INR)</label>
                        <input type="text" id="System_Cost" name="System_Cost" class="form-control fillValuesInput" readonly>
                     </div>
                     <div class="form-group">
                        <label for="Energy_Output" class="fillValues">Energy Output (Kwh/year)</label>
                        <input type="text" id="Energy_Output" name="Energy_Output" class="form-control fillValuesInput"
                           readonly>
                     </div>
                     <div class="form-group">
                        <label for="Co2" class="fillValues">Co2 emissions equivalent (Tonnes)</label>
                        <input type="text" id="Co2" name="Co2" class="form-control fillValuesInput" readonly>
                     </div>
                     <div class="form-group">
                        <label for="Saving" class="fillValues">Cost Saving (INR / year)</label>
                        <input type="text" id="Saving" name="Saving" class="form-control fillValuesInput" readonly>
                     </div>
                     <div class="form-group">
                        <label for="Payback" class="fillValues">Payback (year)</label>
                        <input type="text" id="Payback" name="Payback" class="form-control fillValuesInput" readonly>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="<?= IMG_URL ?>js/owl.carousel.min.js"></script>
<script src="<?= IMG_URL ?>js/jquery-ui.js"></script>
<script src="<?= IMG_URL ?>js/lightbox.js"></script>
<script src="<?= IMG_URL ?>js/magic-grid.cjs.js"></script>
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
   <script type="text/javascript">

    $(document).on("click", ".pagination li.page-item", function () {
        console.log(2);

        $('html, body').animate({
        scrollTop: $("#project-commissioned").offset().top
    });
    });





   // main site key-  6Lf1xL8bAAAAAIG1EDmcjdlnsNFbyCVwDK_XtJfy
   // staging site key - 6LdZE8kbAAAAACqVnpxnn0QXkrvDrnnOCHZUw-9F

    var CaptchaCallback = function() {
    if ( $('#RecaptchaField1').length ) {
        grecaptcha.render('RecaptchaField1', {'sitekey' : '6Le8UhYcAAAAAD8U6c1JmMpwfFalMeCSUAMUshiJ'
        });
    }
    if ( $('#RecaptchaField2').length ) {
       grecaptcha.render('RecaptchaField2', {'sitekey' : '6Le8UhYcAAAAAD8U6c1JmMpwfFalMeCSUAMUshiJ'
       });
    }

    if ( $('#leaderspeakcaptcha').length ) {
       grecaptcha.render('leaderspeakcaptcha', {'sitekey' : '6Le8UhYcAAAAAD8U6c1JmMpwfFalMeCSUAMUshiJ'
       });
    }
  
    // cotactcaptcha
};




</script>
<script>
    function focusareatab(id){
        $(".current_focus_hide").removeClass('current_focus');
        $("#t"+id).addClass('current_focus');
        $(".select-active").removeClass('select-color');
        $("#csr-btn").addClass('select-color');
        $('#csr-slide'+id).slick('refresh');
        $('#csr-slide-img'+id).slick('refresh');
       
    }
$(document).ready(function () {
    $("#testover").hide();
    $(".read_m_n1").on("click", function () {
        var buttonvalue= $(this).val();
        if(buttonvalue=="read-more"){
            $('#testover').show();
            $(this).val('read less');
             $(this).text('Read Less  >');
        }
        else{
            $(this).val('read-more');
             $(this).text('Read More  >');
               $('#testover').hide();
        }
        // var txt = $("#testover").is(':visible') ? 'Read More' : 'Read Less';
        // $(".read_m_n1").text(txt);
        
    });
});


function calculators(){
    var customRadioInline1 = $('input[type=radio][name=customRadioInline1]:checked').val();
    var csrf_frontend = $('#csrf_frontend').val();
    if($("#cal_name").val() == ""){
        alert("Please enter name.");
        $("#cal_name").focus();
        return false;
    }
    if($("#cal_email").val() == ""){
        alert("Please enter name.");
        $("#cal_email").focus();
        return false;
    }else {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!($("#cal_email").val()).match(mailformat)) {
            alert("You have entered an invalid email.");
            $("#cal_email").focus();
            return false;
        }
    }
    if($("#cal_phone").val() == ""){
        alert("Please enter phone number.");
        $("#cal_phone").focus();
        return false;
    }else if ($("#cal_phone").val().length > 10 || $("#cal_phone").val().length < 10) {
        alert('Please enter a valid phone number.');
        $('#cal_phone').focus();
        return false;
    } else {
        var letters = /^[0-9]+$/;
        if (!($("#cal_phone").val()).match(letters)) {
            alert('Please enter a valid phone number.');
            $("#cal_phone").focus();
            return false;
        }
    }
    if ($('#rooftop_area').val() == '') {
        alert('Please enter rooftop area in meters.');
        $('#rooftop_area').focus()
        return false;
    }
    
    $.ajax({
        type: 'get',
        url: BASE_URL + 'rooftop-calculator',
        data: 'name=' + $("#cal_name").val() + '&email=' + $("#cal_email").val() + '&phone=' + $("#cal_phone").val() + '&type=' + customRadioInline1 + '&_csrf-frontend=' + csrf_frontend + '&rooftop_area=' + $('#rooftop_area').val(),
        success: function (response){
            var result = response.split("@@");
            $(".fillValues").addClass("active");
            $(".fillValuesInput").prop('readonly', true);
            $("#Area").val(result[0]);
            $("#Capacity").val(result[1]);
            $("#System_Cost").val(result[2]);
            $("#Energy_Output").val(result[3]);
            $("#Co2").val(result[4]);
            $("#Saving").val(result[5]);
            $("#Payback").val(result[6]);
            
        }
    });
}



function rooftopFrm(){
    var capth = grecaptcha.getResponse();
        var captchlength = capth.length;
                if(captchlength==0){
                        alert("This capthca is required");
                return false;          
        }


    if($("#name").val() == ""){
        alert("Please enter name.");
        $("#name").focus();
        return false;
    }
    if($("#email").val() == ""){
        alert("Please enter name.");
        $("#email").focus();
        return false;
    }else {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!($("#email").val()).match(mailformat)) {
            alert("You have entered an invalid email.");
            $("#email").focus();
            return false;
        }
    }
    if($("#phone").val() == ""){
        alert("Please enter phone number.");
        $("#phone").focus();
        return false;
    }else if ($("#phone").val().length > 10 || $("#phone").val().length < 10) {
        alert('Please enter a valid phone number 0.');
        $('#phone').focus();
        return false;
    } else {
        var letters = /^[0-9]+$/;
        if (!($("#phone").val()).match(letters)) {
            alert('Please enter a valid phone number 1.');
            $("#phone").focus();
            return false;
        }
    }
    if($("#designation").val() == ""){
        alert("Please enter designation.");
        $("#designation").focus();
        return false;
    }
    if($("#company_name").val() == ""){
        alert("Please enter company name.");
        $("#company_name").focus();
        return false;
    }
    if($("#city").val() == ""){
        alert("Please enter city.");
        $("#city").focus();
        return false;
    }
    if($("#annual_turnover").val() == ""){
        alert("Please enter annual turnover.");
        $("#annual_turnover").focus();
        return false;
    }
    if($("#average_monthly_bill").val() == ""){
        alert("Please enter average monthly bill.");
        $("#average_monthly_bill").focus();
        return false;
    }
    if($("#message").val() == ""){
        alert("Please enter message.");
        $("#message").focus();
        return false;
    }
    if($("#monthly_electricity_consuption").val() == ""){
        alert("Please enter monthly electricity consuption.");
        $("#monthly_electricity_consuption").focus();
        return false;
    }
    if($("#approximate_space_available").val() == ""){
        alert("Please enter approximate space available.");
        $("#approximate_space_available").focus();
        return false;
    }
}




</script>
<script>
 var JS_BASE_URL = '<?= IMG_URL ?>';  
  var BASE_URL = '<?= BASE_URL ?>';  
  $("#btn_menu").click(function(){
    $(this).toggleClass('active');
   $("#header-menu").toggleClass('active');

});

</script>
<script>
let magicGrid = new MagicGrid({
  container: '#columns',
  animate: true,
  gutter: 10,
  static: true,
  useMin: true
});
magicGrid.listen();
</script>

<!-- n_btn_play -->
<script >
  $(document).ready(function(){
    
  })
  $(window).scroll(function() {

    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
    } else {
      $('.back-to-top').fadeOut('slow');
    }
  });

  $('.back-to-top').click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 1500, 'easeInOutExpo');
    return false;
  });
</script>
<script src="<?= IMG_URL ?>js/style.js"></script>
<script src="<?= IMG_URL ?>js/fSelect.js"></script>

<script src="<?= IMG_URL ?>js/slick.min.js"></script>
<script src="<?= IMG_URL ?>js/project-slider.js"></script>
<script src="<?= IMG_URL ?>js/aos.js"></script>
<script src="<?= IMG_URL ?>js/main.js"></script>


<script src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<script>
  AOS.init();
</script>

 <script>
  var $div2blink1 = $(".mapdot-1"); // Save reference, only look this item up once, then save
  var backgroundInterval = setInterval(function(){
    $div2blink1.toggleClass("backgroundRed1");
 },500);
 var $div2blink2 = $(".mapdot-2"); // Save reference, only look this item up once, then save
  var backgroundInterval = setInterval(function(){
    $div2blink2.toggleClass("backgroundRed2");
 },500);
 var $div2blink3 = $(".mapdot-3"); // Save reference, only look this item up once, then save
  var backgroundInterval = setInterval(function(){
    $div2blink3.toggleClass("backgroundRed3");
 },500);
 var $div2blink4 = $(".mapdot-4"); // Save reference, only look this item up once, then save
  var backgroundInterval = setInterval(function(){
    $div2blink4.toggleClass("backgroundRed4");
 },500);
 var $div2blink5 = $(".mapdot-5"); // Save reference, only look this item up once, then save
  var backgroundInterval = setInterval(function(){
  
    $div2blink5.toggleClass("backgroundRed5");
 },500);
  var $div2blink6 = $(".mapdot-6"); // Save reference, only look this item up once, then save
  var backgroundInterval = setInterval(function(){
    $div2blink6.toggleClass("backgroundRed6");
 },500);
var $div2blink7 = $(".mapdot-7"); // Save reference, only look this item up once, then save
var backgroundInterval = setInterval(function(){
$div2blink7.toggleClass("backgroundRed7");
},500);
</script>

<!-- for dropmenu  -->
<script >
  
$(document).ready(function(){
  $("#parent_menu li.has-sub-menu a").click(function(){
    $(this).parents('li').toggleClass('dropclassactie');

     var tagid = $(this).data('tag');
    $("#parent_menu li.has-sub-menu").removeClass('active');
    
    $(".dropdown_container").removeClass('active');
     $("#"+tagid).addClass('active');
     $("#parent_menu li.has-sub-menu a").removeClass('active');
    $(this).addClass('active');
    
  });
});


$(document).ready(function(){
  $(".dropchildli").hover(function(){
  var tagid = $(this).data('tag');
  $(".dropchildli").removeClass('active');
  $(this).addClass('active');
  $(".dropdown_menu_data_dynamic").removeClass('active');
  $("#"+tagid).addClass('active');
});

  $(".close_dropdown").click(function(){
   var tagid = $(this).data('tag');
   $(".has-sub-menu a").removeClass('active');
  $("#"+tagid).removeClass('active');
  });
});

function pauseVid() 
{ 

    var iframe = document.getElementById('iframeoff');
    iframe.src = iframe.src;
    player.stopVideo();
   
}





</script>

<script>  

$('.other_manament_slider_n').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: false,
  autoplaySpeed: 2000,
  speed:1300,
infinite: true,
  arrows:true,
   responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});


$('.slider_2_item').slick({
  slidesToShow: 2,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  dots:true,
  arrows:false

});




</script>

<script>
  
   $(window).resize(function(){
      if ($(window).width() <480) {
   $('#t1').removeClass('current_focus');
}
else {
   $('#t1').addClass('current_focus');
}
  });

$('.close-scroll').click(function(){
    $('#body').toggleClass('scroll-off');
  });

var elem = document.getElementById("fullscreen");
function openFullscreen() {
    elem.webkitRequestFullscreen();  
}

//search 

$(window).on('load', function() { // makes sure the whole site is loaded 

 $('#loading').fadeOut('slow');
   $('.container').css('display', 'block');
  
});




  $(".modal").click(function(){
 $("#popup").css("display", "none");
 $('body').removeClass('modalscroll');
  }); 

$('#top_banner').owlCarousel({
    animateOut: 'fadeOut',    
    loop:false,
    margin:10,
    nav:false,
    centerMode:true,
    dots:true, 
    autoplay:true, 
    autoplaySpeed: 1500,
    smartSpeed: 2000, 
    responsive:{
        0:{
            items:1,
        },
        600:{
            items:1
        },
        1000:{
            items:1,
            loop:true
        }
    }

});


$('#top_bannermobile').owlCarousel({
    animateOut: 'fadeOut',    
    loop:false,
    margin:10,
    nav:false,
    centerMode:true,
    dots:true, 
    autoplay:true, 
    autoplaySpeed: 1500,
    smartSpeed: 2000, 
    responsive:{
        0:{
            items:1,
        },
        600:{
            items:1
        },
        1000:{
            items:1,
            loop:true
        }
    }

});
</script>

<script >
    $(document).ready(function(){

$("#btn_menu").click(function(){
     ("#header-menu").animate({
    height: "auto"
  }, 1500 ); // how long the animation should be
});

});

$("#commissionedfilter").click(function () {

    var type= $("#insttype").val();
     var location= $("#location").val();


    $.ajax({
        type: 'get',
        url: BASE_URL+'filtercommsion',
        data: {type: type, location: location,},
        success: function (data) {

             $("#pagination_script").empty();
          
       
              var result = $.parseJSON(data);
     
         $("#filterdata").html(result.data);
          if(result.data !=""){

              var numberOfItems = $("#filterdata .col-md-4").length;
    var limitPerPage = 6;
    // Total pages rounded upwards
    var totalPages = Math.ceil(numberOfItems / limitPerPage);
    // Number of buttons at the top, not counting prev/next,
    // but including the dotted buttons.
    // Must be at least 5:
    var paginationSize = 7; 
    var currentPage;

    function showPage(whichPage) {

     


        if (whichPage < 1 || whichPage > totalPages) return false;
        currentPage = whichPage;
        $("#filterdata .col-md-4").hide()
            .slice((currentPage-1) * limitPerPage, 
                    currentPage * limitPerPage).show();
        // Replace the navigation items (not prev/next):            
        $(".pagination li").slice(1, -1).remove();
        getPageList(totalPages, currentPage, paginationSize).forEach( item => {
            $("<li>").addClass("page-item")
                     .addClass(item ? "current-page" : "disabled")
                     .toggleClass("active", item === currentPage).append(
                $("<a>").addClass("page-link").attr({
                    href: "javascript:void(0)"}).text(item || "...")
            ).insertBefore("#next-page");
        });
        // Disable prev/next when at first/last page:
        $("#previous-page").toggleClass("disabled", currentPage === 1);
        $("#next-page").toggleClass("disabled", currentPage === totalPages);
        return true;
    }

    // Include the prev/next buttons:
    $(".pagination").append(
        $("<li>").addClass("page-item").attr({ id: "previous-page" }).append(
            $("<a>").addClass("page-link").attr({
                href: "javascript:void(0)"}).text("Prev")
        ),
        $("<li>").addClass("page-item").attr({ id: "next-page" }).append(
            $("<a>").addClass("page-link").attr({
                href: "javascript:void(0)"}).text("Next")
        )
    );
    // Show the page links
    $("#filterdata").show();
    showPage(1);

    // Use event delegation, as these items are recreated later    
    $(document).on("click", ".pagination li.current-page:not(.active)", function () {
    
       
        return showPage(+$(this).text());
    });
    $("#next-page").on("click", function () {
 
        
        return showPage(currentPage+1);
    });

    $("#previous-page").on("click", function () {
        return showPage(currentPage-1);
    });

        }
        else{
          
            $(".pagination").empty();
        }
    }
    });
})


</script>

<div id="pagination_script">
    

<script>
  // pagination 
  function getPageList(totalPages, page, maxLength) {
    if (maxLength < 5) throw "maxLength must be at least 5";

    function range(start, end) {
        return Array.from(Array(end - start + 1), (_, i) => i + start); 
    }

    var sideWidth = maxLength < 9 ? 1 : 2;
    var leftWidth = (maxLength - sideWidth*2 - 3) >> 1;
    var rightWidth = (maxLength - sideWidth*2 - 2) >> 1;
    if (totalPages <= maxLength) {
        // no breaks in list
        return range(1, totalPages);
    }
    if (page <= maxLength - sideWidth - 1 - rightWidth) {
        // no break on left of page
        return range(1, maxLength - sideWidth - 1)
            .concat(0, range(totalPages - sideWidth + 1, totalPages));
    }
    if (page >= totalPages - sideWidth - 1 - rightWidth) {
        // no break on right of page
        return range(1, sideWidth)
            .concat(0, range(totalPages - sideWidth - 1 - rightWidth - leftWidth, totalPages));
    }
    // Breaks on both sides
    return range(1, sideWidth)
        .concat(0, range(page - leftWidth, page + rightWidth),
                0, range(totalPages - sideWidth + 1, totalPages));
}



    // var numberOfItems = $("#filterdata .col-md-4").length;
    // var limitPerPage = 6;
    // var totalPages = Math.ceil(numberOfItems / limitPerPage);
           
    // var paginationSize = 7; 
    // var currentPage;

    // function showPage(whichPage) {
    //        var numberOfItems = $("#filterdata .col-md-4").length;
    // var limitPerPage = 6;
    // var totalPages = Math.ceil(numberOfItems / limitPerPage);
         
    // var paginationSize = 7; 
    // var currentPage;

    //     if (whichPage < 1 || whichPage > totalPages) return false;

    //     currentPage = whichPage;

    //     $("#filterdata .col-md-4").hide()
    //         .slice((currentPage-1) * limitPerPage, 
    //                 currentPage * limitPerPage).show();           
    //     $(".pagination li").slice(1, -1).remove();
       
    //     getPageList(totalPages, currentPage, paginationSize).forEach( item => {
    //         $("<li>").addClass("page-item")
    //                  .addClass(item ? "current-page" : "disabled")
    //                  .toggleClass("active", item === currentPage).append(
    //             $("<a>").addClass("page-link").attr({
    //                 href: "javascript:void(0)"}).text(item || "...")
    //         ).insertBefore("#next-page");
    //     });
    //     $("#previous-page").toggleClass("disabled", currentPage === 1);
    //     $("#next-page").toggleClass("disabled", currentPage === totalPages);
    //     return true;
    // }
    // $(".pagination").append(
    //     $("<li>").addClass("page-item").attr({ id: "previous-page" }).append(
    //         $("<a>").addClass("page-link").attr({
    //             href: "javascript:void(0)"}).text("Prev")
    //     ),
    //     $("<li>").addClass("page-item").attr({ id: "next-page" }).append(
    //         $("<a>").addClass("page-link").attr({
    //             href: "javascript:void(0)"}).text("Next")
    //     )
    // );
    // $("#filterdata").show();
    // showPage(1);  
    // $(document).on("click", ".pagination li.current-page:not(.active)", function () {
    //      $('html, body').animate({
    //     scrollTop: $("#filterdata").offset().top
    // }, 2000);
    //     return showPage(+$(this).text());
    // });
    // $("#next-page").on("click", function () {
    //            alert(currentPage);
    //     return showPage(currentPage+1);

    //     $.scrollTo($('#project-commissioned'), 1000);

    // });

    // $("#previous-page").on("click", function () {
    //     return showPage(currentPage-1);
    //       $.scrollTo($('#project-commissioned'), 1000);
    // });


// Below is an example use of the above function.
$(function () {
    // Number of items and limits the number of items per page
    var numberOfItems = $("#filterdata .col-md-4").length;
    var limitPerPage = 6;
    // Total pages rounded upwards
    var totalPages = Math.ceil(numberOfItems / limitPerPage);
    // Number of buttons at the top, not counting prev/next,
    // but including the dotted buttons.
    // Must be at least 5:
    var paginationSize = 7; 
    var currentPage;

    function showPage(whichPage) {

     


        if (whichPage < 1 || whichPage > totalPages) return false;
        currentPage = whichPage;
        $("#filterdata .col-md-4").hide()
            .slice((currentPage-1) * limitPerPage, 
                    currentPage * limitPerPage).show();
        // Replace the navigation items (not prev/next):            
        $(".pagination li").slice(1, -1).remove();
        getPageList(totalPages, currentPage, paginationSize).forEach( item => {
            $("<li>").addClass("page-item")
                     .addClass(item ? "current-page ddd" : "disabled")
                     .toggleClass("active", item === currentPage).append(
                $("<a>").addClass("page-link").attr({
                    href: "javascript:void(0)"}).text(item || "...")
            ).insertBefore("#next-page");
        });
        // Disable prev/next when at first/last page:
        $("#previous-page").toggleClass("disabled", currentPage === 1);
        $("#next-page").toggleClass("disabled", currentPage === totalPages);
        return true;
    }

    // Include the prev/next buttons:
    $(".pagination").append(
        $("<li>").addClass("page-item").attr({ id: "previous-page" }).append(
            $("<a>").addClass("page-link").attr({
                href: "javascript:void(0)"}).text("Prev")
        ),
        $("<li>").addClass("page-item").attr({ id: "next-page" }).append(
            $("<a>").addClass("page-link").attr({
                href: "javascript:void(0)"}).text("Next")
        )
    );
    // Show the page links
    $("#filterdata").show();
    showPage(1);

    // Use event delegation, as these items are recreated later    
    $(document).on("click", ".pagination li.ddd:not(.active)", function () {
    
        return showPage(+$(this).text());
    });
    $("#next-page").on("click", function () {

       
        return showPage(currentPage+1);
    });

    $("#previous-page").on("click", function () {
         
        return showPage(currentPage-1);
    });
});



</script>
</div>



<script>

  if ($(window).width() <480) {
//     $('.mobile-hightlight').slick({
//   slidesToShow: 1,
//   slidesToScroll: 1,
//   autoplay: false,
//   autoplaySpeed: 2000,
//   dots:false,
//   arrows:true,
//   infinite: true
// });

    $('.mobile-hightlight').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  dots:true,
  arrows:false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
         dots: false,
         arrows:true
      }
    }
  ]

});
}
function contactQuery(formid){
      
    if(formid== "contactform"){
                var capth = grecaptcha.getResponse(1);
    }else{
        
        var capth = grecaptcha.getResponse();
    
    }

        var captchlength = capth.length;
                if(captchlength==0){
                        alert("This capthca is required");
                        return false;
                        
        }
   
    if($("#"+formid+ " #enqtype").val() == ""){
        alert("Please select type.");
        $("#"+formid+ " #enqtype").focus();
        return false;
    }
    if($("#"+formid+ " #enqcname").val() == ""){
        alert("Please enter your name.");
        $("#"+formid+ " #enqcname").focus();
        return false;
    }
    if($("#"+formid+ " #enqemail_id").val() == ""){
        alert("Please enter email id.");
        $("#"+formid+ " #enqemail_id").focus();
        return false;
    }else {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!($("#"+formid+ " #enqemail_id").val()).match(mailformat)) {
            alert("You have entered an invalid email.");
            $("#"+formid+ " #enqemail_id").focus();
            return false;
        }
    }
    if($("#"+formid+ " #enqmessage").val() == ""){
        alert("Please enter message.");
        $("#"+formid+ " #enqmessage").focus();
        return false;
    }
    var phoneNo = $("#"+formid+ " #enqphone").val();
    if (phoneNo == '') {
                $("#"+formid+ " #enqphone").focus();
                 alert("Please Entere  Phone No.");

                return false;
     }else if(phoneNo.value.length < 10 || phoneNo.value.length > 10){
                $("#"+formid+ " #enqphone").focus();
                alert("Please Entere Valid  Phone No.");
                 return false;
 }
    // if($("#captcha_code").val() == ""){
        // alert("Please enter captcha code.");
        // $("#captcha_code").focus();
        // return false;
    // }
}

// function refreshCaptcha(){
    // var id = Math.random();
    // $("#captchaVal").attr("src", JS_BASE_URL + 'captcha?id=' + id);  
// }
</script>


<script>
    function openpdf(id){
      $("#pdfmodal"+id).dialog();
    }

      // jQuery('#pdfmodal'+id).contents().find("#toolbarViewerRight").hide();


$(".page-link").click(function(){
     $('html, body').animate({
        scrollTop: $("#project-commissioned").offset().top
    },'slow');

})


</script>
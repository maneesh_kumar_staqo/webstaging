<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main">	    
	<div class="container">
		<div class="register-form-container register">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="text-center" style="margin-bottom: 5px;"><?= $this->title; ?></h3>
				</div>
				<div class="panel-body">
						<form action="#" method="post" accept-charset="utf-8" name="login_form" id="login_form" class="form-horizontal" enctype="multipart/form-data">
						<fieldset>
							<!-- Text input-->
							<div class="confirm-div"></div>
							<div class="form-group">
								<label class="col-md-4 control-label">E-Mail<span class="mandat">*</span></label>
								<div class="col-md-5 inputGroupContainer">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
										<input name="email" placeholder="E-Mail Address" class="form-control" type="text" id="login-email" autocomplete="off">
									</div>
								</div>
							</div>
							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-4 control-label">Password<span class="mandat">*</span></label>
								<div class="col-md-5 inputGroupContainer">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-key"></i></span>
										<input name="password" placeholder="Password" class="form-control" type="password" id="login-password" autocomplete="off">
									</div>
								</div>
							</div>
							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-4 control-label">Date of Birth<span class="mandat">*</span></label>
								<div class="col-md-5 inputGroupContainer">
									<div class="row">
										<div class="col-xs-4">
											<div class="input-group">
												<select class="form-control" name="regis-date" id="login-date">
													<option selected="selected" hidden value=""> Date</option>
													<option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option>
												</select>
											</div>
										</div>
										<div class="col-xs-4">
											<div class="input-group">
											   <select class="form-control" name="regis-month" id="login-month">
													<option selected="selected" hidden value="" >Month</option>
													<option value="01">Jan</option>
													<option value="02">Feb</option>
													<option value="03">Mar</option>
													<option value="04">Apr</option>
													<option value="05">May</option>
													<option value="06">Jun</option>
													<option value="07">Jul</option>
													<option value="08">Aug</option>
													<option value="09">Sep</option>
													<option value="10">Oct</option>
													<option value="11">Nov</option>
													<option value="12">Dec</option>
												</select>
											</div>
										</div>
										<div class="col-xs-4">
											<div class="input-group">
												<select class="form-control" name="regis-year" id="login-year">
													<option selected="selected" hidden value="">Year</option>
													<option value="1950">1950</option><option value="1951">1951</option><option value="1952">1952</option><option value="1953">1953</option><option value="1954">1954</option><option value="1955">1955</option><option value="1956">1956</option><option value="1957">1957</option><option value="1958">1958</option><option value="1959">1959</option><option value="1960">1960</option><option value="1961">1961</option><option value="1962">1962</option><option value="1963">1963</option><option value="1964">1964</option><option value="1965">1965</option><option value="1966">1966</option><option value="1967">1967</option><option value="1968">1968</option><option value="1969">1969</option><option value="1970">1970</option><option value="1971">1971</option><option value="1972">1972</option><option value="1973">1973</option><option value="1974">1974</option><option value="1975">1975</option><option value="1976">1976</option><option value="1977">1977</option><option value="1978">1978</option><option value="1979">1979</option><option value="1980">1980</option><option value="1981">1981</option><option value="1982">1982</option><option value="1983">1983</option><option value="1984">1984</option><option value="1985">1985</option><option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option>
												</select>
											</div>
										</div>												
									</div>
								</div>
							</div>
							<!-- Text input-->
							<div class="form-group">
							
							</div>
							<div class="text-center form-group ">
								<div class="col-md-12">
									<p>Fields marked with <span class="mandat">*</span> are mandatory </p>
								</div>
							</div>

							<!-- Select Basic -->
							<!-- Button -->
							<div class="form-group">
								<label class="col-md-4 control-label"></label>
								<div class="col-md-4">
							  <input type="submit" name="submit" value="Submit" id="login-submit" class="btn btn-primary btnClass">
								</div>
							</div>
							<div class="text-center form-group ">
								<div class="col-md-12">
									<p><a class="register-link" href="register-online.html" onClick="matEventTracker('signup','new','click');">Click here to Register</a></p>
									<p><a class="register-link" href="forgot-password.html" onClick="matEventTracker('password','forgot','click');">Forgot Password</a></p>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
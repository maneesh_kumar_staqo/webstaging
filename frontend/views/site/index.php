<?php
/* @var $this yii\web\View */
$this->title = 'Home Hero Future Energies';
?>

<body id="body">
   <div class="" id="wrapper">
      <?php
         // echo "<pre>";print_r($pageData);die;
      if(!empty($pageData['banners'])){  ?>
      <section class="mobile-hidden">
         <div class="n_home_slider">
            <?php foreach ($pageData['banners'] as $key => $value) { ?>
            <div class="n_home_slider_item"  style="background-image:url('<?= BASE_BE_URL  ?>uploads/banners/<?= $value['image'] ?>');">
              
               <div class="n_home_slider_item_data verticle_right_slider">
                  <div>
                     <h4>
                        <?= $value['title'] ?>
                     </h4>

                     <a href="<?= $value['image_link'] ?>">
                        <button type="button"
                           class="btnallred btn btn-sm btn-theme-danger waves-effect waves-light"><span>View More &nbsp;
                              &gt;</span> </button>
                     </a>
                  </div>
               </div>
            </div>
            <?php } ?>
         </div>
      </section>
      <?php  } ?>
       <?php
         // echo "<pre>";print_r($pageData);die;
      if(!empty($pageData['banners'])){  ?>
      <section class="desktop-hidden">
         <div class="n_home_slider">
            <?php foreach ($pageData['banners'] as $key => $value) { ?>
            <div class="n_home_slider_item"  style="background-image:url('<?= BASE_BE_URL  ?>uploads/banners/<?= $value['mobile_image'] ?>');">
              
               <div class="n_home_slider_item_data verticle_right_slider">
                  <div>
                     <h4>
                        <?= $value['title'] ?>
                     </h4>

                     <a href="<?= $value['image_link'] ?>">
                        <button type="button"
                           class="btnallred btn btn-sm btn-theme-danger waves-effect waves-light"><span>View More &nbsp;
                              &gt;</span> </button>
                     </a>
                  </div>
               </div>
            </div>
            <?php } ?>
         </div>
      </section>
      <?php  } ?>
      <?php
      if(!empty($statiData)){ ?>
      <section class="infoSection n_video_section   text-center">
         <h2 class="section-title fadeInElement" style="color: var(--heading); opacity:1;">HFE Statistics</h2>
         <div class="info-table">
            <div class="container">
               <div class="row mobile-hightlight" >
                  <?php
                        foreach ($statiData as $key => $value) {
                           if($key%2==0)  
                           {  ?>
                  <div class="col-sm-3 table-col-info">
                     <div class="img-wrapper fadeInElement" 
                        style="background-image: url(<?= BASE_BE_URL  ?>uploads/statics/<?=$value['image'] ?>); opacity: 1;  ">
                     </div>
                     <div class="text-wrapper changeColorElement" data-aos="fade-down">
                        <div class="text-wrapper-caption">
                           <div class="info-number">
                              <?=$value['title'] ?>
                           </div>
                           <div class="info-title">
                              <?=$value['description'] ?>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php }else{ ?>
                  <div class="col-sm-3 table-col-info ">
                     <div class=" sec_bg text-wrapper changeColorElement aos-init aos-animate" data-aos="fade-down"
                        style="background:var(--secbg)">
                        <div class="text-wrapper-caption">
                           <div class="info-number animate__animated animate__bounce">
                              <?=$value['title'] ?>
                           </div>
                           <div class="info-title">
                              <?=$value['description'] ?>
                           </div>
                        </div>
                     </div>
                     <div class="img-wrapper fadeInElement aos-init aos-animat" data-aos="fade-up"
                        style="background-image: url(<?= BASE_BE_URL  ?>uploads/statics/<?=$value['image'] ?>); opacity: 1; ">
                     </div>
                  </div>
                  <?php }
                           ?>
                  <?php }
                     ?>
               </div>
            </div>
         </div>
      </section>
      <?php } ?>

      <?php if(!empty($investerlogo)){ ?>


      <section class=" n_video_section bg_sec  padding-top-10 mrb_0 ">

         <section class=" mrb_60">
            <div class="container">
               <div class="">
                  <h2 class="section-title fadeInElement text-center ">Investor Relations</h2>
                  <div class="shareholders_logo_n2 sec_white">
                     <ul class="mobile-hidden" >
                        <li class="">
                           <div class="investor_relation_heading_nd1 shareholder_hds">Our Shareholders</div>
                        </li>
                        <?php
                  foreach ($investerlogo as $key => $value) { ?>
                        <li>
                           <img src="<?= BASE_BE_URL  ?>uploads/investor/<?= $value['image'] ?>">
                        </li>
                        <?php  }
                  
                  ?>


                     </ul>
                     <?php if(!empty($investerlogo)){ ?>
                        <div class="desktop-hidden text-center">
                           <div class="investor_relation_heading_nd1 shareholder_hds">Our Shareholders</div>
                            <div class="mobile-hightlight mobile_investor_logo_n1">
                              <?php foreach ($investerlogo as $key => $value) { ?>
                              <div class="mobile_investor_logo_n1_item">
                                 <img src="<?= BASE_BE_URL  ?>uploads/investor/<?= $value['image'] ?>">
                           </div>

                              <?php } ?>
                            </div>
                     </div>
                     <?php } ?>
                 
                  </div>
               </div>
            </div>
         </section>
         <?php   } ?>

         <div class="container">
            <div class="row">
               <?php  if(!empty($corporatevideo)){ ?>
                  <div class="text-center lp-home-banner-video col-md-5 col-sm-12 col-xs-12">
                  <h5 class="investor_relation_heading_nd1">HFE Corporate Video </h5>
                  <div class="lp-video-img-outer lp-video-img-outer-feature" onclick="openvideo('cor');">
                     <video class="video1" id="video1" loop="loop" poster="<?= BASE_BE_URL  ?>uploads/youtube/<?= $corporatevideo['video_thumb_image'] ?>"></video>
                     <span class="video__play__icon color-white">
                        <span class="video__play__icon__text">Play</span>
                     </span>
                  </div>


               </div>

               <?php }?>
               
               <?php if(!empty($hightlight)){ ?>
               <div class="text-center  lp-enterprenure-content col-md-7 col-sm-12 col-xs-12">
                  <div class="investor_relation_heading_nd1"><?= $hightlight[0]['heading'] ?></div>
                  <p>
                     <?= $hightlight[0]['description'] ?>
                  </p>

                  <div class="highlights_nd1">
                     <?php foreach ($hightlight as $key => $value) { ?>
                             <div class="slider_item_list_nd1 lp-enterprenure-content-facts">
                        <div class="highlights_card_nd1 lp-enterprenure-content-facts-inner">
                           <h4><?= $value['title'] ?></h4>
                        </div>
                     </div>
                    <?php  } ?>
                  </div>
               </div>
               <?php  } ?>
            </div>
         </div>

      </section>

<?php if(!empty($SocialAction)){ ?>
   <section class="n_video_section theme_sec mrb_0">
         <div class="container">
            <div class="row">

               <div class="col-md-6">
                  <div class="n_big_video_thubnail_data">
                     <h2 class="section-title fadeInElement n1_blog_heading text-center"><?= $SocialAction[0]['sec_heading'] ?></h2>
                        <div class="n_big_video_thubnail desktop-hidden">
                     <div class="owl-carousel owl-theme top_banner_owl_button" id="top_bannermobile">
                        <?php foreach ($SocialAction as $key => $value) { ?>
                            <div class="item">
                           <img src="<?= BASE_BE_URL  ?>uploads/social/<?= $value['image'] ?>">
                        </div>
                       <?php } ?>
                       
                     </div>
                  </div>

                     <p><?= $SocialAction[0]['description'] ?> </p>
                     <a href="<?= $SocialAction[0]['readmore_link'] ?>">
                        <button type="button"
                           class="btnallred btn btn-sm btn-theme-danger waves-effect waves-light"><span>Read More &nbsp;
                              &gt;</span> </button>
                     </a>
                  </div>
               </div>
               <div class="col-md-6 mobile-hidden">
                  <div class="n_big_video_thubnail">
                     <div class="owl-carousel owl-theme top_banner_owl_button" id="top_banner">
                        <?php foreach ($SocialAction as $key => $value) { ?>
                            <div class="item">
                           <img src="<?= BASE_BE_URL  ?>uploads/social/<?= $value['image'] ?>">
                        </div>
                       <?php } ?>
                       
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
<?php } ?>
      

      <section class="n_video_section bg_sec n_blog_other  mrb_0 ">
         <div class="container">
            <div class="row">
               <?php if(!empty($blogsData)){ ?>
                   <div class="col-lg-4 col-md-4 col-sm-12">
                  <div class="box_3_div">
                     <h3 class="investor_relation_heading_nd1">Blogs</h3>
                     <div class="sec7-left wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="card card-image cust-card blog_slider_n ">
                           <?php foreach ($blogsData as $key => $value) { ?>
                                   <div class="content">
                              <a href="<?php BASE_URL ?>blogs/<?= $value['page_url'] ?>">

                                 <img class="content-image" src="<?= BASE_BE_URL  ?>uploads/blogs/<?= $value['image'] ?>">
                                 <div class="content-details fadeIn-top">
                                    <p class=" blog-hover-text"><?= $value['title'] ?></p>
                                 </div>
                              </a>
                           </div>
                          <?php } ?>
                        </div>
                     </div>
                  </div>
               </div>
               <?php } ?>
              
               <div class="col-lg-4 col-md-4 col-sm-12">
                  <div class="box_3_div">
                     <h3 class="investor_relation_heading_nd1">Social Updates</h3>
                     <div class="change_article_big active n_all_box_4_1_img socialcard">
                        <a class="twitter-timeline" data-width="400" data-height="100%"
                           href="https://twitter.com/HeroFuture_HFE?ref_src=twsrc%5Etfw"></a>
                     </div>

                  </div>
               </div>
               <?php 
               if(!empty($vlogData)){ ?>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                  <div class="box_3_div">
                     <h3 class="investor_relation_heading_nd1">Vlogs</h3>
                     <div class="sec7-right-scroll wow fadeInRight  cart_three position_relative" id="style-3"
                        style="visibility: visible; animation-name: fadeInRight;">
                        <div class="vlog_slider">
                           <?php foreach ($vlogData as $key => $value) { ?>
                              
                                <div class="vlog_slider_item">
                              <div class="text-left " onclick="openvideo(<?= $value['id'] ?>)">
                                 <img src="<?= BASE_BE_URL  ?>uploads/youtube/<?= $value['video_thumb_image'] ?>" class="img-fluid small-video">
                                 <span class="video__play__icon color-white">
                                    <span class="video__play__icon__text">Play</span>
                                 </span>
                              </div>

                              <div class="card_details_bottom_ds"><p class=""><?= $value['title'] ?></p></div>
                           </div>
                           


                          <?php } ?>
                         
                         
                        </div>
                     </div>
                  </div>

               </div>
              <?php }
               ?>
               
            </div>
         </div>
      </section>
   </div>
   <?php
    if(!empty($vlogData)){ 
      foreach ($vlogData as $key => $value) { ?>
           <div id="video_overlay<?= $value['id'] ?>" class="video_overlay1" onclick="pauseVid()">
            <div class="video_play" id="video-section<?=  $value['id'] ?>">
            <button type="button" class="close" onclick="pauseyoutube(<?=  $value['id'] ?>)">×</button>
            <iframe width="853" height="480" id="youtube<?=  $value['id'] ?>" src="<?= $value['video_link'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            </div>

     <?php } }
   ?>


 <?php
      if(!empty($corporatevideo)) { ?>
           <div id="video_overlaycor" class="video_overlay1" onclick="pauseVid()">
            <div class="video_play" id="video-sectioncor">
            <button type="button" class="close" onclick="pauseyoutube('cor')">×</button>
            <iframe width="853" height="480" id="youtubecor" src="<?= $corporatevideo['video_link'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            </div>
            

     <?php } ?>
</body>
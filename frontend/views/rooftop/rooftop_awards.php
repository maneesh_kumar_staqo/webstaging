<section class="rts-sec6bottom desktop-view">
<div class="container">

<div class="row">
  <div class="col-md-12 col-lg-12 col-sm-12">  
	<h3 class="mb-3 color-red float-left wow fadeInDown">Awards & Certifications</h3>  
  </div>    
</div>   
<div class="row">
  <div class="col-md-12 col-lg-12 col-sm-12">
	<div class="rts-award">
	  <!-- <img src="<?= BASE_BE_URL; ?>uploads/awards/<?= !empty($data[0]['image']) ? $data[0]['image'] : ''; ?>" class="img-fluid"> -->
	  <div class="rts-cust-caption mob-cust-caption">
		  <div>
			<?= !empty($data[0]['description']) ? $data[0]['description'] : ''; ?>
		  </div>
	  </div>
	</div>
  </div>
</div>        
</div>
</section>

<!-- mobile view for award -->
<section class="rts-sec6bottom mobile-view">
<div class="container">

<div class="row">
  <div class="col-md-12 col-lg-12 col-sm-12">  
	<h3 class="mb-3 ml-3 color-red float-left wow fadeInDown">Awards & Certifications</h3>  
  </div>    
</div>   
<div class="row">
  <div class="col-md-12 col-lg-12 col-sm-12">
  </div>
</div>

<div class="row">
  <div class="col-md-12 col-lg-12 col-sm-12">
	<div class="rts-award">
	  <img src="<?= BASE_BE_URL; ?>uploads/awards/<?= !empty($data[0]['image']) ? $data[0]['image'] : ''; ?>" class="img-fluid"> 
	  <div class="rts-cust-caption mob-cust-caption">
		  <div>
			<?= !empty($data[0]['description']) ? $data[0]['description'] : ''; ?>
		  </div>
	  </div>
	</div>
  </div>
</div>
</div>
</section>
<!-- mobile view award ends -->
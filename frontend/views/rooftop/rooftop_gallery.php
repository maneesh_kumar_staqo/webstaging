<?php   ?>
<section class="rts-sec2bottom hfe-reg-vender-middle form-label n_video_section  mrb_0 n_video_section ">
    <div class="container">
        <div class="row rts-assessment-left">
            <div class="rts-assessment-bgcolor row">

                <div class="col-md-9 col-lg-9 col-sm-9 col-12 assessment-form" id="calc">
                    <div class="sec_padding_n2 bg_sec">
                        <div class="investor_relation_heading_nd1 left">Get your free Solar Assessment done now</div>
                        <p class="wow fadeInRight">Fill up the form to get your solar assessment or use the energy
                            savings calculator below.</p>
                        <div>

                            <button type="button" class="btnallred btn btn-sm btn-theme-danger waves-effect waves-light"
                                data-toggle="modal" data-target="#modalcalc"><span>ENERGY SAVING CALCULATOR &nbsp;
                                    ></span> </button>
                        </div>

                        <form action="<?= BASE_URL; ?>rooftop-solar-query"  method="post">
                          	<input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
								value="<?=Yii::$app->request->csrfToken?>" />

                            <div class="row">
                                <div class="col-sm-4 col-lg-4 col-md-4">
                                    <div class="form-group ">
                                        <label for="name" class="">Name</label>
                                        <input type="text" id="name" name="name" class="form-control" maxlength="50">

                                    </div>
                                    <div class="form-group ">
                                        <label for="designation" class="">Designation</label>
                                        <input type="text" id="designation" name="designation" class="form-control"
                                            maxlength="50">

                                    </div>
                                    <div class="form-group ">
                                        <label for="annual_turnover" class="">Annual Turnover</label>
                                        <input type="text" id="annual_turnover" name="annual_turnover"
                                            class="form-control" maxlength="20">

                                    </div>
                                    <div class="form-group ">
                                        <label for="monthly_electricity_consuption" class="">Monthly Electricity
                                            Consuption</label>
                                        <input type="text" id="monthly_electricity_consuption"
                                            name="monthly_electricity_consuption" class="form-control" maxlength="30">

                                    </div>
                                </div>
                                <div class="col-sm-4 col-lg-4 col-md-4">
                                    <div class="form-group ">
                                        <label for="email" class="">Email</label>
                                        <input type="text" id="email" name="email" class="form-control" maxlength="40">

                                    </div>
                                    <div class="form-group">
                                        <label for="company_name" class="">Company Name</label>
                                        <input type="text" id="company_name" name="company_name" class="form-control"
                                            maxlength="50">

                                    </div>
                                    <div class="form-group ">
                                        <label for="average_monthly_bill" class="">Average Monthly Bill</label>
                                        <input type="text" id="average_monthly_bill" name="average_monthly_bill"
                                            class="form-control" maxlength="20">

                                    </div>
                                    <div class="form-group ">
                                        <label for="approximate_space_available" class="">Approximate Space
                                            Available</label>
                                        <input type="text" id="approximate_space_available"
                                            name="approximate_space_available" class="form-control" maxlength="30">

                                    </div>
                                </div>
                                <div class="col-sm-4 col-lg-4 col-md-4">
                                    <div class="form-group ">
                                        <label for="Phone" class="">Phone</label>
                                        <input type="text" id="phone" name="phone" class="form-control" maxlength="10">

                                    </div>
                                    <div class="form-group ">
                                        <label for="city" class="">City</label>
                                        <input type="text" id="city" name="city" class="form-control" maxlength="30">

                                    </div>
                                    <div class="form-group ">
                                        <label for="message" class="">Message</label>
                                        <textarea id="message" name="message" rows="3" cols="50"
                                            class="form-control"></textarea>

                                    </div>

                                </div>
                                  <div class="col-sm-12 col-lg-12 col-md-12">
                                    <div id="RecaptchaField1" class="g-recaptcha" style=" "></div>
                                </div>
                                <div class="col-sm-12 col-lg-12 col-md-12">
                                    <button type="submit"
                                        class="btnallred btn btn-sm btn-theme-danger waves-effect waves-light"
                                        id="rooftop_form" onclick="return rooftopFrm();"><span>Submit &nbsp; ></span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 col-12">
                    <div class="cal_video">
                        <?php if(!empty($data[0]['video_link'])){ ?>

                     
                        <div class="investor_relation_heading_nd1 left">Video Gallery

                        </div>

                        <iframe width="100%" height="180" src="<?= $data[0]['video_link']; ?>"
                            title="YouTube video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                            <p class="ci_videod_gallery_btn"><a href="<?= BASE_URL ?>video-gallery">View More</a></p>
                            <?php     } ?>
                    </div>
                    <div class="cal_knowmore">
                        <div class="investor_relation_heading_nd1 left">Know More

                        </div>

                        <div>
                            <ul class="video-gallery lightgallery" >
                                <?php
                                    // echo "<pre>";print_r($data['knowmore']['rooftop_knowmore']);die;
                                if(!empty($data['knowmore'])){
                                    foreach ($data['knowmore']['rooftop_knowmore'] as $key => $value) { ?>
                                         <li>
                                           <!--  <?= BASE_BE_URL; ?>uploads/images/<?= !empty($value['image']) ? $value['image'] : ''; ?> -->
                                    <a href="javascript:void(0);" class=""
                                        data-exthumbnail="<?= BASE_BE_URL; ?>uploads/images/<?= !empty($value['image']) ? $value['image'] : ''; ?>#view=fitV " onClick="openpdf('<?= $key ?>')">
                                        <img src="<?= IMG_URL ?>images/photo.png"> <?= $value['title'] ?></a>
                                </li>
                                   <?php  }
                                    
                                } ?>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="width20 desktop-view">

            </div>
        </div>
    </div>
</section>

<style >
@-moz-document domain(herofutureenergies.com) {
   #outerContainer #mainContainer div.toolbar {
  display: none !important; /* hide PDF viewer toolbar */
}
#outerContainer #mainContainer #viewerContainer {
  top: 0 !important; /* move doc up into empty bar space */
}


}



</style>

<!-- pdf modal  -->
<?php 
   if(!empty($data['knowmore'])){
    foreach ($data['knowmore']['rooftop_knowmore'] as $key => $value) { ?>
<div id="pdfmodal<?= $key ?>" style="display:none" class="pdfmodal">
    <div>
    <iframe src="<?= BASE_BE_URL; ?>uploads/images/<?= !empty($value['image']) ? $value['image'] : ''; ?>#toolbar=0&navpanes=0"></iframe>
    </div>
</div> 


<?php  }  } ?>


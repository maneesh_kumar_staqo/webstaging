<?php 
if(!empty($data)){ ?>
	      <div class="about-us-page" id="wrapper">
         <section class="mobile-hidden">
            <div class="n_home_slider">
               <?php foreach ($data as $key => $value) { ?>
                    <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $value['image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= $value['title']; ?></h4>
                        <p class="text-right">
                          <?= $value['sub_title']; ?>
                        </p>
                     </div>
                  </div>
               </div>
               <?php } ?>
             
            </div>
         </section>
<?php }
?>


<?php 
if(!empty($data)){ ?>
         <div class="about-us-page" id="wrapper">
         <section class="desktop-hidden">
            <div class="n_home_slider">
               <?php foreach ($data as $key => $value) { ?>
                    <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $value['mobile_image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= $value['title']; ?></h4>
                        <p class="text-right">
                          <?= $value['sub_title']; ?>
                        </p>
                     </div>
                  </div>
               </div>
               <?php } ?>
             
            </div>
         </section>
<?php }
?>

<section class="rts-sec7bottom desktop-view">
<div class="container">
  <div class="row">
	<div class="col-md-12 col-lg-12 col-sm-12">  
	  <h3 class="mb-3 color-red float-left wow fadeInDown words">Words of <br> Appreciation</h3>  
	</div>    
  </div> 

  <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12">
	<div class="owl-carousel owl-theme about-owl-button" id="appreciation">
<?php 
	if(!empty($data)){
		foreach($data as $value){
?>
	  <div class="item">
		  <div class="appreciation-left">
			<h3 class="wow fadeInUp"><?= $value['title']; ?></h3>
			<?= $value['description']; ?>
			</div>
<!--
			<div class="appreciation-right-img">
			  <img src="<?= BASE_BE_URL; ?>uploads/images/<?= $value['image']; ?>" class="img-fluid">
			</div>
-->
		  </div>
<?php 
		}
	}
?>
	</div>
  </div>
</div>

</div>

</section>



<!-- mobile view -->

<section class="rts-sec7bottom mobile-view">
    <div class="container">
      <div class="row mt-5 mb-5">
        <div class="col-md-12 col-lg-12 col-sm-12">  
            <h3 class="mb-3 ml-5 color-red float-left wow fadeInDown words">Words of <br> Appreciation</h3>
        </div>    
      </div>
    </div>
  </section>

<section class="rts-sec7bottom mobile-view">
<div class="container">
  <div class="row">
	<!-- <div class="rts-appreciation-bgcolor"> -->
	  <div class="col-md-12 col-lg-12 col-sm-12">
		<div class="owl-carousel owl-theme" id="appreciation-mob-slider">
<?php 
	if(!empty($data)){
		foreach($data as $value){
?>	
		<div class="item">
			  <div>
			  		<div class="row mt-5">
<!--
						<div class="col-lg-12 col-md-12 col-sm-12">
						   <img src="img/rts/appreciation.jpg" class="img-fluid">
						</div>
-->
					</div>
				  	<h3 class="wow fadeInUp"><?= $value['title']; ?></h3>
				  	<?= $value['description']; ?>
			  </div>
		  </div>
<?php 
		}
	}
?>	
		</div>            
	  </div>
	</div>
</div>
</section>

<!-- mobile view -->

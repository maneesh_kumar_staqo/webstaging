<?php if(!empty($data)){ ?>
     <section class="n_video_section theme_sec mrb_0 mrt_0 pdt_0 " id="feature-product" style="padding-bottom: 0px;">
            <div class="container">
               <div class="bg_sec sec_padding_n2">
                  <div class="row">
                     <div class="col-md-12">
                        <h2 class="section-title text-left" style="padding: 0px 20px;">FEATURED PROJECTS  </h2>
                     </div>
                     <div class="col-md-12">
                        <div class="c_and_i_feature_product_list ">
                            <?php foreach ($data as $key => $value) { ?>
                                <div class="c_and_i_feature_product_item">
                              <div class="card_white">
                                 <img src="<?= BASE_BE_URL; ?>uploads/project/<?= $value['image']; ?>">
                                 <div class="c_and_i_feature_product_item_content">
                                    <h4><?= $value['title'] ?></h4>
                                    <div>
                                        <?= $value['description'] ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                          <?php  } ?>
                          
                          
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      
      </section>
    <?php } ?>
<?php
$this->title = 'Contact Us';
use frontend\models\Rooftop ;
use frontend\models\Utility;
$model = new Rooftop();
$utility = new Utility();
$formHeading = $utility->getPageTitle('ci-solution');
?>
<?php
// echo "<pre>";print_r($data);die;

if(!empty($data)){ ?>
	
  <section class="n_video_section bg_sec  mrb_0 contact-sec2bottom mobile_change_query_details " id="for-business-queries">
         <div class="container">
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12">
	 <div class="visit-heading">
                     <h2 class="section-title text-left">For Business Queries</h2>
                  </div>
	<div class="contact-visit">
	  <ul class="nav nav-pills mb-5" id="pills-tab" role="tablist">
<?php 
		if(!empty($data)){
			$count = 1;
			foreach($data as $key => $cat){
				$class = '';
				if($count == 1){
					$class = 'active';
				}
?>	  
			<li class="nav-item">
			  <a class="nav-link contact-visit-link <?= $class;  ?>" id="pills-home-tab<?= $count; ?>" data-toggle="pill" href="#tabid<?= $key?>" role="tab" aria-controls="pills-home" aria-selected="true"><?= $cat['location']; ?></a>
			</li>
			<span class="video-pipe-small"></span>
<?php
			$count++;
		}
	}
?>		
	  </ul>
	  <div class="tab-content cust-tab-visit pt-2 pl-1" id="pills-tabContent">
<?php 
		if(!empty($data)){
			$count = 1;
			foreach($data as $key => $cat){
				$class = '';
				if($count == 1){
					$class = 'show active';
				}
?>		  
	<div class="tab-pane fade visit visit-border-bottom <?= $class ?>" id="tabid<?= $key?>" role="tabpanel"
		aria-labelledby="pills-home-tab">
		<div class="row ">
			<div class="col-lg-3 col-md-3 col-sm-3">
				<img src="<?= BASE_BE_URL; ?>uploads/contact/<?= $cat['image']; ?>"
					class="img-fluid address-img">
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9">
				<div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="visit-pra">
	
								
								<p> Email: <a
										href="mailto:<?= $cat['email']; ?>"><?= $cat['email']; ?></a>
								</p>
							</div>
							
						</div>
						<div class="border-bottom mobile-view"></div>
						<div class="border-bottom"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
				$count++;
			}
		}
?>
		  
	  </div>
	</div>
  </div>
</div>
	</div>
</section>


<?php } ?>
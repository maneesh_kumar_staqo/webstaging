<?php 
if(!empty($data)){
	foreach($data as $value){
?>		
<section class="rts-sec8bottom tracking-sec">
<div class="row">
  <div class="col-lg-3 col-md-3 col-sm-3 mobile-view">
	  <div class="tracking-heading">
		<h3 class="text-left color-red wow fadeInDown"><?= !empty($value['title']) ? $value['title'] : ''; ?></h3>
	  </div>          
  </div>
  <div class="col-lg-9 col-md-9 col-sm-9 orderchange">
	  <div class="tracking-sec-content">
		<div class="tracking">
			<div class="wow fadeInRight">
				<?= !empty($value['description']) ? $value['description'] : ''; ?>
			</div>
		</div>
	  </div>
	  
  </div>
  <div class="col-lg-3 col-md-3 col-sm-3 desktop-view">
	  <div class="tracking-heading mt-4">
		<h3 class="text-left color-red wow fadeInDown"><?= !empty($value['title']) ? $value['title'] : ''; ?></h3>
	  </div>	  
  </div>
</div>
</section>
<?php 
	}
}
?>
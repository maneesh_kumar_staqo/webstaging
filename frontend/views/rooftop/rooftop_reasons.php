

<?php 
if(!empty($data)){ ?>
<section class="n_video_section bg_sec mrb_0 rts-sec10bottom " id="why-us">
         <div class="container">
            <div class="row">
				 <div class="col-lg-4 col-md-4 col-sm-4 desktop-hidden">
                  <div class="reasons-heading">
                     <h2 class="section-title text-center">Why  Partner with  HFE 
                     </h2>
                     <!--   <p>The name stands for trust, quality and timeliness</p>    -->
                  </div>
               </div>
               <div class="col-lg-8 col-md-8 col-sm-8">
			<div class="row divcustom2 mobile-hightlight">
				<?php 
							if(!empty($data)){
								$count = 1;
								foreach($data as $value){
									$class = '';
									if($count == 1){
										$class = 'lefttop current mh-275';
									}else if($count == 2){
										$class = 'mr-bottom70';
									}else if($count == 3){
										$class = 'lefttop mh-275';
									}else if($count == 4){
										$class = 'mh342';
									}else if($count == 5){
										$class = 'neg32 mh-250';
									}
									else if($count == 7){
										$class = 'mx-auto neg90';
									}
						?>
				<div class="col-container-4 <?= $class; ?>">
					<div class="reason-img colaction">
						<div class="reasonhead">
							<img src="<?= BASE_BE_URL; ?>uploads/reasons/<?= $value['icon']; ?>">
							<div class="reasonhead-right mb-5">
								<p>
									<?= $value['title']; ?>
								</p>
							</div>
							<?= $value['description']; ?>
						</div>
					</div>
				</div>
				<?php $count++; } } ?>
			</div>
               </div>
               <div class="col-lg-4 col-md-4 col-sm-4 mobile-hidden">
                  <div class="reasons-heading">
                     <h2 class="section-title text-left">Why <br> Partner <br> with <br> HFE <br> 
                     </h2>
                     <!--   <p>The name stands for trust, quality and timeliness</p>    -->
                  </div>
               </div>
		
            </div>
         </div>
      </section>
<?php }

?>
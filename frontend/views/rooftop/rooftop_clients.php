<?php if(!empty($data)){ 
  ?>
 <section class="n_video_section " id="our-client">
         <div class="container">
            <div class="row row_align">
               <div class="col-md-4">
                  <div class="">
                     <div class="">
                        <h2 class="section-title text-center mrb_0" style="padding: 0px 20px;">From  Our  Clients</h2>
                     </div>
                  </div>
               </div>
               <div class="col-md-8">
                  <div class="testimoniial_sllider_container_n2">
                     <div class="testimonial_slider_btn_n2">
                        <ul>
                           <li  id="pastnext">
                              <i class="fa fa-angle-left" aria-hidden="true"></i>
                           </li>
                           <li id="pastprevious">
                              <i class="fa fa-angle-right" aria-hidden="true"></i>
                           </li>
                        </ul>
                     </div>
                     <div class="testimoniial_sllider_n2_list">
                       <?php foreach ($data as $key => $value) { ?>
                          <div class="testimoniial_sllider_n2_item">
                           <div class="row align_center">
                              <div class="col-md-8 col-12">
                                 <div class="testimoniial_sllider_n2_card">
                                    <div class="Client_speak_detail_n1">
                                       <p>
                                          <b><?= $value['title'] ?></b>
                                       </p>
                                       <p class="color_grey">
                                         <?= $value['short_description'] ?>
                                       </p>
                                    </div>
                                    <!-- <h4></h4> -->
                                    <div class="Client_speak_detail_dsc_n1">
                                       <p>
                                          <?= $value['description'] ?>
                                       </p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 col-12 mar_l_minus">
                                 <div class="testimoniial_sllider_n2_image">
                                    <img src="<?= BASE_BE_URL; ?>uploads/images/<?= $value['image']; ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                      <?php  } ?>
                        
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
<?php } ?>
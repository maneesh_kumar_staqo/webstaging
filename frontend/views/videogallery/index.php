<?php
$this->title = 'Video Gallery';
use frontend\models\Videogallery;
$model = new Videogallery();
// echo "<pre>";print_r($bannerData);die;
?>
<style type="text/css">
  .vid-container .ytp-button{
  display: none!important;
}
</style>
<!-- mobile view section 1 start -->

<!-- mobile view section 1 ends -->

<?php if(!empty($bannerData)){ ?>
	   <section class="mobile-hidden">
         <div class="n_home_slider">
         	<?php foreach ($bannerData as $key => $value) { ?>
         		 <div class="n_home_slider_item" style="background-image: url('<?= !empty($value['image']) ? BASE_BE_URL.'uploads/banners/'.$value['image'] : ''; ?>');">
               <div class="n_home_slider_item_data verticle_center_slider">
                  <div>
                     <h4 class="text-right"><?= $value['title']; ?></h4>
                     <p class="text-right">
                       <?= $value['sub_title']; ?>
                     </p>
                  </div>
               </div>
            </div>
         <?php	} ?>
           
         </div>
      </section>
<?php } ?>



<?php if(!empty($bannerData)){ ?>
	   <section class="desktop-hidden">
          <div class="n_home_slider">
         	<?php foreach ($bannerData as $key => $value) { ?>
         		 <div class="n_home_slider_item" style="background-image: url('<?= !empty($value['mobile_image']) ? BASE_BE_URL.'uploads/banners/'.$value['mobile_image'] : ''; ?>');">
               <div class="n_home_slider_item_data verticle_center_slider">
                  <div>
                     <h4 class="text-right"><?= $value['title']; ?></h4>
                     <p class="text-right">
                       <?= $value['sub_title']; ?>
                     </p>
                  </div>
               </div>
            </div>
         <?php	} ?>
           
         </div>
      </section>
<?php } ?>

    <section class="n_video_section theme_sec mrb_40 " >
         <div class="container">
	<div class="row">      
  		<div class="col-lg-12 col-md-12 col-sm-12" id="video-banner-play">
			<ul class="nav nav-pills mb-5 justify-content-center" id="pills-tab" role="tablist">
			<?php if(!empty($category)){
				$count = 1;
				foreach($category as $cat){
					$class = '';
					if($count == 1){
						$class = 'active';
					} ?>
				<li class="nav-item">
					<a class="nav-link video-block-link <?= $class; ?>" id="pills-home-tab" data-toggle="pill" href="#<?= str_replace(" ", "",$cat); ?>" role="tab" aria-controls="pills-home" aria-selected="true"><?= $cat; ?> </a> 
				</li>
				<span class="video-pipe-small"></span>
			<?php  $count++; } } ?>	
			</ul>
			<div class="tab-content cust-tab-visit pt-2 pl-1" id="pills-tabContent">
			<?php  if(!empty($category)){
				$count = 1;
				foreach($category as $key => $cat){
					$class = '';
					if($count == 1){
						$class = 'show active';
					}
					$pageData = $model->getPageData($key);	?>	 
				<div class="tab-pane fade <?= $class; ?>" id="<?= str_replace(" ", "",$cat); ?>" role="tabpanel" aria-labelledby="pills-home-tab">
		  			<div class="row">
						<div class="col-lg-8 col-md-8 col-sm-8 video-big">
			  				<div class="vid-container">
								<iframe id="vid_frame_<?= $count; ?>" class="iframe" src="<?= !empty($pageData[0]['video_link']) ? $pageData[0]['video_link'] : ''; ?>" frameborder="0" width="200" height="440"></iframe>
			  				</div>
			  				<span class="video-pipe-big"></span>
			  				<p id="video_title_<?= $count; ?>"><?= !empty($pageData[0]['title']) ? $pageData[0]['title'] : ''; ?></p>
			  			</div>
			  			<div class="col-lg-4 col-md-4 col-sm-4">
			  				<div class="video-scroll-block">
								<div class="vid-list-container">
				  					<ul class="ol" id="vid-list">
									<?php 
										if(!empty($pageData)){
											foreach($pageData as $value){				  
											$title = str_replace("'", "", $value['title']);
									?>	
										<li>
					  						<a href="javascript:void(0);" onClick='changeVideo("<?= $count; ?>", "<?= $value['video_link']; ?>", "<?= $title; ?>");'>
												<span class="vid-thumb"><img width=100 src="<?= BASE_BE_URL.'uploads/youtube/'.$value['video_thumb_image']; ?>" /></span>
												<div class="desc"><?= $value['title']; ?></div>
					  						</a>
										</li>
									<?php } } ?>
									</ul>
								</div>
			  				</div>
						</div>
		  			</div>
				</div>
			<?php  $count++; } } ?>
			</div>
		</div>
  	</div>
											</div>
</section>



<section class="sec7CsrBottom-mob mob-gallery-hide">
	<?php  if(!empty($category)){
		$count = 1;
		foreach($category as $key => $cat){
			$class = '';
			if($count == 1){
				$class = 'show active';
			}
			$pageData = $model->getPageData($key);	?>	
	<div class="row">
	  <div class="col-lg-12 col-12"> 
		<div class="gallery-sec-content pr-price d<?= $count; ?>">
			<div class="row">
			  <div class="col-sm-12 col-lg-12 col-md-12 col-12">
				<div class="vid-container">
				  <iframe id="mobile_vid_frame_<?= $count; ?>" class="iframe" src="<?= !empty($pageData[0]['video_link']) ? $pageData[0]['video_link'] : ''; ?>" frameborder="0" width="100%" height="200" style="width:100%!important;"></iframe>				  
				</div>
			  </div>
			</div>
			<div class="border-bottom"></div>
			<div class="row">
			  <div class="col-sm-12 col-lg-12 col-md-12 col-12">
				<div class="vid-list-container">
				  <ul class="ol" id="vid-list">
					<?php 
						if(!empty($pageData)){
							foreach($pageData as $value){				  
					?>	
						<li>
	  						<a href="javascript:void();" onClick="document.getElementById('mobile_vid_frame_<?= $count; ?>').src='<?= $value['video_link']; ?>'">
								<span class="vid-thumb"><img width=100 src="<?= BASE_BE_URL.'uploads/youtube/'.$value['video_thumb_image']; ?>" /></span>
								<div class="desc"><?= $value['title']; ?></div>
	  						</a>
						</li>
					<?php } } ?>					                                             
				  </ul>
				</div>
			  </div>
			</div>    
		</div>
	  </div> 
	</div> 
<?php  $count++; } } ?>
</section>

<script>
function changeVideo(idVal, link, title){

	$("#vid_frame_" + idVal).attr('src', link);  
	$("#video_title_" + idVal).text(title);
}
</script>
<?php
/* @var $this yii\web\View */
$this->title = 'Home Hero Future Energies';
if(!empty($pageData['page_priority_data'])){
	foreach($pageData['page_priority_data'] as $priority){
		$data = '';
		$page_category_data = '';
		if(!empty($pageData[$priority['page_type']])){
			$data = $pageData[$priority['page_type']];
		}
		if(!empty($pageData['page_category_data'])){
			$page_category_data = $pageData['page_category_data'];
		}
		if(file_exists(dirname(__FILE__) .'/'.$priority['page_type'].'.php')){
			echo $this->render($priority['page_type'], ['data' => $data, 'category' => $page_category_data]);	
		}
	}
}
?>
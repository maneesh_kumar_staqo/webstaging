<section class="csr-sec2bottom pr-5 pl-5 desktop-hide">
<div class="row">
  <div class="col-lg-10 col-md-10 col-sm-10 sec2order">

	<div class="csr-future-left-img">
	  <div class="slider slider-for csr-leftside-slide-change csr-slide-img" id="csr-slide-img">
<?php 
	if(!empty($data)){
		foreach($data as $focus){
?>			
		<div><img src="<?= BASE_BE_URL; ?>uploads/focusareas/<?= $focus['image']; ?>" class="img-fluid"></div>
<?php 
		}
	}
?>
	  </div>
	  <!-- background slider ends -->
	  <!-- card slider start here -->
	  <div class="csr-rightside-slide-change">
		<div class="slider slider-nav csr-slide" id="csr-slide">
		
		<?php 
	if(!empty($data)){
		foreach($data as $focus){
?>		
		<div class="card">
		  <div class="card-body">
			<div class="card-body-content wow fadeInLeft">
			  <h4 class="h5"><?= $focus['state_name']; ?></h4>
			  <hr class="hr-border"></hr>
			  <p class="h5">Operational</p>
			  <p class="h5">Beneficiary:</p>
			  <p class="h2" style=""><?= $focus['operational_beneficiary']; ?></p>
			</div>                            
		  </div>
		</div>
<?php 
		}
	}
?>
		
		
		</div>
	  </div>
	</div>
	
  </div>
</div>
</section>
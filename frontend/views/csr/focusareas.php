<?php 
use frontend\models\Csr;
$objCsr = new Csr();
?>
<style>.slick-track{margin-left: 0;}</style>
<!-- Category Buttons -->
   <section class="n_video_section theme_sec mrb_0 pdb_0 " >
            <div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
                     <h2 class="section-title">Focus Areas</h2>
                  </div>
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="csr-gallery-sec-heading ">
			<?php 
			if(!empty($category)){
				$count = 1;
				foreach($category as $value){
					$class = 'select-active';
					if($count == 1){
						$class = 'select-color';
					}
					if($count >= 3){
						$class = 'select-active'.($count-1);
					}
					
			?>			
				<button type="button" class="btn csr-tab-btn btn-pad-2 <?= $class; ?> sel<?= $count; ?>" id="csr-btn<?= $count; ?>" onClick="focusareatab('<?= $count; ?>')" data-tag="t<?= $count; ?>"><?= $value['title']; ?></button>
			<?php  $count++; } } ?>		
			</div> 
		</div>
	</div>
				</div>
</section>
<!-- Category Buttons -->
<!--Focus Area Slider-->
<?php 
if(!empty($category)){
	$count = 1;
	$count2 = 2;
	foreach($category as $value){
		$data = $objCsr->getFocusData($value['id']);
		$class = '';
		if($count == 1){
			$count2 = '';	
			$class = 'current_focus';
		}else{
			$count2 = $count;	
		}
?>			
<section class="n_video_section mrt_0 csr-sec2bottom pr-5 current_focus_hide <?= $class; ?> pl-5" id="t<?= $count; ?>">
<div class="container">
	<div class="row">
  		<div class="col-lg-10 col-md-10 col-sm-10 sec2order">
			<div class="csr-future-left-img">
	  			<div class="slider slider-for csr-leftside-slide-change" id="csr-slide-img<?= $count2; ?>">
				<?php  if(!empty($data)){ foreach($data as $focus){	  ?>		
					<div>
						<img src="<?= BASE_BE_URL; ?>uploads/focusareas/<?= $focus['image']; ?>" class="img-fluid">
					</div>
				<?php  } } ?>	
	  			</div>
	  			<div class="csr-rightside-slide-change">
					<div class="slider slider-nav" id="csr-slide<?= $count2; ?>">
					<?php  if(!empty($data)){ foreach($data as $focus){	 ?>		
						<div class="card">
						  	<div class="card-body">
								<div class="card-body-content">
							  		<!-- <h4 class="h2"><?= $focus['state_name']; ?></h4> -->
<!-- <hr class="hr-border"></hr> -->
							  		<?php if(!empty($focus['operational_beneficiary'])){ ?>
									  	
<!--
									  	<p class="h5">Operational</p>
									  	<p class="h5">Beneficiary:</p>
-->
									  	<p class="h5" style=""><?= $focus['operational_beneficiary']; ?></p>
									<?php } ?>
								</div>                            
						 	 </div>
						</div>
					<?php  } } ?>		
					</div>
	  			</div>
			</div>	
  		</div>
	</div>
</section>
									  </div>
<?php $count2++; $count++; } } ?>

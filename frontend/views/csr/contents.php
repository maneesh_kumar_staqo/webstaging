

 <section class="n_video_section theme_sec mrb_40 " style="padding-bottom: 0px;">
            <div class="container">
               <div class="bg_sec sec_padding_n2">
                  <div class="row">
                     <div class="col-md-12">
                        <h2 class="section-title text-left"><?= !empty($data[0]['title']) ? $data[0]['title'] : ''; ?></h2>
                     </div>
                     <div class="col-md-12">
                        <div class="about_data_content mrb_40" id="about-hero-group">
						<?= !empty($data[0]['description']) ? $data[0]['description'] : ''; ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
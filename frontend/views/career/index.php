<?php
$this->title = 'Careers';
use frontend\models\Utility;
$utility = new Utility();
$formHeading = $utility->getPageTitle('Career');
?>


<?php if(!empty($bannerData)){ ?>


   <section class="mobile-hidden">
            <div class="n_home_slider">
				<?php  if(!empty($bannerData)){ 
                        foreach($bannerData as $banner){
                    ?>
               <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $banner['image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= !empty($banner['title']) ? $banner['title'] : ''; ?> </h4>
                        <p class="text-right">
                           <?= !empty($banner['sub_title']) ? $banner['sub_title'] : ''; ?>
                        </p>
                     </div>
                  </div>
               </div>
			   <?php }} ?>
            </div>
         </section>
<?php } ?>

<?php if(!empty($bannerData)){ ?>


   <section class="desktop-hidden">
            <div class="n_home_slider">
				<?php  if(!empty($bannerData)){ 
                        foreach($bannerData as $banner){
                    ?>
               <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $banner['mobile_image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= !empty($banner['title']) ? $banner['title'] : ''; ?> </h4>
                        <p class="text-right">
                           <?= !empty($banner['sub_title']) ? $banner['sub_title'] : ''; ?>
                        </p>
                     </div>
                  </div>
               </div>
			   <?php }} ?>
            </div>
         </section>
<?php } ?>




<section class="n_video_section theme_sec mrb_0 pdb_0 " id="Why-work-with-HFE">
         <div class="container" style="display: block;">
            <div class="bg_sec sec_padding_n2">
               <div class="row">
                  <div class="col-md-12">
                     <h2 class="section-title text-left"><?= !empty($contentData[0]['title']) ? $contentData[0]['title'] : ''; ?> </h2>
                  </div>
                  <div class="col-md-12">
                     <div class="about_data_content mrb_40" id="about-hero-group">
                         <?= !empty($contentData[0]['description']) ? $contentData[0]['description'] : ''; ?>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </section>



      <section class="n_video_section mrb_0  make_happy  career-sec2bottom " id="We-Make" >
         <div class="container">
  <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
	  <div class="footer-copy ml-4 ">
		<h3 class="mbl"><span class="opecity_04">We make</span>
		  <span class="footer-slider-wrapper">
			<span class="footer-slider roll-it">
<?php 
	if(!empty($wemakeData)){
		foreach($wemakeData as $wemake){
?>			
			  <span><strong>&nbsp;<?= $wemake['title']; ?> </strong></span>
<?php 
		}
	}
?>	
			</span>
		  </span>
		</h3>
	  </div>



	  <div class="owl-carousel owl-theme ap" id="work-place-slider">
<?php 
	if(!empty($sliderData)){
		$count = 1;
		foreach($sliderData as $key => $slide){
			$class = '';
			if($count == 1){
				$class = 'img-slider-center';	
			}
?>	  
		<div class="item <?= $class; ?>"><img src="<?= BASE_BE_URL; ?>uploads/career/<?= $slide['image']; ?>" class="img-fluid gallery-popup" onclick="currentSlide4(<?= $key; ?>)"></div>
<?php 
			$count++;
		}
	}
?>	
	</div>

	<!-- gallery popup -->

<div id="myModal" class="gallery-modal">
		<span class="gallery-close" id="close-gallery" onclick="closegallery()">&times;</span>
		<div class="container gallery-popup-container">
<?php 
	if(!empty($sliderData)){
		foreach($sliderData as $key => $slide){
?>		
		  <div class="row">
			<div class="col-md-8 mx-auto">
			  <div class="mySlides4 text-center" id="gal<?= $key ?>">
				<img src="<?= BASE_BE_URL; ?>uploads/career/<?= $slide['image']; ?>">
			  </div>
			</div>
		  </div>
<?php 
		}
	}
?>
		  <a class="gallery_prev" onclick="plusSlides4(-1)">&#10094;</a>
		  <a class="gallery_next" onclick="plusSlides4(1)">&#10095;</a>
		  
		</div>
	  </div>


	<!-- gallery popup ends -->

	</div>
  </div>  
</div>
</section>





<section class="n_video_section mrb_0 bg_sec  " id="Learn-to-win-the-HFE-way">

<?php 
	if(!empty($learnData)){
		foreach($learnData as $learn){
?>
	<div class="container">
	  <div class="row mob-padd">
		<div class="col-lg-12 col-md-12 col-sm-12">
		  <h2 class="section-title mb-3"><?= $learn['title']; ?></h2>
		  <?= $learn['description']; ?>
		</div>
	  </div>  
	  <div class="row mt-4">
		<div class="col-lg-4 col-md-4 col-sm-4 mb-4">
		  <div class="card career-card">
			<img class="card-img-top" src="<?= BASE_BE_URL; ?>uploads/career/<?= $learn['image']; ?>" alt="Card image cap">
			<div class="card-body">
			  <p class="card-text"><?= $learn['image_short_description']; ?></p>
			</div>
		  </div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 mb-4">
		  <div class="card career-card">
			<img class="card-img-top" src="<?= BASE_BE_URL; ?>uploads/career/<?= $learn['image2']; ?>" alt="Card image cap">
			<div class="card-body">
			  <p class="card-text"><?= $learn['image2_short_description']; ?></p>
			</div>
		  </div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 mb-4">
		  <div class="card career-card">
			<img class="card-img-top" src="<?= BASE_BE_URL; ?>uploads/career/<?= $learn['image3']; ?>" alt="Card image cap">
			<div class="card-body">
			  <p class="card-text"><?= $learn['image3_short_description']; ?></p>
			</div>
		  </div>
		</div>
	  </div>
	</div>
<?php 
		}
	}
?>
</section>



<!-- mobile view lear  to win -->


	<section class="n_video_section mrb_0 career-sec5bottom pdb_0" id="Highlight-of-Campus-Placements">
		<div class="container">
  <div class="row">

	<div class="col-lg-9">
	  <div class="career-sec5-content">
		<h2 class="section-title" style="visibility: visible; animation-name: fadeInLeft;">HIGHLIGHT of <br> CAMPUS PLACEMENTS
		</h2>



	  <div class="about-sec4-tabs desktop-hidden">
		<div class="row">
		  <div class="col-md-4">
			<div class="career-pltabs-current"></div>
					<div class="career-divimg"></div>
				</div>
				<div class="col-md-4">
					<ul class="tabs">
					  <div class="career-right-tabs-img">
<?php 
	if(!empty($campusData)){
		$count = 1;
		foreach($campusData as $campus){
			if($count == 3){
?>					  
				<li class="tab-link career-img-tab-<?= $count; ?>">
				<div class="career-tab-link-img">
					<img src="<?= BASE_BE_URL; ?>uploads/career/<?= $campus['image']; ?>" class="career-img-tab<?= $count; ?> grayscale-img" data-tab="tab-<?= $count; ?>">
				</div>
				</li>
<?php 
			}else{
?>
						<li class="tab-link career-img-tab-<?= $count; ?>" style="<?php if($count==1){ echo "display:none;"; } ?>">
						  <img src="<?= BASE_BE_URL; ?>uploads/career/<?= $campus['image']; ?>" class="career-img-tab<?= $count; ?> grayscale-img" data-tab="tab-<?= $count; ?>"/>
						</li>
<?php 
			}
			$count++;
		}
	}
?>	
					  </div>
					</ul>
				</div>
			</div>
	  </div>
	
		<div class="career-sec5-text">
<?php 
	if(!empty($campusData)){
		$count = 1;
		$count2 = 6;
		foreach($campusData as $campus){
			$class = '';
			if($count == 1){
				$class = 'wow fadeInLeft';
			}
?>		
		   <div id="tab-<?= $count; ?>" class="career-tab-content<?= $count2; ?> career-tab-bgcolor p-5">
				<h3 class="color-red <?= $class; ?>"><?= $campus['title']; ?></h3>
				<h4 class="highlight-text"><?= $campus['short_description']; ?></h4>
				<p class="highlight-pra-padd <?= $class; ?>"><?= str_replace("</p>", "", str_replace("<p>", "", $campus['description'])); ?></p>
			</div>
<?php 
			$count2--;
			$count++;
		}
	}
?>	
		</div>
	  </div>          
	</div>
	<div class="col-lg-3 mobile-hidden">
	  <div class="about-sec4-tabs">
		<div class="row">
		  <div class="col-md-4">
			<div class="career-pltabs-current"></div>
					<div class="career-divimg"></div>
				</div>
				<div class="col-md-4">
					<ul class="tabs">
					  <div class="career-right-tabs-img">
<?php 
	if(!empty($campusData)){
		$count = 1;
		foreach($campusData as $campus){
			if($count == 3){
?>					  
				<li class="tab-link career-img-tab-<?= $count; ?>">
				<div class="career-tab-link-img">
					<img src="<?= BASE_BE_URL; ?>uploads/career/<?= $campus['image']; ?>" class="career-img-tab<?= $count; ?> grayscale-img" data-tab="tab-<?= $count; ?>">
				</div>
				</li>
<?php 
			}else{
?>
						<li class="tab-link career-img-tab-<?= $count; ?>" style="<?php if($count==1){ echo "display:none;"; } ?>">
						  <img src="<?= BASE_BE_URL; ?>uploads/career/<?= $campus['image']; ?>" class="career-img-tab<?= $count; ?> grayscale-img" data-tab="tab-<?= $count; ?>"/>
						</li>
<?php 
			}
			$count++;
		}
	}
?>	
					  </div>
					</ul>
				</div>
			</div>
	  </div>
	</div>
  </div>
  </div>
  
</section>

	<section class="career-sec6bottom n_video_section  mrb_0 pdb_0" id="Vacancies">
		<div class="container">
<?php 
	if(!empty($vacanciesData)){
		$count = 1;
		foreach($vacanciesData as $vacancies){
?>
  <div class="row mt-4">
	<div class="col-lg-12 col-md-12 col-sm-12">
	 <?php if($count == 1){ ?>
	  <h2 class="mob-padd mb-3 section-title">Vacancies</h2>
	 <?php } ?>
	  <div class="Vacancy-block">
		<p><a href="<?= BASE_BE_URL; ?>uploads/careerbanner/<?= $vacancies['pdf']; ?>" target="_blank"><?= $vacancies['title']; ?></a></p>
		<?= !empty($vacancies['short_description']) ? $vacancies['short_description'] : ''; ?>
	  </div>
	</div>
  </div>  
<?php 
			$count++;
		}
	}
?>	
</div>
</section>


<section class="n_video_section" id="Apply-with-HFE">
	<div class="container">
  <div class="row mt-4">
	<div class="col-lg-12 col-md-12 col-sm-12">
	<form action="<?= BASE_URL; ?>careers" method="post" id="career_form" enctype="multipart/form-data">
	<input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
								value="<?=Yii::$app->request->csrfToken?>" />

	  <div class="career-form">
		<?php if(!empty($msg)){ echo "<div style='text-align:center;font-size:18px;color:green;'>".$msg."</div>"; } ?>
		<h2 class="mb-3 section-title"><?= !empty($formHeading) ? $formHeading : ''; ?></h2>
		<div class="row">
		  <div class="col-lg-4 col-md-4 col-sm-4">
			<div class="form-group">
				<label>Select Job Type</label>
			  <select class="test form-control" id="select" name="apply_for" >
				<option value="" selected="select">Select Job Type</option>
				<option value="Communications Executive">Communications Executive</option>
				<option value="Accountant">Accountant</option>
				<option value="Investment Analyst">Investment Analyst</option>
			  </select>
			</div>
		  </div>
		  <div class="col-lg-4 col-md-4 col-sm-4">
			<div class="form-group">
				<label for="Name" class="">Name</label>
			  <input type="text" id="name" name="name" class="form-control">
			  
			</div>
		  </div>
		  <div class="col-lg-4 col-md-4 col-sm-4">
			<div class="form-group">
				<label for="Email" class="">Email</label>
			  <input type="text" id="email_id" name="email_id" class="form-control">
			 
			</div>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-6 col-md-6 col-sm-6">
			<div class="form-group">
				<label for="contact_no" class="">Contact Number</label>
			  <input type="text" id="contact_no" name="contact_no" class="form-control" maxlength="10">
			  
			</div>
		  </div>
		  <div class="col-lg-6 col-md-6 col-sm-6">
			<div class="form-group">
				<label for="current_organisation" class="">Current Organisation</label>
			  <input type="text" id="current_organisation" name="current_organisation" class="form-control">
			 
			</div>
		  </div>
		</div>

		<div class="row">
		  <div class="col-lg-6 col-md-6 col-sm-6">
			<div class="form-group">
				<label for="current_designation" class="">Current Designation</label>
			  <input type="text" id="current_designation" name="current_designation" class="form-control">
			  
			</div>
		  </div>
		  <div class="col-lg-6 col-md-6 col-sm-6">
			<div class="form-group">
				<label for="experience" class="">No. of year of experience</label>
			  <input type="text" id="experience" name="experience" class="form-control">
			 
			</div>
		  </div>
		  <div class="col-lg-12 col-md-12 col-sm-12">
			<div class="form-group">
				<label>Upload Resume (.doc/.pdf)</label>
			</div>
			
			<div class="custom-uploadbtn mb-3"  id="venderimgupload1">
			  <input type="file" name="image1" id="image1" accept=".doc, .pdf" onchange="getFilename();"/>
			  
			</div>
			<div id="img_name"></div>
	
			 	<div class="form-group">
			 <div id="RecaptchaField1" class="g-recaptcha" style=" "></div>
			</div>
			 
			 
			<div class="form-group">
				<button type="button" class="btnallred btn btn-sm btn-theme-danger waves-effect waves-light"
					onClick="return validateCareer();"><span>SUBMIT &nbsp; &gt;</span> </button>
			</div>
		  </div>
		
		</div>
	  </div>
	  </form>
	</div>
  </div>       
</div>
</section>
<script>
function validateCareer(){
	  var capth = grecaptcha.getResponse();
        var captchlength = capth.length;
                if(captchlength==0){
                        alert("This capthca is required");
                return false;          
        }

	if($("#select").val() == ""){
		alert("Please select job type.");
		$("#select").focus();
		return false;
	}
	if($("#name").val() == ""){
		alert("Please enter name.");
		$("#name").focus();
		return false;
	}
	if($("#email_id").val() == ""){
		alert("Please enter email id.");
		$("#email_id").focus();
		return false;
	}else {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!($("#email_id").val()).match(mailformat)) {
            alert("You have entered an invalid email address.");
            $("#email_id").focus();
            return false;
        }
    }
	if($("#contact_no").val() == ""){
		alert("Please enter contact number.");
		$("#contact_no").focus();
		return false;
	}else if ($("#contact_no").val().length > 10 || $("#contact_no").val().length < 10) {
        alert('Please enter a valid contact number.');
        $('#contact_no').focus();
        return false;
    } else {
        var letters = /^[0-9]+$/;
        if (!($("#contact_no").val()).match(letters)) {
            alert('Please enter a valid contact number.');
            $("#contact_no").focus();
            return false;
        }
    }
	if($("#current_organisation").val() == ""){
		alert("Please enter current organisation.");
		$("#current_organisation").focus();
		return false;
	}
	if($("#current_designation").val() == ""){
		alert("Please enter current designation.");
		$("#current_designation").focus();
		return false;
	}
	if($("#experience").val() == ""){
		alert("Please enter experience.");
		$("#experience").focus();
		return false;
	}
	if($("#image1").val() == ""){
		alert("Please select resume.");
		$("#image1").focus();
		return false;
	}else{
		var fileExtension = ['doc', 'pdf', 'docx', 'DOC', 'PDF', 'DOCX'];
        if ($.inArray($("#image1").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only formats are allowed : "+fileExtension.join(', '));
			$("#image1").focus();
			return false;
        }
	}
	$("#career_form").submit();
}

function getFilename(){
	var filename = getFileNameVal($("#image1").val());
	$("#img_name").text(filename);
}

function getFileNameVal(path) {
	return path.match(/[-_\w]+[.][\w]+$/i)[0];
}
</script>
<?php
$this->title = 'Career';
?>

<section class="mt-4 top_banner desktop-view">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="owl-carousel owl-theme top_banner_owl_button" id="top_banner">
                <?php 
                    if(!empty($bannerData)){ 
                        foreach($bannerData as $banner){
                    ?>
                <div class="item">
                    <div class="row">
                        <div class="col-md-2 col-lg-2 col-sm-2 left-order-change top_banner_text">
                            <div class="left-side-slider">
                                <h1 class="animated fadeInRight delay-200 color-cyan"><?= !empty($banner['title']) ? $banner['title'] : ''; ?></h1>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10">
                            <img src="<?= BASE_BE_URL; ?>uploads/banners/<?= $banner['image']; ?>">
                        </div>
                    </div>
                </div>
                <?php 
                        }
                    }
                ?>
            </div>
        </div>
    </div>
</section>
<section class="top_banner_bottom ml-1 desktop-view">
    <div class="container">
      <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-12">
          <div class="top_banner_bottom-content text">
            <h3 class="color-red mb-3 ml-3 wow fadeInLeft text-uppercase"><?= !empty($contentData[0]['title']) ? $contentData[0]['title'] : ''; ?></h3>
            <div class="mb-4 pl-3 wow fadeInRight">
              <?= !empty($contentData[0]['description']) ? $contentData[0]['description'] : ''; ?>
          </div>
          </div>
        </div>
      </div>
    </div>
  </section>



<!-- mobile view section 1 start -->
<section class="mt-3 top_banner mobile-view">
<div class="row"> 
<div class="col-lg-12 col-md-12 col-sm-12">
<div class="owl-carousel owl-theme top_banner_owl_button" id="top_banner_mob">
<?php 
if(!empty($bannerData)){ 
	foreach($bannerData as $banner){	
?>
            <div class="item">
				<img src="<?= BASE_BE_URL; ?>uploads/banners/<?= $banner['mobile_image']; ?>">
                    <div class="left-side-slider">
              <div class="career-left-side-slider">
				  <h1 class="color-white"><?= !empty($bannerData[0]['title']) ? $bannerData[0]['title'] : ''; ?></h1>
				</div>
            </div>
			</div>
<?php 
	}
}
?> 			
        </div>        
      </div>      
    </div>    
  </section>

<section class="top_banner_bottom ml-3 mobile-view">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-md-10 col-sm-12">
  				<div class="slider-bottom-content text">
					<h3 class="color-red mt-5 ml-3 mb-3 wow fadeInLeft text-uppercase"><?= !empty($data[0]['title']) ? $data[0]['title'] : ''; ?></h3>
	 				<div class="pl-3 wow fadeInRight">
	 					<?= !empty($contentData[0]['description']) ? $contentData[0]['description'] : ''; ?>
	 				</div>
  				</div>
			</div>
		</div>
	</div>
</section>

<!-- mobile view section 1 ends -->


<section class="career-sec2bottom">
<div class="container">
  <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
	  <div class="footer-copy ml-4 desktop-view">
		<h3 class="mbl">We make
		  <span class="footer-slider-wrapper">
			<span class="footer-slider roll-it">
<?php 
	if(!empty($wemakeData)){
		foreach($wemakeData as $wemake){
?>			
			  <span><strong>&nbsp;<?= $wemake['title']; ?> </strong></span>
<?php 
		}
	}
?>	
			</span>
		  </span>
		</h3>
	  </div>

	  <div class="footer-copy ml-4 mobile-view">
		<h3 class="mbl">We make
		  <span class="footer-slider-wrapper">
			<span class="footer-slider roll-it">
<?php 
	if(!empty($wemakeData)){
		foreach($wemakeData as $wemake){
?>			
			  <span><strong>&nbsp;<?= $wemake['title']; ?> </strong></span>
<?php 
		}
	}
?>	
			</span>
		  </span>
		</h3>
	  </div>

	  <div class="owl-carousel owl-theme ap" id="work-place-slider">
<?php 
	if(!empty($sliderData)){
		$count = 1;
		foreach($sliderData as $slide){
			$class = '';
			if($count == 1){
				$class = 'img-slider-center';	
			}
?>	  
		<div class="item <?= $class; ?>"><img src="<?= BASE_BE_URL; ?>uploads/career/<?= $slide['image']; ?>" class="img-fluid gallery-popup" onclick="currentSlide4(<?= $count; ?>)"></div>
<?php 
			$count++;
		}
	}
?>	
	</div>

	<!-- gallery popup -->

<div id="myModal" class="gallery-modal">
		<span class="gallery-close" id="close-gallery" onclick="closegallery()">&times;</span>
		<div class="container gallery-popup-container">
<?php 
	if(!empty($sliderData)){
		foreach($sliderData as $slide){
?>		
		  <div class="row">
			<div class="col-md-8 mx-auto">
			  <div class="mySlides4 text-center">
				<img src="<?= BASE_BE_URL; ?>uploads/career/<?= $slide['image']; ?>">
			  </div>
			</div>
		  </div>
<?php 
		}
	}
?>
		  <a class="gallery_prev" onclick="plusSlides4(-1)">&#10094;</a>
		  <a class="gallery_next" onclick="plusSlides4(1)">&#10095;</a>
		  
		</div>
	  </div>


	<!-- gallery popup ends -->

	</div>
  </div>  
</div>
</section>



<section class="career-sec3bottom">

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12">
	  <div class="career-sec3-img">
		<div class="owl-carousel owl-theme" id="leadeeship-forum">
			<?php 
				if(!empty($thoughtData)){
					foreach($thoughtData as $thought){
			?>
		  <div class="item">
		  <div class="row">
			<div class="col-md-4">
			 <img src="<?= BASE_BE_URL; ?>uploads/career/<?= $thought['image']; ?>" class="img-fluid career-img">
			</div>
			
				 <div class="col-lg-8 col-md-8 col-sm-8">
				  <div class="career-sec3-right">
					<div class="career-sec3-right-content">
						<h3 class="mb-3 color-red wow fadeInRight text-uppercase"><?= $thought['title']; ?></h3>
						<p class="wow fadeInLeft"><?= $thought['description']; ?></p>
					</div>
				  </div>	  
			  </div>
			
		
		  </div>
		 
		  
		  </div>
		  <?php 
		}
	}
?>
		</div>

   
	  </div>
	  
  </div>

 
  
</div>

</section>


<section class="career-sec4bottom desktop-view">
<?php 
	if(!empty($learnData)){
		foreach($learnData as $learn){
?>
	<div class="container">
	  <div class="row mob-padd">
		<div class="col-lg-12 col-md-12 col-sm-12">
		  <h3 class="mb-3"><?= $learn['title']; ?></h3>
		  <?= $learn['description']; ?>
		</div>
	  </div>  
	  <div class="row mt-4">
		<div class="col-lg-4 col-md-4 col-sm-4 mb-4">
		  <div class="card career-card">
			<img class="card-img-top" src="<?= BASE_BE_URL; ?>uploads/career/<?= $learn['image']; ?>" alt="Card image cap">
			<div class="card-body">
			  <p class="card-text"><?= $learn['image_short_description']; ?></p>
			</div>
		  </div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 mb-4">
		  <div class="card career-card">
			<img class="card-img-top" src="<?= BASE_BE_URL; ?>uploads/career/<?= $learn['image2']; ?>" alt="Card image cap">
			<div class="card-body">
			  <p class="card-text"><?= $learn['image2_short_description']; ?></p>
			</div>
		  </div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 mb-4">
		  <div class="card career-card">
			<img class="card-img-top" src="<?= BASE_BE_URL; ?>uploads/career/<?= $learn['image3']; ?>" alt="Card image cap">
			<div class="card-body">
			  <p class="card-text"><?= $learn['image3_short_description']; ?></p>
			</div>
		  </div>
		</div>
	  </div>
	</div>
<?php 
		}
	}
?>
</section>



<!-- mobile view learn to win -->

<section class="career-sec4bottom mobile-view">
<?php 
	if(!empty($learnData)){
		foreach($learnData as $learn){
?>
<div class="container">
  <div class="row mob-padd">
	<div class="col-lg-12 col-md-12 col-sm-12">
	  <h3 class="mb-3"><?= $learn['title']; ?></h3>
	  <?= $learn['description']; ?>
	</div>
  </div>  
  <div class="row mt-4">
	<div class="col-12 mb-4">
	  <!-- mobile view -->

  <div class="owl-carousel owl-theme mobile-view" id="learn-to-win">
		<div class="item">
		  <div class="card career-card">
			<img class="card-img-top" src="<?= BASE_BE_URL; ?>uploads/career/<?= $learn['image']; ?>" alt="Card image cap">
			<div class="card-body">
			  <p class="card-text"><?= $learn['image_short_description']; ?></p>
			</div>
		  </div>
		</div>
		<div class="item">
		 <div class="card career-card">
			<img class="card-img-top" src="<?= BASE_BE_URL; ?>uploads/career/<?= $learn['image2']; ?>" alt="Card image cap">
			<div class="card-body">
			  <p class="card-text"><?= $learn['image2_short_description']; ?></p>
			</div>
		  </div>
		</div>
		<div class="item">
		  <div class="card career-card">
			<img class="card-img-top" src="<?= BASE_BE_URL; ?>uploads/career/<?= $learn['image3']; ?>" alt="Card image cap">
			<div class="card-body">
			  <p class="card-text"><?= $learn['image3_short_description']; ?></p>
			</div>
		  </div>
		</div>

  </div>

  <!-- mobile view ends here -->
	</div>      
  </div>
<!-- 
  <div class="row mt-2">
	<div class="col-lg-12 col-md-12 col-sm-12">
	  <p>
		HFE is proud to create and offer such engaging and resourceful opportunities to our
driven employees and is dedicated to making sure there are ample opportunities for
everyone in the future.
	  </p>
	</div>
  </div> -->
</div>
<?php 
		}
	}
?>
</section>

<!-- mobile view lear  to win -->


<section class="career-sec5bottom desktop-view">
  <div class="row">
	<div class="col-lg-9">
	  <div class="career-sec5-content">
		<h3 class="mb-3 color-blue wow fadeInLeft">HIGHLIGHT of <br> CAMPUS PLACEMENTS</h3>
		<div class="career-sec5-text">
<?php 
	if(!empty($campusData)){
		$count = 1;
		$count2 = 6;
		foreach($campusData as $campus){
			$class = '';
			if($count == 1){
				$class = 'wow fadeInLeft';
			}
?>		
		   <div id="tab-<?= $count; ?>" class="career-tab-content<?= $count2; ?> career-tab-bgcolor p-5">
				<h3 class="color-red <?= $class; ?>"><?= $campus['title']; ?></h3>
				<h4 class="highlight-text"><?= $campus['short_description']; ?></h4>
				<p class="highlight-pra-padd <?= $class; ?>"><?= str_replace("</p>", "", str_replace("<p>", "", $campus['description'])); ?></p>
			</div>
<?php 
			$count2--;
			$count++;
		}
	}
?>	
		</div>
	  </div>          
	</div>
	<div class="col-lg-3">
	  <div class="about-sec4-tabs">
		<div class="row">
		  <div class="col-md-4">
			<div class="career-pltabs-current"></div>
					<div class="career-divimg"></div>
				</div>
				<div class="col-md-4">
					<ul class="tabs">
					  <div class="career-right-tabs-img">
<?php 
	if(!empty($campusData)){
		$count = 1;
		foreach($campusData as $campus){
			if($count == 3){
?>					  
				<li class="tab-link career-img-tab-<?= $count; ?>">
				<div class="career-tab-link-img">
					<img src="<?= BASE_BE_URL; ?>uploads/career/<?= $campus['image']; ?>" class="career-img-tab<?= $count; ?> grayscale-img" data-tab="tab-<?= $count; ?>">
				</div>
				</li>
<?php 
			}else{
?>
						<li class="tab-link career-img-tab-<?= $count; ?>">
						  <img src="<?= BASE_BE_URL; ?>uploads/career/<?= $campus['image']; ?>" class="career-img-tab<?= $count; ?> grayscale-img" data-tab="tab-<?= $count; ?>"/>
						</li>
<?php 
			}
			$count++;
		}
	}
?>	
					  </div>
					</ul>
				</div>
			</div>
	  </div>
	</div>
  </div>
</section>


<!-- mobile view  -->

<section class="career-sec5bottom mobile-view">
  <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
	  <div class="career-sec5-content">
		<h3 class="mb-3 color-blue wow fadeInLeft">HIGHLIGHT of <br> CAMPUS PLACEMENTS</h3>
		<div class="career-sec5-text">
		  <div class="mob-testimonial">
		  <div class="owl-carousel owl-theme" id="student-mob-banner">
<?php 
	if(!empty($campusData)){
		$count = 1;
		foreach($campusData as $campus){
?>		  
			<div class="item">
				<div class="row">
				<div class="col-4">
				  <img src="<?= BASE_BE_URL; ?>uploads/career/<?= $campus['image']; ?>">                     
				</div>
				 
				 <div class="col-8">
				 <h3><?= $campus['title']; ?></h3>
				  <p><?= $campus['short_description']; ?></p>
				</div>
				</div>
				<div class="row mt-3">
				<div class="col-12">
				   <p class="card-text"><?= $campus['description']; ?></p>
				</div>
				
			  </div>            
			</div>
<?php 
			$count++;
		}
	}
?>				
		  </div> 
		  </div> 
		</div>          
	</div>
  </div>
</div>
</section>



<!-- mobile view ends here -->


<section class="career-sec6bottom">
<div class="container">
<?php 
	if(!empty($vacanciesData)){
		$count = 1;
		foreach($vacanciesData as $vacancies){
?>
  <div class="row mt-4">
	<div class="col-lg-12 col-md-12 col-sm-12">
	 <?php if($count == 1){ ?>
	  <h3 class="mob-padd mb-3">Vacancies</h3>
	 <?php } ?>
	  <div class="Vacancy-block">
		<p><a href="<?= BASE_BE_URL; ?>uploads/career/<?= $vacancies['pdf']; ?>" target="_blank"><?= $vacancies['title']; ?></a></p>
		<?= !empty($vacancies['short_description']) ? $vacancies['short_description'] : ''; ?>
	  </div>
	</div>
  </div>  
<?php 
			$count++;
		}
	}
?>	
</div>
</section>



<section class="career-sec7bottom">
<div class="container">
  <div class="row mt-4">
	<div class="col-lg-12 col-md-12 col-sm-12">
	<form action="<?= BASE_URL; ?>career/index" method="post" id="career_form" enctype="multipart/form-data">
	<input type="hidden" name="_csrf-frontend" value="<?=Yii::$app->request->getCsrfToken()?>" />
	  <div class="career-form">
		<?php if(!empty($msg)){ echo "<div style='text-align:center;font-size:18px;color:green;'>".$msg."</div>"; } ?>
		<h3 class="mb-3">Apply with HFE</h3>
		<div class="row">
		  <div class="col-lg-4 col-md-4 col-sm-4">
			<div class="md-form mb-0 career-dropdown-list">
			  <select class="test" id="select" name="apply_for" multiple="multiple">
				<option value="" selected>Select Job Type</option>
				<option value="Communications Executive">Communications Executive</option>
				<option value="Accountant">Accountant</option>
				<option value="Investment Analyst">Investment Analyst</option>
			  </select>
			</div>
		  </div>
		  <div class="col-lg-4 col-md-4 col-sm-4">
			<div class="md-form mb-0 career-cust-input">
			  <input type="text" id="name" name="name" class="form-control">
			  <label for="Name" class="">Name</label>
			</div>
		  </div>
		  <div class="col-lg-4 col-md-4 col-sm-4">
			<div class="md-form mb-0 career-cust-input">
			  <input type="text" id="email_id" name="email_id" class="form-control">
			  <label for="Email" class="">Email</label>
			</div>
		  </div>
		</div>
		
		<div class="row">
		  <div class="col-lg-6 col-md-6 col-sm-6">
			<div class="md-form mb-0 career-cust-input">
			  <input type="text" id="contact_no" name="contact_no" class="form-control" maxlength="10">
			  <label for="contact_no" class="">Contact Number</label>
			</div>
		  </div>
		  <div class="col-lg-6 col-md-6 col-sm-6">
			<div class="md-form mb-0 career-cust-input">
			  <input type="text" id="current_organisation" name="current_organisation" class="form-control">
			  <label for="current_organisation" class="">Current Organisation</label>
			</div>
		  </div>
		</div>

		<div class="row">
		  <div class="col-lg-6 col-md-6 col-sm-6">
			<div class="md-form mb-0 career-cust-input">
			  <input type="text" id="current_designation" name="current_designation" class="form-control">
			  <label for="current_designation" class="">Current Designation</label>
			</div>
		  </div>
		  <div class="col-lg-6 col-md-6 col-sm-6">
			<div class="md-form mb-0 career-cust-input">
			  <input type="text" id="experience" name="experience" class="form-control">
			  <label for="experience" class="">No. of year of experience</label>
			</div>
		  </div>
		  <div class="col-lg-12 col-md-12 col-sm-12">
			<div class="file-upload-label">
				<label>Upload Resume (.doc/.pdf)</label>
			</div>
			
			<div class="custom-uploadbtn mb-3"  id="venderimgupload1">
			  <input type="file" name="image1" id="image1" accept=".doc, .pdf" />
			  <label class="addtext1" for="image1">
				<span class="hidetext1">Choose File</span>
			  </label>
			</div>
		  </div>
		  <button type="button" class="mt-4 btnallred btn-outline-danger float-left ml-3 mb-4 float-right" onClick="return validateCareer();"><span>SUBMIT &nbsp; &gt;</span> </button>
		</div>
	  </div>
	  </form>
	</div>
  </div>       
</div>
</section>
<script>
function validateCareer(){
	if($("#select").val() == ""){
		alert("Please select job type.");
		$("#select").focus();
		return false;
	}
	if($("#name").val() == ""){
		alert("Please enter name.");
		$("#name").focus();
		return false;
	}
	if($("#email_id").val() == ""){
		alert("Please enter email id.");
		$("#email_id").focus();
		return false;
	}else {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!($("#email_id").val()).match(mailformat)) {
            alert("You have entered an invalid email address.");
            $("#email_id").focus();
            return false;
        }
    }
	if($("#contact_no").val() == ""){
		alert("Please enter contact number.");
		$("#contact_no").focus();
		return false;
	}else if ($("#contact_no").val().length > 10 || $("#contact_no").val().length < 10) {
        alert('Please enter a valid contact number.');
        $('#contact_no').focus();
        return false;
    } else {
        var letters = /^[0-9]+$/;
        if (!($("#contact_no").val()).match(letters)) {
            alert('Please enter a valid contact number.');
            $("#contact_no").focus();
            return false;
        }
    }
	if($("#current_organisation").val() == ""){
		alert("Please enter current organisation.");
		$("#current_organisation").focus();
		return false;
	}
	if($("#current_designation").val() == ""){
		alert("Please enter current designation.");
		$("#current_designation").focus();
		return false;
	}
	if($("#experience").val() == ""){
		alert("Please enter experience.");
		$("#experience").focus();
		return false;
	}
	if($("#image1").val() == ""){
		alert("Please select resume.");
		$("#image1").focus();
		return false;
	}else{
		var fileExtension = ['doc', 'pdf'];
        if ($.inArray($("#image1").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only formats are allowed : "+fileExtension.join(', '));
			$("#image1").focus();
			return false;
        }
	}
	$("#career_form").submit();
}
</script>
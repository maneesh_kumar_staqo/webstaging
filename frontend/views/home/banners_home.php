<?php 
	$serverData = $_SERVER['HTTP_USER_AGENT']; ?>	
<section class="mt-4 sec1bottom home desktop-view">
	<div class="row">      
	  	<div class="col-lg-12 col-md-12 col-sm-12" id="banner-play" data-type="play"  ondblclick="openFullscreen();">
			<div id="video1"> 				
					<video id="fullscreen" onclick="this.paused ? this.play() : this.pause();" class="video-top" <?= $autoplay; ?>  >
				  		<source src="<?= !empty($data[0]['image']) ? BASE_BE_URL.'uploads/images/'.$data[0]['image'] : ''; ?>">
			  		</video>
			  		<span class="fullscreenbtn"  onclick="openFullscreen();"> <i class="fas fa-expand"></i></span>
		          	<span class="share-video"><i class="fas fa-share-alt"></i></span>
		          	<ul class="share-video-icons">
			            <li><a href="https://api.addthis.com/oexchange/0.8/forward/facebook/offer?url=http%3A%2F%2Flocalhost%2Fhfe%2Fimg%2FHFE-Film.mp4&pubid=ra-42fed1e187bae420&title=Hero%20Future%20Energies&ct=1" target="_blank"><i class="fab fa-facebook-f facebook"></i></a></li>
			            <li><a href="https://api.addthis.com/oexchange/0.8/forward/twitter/offer?url=http%3A%2F%2Flocalhost%2Fhfe%2Fimg%2FHFE-Film.mp4&pubid=ra-42fed1e187bae420&title=Hero%20Future%20Energies&ct=1" target="_blank"><i class="fab fa-twitter twitter" target="_blank"></i></a></li>
			            <li><a href="https://api.addthis.com/oexchange/0.8/forward/linkedin/offer?url=http%3A%2F%2Flocalhost%2Fhfe%2Fimg%2FHFE-Film.mp4&pubid=ra-42fed1e187bae420&title=Hero%20Future%20Energies&ct=1" target="_blank"><i class="fab fa-linkedin-in linkedin"></i></a></li>
			            <li><a href="https://www.instagram.com/hero_futureenergies/" target="_blank"><i class="fab fa-instagram instagram"></i></a></li>
			            <li><a href="https://www.youtube.com/channel/UC_i6ZrJLBLhmU1VDZq5wvTw" target="_blank"><i class="fab fa-youtube youtube"></i></a></li>
		          	</ul>
			</div> 
	  	</div>
	</div>

	<div class="row padd-12">
	  	<div class="slider-bottom text-center">
			<div class="col-lg-12 col-md-12 col-sm-12">
		  		<div class="slider-bottom-content text">
		   			<p class="p-3 mt-5 text-animate home-banner-bottom clear-both"><?= !empty($data[0]['title']) ? $data[0]['title'] : ''; ?></p>
		   		</div>	  
			</div>
	  	</div> 
	</div>
</section>
<?php } ?>

<!-- mobile view section 1 start -->

<section class="mt-4 sec1bottom mobile-view">
	<div class="row">      
  		<div class="col-lg-12 col-md-12 col-sm-12" id="mob-banner-play">
			<img src="<?= !empty($data[0]['mobile_image']) ? BASE_BE_URL.'uploads/banners/'.$data[0]['mobile_image'] : ''; ?>" class="img-fluid">
			<span class="video__play__icon color-white">
				<span class="video__play__icon__text">Play</span>
			</span>	
  		</div>
	</div>
	  	
	<section class="slider-bottom ml-5">
		<div class="container">
		  	<div class="row">
				<div class="col-lg-10 col-md-10 col-sm-12">
			  		<div class="slider-bottom-content text">
				  		<p class="p-3 mt-5 text-animate mob-home-banner-bottom"><?= !empty($data[0]['title']) ? $data[0]['title'] : ''; ?></p>
				  	</div>	 
				</div>
		  	</div>
		</div>
	</section>
</section>  

<!-- mobile view section 1 ends -->

<!-- video overlay secion -->

<div id="video_overlay1">
	<div class="video_play" id="video-section">
		<iframe width="853" id="iframeoff" height="480" src="<?= !empty($data[0]['image_link']) ? $data[0]['image_link'] : ''; ?>" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"></iframe>
	</div>
	<span class="overlay-close" id="pause" onclick="pauseVid()">x</span>
</div>

<!-- video overlay secion ends here -->
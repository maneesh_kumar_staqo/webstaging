<?php 
if(!empty($data)){
	foreach($data as $value){
		if($value['title'] == "OUR <br> PROJECTS"){
?>
<section class="sec2bottom">
	<div class="row">
	  	<div class="col-lg-6 col-md-6 col-sm-12 sec2order">
			<div class="future-left-img">
			  	<img src="<?= BASE_BE_URL; ?>uploads/images/<?= $value['image']; ?>" class="img-fluid img wow fadeInDown">
			</div>	
	  	</div>

	  	<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="future">
				<div class="mb-5 text-animate">
			  		<?= str_replace("</p>", "", str_replace("<p>", "", $value['description'])); ?>
			  	</div>
			  	<a href="<?= $value['link']; ?>" target="_blank">
		  		<button type="button" class="btnallred btn btn-sm btn-outline-danger"><span>ALL PROJECTS &nbsp; ></span> </button>
		  	</a>		  	
			</div>
			<div class="future2 scrollclass">
			  	<h1 class="highlight"><?= $value['title']; ?></h1>
			</div>		  
	  	</div>
	</div>
</section>

<?php }if($value['image_position'] == "Right" && $value['title'] != "Towards a Positive" && $value['title'] != "OUR <br> PROJECTS"){ ?>

<section class="sec4bottom">
	<div class="row">
	 	<div class="col-lg-5 col-md-5 col-sm-12 orderchange">
	  		<div class="sec4-left sec4-left-m">
				<div class="sec4-left-bottom">
				   <h3 class="mb-3 wow fadeInDown"><?= $value['title']; ?></h3>
				   <p class="mb-5 wow fadeInUp mob-bottom hide-br"><?= str_replace("</p>", "", str_replace("<p>", "", $value['description'])); ?></p>
				   <a href="<?= $value['link']; ?>">
					<button type="button" class="btnallred btn btn-sm btn-outline-danger"><span>KNOW MORE &nbsp; ></span> </button>  
					</a>		  
				</div>	  
	  		</div>	 
	  	</div>
	  	<div class="col-lg-7 col-md-7 col-sm-12">
			<div class="sec4-img wow fadeInRight">
		  		<img src="<?= BASE_BE_URL; ?>uploads/images/<?= $value['image']; ?>" class="img-fluid">
			</div>
	   	</div>
	</div>
</section> 

<?php }if($value['image_position'] == "Left" && $value['title'] != "Towards a Positive" && $value['title'] != "OUR <br> PROJECTS"){ ?>

<section class="sec3bottom sec3">
	<div class="row">      
	  	<div class="col-lg-8 col-md-8 col-sm-12 z-index-999">
			<img src="<?= BASE_BE_URL; ?>uploads/images/<?= $value['image']; ?>" class="img-fluid img wow fadeInLeft">
	  	</div>

	  	<div class="col-lg-4 col-md-4 col-sm-12 orderchange">
			<div class="sec3-left">
				<h3 class="wow fadeInRight mb-3"><?= $value['title']; ?></h3>
		 		<p class="mb-5"><?= str_replace("</p>", "", str_replace("<p>", "", $value['description'])); ?></p>
				<a href="<?= BASE_BE_URL; ?>uploads/images/<?= $value['image_pdf'].'#view=fitV'; ?>" target="_blank">
					<button type="button" class="btnallwhite btn btn-sm btn-outline-white"><span>KNOW MORE &nbsp; ></span> </button>
				</a>
			</div>  
	  	</div>
	</div>
</section>

<?php }if($value['title'] == "Towards a Positive"){ ?>

<section class="sec6bottom">
	<div class="row">     
	  	<div class="col-lg-7 col-md-7 col-sm-12 z-index-999">
			<div class="sec4-img">
		  		<img src="<?= BASE_BE_URL; ?>uploads/images/<?= $value['image']; ?>" class="img-fluid wow fadeInLeft">
			</div>
	   	</div>
	   	<div class="col-lg-5 col-md-5 col-sm-12 orderchange">
	  		<div class="sec6-left">
				<div class="sec6-left-bottom">
					<div class="sec6-left-bottom-width">
					  	<h3 class="mb-3 wow fadeInDown"><?= $value['title']; ?></h3>
					  	<img src="img/change.png" class="img-fluid ml-5 mb-4 wow fadeInUp img-width">
					  	<p class="mb-5 wow fadeInDown mob-bottom"><?= str_replace("</p>", "", str_replace("<p>", "", $value['description'])); ?></p>
					  	<a href="<?= $value['link']; ?>" target="_blank">
					  		<button type="button" class="btnall btn btn-sm btn-outline-green"><span>KNOW MORE &nbsp; ></span> </button>
					  	</a> 
					</div>
				</div>	  
	  		</div>	 
	  	</div>
	</div>
</section>
<?php  } } } ?>

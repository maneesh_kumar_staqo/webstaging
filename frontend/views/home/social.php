<section class="sec7bottom p-0">
<div class="row">
 <div class="col-lg-4 col-md-4 col-sm-12">
  <h3 class="mob-pad01 color-red">Blogs</h3>
  <div class="sec7-left wow fadeInUp">
	 <div class="card card-image cust-card">
		<div class="content">
		  <a href="<?= !empty($blogsData['page_url']) ? BASE_URL.'blogs/'.$blogsData['page_url'] : ''; ?>">
			<div class="content-overlay"></div>
			<img class="content-image" src="<?= !empty($blogsData['image']) ? BASE_BE_URL.'uploads/blogs/'.$blogsData['image'] : ''; ?>">
			<div class="content-details fadeIn-top">
			 <p class="mx-5 mb-5 blog-hover-text"><?= !empty($blogsData['title']) ? $blogsData['title'] : ''; ?></p>
			</div>
		  </a>
		</div>
	  </div>
	</div>
  </div>
  
  <div class="col-lg-4 col-md-4 col-sm-12">
	<h3 class="mob-pad01 color-red">Social Updates</h3>
	<div class="sec7-right wow fadeInUp"> 
	  <div class="card card-image socialcard">
		<div class="text-center sec7-right-content">
		  <a class="twitter-timeline" data-width="100%" data-height="306" href="https://twitter.com/HeroFuture_HFE?ref_src=twsrc%5Etfw">HFE Tweets</a>
		<div class="twitter-box">&nbsp;</div>

		</div>
	  </div>
	</div>
   </div>

   <div class="col-lg-4 col-md-4 col-sm-12">
	<h3 class="color-red mob-pad01">Videos</h3>
	<div class="sec7-right-scroll wow fadeInRight mob-pad01" id="style-3"> 
	  <!-- <div class="card card-image"> -->
		<div class="text-left" id="on">
		  <img src="<?= !empty($youtubeData['video_thumb_image']) ? BASE_BE_URL.'uploads/youtube/'.$youtubeData['video_thumb_image'] : ''; ?>" class="img-fluid small-video">
		  <span class="video__play__icon color-white">
			<span class="video__play__icon__text">Play</span>
		</span>
		 
		</div>
	  </div>
	<!-- </div> -->
   </div>

  </div>

</section>

<!-- video overlay secion -->

<div id="video_overlay">
<div class="video_play" id="video-section">
<iframe width="853" id="iframeofff" height="480" src="<?= !empty($youtubeData['video_link']) ? $youtubeData['video_link'] : ''; ?>" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowFullScreen="allowFullScreen"></iframe>

</div>
<span class="overlay-close" onclick="pauseVid()">x</span>
</div>

<!-- video overlay secion ends here -->

<script src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<script src="<?= BASE_URL; ?>js/twitter.js" charset="utf-8"></script>
<?php
/* @var $this yii\web\View */
use frontend\models\Bgel;
use frontend\models\InvestorRelation;
$model1 = new InvestorRelation();
$coverage = $model1->getCoverageData();
$policies = $model1->getPoliciesData();
$model = new Bgel();
$this->title = 'Home Hero Future Energies';
if(!empty($pageData['page_priority_data'])){

	foreach($pageData['page_priority_data'] as $key => $priority){
		$data = '';
		$page_category_data = '';
	
		if(!empty($pageData[$priority['page_type']])){
			$data = $pageData[$priority['page_type']];
		}
		if(!empty($pageData['page_category_data'])){
			$page_category_data = $pageData['page_category_data'];
		}
    
		if(file_exists(dirname(__FILE__) .'/'.$priority['page_type'].'.php')){
			echo $this->render($priority['page_type'], ['data' => $data,'key' => $key,'coverage' => $coverage,'policies'=>$policies]);
			
		}

		
	}
}
?>
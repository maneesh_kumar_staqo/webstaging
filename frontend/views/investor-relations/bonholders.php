<section class="n_video_section bg_sec  mrb_0">
            <div class="container" style="display: block;">
               <div class="row">
                <?php if(!empty($coverage)){ ?>
                  <div class="col-md-4">
                     <div class="mobile_item_dn_n1" id="media-coverage">
                        <div class="mobile_item_dn_n1_heading">Media Coverage</div>
                        <div class="mobile_item_dn_n1_card">
                            <ul>
                            <?php foreach ($coverage as $key => $value) { ?>
                              <li>
                                 <a href="<?= BASE_URL ?>newsroom/<?= $value['page_url']  ?>"><?= $value['title'] ?></a>
                                 <span><?= $value['publish_date'] ?></span>
                              </li>
                             <?php } ?>
                           </ul>
                           <div class="text-left card_readmore_dn_n1">
                              <a href="<?= BASE_URL ?>newsroom">
                        <button type="button" class="btnallred btn btn-sm btn-theme-danger waves-effect waves-light"><span>View More &nbsp; &gt;</span> </button>  
                        </a> 
                           </div>
                        </div>
                     </div>
                  </div>
              <?php } ?>
              <?php if(!empty($policies)){ ?>
                    <div class="col-md-4">
                     <div class="mobile_item_dn_n1" id="Policies">
                        <div class="mobile_item_dn_n1_heading">Policies</div>
                        <div class="mobile_item_dn_n1_card">
                         <ul style="    list-style: auto;
                  padding-left: 20px;">
                  <?php foreach ($policies as $key => $value) { ?>
                  
                              <li>
                                 <a href="<?= BASE_BE_URL ?>uploads/policies/<?= $value['pdf_image'] ?>#view=fitV"><?= $value['title'] ?></a>
                               
                              </li>
                          <?php } ?>
                              
                           </ul>
                           <div class="text-left card_readmore_dn_n1">
                             <a href="<?= BASE_URL ?>policies">
                        <button type="button" class="btnallred btn btn-sm btn-theme-danger waves-effect waves-light"><span>View More &nbsp; &gt;</span> </button>  
                        </a>
                           </div>
                        </div>
                     </div>
                  </div>
              <?php } ?>
                  <?php if(!empty($data)){ ?>
                  <div class="col-md-4">
                     <div class="mobile_item_dn_n1" id="bonholder">
                        <div class="mobile_item_dn_n1_heading">Bondholder's Information
						</div>
                        <div class="mobile_item_dn_n1_card">
                           <?= $data[0]['description']  ?>
                           <div class="text-left card_readmore_dn_n1">
                             <a href="<?= $data[0]['view_more'] ?>" target="_blank">
                        <button type="button" class="btnallred btn btn-sm btn-theme-danger waves-effect waves-light"><span> Click here  &nbsp; &gt;</span> </button>  
                        </a>
                           </div>
                        </div>
                     </div>
                  </div>
              <?php } ?>
               </div>
            </div>
         </section>
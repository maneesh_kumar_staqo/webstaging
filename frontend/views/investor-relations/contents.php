<?php if(!empty($data)){ ?>
<section class="n_video_section theme_sec  mrb_0" id="BGEL">
            <div class="container" style="display: block;">
               <div class="">
                     <h2 class="section-title text-center">BGEL Disclosures</h2>
                  </div>
               <div class="theme_blue_bg_sec_box">
                  <div class="row">
                  
                  <div class="col-md-12">
                     <div class="sec_box_blue">
                        <div class="property-item horizontal expertise-slide">
                           <div class="thumb">
                              <img src="<?= BASE_BE_URL; ?>uploads/contents/<?= !empty($data[0]['image']) ? $data[0]['image'] : ''; ?>" alt="">
                           </div>
                           <div class="content">
                              <!-- <p class="post-type">Our Areas of Expertise</p> -->
                              <h3 class="post-title"><?= $data[0]['title'] ?></h3>
                              <div class="excerpt">
                                 <p><?= $data[0]['description'] ?>
                                       </p>
                                  <a href="<?= BASE_URL ?>bgel">
                        <button type="button" class="btnallred btn btn-sm btn-theme-danger waves-effect waves-light"><span>View More &nbsp; &gt;</span> </button>  
                        </a>
                              
                              </div>
                          
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               </div>
            </div>
         </section>
<?php } ?>
<?php
  // echo "<pre>";print_r($data);die;
?>

<?php if(!empty($data)){ ?>
  <section>
   <div class="container">
               <div class="investors_r_with_page" id="managment">
                  <div class="investors_r_card_1">
                     <div class="tile-wrapper  n_home_slider" >
                       <?php foreach ($data as $key => $value) { ?>
                           <div>
                            <!-- data-target="#directormodal<?= $key ?>" -->
                           <a href="javascript:void(0);"  data-toggle="modal" class="tile tile-2x1" data-analytics-link="about-us-ir-module-2--Investor Relations">
                              <div class="tile-inner">
                                 <div class="image-wrapper">
                                    <img srcset="<?= BASE_BE_URL; ?>uploads/leadership/<?= !empty($value['image']) ? $value['image'] : ''; ?>" alt="">
                                    <div role="img" aria-label="playIcon_" class="playIcon_"></div>
                                 </div>
                                 <div class="content-wrapper ">
                                    
                                    <div class="main-content">
                                   
                                       <?=  $value['description']  ?>
                                
                                       <h3><?=  $value['name']  ?></h3>
                                       <span class="date"><?=  $value['designation']  ?></span>
                                    </div>
                                 </div>
                              </div>
                           </a>
                        </div>
                    <?php   } ?>
                      
                  
                     </div>
                  </div>
               </div>
            </div>
         </section>
<?php } ?>


<?php if(!empty($data)){ foreach ($data as $key => $value) { ?>

<div class="modal centermodel" id="directormodal<?= $key ?>" style="background-color: rgba(255, 255, 255, 0.94); display: none;" aria-hidden="true">
      <div class="modal-dialog modal-fluid fluid-modal-width">
        <div class="modal-content">
            <div class="modal-body cust-modal-body">
            <button type="button" class="close float-right close-overflow" data-dismiss="modal" style="color:#fff">×</button>
            <h3 class="float-left text-uppercase"><?=  $value['name']  ?></h3> <br> <br>
            <h5 class="float-left text-capitalize" style="line-height: 0; font-size: 1.1rem!important;"><?=  $value['designation']  ?></h5>
            <hr>
            <div class="row">
                <div class="col-md-4">
                <img src="<?= BASE_BE_URL; ?>uploads/leadership/<?= !empty($value['image']) ? $value['image'] : ''; ?>" class="img-fluid">
                </div>
              <div class="col-md-8">
              <div> 
                <?=  $value['description']  ?>
              </div>
</div>            </div>
          </div>
        </div>
      </div>
    </div>
<?php } } ?>
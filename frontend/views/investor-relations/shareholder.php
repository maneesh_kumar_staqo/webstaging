<?php if(!empty($data)){
  // echo "<pre>";print_r($data);die; 
  ?>
  
  
   <section class="n_video_section theme_sec mrb_0 " id="our-shareholders">
            <div class="container">
               <div class=" ">
                  <div class="row">
                     <div class="col-md-12">
                        <h2 class="section-title text-center">Our Shareholders</h2>
                     </div>
                  </div>
                  <div class="row mobile-hightlight Shareholders_mobile-slider">
                     <?php foreach ($data as $key => $value) { ?>
                        <div class="col-md-4">
                        <div class="our_shareholder_sec_item">
                           <img src="<?= BASE_BE_URL; ?>uploads/investor/<?= !empty($value['image']) ? $value['image'] : ''; ?>" draggable="false">
                        </div>
                     </div>
                   <?php  } ?>
                    </div>
                    
                  </div>
               </div>
            </div>
         </section>
  <?php
} ?>
<?php
$this->title = 'Policies';
use frontend\models\Policies;
   $model = new Policies();

   $newyear = $model->getDisclouserDataitem($page_id);
?>
<?php if(!empty($bannerData)){ ?>
 <section class="mobile-hidden">
            <div class="n_home_slider">
			<?php 
				foreach($bannerData as $banners){
			?>
               <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $banners['image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= $banners['title']; ?><br> </h4>
                        <p class="text-right"><?= $banners['sub_title']; ?>
                        </p>
                     </div>
                  </div>
               </div>
				<?php  }  ?>
            </div>
         </section>
      <?php } ?>

      <?php if(!empty($bannerData)){ ?>
 <section class="desktop-hidden">
            <div class="n_home_slider">
			<?php 
				foreach($bannerData as $banners){
			?>
               <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $banners['mobile_image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= $banners['title']; ?><br> </h4>
                        <p class="text-right"><?= $banners['sub_title']; ?>
                        </p>
                     </div>
                  </div>
               </div>
				<?php  }  ?>
            </div>
         </section>
      <?php } ?>


<?php
if(!empty($contentData)){ ?>

 <section class="bg_sec  theme_sec mrb_40 " style="padding-bottom: 0px;">
         <div class="container">
            <div class=" ">
               <div class="row">
                 
                  <div class="col-md-12">
                     <div class="about_data_content mrb_40" id="about-hero-group">
						 <h2 class="secstion-title"><?= $contentData[0]['title'] ?></h2>
                        <div class="text-center">
						<?= $contentData[0]['description'] ?>
						</div>
                     </div>
                    
                  </div>
               </div>
            </div>
         </div>
      </section>
<?php }
?>
<!-- mobile banner ends here -->
<?php if(!empty($policy)){ ?>
	<section class="n_video_section theme_secpolicy-sec2bottom">
		<div class="container">
			<div class="row">
	<?php 
	if(!empty($policy)){
		$count = 1;
		foreach($policy as $value){
			if($count%2 == 1 || $count == 1){ ?>
    
    
	<div class="col-lg-6 col-md-6 col-sm-6 mb-3">
		<a href="<?= BASE_BE_URL; ?>uploads/policies/<?= $value['pdf_image'].'#view=fitV'; ?>" class="policies-color-anc" target="_blank">
    <p class="policy-padd"><?= $value['title']; ?></p>
		</a>
        
	</div>
	<?php } } } ?>
   
			</div>
		
		</div>
</section>
<?php } ?>




<?php
if(!empty($disclouser)){ ?>
<section class="n_video_section bgel-sec-3 ">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-lg-12 col-sm-12">
            <h2 class="section-title text-left">DISCLOSURES </h2>
         </div>
         <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="accordion md-accordion" id="accordionfirst" role="tablist" aria-multiselectable="true">
               <div class="row">
                  <?php 
                
                     if (!empty($newyear)) {
                         $connt=1;

                         foreach ($newyear as $key => $discrow) {
                           if(!empty($discrow['years'])){ ?>
                              <div class="col-lg-6 col-md-6 col-sm-6 list_disclouser_list">
                     <div id="accordion" role="tablist">
                        <div class="card box-shadow-none">
                           <div class="card-header" role="tab" id="headingOne1">
                              <a data-toggle="collapse" data-parent="#accordionfirst"
                                 href="#collapseOne<?= $connt?>" aria-expanded="false"
                                 aria-controls="collapseOne<?= $connt?>" class="collapsed">
                                 <p class="mb-0">
                                    <?= $discrow['years']; ?>
                                 </p>
                              </a>
                           </div>
                           <div id="collapseOne<?= $connt?>" class="collapse" role="tabpanel"
                              aria-labelledby="headingOne1" data-parent="#accordionfirst" style="">
                              <div class="card-body">
                                 <div class="">
                                    <?php
                                       $years=$discrow["years"];
                                       $disctitleresult = $model->getAnnualQuarterly($years,$page_id); 
                                       $disctitleresult1 = $model->getMonthly($years,$page_id);
                                       
                             
                                       ?>
                                    <div class="row">
                                       <?php 
                                          if (!empty($disctitleresult)) {
                          

                                             $annual=1;
                                             ?>
                                       <div class=" pd_0 <?php if (!empty($disctitleresult1)) {echo "col-md-6";}else{echo "col-md-12";} ?>">
                                          <p class="disc_tbl_head"><b>Annual / Quarterly</b></p>
                                          <?php
                                             if (!empty($disctitleresult)) {
                                             foreach ($disctitleresult as $key => $disctitlerow) { {
                                                $month_num =$disctitlerow['month'];
                                                if(!empty($month_num)){
                                                   $month_name = date("F", mktime(0, 0, 0, $month_num, 10));
                                                }
                                                else{
                                                   $month_name="";
                                                }

                                                ?>
                                     
                                             <a href="<?= !empty($disctitlerow['pdf']) ? BASE_BE_URL.'uploads/disclosures/'.$disctitlerow['pdf'] : ''; ?>#view=fitV" target="_blank">
                                          <p class="discloser_link_title"><?= $month_name; ?> - <?= $disctitlerow['title']; ?></p>
                                       </a>
                                         
                                          <?php }}?>
                                       </div>
                                       <?php 
                                          }
                                          else{
                                              $annual=0;
                                          }
                                          ?>
                                       <?php 
                                          if (!empty($disctitleresult1)) {
                                          ?>
                                       <div class=" <?php if($annual==1){echo "col-md-6";}else{ echo "col-md-12";}?> pd_0">
                                          <p class="disc_tbl_head">
                                             <b>Monthly</b>
                                          </p>
                                          <?php
                                             if (!empty($disctitleresult1)) {
                                             
                                                foreach ($disctitleresult1 as $key => $disctitlerow1) {
                                                    $month_num =$disctitlerow1['month'];
                                                $month_name = date("F", mktime(0, 0, 0, $month_num, 10));
                                                 ?>
                                        
                                             <a href="<?= !empty($disctitlerow1['pdf']) ? BASE_BE_URL.'uploads/disclosures/'.$disctitlerow1['pdf'] : ''; ?>#view=fitV" target="_blank">
                                          <p class="discloser_link_title"><?= $month_name ?> - <?= $disctitlerow1['title']; ?></p></a>
                                         
                                          <?php }}?>
                                       </div>
                                       <?php } } else{ ?>

                                           <div class=" col-md-12">
                                          <p class="disc_tbl_head">
                                             <b>Monthly</b>
                                          </p>
                                          <?php
                                             if (!empty($disctitleresult1)) {
                                             
                                                foreach ($disctitleresult1 as $key => $disctitlerow1) { 
                                                $month_num =$disctitlerow1['month'];
                                                $month_name = date("F", mktime(0, 0, 0, $month_num, 10));
                                                   ?>
                                        
                                             <a href="<?= !empty($disctitlerow1['pdf']) ? BASE_BE_URL.'uploads/disclosures/'.$disctitlerow1['pdf'] : ''; ?>#view=fitV" target="_blank">
                                          <p class="discloser_link_title"><?=  $month_name ?> - <?= $disctitlerow1['title']; ?></p></a>
                                         
                                          <?php }}?>
                                       </div>

                                      <?php  } ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                           <?php 
                             
                   $connt++; } }  } ?>
                 <!--  <div class="col-lg-12 col-md-12 col-sm-12 text-left">
                     <button type="button"
                        class="btnallred btn btn-sm btn-outline-danger" id="load_more"><span>Load More&nbsp; &gt;</span> </button>
                  </div> -->
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<?php }


?>


<?php
if(!empty($address)){ ?>
   
<section class="n_video_section ">
   <div class="container"> 
<div class="row justify-content-center">
  <div class="col-lg-12 col-md-12 col-sm-12">
	<div class="query-heading mb-4">
	  <h3>For Investors Contact</h3>
	</div>        
	<div class="query-block">
	  <div class="row">
<?php 
if(!empty($address)){
	$count = 1;
	foreach($address as $value){
?>		
            <div class="col-lg-4 col-md-4 col-sm-4">
              <?php 
				echo $value['address'];
				if($count != count($address)){
					echo '<div class="bgel-investor-border-right"></div>';
				}
			  ?>
            </div>
<?php 
		$count++;
	}
}
?>
	  </div>
	</div>
  </div>
</div>
 </div>
</section>

<?php }
?>
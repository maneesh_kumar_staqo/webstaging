<?php
$this->title = 'Contact Us';
use frontend\models\Contact;
use frontend\models\Utility;
$model = new Contact();
$utility = new Utility();
$formHeading = $utility->getPageTitle('Contact');
?>


<?php 
if(!empty($bannerData)){  ?>
     <section class="mobile-hidden">
            <div class="n_home_slider">
				<?php 	foreach($bannerData as $banners){	 ?>
               <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $banners['image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= $banners['title']; ?></h4>
                        <p class="text-right">
                          <?= $banners['sub_title']; ?>
                        </p>
                     </div>
                  </div>
               </div>
			   <?php  } ?>
            </div>
         </section>
		 <?php } ?>


<?php 
if(!empty($bannerData)){  ?>
     <section class="desktop-hidden">
            <div class="n_home_slider">
				<?php 	foreach($bannerData as $banners){	 ?>
               <div class="n_home_slider_item" style="background-image: url('<?= BASE_BE_URL; ?>uploads/banners/<?= $banners['mobile_image']; ?>');">
                  <div class="n_home_slider_item_data verticle_center_slider">
                     <div>
                        <h4 class="text-right"><?= $banners['title']; ?></h4>
                        <p class="text-right">
                          <?= $banners['sub_title']; ?>
                        </p>
                     </div>
                  </div>
               </div>
			   <?php  } ?>
            </div>
         </section>
		 <?php } ?>



         <section class="  n_video_section   mrb_40 " >
            <div class="container">
<div class="row justify-content-center">
  <div class="col-lg-10 col-md-10 col-sm-10">
	<div class="contact-form">
	  <div class="contact-form-heading mb-4">
		<h2 class="section-title text-center"><?= !empty($formHeading) ? $formHeading : ''; ?></h2>          
	  </div>
	  <div class="contact_form_n1 mrb_40" id="contact">
		  <form action="<?= BASE_URL; ?>contactquery" method="post" id="contactform">

							<input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
								value="<?=Yii::$app->request->csrfToken?>" />

							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12">
									<div class="form-group   contact-dropdown-list">
										<select class="test validatesel form-control" id="enqtype" name="type">
											<option value="" class="sel" selected>Select</option>
											<option value="General Information">General Information</option>
											<!-- <option value="HR">HR</option> -->
											<option value="C&I Enquires">C&I Enquires</option>
											<option value="International Business">International Business</option>

										</select>
										<span id="sel_error"></span>
									</div>

									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12">
											<div class="form-group  ">

												<input type="text" id="enqcname" name="cname" class="form-control"
													placeholder=" Your Company Name">

												<span id="nm_error"></span>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6">
											<div class="form-group   ">

												<input type="text" id="enqemail_id" name="email_id" class="form-control"
													placeholder=" Your Email Id">

												<span id="em_error"></span>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6">
											<div class="form-group   ">

												<input type="text" id="enqphone" name="phone" class="form-control"
													placeholder=" Your Contact No.">

												<span id="phone_error"></span>
											</div>
										</div>
									</div>

									<div class="form-group ">
										<textarea class="form-control"  id="enqmessage" name="message"> Write Here
                                       </textarea>
				

				  <span id=" msg_error"></span>
				</div>
				
								 <div id="RecaptchaField2" class="g-recaptcha" style=" "></div>
				
			  </div>
			</div>
			<button type="submit" class="btnallred btn mb-0 btn-sm btn-theme-danger waves-effect waves-light" id="contact_form" onclick="return contactQuery('contactform');"><span>SUBMIT &nbsp; &gt;</span> </button>
		  </form>
		</div>
	</div>
  </div>
</div>
<input type="hidden" value="<?= $captcha_code_val; ?>" id="cap_val">
				</div>
</section>



         <section class="n_video_section bg_sec  mrb_0 contact-sec2bottom desktop-view" id="for-business-queries">
            <div class="container">

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12">
	<div class="visit-heading">
	  <h3>Visit Us</h3>          
	</div>
	<div class="contact-visit">
	  <ul class="nav nav-pills mb-5" id="pills-tab" role="tablist">
<?php 
		if(!empty($category)){
			$count = 1;
			foreach($category as $cat){
				$class = '';
				if($count == 1){
					$class = 'active';
				}
?>	  
			<li class="nav-item">
			  <a class="nav-link contact-visit-link <?= $class; ?>" id="pills-home-tab<?= $count; ?>" data-toggle="pill" href="#<?= str_replace(" ", "", $cat); ?>" role="tab" aria-controls="pills-home" aria-selected="true"><?= $cat; ?></a>
			</li>
			<span class="video-pipe-small"></span>
<?php
			$count++;
		}
	}
?>		
	  </ul>
	  <div class="tab-content cust-tab-visit pt-2 pl-1" id="pills-tabContent">
<?php 
		if(!empty($category)){
			$count = 1;
			foreach($category as $key => $cat){
				$class = '';
				if($count == 1){
					$class = 'show active';
				}
?>		  
		<div class="tab-pane fade visit visit-border-bottom <?= $class; ?>" id="<?= str_replace(" ", "", $cat); ?>" role="tabpanel" aria-labelledby="pills-home-tab">
<?php 
	$pageData = $model->getPageData($key);
	if(!empty($pageData)){
		foreach($pageData as $value){
?>
		
		  <div class="row mb-4">
			<div class="col-lg-3 col-md-3 col-sm-3">
			  <img src="<?= BASE_BE_URL; ?>uploads/contact/<?= $value['image']; ?>" class="img-fluid address-img">
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9">
			  <div>
				<div class="row">
				  <div class="col-lg-6 col-md-6 col-sm-6">
					<h3><?= $value['title']; ?></h3>
					<div class="visit-pra">
						<?= $value['address1']; ?>
					</div>					
					<p><a href="<?= $value['direction_link']; ?>" target="_blank">Directions</a></p>
				  </div>
				  <div class="border-bottom mobile-view"></div>
				  <?php if(!empty($value['address2'])){ ?>
					  <div class="col-lg-6 col-md-6 col-sm-6">
						<h3>Corporate Office</h3>
						<div class="visit-pra">
							<?= $value['address2']; ?>
						</div>
						<p><a href="<?= $value['direction_link2']; ?>" target="_blank">Directions</a></p>
					  </div>
				  <?php } ?>
				  <div class="border-bottom"></div>
				</div>
			  </div>
			</div>
		  </div>
<?php
		}
	}
?>

		</div> 
<?php
				$count++;
			}
		}
?>
		  
	  </div>
	</div>
  </div>
</div>
</div>
</section>




<!-- mobile -->

<section class="newsroom-sec2bottom mobile-view">
<div class="row justify-content-center">
  <div class="col-lg-12 col-md-12 col-sm-12">
	<div class="container">
	  <div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4 text-center">
			<div class="visit-heading">
			  <h3>Visit Us</h3>          
			</div>
			<br>
			<select class="test form-control" id="contact-sel">
<?php 
		if(!empty($category)){
			$count = 1;
			foreach($category as $cat){
				$class = '';
				if($count == 1){
					$class = 'selected="selected"';
				}
?>			
					<option value="<?= $count; ?>" <?= $class; ?>><?= $cat; ?></option>
<?php 
				$count++;
			}
		}
?>		
			  
			</select>
		</div>
	  </div>
	  <br>
<?php 
if(!empty($category)){
	$count = 1;
	foreach($category as $key => $cat){
		
?>	  
	  <div class="pr-price d<?= $count; ?>">
<?php 
$pageData = $model->getPageData($key);
if(!empty($pageData)){
	foreach($pageData as $value){
?>	  
		<div class="row mb-4" id="india-address">
			<div class="col-lg-3 col-md-3 col-sm-3 text-center">
			  <img src="<?= BASE_BE_URL; ?>uploads/contact/<?= $value['image']; ?>" class="img-fluid">
			</div>

			<div class="col-lg-9 col-md-9 col-sm-9">
			  <div>
				<div class="row">
				  <div class="col-lg-6 col-md-6 col-sm-6 ">
					<h3><?= $value['title']; ?></h3>
					<?= $value['address1']; ?>
					<p>
					  <a href="<?= $value['direction_link']; ?>" target="_blank">Direction</a>
					</p>
				  </div>
				  <div class="border-bottom mobile-view"></div>
				  <?php if(!empty($value['address2'])){ ?>
				  <div class="col-lg-6 col-md-6 col-sm-6">
					<h3>Corporate Office</h3>
					<?= $value['address2']; ?>
					<p>
					  <a href="<?= $value['direction_link2']; ?>" target="_blank">Directions</a>
					</p>
				  </div>
				  <?php } ?>
				  <div class="border-bottom"></div>
				</div>
			  </div>
			</div>

		 </div>
<?php
	}
}
?>
	  </div>
<?php 
			$count++;
	}
}
?>

	</div>
  </div>
  </div>

</section> 

<!-- mobile -->

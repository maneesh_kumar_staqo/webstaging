<?php
define('BASE_URL', 'https://herofutureenergies.com/webstaging/');
define('IMG_URL', 'https://herofutureenergies.com/webstaging/frontend/web/');
define('BASE_BE_URL', 'https://herofutureenergies.com/webstaging/backend/web/');
define('IMG_UPLOAD_URL', '../../backend/web/uploads/');
include_once(dirname(__FILE__) . '/urlmanager.php');
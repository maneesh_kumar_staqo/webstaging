// CSR page project gallery

$('#csr-slide-img').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    
    fade: true,
    nextArrow:"<img class='a-left control-c prev csr-cust-slick-control-left slick-next' src='"+ JS_BASE_URL +"images/left.png'>",
    prevArrow:"<img class='a-right control-c csr-cust-slick-control-next next slick-next' src='"+ JS_BASE_URL +"images/right.png'>",
    asNavFor: '#csr-slide'
  });
  $('#csr-slide').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: '#csr-slide-img'
  });




$('#csr-slide-img2').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    
    fade: true,
    nextArrow:"<img class='a-left control-c prev csr-cust-slick-control-left slick-next' src='"+ JS_BASE_URL +"images/left.png'>",
    prevArrow:"<img class='a-right control-c csr-cust-slick-control-next next slick-next' src='"+ JS_BASE_URL +"images/right.png'>",
    asNavFor: '#csr-slide2'
  });
  $('#csr-slide2').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: true,
    arrows: false,
    asNavFor: '#csr-slide-img2'
  });




$('#csr-slide-img3').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    
    fade: true,
    nextArrow:"<img class='a-left control-c prev csr-cust-slick-control-left slick-next' src='"+ JS_BASE_URL +"images/left.png'>",
    prevArrow:"<img class='a-right control-c csr-cust-slick-control-next next slick-next' src='"+ JS_BASE_URL +"images/right.png'>",
    asNavFor: '#csr-slide3'
  });
  $('#csr-slide3').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: '#csr-slide-img3'
  });




$('#csr-slide-img4').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    
    fade: true,
    nextArrow:"<img class='a-left control-c prev csr-cust-slick-control-left slick-next' src='"+ JS_BASE_URL +"images/left.png'>",
    prevArrow:"<img class='a-right control-c csr-cust-slick-control-next next slick-next' src='"+ JS_BASE_URL +"images/right.png'>",
    asNavFor: '#csr-slide4'
  });
  $('#csr-slide4').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: '#csr-slide-img4'
  });





  $("#csr-btn1").click(function(){
   // alert("fd");
    $('.csr-tab-btn').addClass('select-color');
     $('.sel2').removeClass('select-color');
     $('.sel3').removeClass('select-color');
     $('.sel4').removeClass('select-color');
  });
 $("#csr-btn2").click(function(){
   // alert("fd");
    $('.select-active').addClass('select-color');
     $('.sel1').removeClass('select-color');
     $('.sel3').removeClass('select-color');
     $('.sel4').removeClass('select-color');
  });
 $("#csr-btn3").click(function(){
   // alert("fd");
    $('.select-active2').addClass('select-color');
     $('.sel2').removeClass('select-color');
     $('.sel1').removeClass('select-color');
     $('.sel4').removeClass('select-color');
  });
 $("#csr-btn4").click(function(){
   // alert("fd");
    $('.select-active3').addClass('select-color');
     $('.select-active2').removeClass('select-color');
     $('.sel2').removeClass('select-color');
     $('.sel1').removeClass('select-color');
     $('.sel3').removeClass('select-color');
  });


 $(document).ready(function(){
     $(function () {
        $('.pr-price').hide();
        $('.d1').show();
        $('#select').on("change",function () {
        $('.pr-price').hide();
        $('.d'+$(this).val()).show();
        $('.sh').removeClass('inactive');
        $('.cat').addClass('inactive');
        }).val(1); // reflect the div shown 
      });

  });

// category select







$("#video1").mouseover(function(){
    $(".share-video").slideDown();
    $(".fullscreenbtn").slideDown();
});

$("#video1").mouseleave(function(){
    $(".share-video").slideUp();
    $(".fullscreenbtn").slideUp();
});

$(".share-video").click(function(){
    $(".share-video-icons").slideDown();
});

$(".share-video-icons").mouseleave(function(){
    $(".share-video-icons").slideUp();
});

/* header */

$(".hamburger").click(function(){
    $("body").toggleClass("menu-icon-fixed");
});




/* header ends here */

// overlay video start here
$(".onyoutube").click(function(){
    $("#video_overlay").css("display","block");
    $("body").addClass("menu-icon-fixed");
});
function openvideo(unid) {
    $("#video_overlay"+unid).css("display", "block");
    $('#youtube' + unid)[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
        $("body").removeClass("menu-icon-fixed");


    $("body").addClass("menu-icon-fixed");
}

function pauseyoutube(unid) {
    $("#video_overlay" + unid).hide();
    var iframe = document.getElementById('youtube' + unid);
    iframe.src = iframe.src;
    player.stopVideo();

}



$(".onyoutube3").click(function(){
    $("#video_overlay3").css("display","block");
    $("body").addClass("menu-icon-fixed");
});


function videomodal(){

    $("#video_overlay1").css("display","block");
    $("body").addClass("menu-icon-fixed");
}

$("#video_overlay").click(function(){
    $("#video_overlay").css("display","none");
    $('#iframeoff')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
    $("body").removeClass("menu-icon-fixed");
});


$("#video_overlay3").click(function(){
    $("#video_overlay3").css("display","none");
    $('#youtube3')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
    $("body").removeClass("menu-icon-fixed");
});

// Homepage banner mobile overlay video start here
$("#mob-banner-play").click(function(){
    $("#video_overlay1").css("display","block");
    $("body").addClass("menu-icon-fixed");
});

$("#video_overlay1").click(function(){
    $("#video_overlay1").css("display","none");
    $('#iframeoff')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
    $("body").removeClass("menu-icon-fixed");
});





  $('#profile-tab').click(function(){
  //alert('tab 2');
  $('#profile').addClass('show active');
  $("#home").removeClass('show active');
});






$("#button").click(function() {

    $('html, body').animate({

        scrollTop: $("#divscroll").offset().top
    }, 2000);
});

$(document).ready(function(){
     $(function () {
        $('.pr-price').hide();
        $('.d1').show();
        $('#select').on("change",function () {
        $('.pr-price').hide();
        $('.d'+$(this).val()).show();
        $('.sh').removeClass('inactive');
        $('.cat').addClass('inactive');
        $("#mobile-gallery-slider").removeClass("inactive");
        $(".statehide-show").addClass("inactive");
        }).val(1); // reflect the div shown 
      });

  });







  $('#pills-home-tab').click(function(){
      $('#global-btn').removeClass('global-btnnone');
      $('#global-btn').addClass('global-btn');
  });  
  $('#pills-profile-tab').click(function(){
      $('#global-btn').removeClass('global-btn');
      $('#global-btn').addClass('global-btnnone');
  }); 

 function deskdivtoggle() {

  var dots = document.getElementById("deskdots");
  var moreText = document.getElementById("deskmore");
  var btnText = document.getElementById("deskmyBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = '<span>' + " Read More  &nbsp; >" + '</span>'; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = '<span>' + " Read Less  &nbsp; >" + '</span>';
    moreText.style.display = "inline";
  }
}






// project page ends


$(".gallery-popup").click(function(){
  console.log('okkk');
  $(".gallery-modal").css("display","block");
 //var current_img = $(this).attr("src");
  //console.log(current_img);
  $("body").addClass("menu-icon-fixed");
  //$(".gallery-modal-content").attr("src",current_img);

});
$(".gallery-close").click(function(){
  $("body").removeClass("menu-icon-fixed");
  $(".gallery-modal").css("display","none");
});

$('.windtab-slider').owlCarousel({
    loop:false,
    margin:20,
    nav:false,
    dots:false,
    autoplay:true, 
    autoplaySpeed: 1500,
    smartSpeed: 2000,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:4
        },
        1000:{
            items:5
        }
    }  
});

$('#gridtab-slider').owlCarousel({
    loop:false,
    margin:20,
    nav:false,
    dots:false,
    autoplay:true, 
    autoplaySpeed: 1500,
    smartSpeed: 2000,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:4
        },
        1000:{
            items:5
        }
    }  
});

$('#energytab-slider').owlCarousel({
  loop:false,
    margin:20,
    nav:false,
    dots:false,
    autoplay:true, 
    autoplaySpeed: 1500,
    smartSpeed: 2000,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:4
        },
        1000:{
            items:5
        }
    }  
});



// career page js ----------------------

$('#career-top-banner').owlCarousel({
    animateOut: 'fadeOut', 
    loop:true,
    margin:10,
    nav:false,
    centerMode:true,
    dots:true, 
    autoplay:true, 
    autoplaySpeed: 1500,
    smartSpeed: 2000,  
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }

});

var owl = $('#work-place-slider');
   owl.owlCarousel({
    
     loop: true,
     margin: 10,
     dots:false,
     autoplay: true,
     autoplayTimeout: 1000,
      autoplaySpeed: 3000,
     slideTransition: 'linear',
     autoplayHoverPause: false,
     responsive:{
        0:{
            items:2,
            nav:false
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:4,
            nav:false,
            loop:true
        }
    }
   });
   $('.owl-carousel').on('mouseover', function() {
     owl.trigger('play.owl.autoplay', [2500]);
   })
   $('.owl-carousel').on('mouseleave', function() {
     owl.trigger('stop.owl.autoplay');
   });



$('#leadeeship-forum').owlCarousel({
    loop:true,
    dots:false,
    responsiveClass:true,
    nav: true,
    autoplay:true, 
    autoplaySpeed: 2500,
    smartSpeed: 2000,
    navText: ["<img src='img/pro/left.png'>","<img src='img/pro/right.png'>"],
    
    responsive:{
        0:{
            items:1,
        
        },
        600:{
            items:1,
          
        },
        1000:{
            items:1,
            nav:true,

        }
    }
});

 
 $(document).ready(function() {  
    $('.career-img-tab-1').hide();
    $('.career-tab-content4').hide();
    $('.career-tab-content5').hide();
    var src = $('.career-img-tab1').attr('src');
    $('.career-divimg').html('<img src="'+src+'" style="width:100%;">');
    $('.career-img-tab-1 img').click(function(){
        var src= $(this).attr('src');
        var tab_id= $(this).attr('data-tab');
        $('.career-divimg').html('<img src="'+src+'" style="width:100%;">');
        $('.career-tab-content4').hide();
        $('.career-tab-content6').show();
        $('.career-tab-content5').hide();
        $('.career-img-tab-3').show();   
        $('.career-img-tab-2').show();
        $('.career-img-tab2').removeClass('career-sec-tab'); 
        $('.career-img-tab-1').hide(); 
    });
    $('.career-img-tab-2 img').click(function(){
        var src= $(this).attr('src');
        var tab_id= $(this).attr('data-tab');
        $('.career-divimg').html('<img src="'+src+'" style="width:100%;">');
        $('.career-tab-content6').hide();
        $('.career-tab-content5').show();
        $('.career-tab-content4').hide();
        $('.career-img-tab-3').show();   
        $('.career-img-tab-2').hide(); 
        $('.career-img-tab-1').show();   
    });
    $('.career-img-tab-3 img').click(function(){
      var src= $(this).attr('src');
      var tab_id= $(this).attr('data-tab');
      $('.career-divimg').html('<img src="'+src+'" style="width:100%;">');
      $('.career-tab-content6').hide();
      $('.career-tab-content4').show();
      $('.career-tab-content5').hide();
      $('.career-img-tab2').addClass('career-sec-tab');
      $('.career-img-tab-3').hide();  
      $('.career-img-tab-1').show();
      $('.career-img-tab-2').show();
    });

    $('.global-1').click(function(){
      $('.career-tab-content1').show(); 
      $('.career-tab-content2').hide(); 
      $('.global-1').addClass('color-red2'); 
      $('.global-2').removeClass('color-red2');         
    });
    $('.global-2').click(function(){
      $('.career-tab-content1').hide();   
      $('.career-tab-content2').show();
      $('.global-1').removeClass('color-red2'); 
      $('.global-2').addClass('color-red2');  
         
    });
});


$('#student-mob-banner').owlCarousel({
   loop:true,
    margin:10,
    nav:false,
    centerMode:true,
    dots:false, 
    autoplay:true, 
    autoplaySpeed: 1500,  
    smartSpeed: 2000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }

});




// career page js ends here


$('#learn-to-win').owlCarousel({
  loop:true,
    margin:20,
    nav:true,
    dots:false,
    autoplay:true, 
    autoplaySpeed: 1500,
    smartSpeed: 2000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }  
});


// RTS page js -------------------------- 

$('#rts-top-banner').owlCarousel({
    animateOut: 'fadeOut', 
    loop:true,
    margin:10,
    nav:false,
    centerMode:true,
    dots:true, 
    autoplay:true, 
    autoplaySpeed: 1500, 
    smartSpeed: 2000, 
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }

});

$('#rts-top-mob-banner').owlCarousel({
   loop:true,
    margin:10,
    nav:true,
    centerMode:true,
    dots:false, 
    autoplay:true, 
    autoplaySpeed: 1500,
    smartSpeed: 2000,  
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }

});

$('#appreciation-mob-slider').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    centerMode:true,
    dots:false, 
    autoplay:true, 
    autoplaySpeed: 1500,
    smartSpeed: 2000,  
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }

});


$('#appreciation').owlCarousel({
    animateOut: 'fadeOut', 
   loop:true,
    dots:false,
    responsiveClass:true,
    nav: true,
    autoplay:true, 
    autoplaySpeed: 1000,
    smartSpeed: 2000,
    navText: ["<img src='img/pro/left.png'>","<img src='img/pro/right.png'>"],    
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:true,
        }
    }
});  


$('#esteemed-client').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    centerMode:true,
    dots:false, 
    autoplay:true, 
    autoplaySpeed: 800,
    smartSpeed: 900,  
    responsive:{
        0:{
            items:2
        },
        600:{
            items:4
        },
        1000:{
            items:5
        }
    }

});


$('#benefits-mob-slider').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    centerMode:true,
    dots:false, 
    autoplay:true, 
    autoplaySpeed: 1500,  
    smartSpeed: 2000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }

});

$('#reasons-mob-slider').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    centerMode:true,
    dots:false, 
    autoplay:true, 
    autoplaySpeed: 1500, 
    smartSpeed: 2000, 
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }

});





// RTS page js ends here



// policies page 

$('#policies-top-banner').owlCarousel({
    animateOut: 'fadeOut', 
    loop:true,
    margin:10,
    nav:false,
    centerMode:true,
    dots:true, 
    autoplay:true, 
    autoplaySpeed: 1500,
    smartSpeed: 2000,  
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }

});

$('#policies-mob-top-banner').owlCarousel({
    animateOut: 'fadeOut', 
    loop:true,
    margin:10,
    nav:true,
    centerMode:true,
    dots:false, 
    autoplay:true, 
    autoplaySpeed: 1500,  
    smartSpeed: 2000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }

});


// policies page ends here



// video page 

    $(document).ready(function(){
     $(function () {
        $('.pr-price').hide();
        $('.d1').show();
        $('#select').on("change",function () {
        $('.pr-price').hide();
        $('.d'+$(this).val()).show();
        $('.sh').removeClass('inactive');
        $('.cat').addClass('inactive');
        }).val(1); 
      });

  }); 

    $(document).ready(function(){
     $(function () {
        $('.pr-price').hide();
        $('.d1').show();
        $('#contact-sel').on("change",function () {
        $('.pr-price').hide();
        $('.d'+$(this).val()).show();
        $('.sh').removeClass('inactive');
        $('.cat').addClass('inactive');
        }).val(1); 
      });

  }); 

    $(document).ready(function(){
     $(function () {
        $('.pr-price1').hide();
        $('.dd1').show();
        $('.category-select').on("change",function () {
		$('.pr-price1').hide();
		$('.dd'+$(this).val()).show();
        $('.dd1').removeClass('inactive');
         $('.sh').addClass('inactive');
            $('.cat').removeClass('inactive');
        }).val(1); 
      });
	  
	  $('.category-select2').on("change",function () {
		if($(this).val() == "no_val_3" || $(this).val() == "no_val_4" || $(this).val() == "no_val_5"){
			var linkVal = $(this).find('option:selected').attr("linkVal");
			window.location.href = linkVal;
		}	
      })
	  
    $('.redirectpage_select').on("change",function () {
		if($(this).val() == ""){
			var linkVal = $(this).find('option:selected').attr("linkVal");
			window.location.href = linkVal;
		}	
	})

  });



// $("#video-banner-play").click(function(){
  
//     $("#video-play-text").css("display","none");
//     $("iframe").css("opacity","1");
//     $("iframe")[0].src += "&autoplay=1";

// });

$('.video-big').click(function(){
    console.log('hi');
     $('.iframe').addClass('video-z');
     $('.video-big-icon').hide();
});



//bgel 




$('.card-header a').click(function() {
    $("i", this).toggleClass("fa-minus fa-plus");
});



// about page

$('#top_banner').owlCarousel({
    animateOut: 'fadeOut',    
    loop:false,
    margin:10,
    nav:false,
    centerMode:true,
    dots:true, 
    autoplay:true, 
    autoplaySpeed: 1500,
    smartSpeed: 2000, 
    responsive:{
        0:{
            items:1,
        },
        600:{
            items:1
        },
        1000:{
            items:1,
            loop:true
        }
    }

});

  $('#top_banner_mob').owlCarousel({
    animateOut: 'fadeOut',    
    loop:false,
    margin:10,
    nav:true,
    centerMode:true,
    dots:false, 
    autoplay:true, 
    autoplaySpeed: 1500,
    smartSpeed: 2000, 
    responsive:{
        0:{
            items:1,
            loop:true
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }

});


 $('#board-indian').owlCarousel({
    //animateOut: 'fadeOut',
    loop:true,
    margin:50,
    nav:true,
    dots:false,  
    //autoplayTimeout: 1000,
    //smartSpeed: 1000,
    smartSpeed: 2000,
    //autoplaySpeed: 3000, 
    responsive:{
        0:{
            items:1,
            margin:0,
        },
        600:{
            items:1
        },
        1000:{
            items:4
        }
    }
});
$('#board-global').owlCarousel({
    //animateOut: 'fadeOut',
    loop:true,
    margin:50,
    nav:true,
    dots:false,  
    smartSpeed: 2000,  
    responsive:{
        0:{
            items:1,
            margin:0,
        },
        600:{
            items:1
        },
        1000:{
            items:4
        }
    }
});

$('#indianmanagementteam').owlCarousel({
    loop:true,
    margin:50,
    nav:true,
    dots:false,  
    smartSpeed: 2000,  
    responsive:{
        0:{
            items:1,
            margin:0,
        },
        769:{
            items:1
        },
        1000:{
            items:4
        }
    }
});
$('#about-gallery11-slider-5').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    dots:false,  
    autoplay:true, 
    smartSpeed: 2000, 
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});


$(document).ready(function() {  
    $('.global-1').click(function(){
      $('.tab-content1').show(); 
      $('.tab-content2').hide(); 
      $('.global-1').addClass('color-red2') 
      $('.global-2').removeClass('color-red2')         
  });
  $('.global-2').click(function(){
      $('.tab-content1').hide();   
      $('.tab-content2').show();
      $('.global-1').removeClass('color-red2') 
      $('.global-2').addClass('color-red2')   
         
  });
});
    $(document).ready(function () {
      $('.vid-item').each(function(index){

        $(this).on('click', function(){    

          var current_index = index+1;
          console.log(current_index);
          $('.vid-item .thumb').removeClass('active');

          $('.vid-item:nth-child('+current_index+') .thumb').addClass('active');
          
        });
      });
    });


// video page ends here

// gallery 4 

var slideIndex4 = 1;
showSlides4(slideIndex4);

function plusSlides4(n) {
  showSlides4(slideIndex4 += n);
}

function currentSlide4(n) {
  showSlides4(slideIndex4 = n);
}

function showSlides4(n) {
  var i;
  var slides4 = document.getElementsByClassName("mySlides4");
  var dots4 = document.getElementsByClassName("demo4");
  var captionText4 = document.getElementById("caption4");
  if (n > slides4.length) {slideIndex4 = 1}
  if (n < 1) {slideIndex4 = slides4.length}
  for (i = 0; i < slides4.length; i++) {
      slides4[i].style.display = "none";
  }
  for (i = 0; i < dots4.length; i++) {
      dots4[i].className = dots4[i].className.replace(" active", "");
  }
  if (slides4[slideIndex4-1]) {
    slides4[slideIndex4-1].style.display = "block";
}
    if (dots4[slideIndex4-1]) {
          dots4[slideIndex4-1].className += " active";
    }
    if(captionText4){
        captionText4.innerHTML = dots4[slideIndex4-1].alt; 
    }

 
}
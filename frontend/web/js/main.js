
$('.n_home_slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: false,
  autoplaySpeed: 2000,
  dots:true,
  arrows:false,
  
});

$('.blog_slider_n').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: false,
  autoplaySpeed: 2000,
  dots:true,
  arrows:false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
         dots: false,
         arrows:true
      }
    }
  ]

});
$('.vlog_slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  dots:true,
  arrows:false
});

$(document).ready(function(){
    
  })
  $(window).scroll(function() {

    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
    } else {
      $('.back-to-top').fadeOut('slow');
    }
  });

  $('.back-to-top').click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 1500, 'easeInOutExpo');
    return false;
  });

// .

$('.slider-default-item-1').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  dots:true,
  arrows:false
});


// $('.n_home_slider').slick({
//   slidesToShow: 1,
//   slidesToScroll: 1,
//   autoplay: false,
//   autoplaySpeed: 2000,
//   dots:true,
//   arrows:false
// });



$('.n1_slider_partner').slick({
     dots: false,
            vertical: true,
            loop: false,
            arrows:false,
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
             autoplay: true,
             draggable: true,
             infinite: true,
});
$('.n_static_slider').slick({
     dots: false,
            vertical: true,
            loop: false,
            arrows:false,
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: false,
             autoplay: true,
             draggable: true,
             infinite: true,


});
$('.highlights_nd1').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  arrows:false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]

});
$('.c_and_i_feature_product_list').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  arrows:true,
  dots:false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]

});

setInterval(function()
{

var next = $('.divcustom .current').removeClass('current').next('div');
if (!next.length) next = next.prevObject.siblings(':first');
next.addClass('current');


}, 2000);

setInterval(function()
{

var next = $('.divcustom2 .current').removeClass('current').next('div');
if (!next.length) next = next.prevObject.siblings(':first');
next.addClass('current');


}, 2000);



// 

$('.testimoniial_sllider_n2_list').slick({
  slidesToShow: 1,
  infinite: true,
  draggable: false,
  slidesToScroll: 1,
  autoplay: false,
  autoplaySpeed: 5000,
  dots:false,
  nextArrow: $("#pastprevious"),
  prevArrow: $('#pastnext'),  


  arrows:true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
          draggable: false,

        dots: false
      }
    },
    {
      breakpoint: 600,

      settings: {
        slidesToShow: 1,
          draggable: false,

        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
          draggable: false,

        arrows:true,
      }
    }

  ]
});

// n_static_slider




// custome accordian 
$(document).ready(function(){
  $('.theme-accordian-list > .theme-accordian-list-item > .answer').hide();
 
    
  $('.theme-accordian-list > .theme-accordian-list-item').click(function() {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active").find(".answer").slideUp();
    } else {
      $(".theme-accordian-list > .theme-accordian-list-item.active .answer").slideUp();
      $(".theme-accordian-list > .theme-accordian-list-item.active").removeClass("active");
      $(this).addClass("active").find(".answer").slideDown();
    }
    return false;
  });
  
});
// end accordian 


// upcoming_event_slider
$('.upcoming_event_slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: false,
  autoplaySpeed: 2000,
  dots:false,
  arrows:true
});




    $(".lightgallery").lightGallery({

        // Please read about gallery options here: http://sachinchoolur.github.io/lightGallery/docs/api.html

        // lightgallery core 
        selector: '.lg-trigger',
        mode: 'lg-fade', // Type of transition between images ('lg-fade' or 'lg-slide').
        height: '100%', // Height of the gallery (ex: '100%' or '300px').
        width: '100%', // Width of the gallery (ex: '100%' or '300px').
        iframeMaxWidth: '100%', // Set maximum width for iframe.
        loop: true, // If false, will disable the ability to loop back to the beginning of the gallery when on the last element.
        speed: 600, // Transition duration (in ms).
        closable: true, // Allows clicks on dimmer to close gallery.
        escKey: true, // Whether the LightGallery could be closed by pressing the "Esc" key.
        keyPress: true, // Enable keyboard navigation.
        hideBarsDelay: 5000, // Delay for hiding gallery controls (in ms).
        controls: true, // If false, prev/next buttons will not be displayed.
        mousewheel: true, // Chane slide on mousewheel.
        download: false, // Enable download button. By default download url will be taken from data-src/href attribute but it supports only for modern browsers. If you want you can provide another url for download via data-download-url.
        counter: true, // Whether to show total number of images and index number of currently displayed image.
        swipeThreshold: 50, // By setting the swipeThreshold (in px) you can set how far the user must swipe for the next/prev image.
        enableDrag: true, // Enables desktop mouse drag support.
        enableTouch: true, // Enables touch support.

        // thumbnial plugin
        thumbnail: true, // Enable thumbnails for the gallery.
        showThumbByDefault: false, // Show/hide thumbnails by default.
        thumbMargin: 5, // Spacing between each thumbnails.
        toogleThumb: true, // Whether to display thumbnail toggle button.
        enableThumbSwipe: true, // Enables thumbnail touch/swipe support for touch devices.
        exThumbImage: 'data-exthumbnail', // If you want to use external image for thumbnail, add the path of that image inside "data-" attribute and set value of this option to the name of your custom attribute.

        // autoplay plugin
        autoplay: false, // Enable gallery autoplay.
        autoplayControls: true, // Show/hide autoplay controls.
        pause: 6000, // The time (in ms) between each auto transition.
        progressBar: true, // Enable autoplay progress bar.
        fourceAutoplay: false, // If false autoplay will be stopped after first user action

        // fullScreen plugin
        fullScreen: true, // Enable/Disable fullscreen mode.

        // zoom plugin
        zoom: true, // Enable/Disable zoom option.
        scale: 0.5, // Value of zoom should be incremented/decremented.
        enableZoomAfter: 50, // Some css styles will be added to the images if zoom is enabled. So it might conflict if you add some custom styles to the images such as the initial transition while opening the gallery. So you can delay adding zoom related styles to the images by changing the value of enableZoomAfter.

        // video options
        videoMaxWidth: '1000px', // Set limit for video maximal width.

        // Youtube video options
        loadYoutubeThumbnail: true, // You can automatically load thumbnails for youtube videos from youtube by setting loadYoutubeThumbnail true.
        youtubeThumbSize: 'default', // You can specify the thumbnail size by setting respective number: 0, 1, 2, 3, 'hqdefault', 'mqdefault', 'default', 'sddefault', 'maxresdefault'.
        youtubePlayerParams: { // Change youtube player parameters: https://developers.google.com/youtube/player_parameters
            modestbranding: 0,
            showinfo: 1,
            controls: 1
        },

        // Vimeo video options
        loadVimeoThumbnail: true, // You can automatically load thumbnails for vimeo videos from vimeo by setting loadYoutubeThumbnail true.
        vimeoThumbSize: 'thumbnail_medium', // Thumbnail size for vimeo videos: 'thumbnail_large' or 'thumbnail_medium' or 'thumbnail_small'.
        vimeoPlayerParams: { // Change vimeo player parameters: https://developer.vimeo.com/player/embedding#universal-parameters 
            byline : 1,
            portrait : 1,
            title: 1,
            color : 'CCCCCC',
            autopause: 1
        },

        // hash plugin (unique url for each slides)
        hash: true, // Enable/Disable hash plugin.
        hgalleryId: 1, // Unique id for each gallery. It is mandatory when you use hash plugin for multiple galleries on the same page.

        // share plugin
        share: false, // Enable/Disable share plugin.
            facebook: true, // Enable Facebook share.
            facebookDropdownText: 'Facebook', // Facebok dropdown text.
            twitter: true, // Enable Twitter share.
            twitterDropdownText: 'Twitter', // Twitter dropdown text.
            googlePlus: true, // Enable Google Plus share.
            googlePlusDropdownText: 'Google+', // Google Plus dropdown text.
            pinterest: true, // Enable Pinterest share.
            pinterestDropdownText: 'Pinterest' // Pinterest dropdown text.

    });



// video page ends here

// gallery 4 

var slideIndex4 = 1;
showSlides4(slideIndex4);

function plusSlides4(n) {

  showSlides4(slideIndex4 += n);
}

function currentSlide4(n) {

  $(".mySlides4").hide();
  $("#gallermodal").addClass('active');
  $("#gal"+n).show();
}

// function showSlides4(n) {
//   var i;
//   var slides4 = document.getElementsByClassName("mySlides4");
//   var dots4 = document.getElementsByClassName("demo4");
//   var captionText4 = document.getElementById("caption4");
//   debugger
//   if (n > slides4.length) {slideIndex4 = 1}
//   if (n < 1) {slideIndex4 = slides4.length}
//   for (i = 0; i < slides4.length; i++) {
//       slides4[i].style.display = "none";
//   }
//   for (i = 0; i < dots4.length; i++) {
//       dots4[i].className = dots4[i].className.replace(" active", "");
//   }
//   slides4[slideIndex4-1].style.display = "block";
//   dots4[slideIndex4-1].className += " active";
//   captionText4.innerHTML = dots4[slideIndex4-1].alt;
// }



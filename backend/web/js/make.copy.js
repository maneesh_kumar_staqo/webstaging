$(document).ready(function() {
	var authorDiv = $('#authordiv');
	var i = $('#authordiv p').size() + 1;
	$('#addfield').click(function() {
		var idVal = "'#author-id-";
		$('<p><input name="content_author_name[]" class="form-control ui-autocomplete-input author-id" onkeypress="getAutocompleteAuthor(' + idVal + i + "'" + ', '+ i +')" id="author-id-' + i + '" autocomplete="off" /><a class="remove" id="remfield">x</a><input type="hidden" name="content_author_id[]" id="get-author-' + i + '" /></p>').appendTo(authorDiv);
		i++;
	});

	$('body').on('click', '#remfield', function() {
		$(this).parents("p").remove();
	});
});


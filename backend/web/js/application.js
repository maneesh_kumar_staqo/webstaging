function removekmp(id, kmp_gallery_id) {
    $('#' + id).remove();
    if (kmp_gallery_id != '' && kmp_gallery_id != 'kmp') {
        $.ajax({
            type: 'get',
            url: JS_BASE_URL + 'keymedicalproceduresmaster/removekmpgallery',
            data: 'kmp_gallery_id=' + kmp_gallery_id,
            success: function (response) {

            }
        });
    }
}

$(function () {
    $("#datepicker").datepicker({dateFormat: "yy-mm-dd"});

});

// for get all page url
function getPageUrl(fieldVal, fieldId){
	fieldVal = $.trim(fieldVal);
	var replaceVal = fieldVal.replace(/[&\/\\#,+()$~%.'!^":` @_*?<>{}]/g,"-").toLowerCase();
	var replaceVal2 = replaceVal.replace(/-{2,}/g, "-");
	$('#'+fieldId).val(replaceVal2);
}

// For save page layout
function savePageLayout(page_type,table_name,page_id){
	var priority = $("#pagePriority_"+page_type).val();
	$.ajax({
		type: 'get',
		url: JS_BASE_URL + 'pagelayout/savepagepriority',
		data: 'page_type=' + page_type + '&table_name=' + table_name + '&page_id=' + page_id + '&priority=' + priority,
		success: function (response) {
			// alert(response);
		}
	});
}

// For delete image from all pages
function deleteImg(div_id){
	$("#uploaded_image_" + div_id).val('');
	$("#delete_img_" + div_id).remove();
}

<?php
$URL_MANAGER = [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    //'suffix' => '/',
    'rules' => [
        // ['pattern' => 'social/<keyword>', 'route' => 'social/index', 'defaults' => ['keyword' => '']],
		'login' => 'site/login',
		'dashboard' => 'site/index',
		'logout' => 'site/logout',
		'change-password' => 'site/changepassword',
		'page-layout' => 'pagelayout',
		'page-layout-create' => 'pagelayout/create',
	],
];
return $URL_MANAGER;
?>
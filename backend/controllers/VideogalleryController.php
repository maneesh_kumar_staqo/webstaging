<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use yii\helpers\ArrayHelper;
use backend\models\Banners;
use backend\models\Category;
use backend\models\Pagelayout;
use backend\models\Videogallery;


/**
 * Videogallery controller
 */
class VideogalleryController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'form'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new Videogallery();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	/**
     * Create new page layout.
     *
     * @return string
     */
    public function actionCreate($page_id=''){
		$model = new Videogallery();
		// for get category name
		$categoryData[""] = "Select Category";
		$data = ArrayHelper::map(Category::find()->where(['and', "type = 'video_gallery'"])->all(),'id','title');
		foreach($data as $key => $val){
			$categoryData[$key] = $val;	
		}
		
		if(!empty($_POST['Videogallery'])){
			$model->attributes = $_POST['Videogallery'];
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/videogallery/update?id='.$page_id.'&type=page-videogallery']);
			}		
		}		
        return $this->render('create', [
				'model' => $model,
				'categoryData' => $categoryData
		]);
    }
	
	
	// update page layout
	public function actionForm($id='',$page_id=''){
		// for get category name
		$categoryData[""] = "Select Category";
		$data = ArrayHelper::map(Category::find()->where(['and', "type = 'video_gallery'"])->all(),'id','title');
		foreach($data as $key => $val){
			$categoryData[$key] = $val;	
		}
		$model = $this->findModel($id);
		if(!empty($_POST['Videogallery'])){
			$model->attributes = $_POST['Videogallery'];
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/videogallery/update?id='.$page_id.'&type=page-videogallery']);
			}
		}
		return $this->render('form', [
				'model' => $model,
				'categoryData' => $categoryData,
				'id' => $id
		]);
    }
	
	// For form 
	public function actionUpdate($id='',$type=''){
		$model = $this->findModelPagelayout($id);
		if(!empty($_POST['Pagelayout'])){ // for update pagelayout data  
			$model->attributes = $_POST['Pagelayout'];
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/videogallery/update?id='.$id.'&type=page-video']);
			}
		}
		// For page priority
		$modelPageLayout = new Pagelayout();
		$pageLayoutPriority = $modelPageLayout->getPriorityData($id);
		
		// For banners
		$bannersModel = new Banners();
		$bannersDataProvider = $bannersModel->getBannerData($id);
		
		// For page Videogallery
		$modelVideogallery = new Videogallery();
		$pageVideogallery = $modelVideogallery->getVideogallery($id);
		
		return $this->render('update', [
				'model' => $model,
				'bannersDataProvider' => $bannersDataProvider,
				'pageVideogallery' => $pageVideogallery,
				'priority' => $pageLayoutPriority,
				'type' => $type,
				'id' => $id
		]);
    }
	
	protected function findModel($id) {
        if (($model = Videogallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	protected function findModelPagelayout($id) {
        if (($model = Pagelayout::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

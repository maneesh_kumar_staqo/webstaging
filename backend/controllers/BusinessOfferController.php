<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use backend\models\BusinessOffer;

/**
 * Awards controller
 */
class BusinessOfferController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new BusinessOffer();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	/**
     * Create new page layout.
     *
     * @return string
     */
    public function actionCreate($page_id='',$type='',$page=''){
		$model = new BusinessOffer();
		if(!empty($_POST['BusinessOffer'])){
			$model->attributes = $_POST['BusinessOffer'];
			
            if(!empty($_FILES['box_image1']['name'])){
				$image_name = time().$_FILES['box_image1']['name'];
				$image_tmp_name = $_FILES['box_image1']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->box_image1 = $image_name;
			}
            if(!empty($_FILES['box_image2']['name'])){
				$image_name = time().$_FILES['box_image2']['name'];
				$image_tmp_name = $_FILES['box_image2']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->box_image2 = $image_name;
			}
            if(!empty($_FILES['box_image3']['name'])){
				$image_name = time().$_FILES['box_image3']['name'];
				$image_tmp_name = $_FILES['box_image3']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->box_image3 = $image_name;
			}
           	$model->page_id = $page_id;
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
	
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/'.$page.'/update?id='.$page_id.'&type='.$type]);
			}		
		}		
        return $this->render('create', [
				'model' => $model
		]);
    }
	
	
	// update page layout
	public function actionUpdate($id='', $page_id='', $type='', $page=''){
		$model = $this->findModel($id);
		if(!empty($_POST['BusinessOffer'])){
			$model->attributes = $_POST['BusinessOffer'];
	
		
            if(!empty($_FILES['box_image1']['name'])){
				$image_name = time().$_FILES['box_image1']['name'];
				$image_tmp_name = $_FILES['box_image1']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->box_image1 = $image_name;
			}else{
               unset($model->box_image1);
            }
            if(!empty($_FILES['box_image2']['name'])){
				$image_name = time().$_FILES['box_image2']['name'];
				$image_tmp_name = $_FILES['box_image2']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->box_image2 = $image_name;
			}else{
               unset($model->box_image2);
            }
            if(!empty($_FILES['box_image3']['name'])){
				$image_name = time().$_FILES['box_image3']['name'];
				$image_tmp_name = $_FILES['box_image3']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->box_image3 = $image_name;
			}else{
               unset($model->box_image3);
            }
				$model->page_id = $page_id;
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
		
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/'.$page.'/update?id='.$page_id.'&type='.$type]);
			}
		}
		return $this->render('update', [
				'model' => $model,
				'id' => $id
		]);
    }
	
	protected function findModel($id) {
        if (($model = BusinessOffer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

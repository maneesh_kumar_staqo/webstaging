<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use backend\models\Contactadd;

/**
 * Contactadd controller
 */
class ContactaddController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new Contactadd();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	// For create
	public function actionCreate(){
		$model = new Contactadd();
		if(!empty($_POST['Contactadd'])){
			$model->attributes = $_POST['Contactadd'];
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
            if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'contact/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}

			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/contactadd/index']);
			}		
		}		
        return $this->render('create', [
				'model' => $model
		]);
    }
	
	
	// For update
	public function actionUpdate($id){
		$model = $this->findModel($id);
		if(!empty($_POST['Contactadd'])){
			$model->attributes = $_POST['Contactadd'];
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
            	$model->image = !empty($_POST['image']) ? $_POST['image'] : '';
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'contact/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}else{
            	unset($model->image);
            }
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/contactadd/index']);
			}
		}
		return $this->render('update', [
				'model' => $model,
				'id' => $id
		]);
    }
	
	protected function findModel($id) {
        if (($model = Contactadd::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
}

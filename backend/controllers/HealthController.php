<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use backend\models\Health;

/**
 * Contents controller
 */
class HealthController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new Health();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	/**
     * Create new page layout.
     *
     * @return string
     */
    public function actionCreate($page_id='',$page='', $type=''){
		$model = new Health();
		if(!empty($_POST['Health'])){
			$model->attributes = $_POST['Health'];
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'contents/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
            if(!empty($_FILES['report']['name'])){
				$image_name = time().$_FILES['report']['name'];
				$image_tmp_name = $_FILES['report']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'contents/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->report = $image_name;
			}
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/'.$page.'/update?id='.$page_id.'&type='.$type]);
			}		
		}		
        return $this->render('create', [
				'model' => $model
		]);
    }
	
	
	// update page layout
	public function actionUpdate($id='',$page_id='',$page='', $type=''){
		$model = $this->findModel($id);
		if(!empty($_POST['Health'])){
			$model->attributes = $_POST['Health'];
			$model->image = !empty($_POST['uploaded_image']) ? $_POST['uploaded_image'] : '';
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'contents/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
            else{
            	unset($model->image);
            }
            if(!empty($_FILES['report']['name'])){
				$image_name = time().$_FILES['report']['name'];
				$image_tmp_name = $_FILES['report']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'contents/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->report = $image_name;
			}else{
            	unset($model->report);
            }
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/'.$page.'/update?id='.$page_id.'&type='.$type]);
			}
		}
		return $this->render('update', [
				'model' => $model,
				'id' => $id
		]);
    }
	
	protected function findModel($id) {
        if (($model = Health::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use yii\helpers\ArrayHelper;
use backend\models\Banners;
use backend\models\Contents;
use backend\models\Shareholders;
use backend\models\Images;
use backend\models\Pagelayout;
use backend\models\Awards;
use backend\models\Benefits;
use backend\models\Governance;
use backend\models\Leadership;
use backend\models\Contactadd;
use backend\models\Bonholders;



/**
 * Rooftop controller
 */
class InvestorRelationsController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'form'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    // For update
    public function actionUpdate($id='',$type=''){
        // echo "<pre>";print_r($type);die;
        $model = $this->findModelPagelayout($id);
        if(!empty($_POST['Pagelayout'])){ // for update pagelayout data
            $model->attributes = $_POST['Pagelayout'];
            $model->updated_on = new \yii\db\Expression('now()');
            $model->updated_by = Yii::$app->user->identity->id;
            if ($model->validate()) {
                $model->save();
                return $this->redirect(['/investor-relations/update?id='.$id.'&type=page-investor-relations']);
            }
        }
        // For page priority
        $modelPageLayout = new Pagelayout();
        $pageLayoutPriority = $modelPageLayout->getPriorityData($id);
        
        // For banners
        $bannersModel = new Banners();
        $bannersDataProvider = $bannersModel->getBannerData($id);
        
        $Governance = new Governance();
        $GovernanceDataProvider = $Governance->getGovernanceData($id);

        $shareholdersModel = new Shareholders();
        $ShareholderDataProvider = $shareholdersModel->getShareholdersData($id);

        $bonholderModel = new Bonholders();
        $BonholdersDataProvider = $bonholderModel->getBonholdersData($id);
        
        $modelleadership = new Leadership();
        $pageLeadership = $modelleadership->getLeaders($id);
    
        $contentsModel = new Contents();
        $contentsDataProvider = $contentsModel->getContentData($id);
        
        return $this->render('update', [
                'model' => $model,
                'bannersDataProvider' => $bannersDataProvider,
                'ShareholderDataProvider' => $ShareholderDataProvider,
                'pageLeadership' => $pageLeadership,
                'priority' => $pageLayoutPriority,
                'GovernanceDataProvider'=>$GovernanceDataProvider,
                'contentsDataProvider'=>$contentsDataProvider,
                'BonholdersDataProvider'=>$BonholdersDataProvider,
                'type' => $type,
                'id' => $id
        ]);
    }
    
    protected function findModelPagelayout($id) {
        if (($model = Pagelayout::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

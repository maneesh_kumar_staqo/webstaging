<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\helpers\Security;
use common\models\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'changepassword'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                // 'actions' => [
                    // 'logout' => ['post'],
                // ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
		$this->layout = 'login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $url = BASE_URL.'dashboard';
			$this->redirect($url);
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout(){
        Yii::$app->user->logout();

        return $this->goHome();
    }
	
	// For change user password
	public function actionChangepassword() {
        $error = '';
        $model = User::findOne(\Yii::$app->user->id);
        if ($model->load($_POST)) {
            if (Security::validatePassword($_POST['User']['old_password_hash'], $model->password_hash)) {
                $model->password_hash = Security::generatePasswordHash($_POST['User']['new_password_hash']);
                $model->new_password_hash = $_POST['User']['new_password_hash'];
                $model->confirm_password_hash = $_POST['User']['new_password_hash'];
                if ($model->save()) {
                    $error = 'Password changed successfully';
                }
            } else {
                $error = 'Unable to verify old password, please try again';
            }
        }

        $model->old_password_hash = '';
        $model->new_password_hash = '';
        $model->confirm_password_hash = '';

        return $this->render('changepassword', [
                    'model' => $model,
                    'error' => $error,
        ]);
    }
	
}

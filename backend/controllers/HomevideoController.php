<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use backend\models\Youtube;
use backend\models\Category;
/**
 * Homevideo controller
 */
class HomevideoController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'updatevideo', 'updaterooftopvideo'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new Youtube();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	public function actionUpdatevideo(){
		$this->layout = false;
		$searchModel = new Youtube();
		if(!empty($_GET['video_id'])){
			$dataProvider = $searchModel->updateVideo($_GET['video_id']);
		}
	}
	
	public function actionUpdaterooftopvideo(){
		$this->layout = false;
		$searchModel = new Youtube();
		if(!empty($_GET['video_id']) && !empty($_GET['type'])){
			$dataProvider = $searchModel->updateRooftopVideo($_GET['video_id'], $_GET['type']);
		}
	}
	
}

<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use yii\helpers\ArrayHelper;
use backend\models\Newsroom;
use backend\models\Category;

/**
 * Newsroom controller
 */
class NewsroomController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new Newsroom();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	/**
     * Create new page layout.
     *
     * @return string
     */
    public function actionCreate(){
		$model = new Newsroom();
		// for get category name
		$categoryData[""] = "Select Category";
		$data = ArrayHelper::map(Category::find()->where(['and', "type = 'news_room'"])->all(),'id','title');
		foreach($data as $key => $val){
			$categoryData[$key] = $val;	
		}
		if(!empty($_POST['Newsroom'])){
			$model->attributes = $_POST['Newsroom'];
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'newsroom/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect('index');
			}		
		}		
        return $this->render('create', [
				'model' => $model,
				'categoryData' => $categoryData
		]);
    }
	
	
	// update page layout
	public function actionUpdate($id=''){
		// for get category name
		$categoryData[""] = "Select Category";
		$data = ArrayHelper::map(Category::find()->where(['and', "type = 'news_room'"])->all(),'id','title');
		foreach($data as $key => $val){
			$categoryData[$key] = $val;	
		}
		$model = $this->findModel($id);
		if(!empty($_POST['Newsroom'])){
			$model->attributes = $_POST['Newsroom'];
			$model->image = !empty($_POST['uploaded_image']) ? $_POST['uploaded_image'] : '';
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'newsroom/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect('index');
			}
		}
		return $this->render('update', [
				'model' => $model,
				'categoryData' => $categoryData,
				'id' => $id
		]);
    }
	
	protected function findModel($id) {
        if (($model = Newsroom::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

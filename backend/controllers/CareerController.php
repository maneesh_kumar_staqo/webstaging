<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use backend\models\Banners;
use backend\models\Contents;
use backend\models\Pagelayout;
use backend\models\Career;
use backend\models\Thought;
use backend\models\Slider;
use backend\models\Campus;
use backend\models\Learn;
use backend\models\Vacancies;


/**
 * Career controller
 */
class CareerController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'form', 'vacancies', 'slider', 'campus', 'learn', 'thought'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new Career();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	/**
     * Create new we make.
     *
     * @return string
     */
    public function actionCreate($page_id=''){
		$model = new Career();
		if(!empty($_POST['Career'])){
			$model->attributes = $_POST['Career'];
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/career/update?id='.$page_id.'&type=page-wemake']);
			}		
		}		
        return $this->render('create', [
				'model' => $model
		]);
    }
	
	
	// update we make
	public function actionForm($id='',$page_id=''){
		$model = $this->findModel($id);
		if(!empty($_POST['Career'])){
			$model->attributes = $_POST['Career'];
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/career/update?id='.$page_id.'&type=page-wemake']);
			}
		}
		return $this->render('form', [
				'model' => $model,
				'id' => $id
		]);
    }
	
	// For form 
	public function actionUpdate($id='',$type=''){
		$model = $this->findModelPagelayout($id);
		if(!empty($_POST['Pagelayout'])){ // for update pagelayout data
			$model->attributes = $_POST['Pagelayout'];
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/career/update?id='.$id.'&type=page-career']);
			}
		}
		// For page priority
		$modelPageLayout = new Pagelayout();
		$pageLayoutPriority = $modelPageLayout->getPriorityData($id);
		
		// For banners
		$bannersModel = new Banners();
		$bannersDataProvider = $bannersModel->getBannerData($id);
		
		// For contents
		$contentsModel = new Contents();
		$contentsDataProvider = $contentsModel->getContentData($id);
		
		// For we make
		$modelCareer = new Career();
		$pageWemake = $modelCareer->getCareer();
		
		// For slider
		$modelSlider = new Slider();
		$pageSlider = $modelSlider->getSlider();
		
		// For thought
		$modelThought = new Thought();
		$pageThought = $modelThought->getThought();
		
		// For learn
		$modelLearn = new Learn();
		$pageLearn = $modelLearn->getLearn();
		
		// For campus
		$modelCampus = new Campus();
		$pageCampus = $modelCampus->getCampus();
		
		// For vacancies
		$modelVacancies = new Vacancies();
		$pageVacancies = $modelVacancies->getVacancies();
		
		return $this->render('update', [
				'model' => $model,
				'bannersDataProvider' => $bannersDataProvider,
				'contentsDataProvider' => $contentsDataProvider,
				'pageWemake' => $pageWemake,
				'pageThought' => $pageThought,
				'pageCampus' => $pageCampus,
				'pageVacancies' => $pageVacancies,
				'pageLearn' => $pageLearn,
				'pageSlider' => $pageSlider,
				'priority' => $pageLayoutPriority,
				'type' => $type,
				'id' => $id
		]);
    }
	
	protected function findModel($id) {
        if (($model = Career::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	protected function findModelPagelayout($id) {
        if (($model = Pagelayout::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	protected function findSliderModel($id) {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	// create/update slider
	public function actionSlider($id='',$page_id=''){
		$model = new Slider();
		if(!empty($id)){
			$model = $this->findSliderModel($id);
		}
		if(!empty($_POST['Slider'])){
			$model->attributes = $_POST['Slider'];
			if(!empty($id)){
				$model->image = !empty($_POST['uploaded_image']) ? $_POST['uploaded_image'] : '';
			}
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'career/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/career/update?id='.$page_id.'&type=page-slider']);
			}
		}
		return $this->render('slider_form', [
				'model' => $model,
				'id' => $id
		]);
    }
	
	protected function findThoughtModel($id) {
        if (($model = Thought::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	// create/update thought
	public function actionThought($id='',$page_id=''){
		$model = new Thought();
		if(!empty($id)){
			$model = $this->findThoughtModel($id);
		}
		if(!empty($_POST['Thought'])){
			$model->attributes = $_POST['Thought'];
			if(!empty($id)){
				$model->image = !empty($_POST['uploaded_image']) ? $_POST['uploaded_image'] : '';
			}
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'career/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/career/update?id='.$page_id.'&type=page-thought']);
			}
		}
		return $this->render('thought_form', [
				'model' => $model,
				'id' => $id
		]);
    }
	
	
	protected function findLearnModel($id) {
        if (($model = Learn::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	// create/update learn
	public function actionLearn($id='',$page_id=''){
		$model = new Learn();
		if(!empty($id)){
			$model = $this->findLearnModel($id);
		}
		if(!empty($_POST['Learn'])){
			$model->attributes = $_POST['Learn'];
			if(!empty($id)){
				$model->image = !empty($_POST['uploaded_image']) ? $_POST['uploaded_image'] : '';
				$model->image2 = !empty($_POST['uploaded_image2']) ? $_POST['uploaded_image2'] : '';
				$model->image3 = !empty($_POST['uploaded_image3']) ? $_POST['uploaded_image3'] : '';
			}
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'career/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
			if(!empty($_FILES['image2']['name'])){
				$image_name = time().$_FILES['image2']['name'];
				$image_tmp_name = $_FILES['image2']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'career/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image2 = $image_name;
			}
			if(!empty($_FILES['image3']['name'])){
				$image_name = time().$_FILES['image3']['name'];
				$image_tmp_name = $_FILES['image3']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'career/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image3 = $image_name;
			}
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/career/update?id='.$page_id.'&type=page-learn']);
			}
		}
		return $this->render('learn_form', [
				'model' => $model,
				'id' => $id
		]);
    }
	
	protected function findCampusModel($id) {
        if (($model = Campus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	// create/update campus
	public function actionCampus($id='',$page_id=''){
		$model = new Campus();
		if(!empty($id)){
			$model = $this->findCampusModel($id);
		}
		if(!empty($_POST['Campus'])){
			$model->attributes = $_POST['Campus'];
			if(!empty($id)){
				$model->image = !empty($_POST['uploaded_image']) ? $_POST['uploaded_image'] : '';
			}
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'career/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/career/update?id='.$page_id.'&type=page-campus']);
			}
		}
		return $this->render('campus_form', [
				'model' => $model,
				'id' => $id
		]);
    }
	
	protected function findVacanciesModel($id) {
        if (($model = Vacancies::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	// create/update vacancies
	public function actionVacancies($id='',$page_id=''){
		$model = new Vacancies();
		if(!empty($id)){
			$model = $this->findVacanciesModel($id);
		}
		if(!empty($_POST['Vacancies'])){
			$model->attributes = $_POST['Vacancies'];
			if(!empty($id)){
				$model->pdf = !empty($_POST['uploaded_image']) ? $_POST['uploaded_image'] : '';
			}
			if(!empty($_FILES['pdf']['name'])){
				$image_name = time().$_FILES['pdf']['name'];
				$image_tmp_name = $_FILES['pdf']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'career/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->pdf = $image_name;
			}
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/career/update?id='.$page_id.'&type=page-vacancies']);
			}
		}
		return $this->render('vacancies_form', [
				'model' => $model,
				'id' => $id
		]);
    }

}

<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use backend\models\Youtube;
use backend\models\Category;
/**
 * Youtube controller
 */
class YoutubeController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new Youtube();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	/**
     * Create new page layout.
     *
     * @return string
     */
    public function actionCreate($page_id='',$type='',$page='youtube'){
		$model = new Youtube();
		// for get category name
		$categoryData[""] = "Select Category";
		// $data = ArrayHelper::map(Category::find()->all(),'id','title');
		

	   $data = ArrayHelper::map(Category::find()->all(),'id','title');
        
		foreach($data as $key => $val){
			$categoryData[$key] = $val;	
		}
		if(!empty($_POST['Youtube'])){
			$model->attributes = $_POST['Youtube'];
			if(!empty($_FILES['video_thumb_image']['name'])){
				$image_name = time().$_FILES['video_thumb_image']['name'];
				$image_tmp_name = $_FILES['video_thumb_image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'youtube/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->video_thumb_image = $image_name;
			}
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/youtube']);
                
			}		
		}		
        return $this->render('create', [
				'model' => $model,
				'categoryData' => $categoryData
		]);
    }
	
	
	// update page layout
	public function actionUpdate($id, $page_id='',$type='',$page=''){
		$model = $this->findModel($id);
		// for get category name
		$categoryData[""] = "Select Category";
		$data = ArrayHelper::map(Category::find()->all(),'id','title');
		foreach($data as $key => $val){
			$categoryData[$key] = $val;	
		}
		if(!empty($_POST['Youtube'])){
			$model->attributes = $_POST['Youtube'];
			$model->video_thumb_image = !empty($_POST['uploaded_image']) ? $_POST['uploaded_image'] : '';
			if(!empty($_FILES['video_thumb_image']['name'])){
				$image_name = time().$_FILES['video_thumb_image']['name'];
				$image_tmp_name = $_FILES['video_thumb_image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'youtube/';
				move_uploaded_file($image_tmp_name, $image_path.$image_name);
				$model->video_thumb_image = $image_name;
			}else{
            	unset($model->video_thumb_image);
            }
			
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				$page = !empty($page) ? $page : 'youtube';
				if(!empty($page_id)){
					return $this->redirect(['/youtube']);	
				}else{
					return $this->redirect(['/youtube']);
				}
			}		
		}
		return $this->render('update', [
				'model' => $model,
				'categoryData' => $categoryData,
				'id' => $id
		]);
    }
	
	protected function findModel($id) {
        if (($model = Youtube::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use backend\models\Ukraine;
use yii\web\UploadedFile;


/**
 * Ukraine controller
 */
class UkraineController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['update', 'index', 'create','all','edit','delete','menudelete','createmenu','editmenu','updatemenu'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                // 'actions' => [
                    // 'logout' => ['post'],
                // ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays Ukraine listing.
     *
     * @return string
     */
 public function actionCreatemenu(){
        $pagetableid=$_POST['pagetableid'];
        $name=$_POST['name'];
        $link=$_POST['link'];
        $seq=$_POST['seq'];
        $visible_in=$_POST['visible_in'];
           if(!empty($name)){
                 Yii::$app->db->createCommand()
                ->insert('ukraine_menu_link', [
                'pageid' => $pagetableid,
                'name' => $name,
                'link'=>$link,
                'status' => $seq,
                'visible_in'=>$visible_in,
                ])->execute();
                 return $this->redirect(Yii::$app->request->referrer);
        }
    }
    public function actionIndex(){
        $searchModel = new Ukraine();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	// For create Announcement
	public function actionCreate($id){
         $data = Ukraine::find()->where(['id' => $id])->one();
        $page_type=$_POST['page_type'];
        if($page_type=="banner-section"){
         $data = Ukraine::find()->where(['id' => $id])->one();
      
        if(!empty($_POST['banner_text'])){
            $t=time();
            $rename = rand(1, 100000);
           
            $cehckiempty=$_FILES['banner_image']['name'];
            $imageName=$_FILES['banner_image']['name'];
            $ext = pathinfo($imageName, PATHINFO_EXTENSION);
            $imageName=$rename.$t.'.'.$ext;
             $target_dir = "uploads/images/";
                 $ext = pathinfo($_FILES["banner_image"]["name"], PATHINFO_EXTENSION);
                 $target_file =$rename.$t.".".$ext;
                 if (move_uploaded_file($_FILES["banner_image"]["tmp_name"], $target_dir.$target_file)) {
                   
                }
              if(!empty($cehckiempty)){
                 Yii::$app->db->createCommand()
                ->insert('ukraine_banner_image', [
                'pageid' => $id,
                'seq' => $_POST['sequence'],
                'banner_text'=>$_POST['banner_text'],
                'banner_image' => $imageName,
                ])->execute();
                 return $this->redirect(Yii::$app->request->referrer);
              }
              
            }
    }elseif($page_type=="section-2")
    {
         $model = $this->findModel($id);
         $data = Ukraine::find()->where(['id' => $id])->one();

           if(!empty($_POST['footer']) && !empty($_POST['header']) ){
                   $menufix="1,2";
                }
                elseif(!empty($_POST['footer'])){
                   $menufix='2,0';
                }
                elseif(!empty($_POST['header']) ){
                     $menufix='1,0';
                }
                else{
                    $menufix='0,0';
                }
                $model['section2_menu']=$menufix;
            $model->gallery_title =$_POST['Ukraine']['gallery_title'];
        if(!empty($_FILES['gallery_image'])){
            // dddddddd
      
         
            $t=time();
            $rename = rand(1, 100000);
            $alltext=$model->gallery_title;
            
      
            
            $cehckiempty=$_FILES['gallery_image']['name'];
            $imageName=$_FILES['gallery_image']['name'];
            
            $ext = pathinfo($imageName, PATHINFO_EXTENSION);
            $imageName=$rename.$t.'.'.$ext;
            
             $target_dir = "uploads/images/";
   
                 $ext = pathinfo($_FILES["gallery_image"]["name"], PATHINFO_EXTENSION);
                 $target_file =$rename.$t.".".$ext;
                 if (move_uploaded_file($_FILES["gallery_image"]["tmp_name"], $target_dir.$target_file)) {
                   
                }
                           
         
            
              if(!empty($cehckiempty)){
                 Yii::$app->db->createCommand()
                ->insert('ukraine_gallery', [
                'pageid' => $id,
                'image' => $imageName,
                'seq' => $_POST['sequence'],
                ])->execute();
              }
            }
    
        
    }
    elseif($page_type=="section-3")
    {
         $model = $this->findModel($id);
         $data = Ukraine::find()->where(['id' => $id])->one();
         if(!empty($_POST['footer']) && !empty($_POST['header']) ){
                   $menufix="1,2";
                }
                elseif(!empty($_POST['footer'])){
                   $menufix='0,2';
                }
                elseif(!empty($_POST['header']) ){
                     $menufix='1,0';
                }
                else{
                    $menufix='0,0';
                }
                $model->section3_menu=$menufix;
                $model->disclouser_title=$_POST['Ukraine']['disclouser_title'];


        if(!empty($_POST['disclouser_year'])){
            $rename = rand(1, 100000);
            $t=time();

         
             
         
             $imageName=$_FILES['disclouser_file']['name'];
            $ext = pathinfo($imageName, PATHINFO_EXTENSION);
            $imageName=$rename.$t.'.'.$ext;
             $target_dir = "uploads/images/";
   
                 $ext = pathinfo($_FILES["disclouser_file"]["name"], PATHINFO_EXTENSION);
                 $target_file =$rename.$t.".".$ext;
                 if (move_uploaded_file($_FILES["disclouser_file"]["tmp_name"], $target_dir.$target_file)) {     
                 }

                 


                if(!empty($_FILES['disclouser_file'])){
                Yii::$app->db->createCommand()
                ->insert('ukraine_disclosure', [
                'pageid' => $id,
                'title'=>$_POST['disclouser_heading'],
                'years'=>$_POST['disclouser_year'],
                'type'=>$_POST['disclouser_type'],
                'file' => $imageName,
                ])->execute();
                }
                $model->save(false);
                return $this->redirect(Yii::$app->request->referrer);
        }
    }
        if ($model->validate()) {
                $model->save(false);
                return $this->redirect(Yii::$app->request->referrer);
        }
    }
	// For update Ukraine data
	public function actionAll($id){

        $model = new Ukraine();
		$modeldata = Ukraine::find()->where(['page_id' => $id])->all();	
        if($_GET['page_type']=="section-2"){
	        $connection = \Yii::$app->db;
            $sql = "SELECT ukraine.gallery_title, ukraine_gallery.id, ukraine_gallery.pageid,ukraine_gallery.image
                FROM ukraine
                INNER JOIN ukraine_gallery
                ON ukraine.id=ukraine_gallery.pageid WHERE ukraine.page_id = $id ORDER BY seq";
                $command = $connection->createCommand($sql);
                $data = $command->queryAll();
                // echo "<pre>";print_r($data);die;
            return $this->render('all', [
				'model' => $model,
                'data' => $data,
                'modeldata'=>$modeldata
        ]);
        }
        elseif($_GET['page_type']=="section-3"){
            $connection = \Yii::$app->db;
            $sql = "SELECT ukraine.disclouser_title, ukraine_disclosure.id, ukraine_disclosure.title,ukraine_disclosure.pageid,ukraine_disclosure.type,ukraine_disclosure.years,ukraine_disclosure.file
                FROM ukraine
                INNER JOIN ukraine_disclosure
                ON ukraine.id=ukraine_disclosure.pageid WHERE ukraine.page_id = $id ORDER BY ukraine_disclosure.years DESC";
                $command = $connection->createCommand($sql);
                $data = $command->queryAll();
                return $this->render('all', [
				'model' => $model,
                'data' => $data,
                'modeldata'=>$modeldata
        ]); 
        }
          elseif($_GET['page_type']=="banner-section"){
            $connection = \Yii::$app->db;
            
            $sql = "SELECT * FROM `ukraine_banner_image` WHERE pageid = $id";
                $command = $connection->createCommand($sql);
                $data = $command->queryAll();
                return $this->render('all', [
				'model' => $model,
                'data' => $data,
                'modeldata'=>$modeldata
        ]); 
        }
           elseif($_GET['page_type']=="menu-section"){
             
            $connection = \Yii::$app->db;
                    $single_user = Ukraine::find()->where(['page_id' => $id])->one();
                    $id=$single_user['id']; 
             $sql = "SELECT * FROM `ukraine_menu_link` WHERE pageid = $id";
                $command = $connection->createCommand($sql);
                $data = $command->queryAll();
             
                return $this->render('all', [
                'model' => $model,
                'data' => $data,
                'modeldata'=>$modeldata
        ]); 
        }
             elseif($_GET['page_type']=="header"){
            
            $connection = \Yii::$app->db;
                    $single_user = Ukraine::find()->where(['page_id' => $id])->one();
                    $id=$single_user['id']; 
             $sql = "SELECT * FROM `ukraine_menu_link` WHERE pageid = $id";
                $command = $connection->createCommand($sql);
                $data = $command->queryAll();
             
                return $this->render('all', [
                'model' => $model,
                'data' => $data,
                'modeldata'=>$modeldata
        ]); 
        }

        
        else{
            return $this->render('all', [
				'model' => $model,
				'data' => $modeldata
        ]);
        }
        

        // $model = $this->findModel($id);
        // $data = Ukraine::find()->where(['page_id' => $id])->all();	
		// return $this->render('all', [
		// 		'model' => $model,
        //         'data'=>$data,
		// ]);
    }
 public function actionEditmenu($id){
        $modeldata = Ukraine::find()->where(['page_id' => $id])->asArray()->one();
        $sql = "SELECT * FROM `ukraine_menu_link` WHERE id=$id;";
                $connection = \Yii::$app->db;
                $command = $connection->createCommand($sql);
                $data = $command->queryAll();
                  return $this->render('edit', [
                'data'=>$data,
                'page_type'=>'menu-section',
                'modeldata'=>$modeldata
                ]);
    }

        public function actionEdit($id,$image,$page_type){
         
           
      
            $data = Ukraine::find()->where(['page_id' => $id])->asArray()->one();
   
            
           
            if($page_type=="banner-section"){
            $connection = \Yii::$app->db;
                $sql = "SELECT * FROM `ukraine_banner_image` WHERE id=$image;";
                $command = $connection->createCommand($sql);
                $data = $command->queryAll();



                   return $this->render('edit', [
                'data'=>$data,
                'page_type'=>$page_type,
                'page_id'=>$id,
		        ]);
            }
            elseif($page_type=="section-2"){
                 $connection = \Yii::$app->db;
                $sql = "SELECT ukraine.gallery_title,ukraine_gallery.seq, ukraine.page_id, ukraine_gallery.id, ukraine_gallery.pageid,ukraine_gallery.image
                FROM ukraine
                INNER JOIN ukraine_gallery
                ON ukraine.id=ukraine_gallery.pageid WHERE ukraine_gallery.id=$image;";
                $command = $connection->createCommand($sql);
                $data = $command->queryAll();
                // echo "<pre>";print_r($data);die;
                return $this->render('edit', [
                'data'=>$data,
                'page_type'=>$page_type,
                'page_id'=>$id,
		        ]);
            }
              elseif($page_type=="section-3"){
                $connection = \Yii::$app->db;
                $sql = "SELECT ukraine.disclouser_title, ukraine.page_id, ukraine_disclosure.id, ukraine_disclosure.pageid,ukraine_disclosure.title,ukraine_disclosure.years,ukraine_disclosure.type,ukraine_disclosure.file
                FROM ukraine
                INNER JOIN ukraine_disclosure
                ON ukraine.id=ukraine_disclosure.pageid WHERE ukraine_disclosure.id=$image;";
                $command = $connection->createCommand($sql);
                $data = $command->queryAll();
                // echo "<pre>";print_r($data);die;
                  return $this->render('edit', [
                'data'=>$data,
                'page_id'=>$id,
                'page_type'=>$page_type
		        ]);
            }

         
             


        }
 public function actionUpdatemenu($id){
            $name=$_POST['name'];
            $link=$_POST['link'];
            $seq=$_POST['seq'];
            $page_type=$_POST['page_type'];
            $pagetypeid=$_POST['pagetypeid'];
            $visible_in=$_POST['visible_in'];
            $data=Yii::$app->db->createCommand("UPDATE ukraine_menu_link SET name = '".$name."',link = '".$link."',status = '".$seq."',visible_in = '".$visible_in."' WHERE id=$id")->execute(); 
       
        return $this->redirect(['ukraine/all?id='.$pagetypeid.'&page_type='.$page_type.'']);   

        }
    public function actionUpdate($page_id){
    

        $page_type=$_POST['page_type'];
        
        $model = Ukraine::find()->where(['page_id' => $page_id])->one();
        
		if($page_type=="banner-section"){        
            $bannertext=$_POST['banner_text'];
            $rename = rand(1, 100000);
             $t=time();
             if(!empty($_FILES["banner_image"]["name"])){
                 $target_dir = "uploads/images/";
            $target_file = $target_dir . $_FILES["banner_image"]["name"];
            $ext = pathinfo($_FILES["banner_image"]["name"], PATHINFO_EXTENSION);
           
            $target_file =$rename.$t.".".$ext;
             }
             else{
   
                   $target_file =$_POST['imagest'];
             }
           
            $model = Ukraine::find()->where(['id' => $_POST['pageid']])->one();
            $connection = \Yii::$app->db;
            $imagit=$_FILES['banner_image']['name'];
            $seq=$_POST['sequence'];
            $data=Yii::$app->db->createCommand("UPDATE ukraine_banner_image SET banner_image = '".$target_file."',seq = '".$seq."',banner_text = '".$bannertext."' WHERE id=$page_id")->execute();        
            $updatid=$_POST['imageid'];
            if(!empty($imagit)){
                    $uploadOk = 1;  
                     if (move_uploaded_file($_FILES["banner_image"]["tmp_name"], $target_dir.$target_file)) {
                }
            }
           
              $connection = \Yii::$app->db;
              $pid=$_POST['pageid'];
                $sql = "SELECT ukraine.gallery_title, ukraine_gallery.id, ukraine_gallery.pageid,ukraine_gallery.image
                FROM ukraine
                INNER JOIN ukraine_gallery
                ON ukraine.id=ukraine_gallery.pageid WHERE ukraine.id = $pid";
                $command = $connection->createCommand($sql);
                $data = $command->queryAll();
                // echo "<pre>";print_r($_POST);die;
                return $this->redirect(['ukraine/all?id='.$_POST['pagetypeid'].'&page_type='.$page_type.'']);   
        }
        elseif($page_type=="section-1"){
      
           
               
                // echo "<pre>";print_r($_POST);die;
                if(!empty($_POST['footer']) && !empty($_POST['header']) ){
                   $menufix="1,2";
                }
                elseif(!empty($_POST['footer'])){
                   $menufix='2,0';
                }
                elseif(!empty($_POST['header']) ){
                     $menufix='1,0';
                }
                else{
                    $menufix='0,0';
                }
                $model['section1_menu']=$menufix;
            
                // echo $menufix;die;
               
              
                     // $data=Yii::$app->db->createCommand("UPDATE ukraine_menu SET section_1 = '".$menufix."' WHERE pageid=$page_id")->execute();
               



           $model['about_title']=$_POST['Ukraine']['about_title'];
           $model['about_desc']=$_POST['Ukraine']['about_desc'];
        }
         elseif($page_type=="section-2"){
                 $rename = rand(1, 100000);
                      $t=time();
                 $target_dir = "uploads/images/";
                 $target_file = $target_dir . $_FILES["gallery_image"]["name"];
                    $ext = pathinfo($_FILES["gallery_image"]["name"], PATHINFO_EXTENSION);
                 $target_file =$rename.$t.".".$ext;
            // /uuuuuu
            $model = Ukraine::find()->where(['id' => $_POST['pageid']])->one();
            $connection = \Yii::$app->db;
            $imagit=$_FILES['gallery_image']['name'];
            $seq=$_POST['sequence'];
            $data=Yii::$app->db->createCommand("UPDATE ukraine_gallery SET image = '".$target_file."',seq = '".$seq."' WHERE id=$page_id")->execute();

            // end new 
            // $command = $connection->createCommand($sql);
            // $data = $command->queryAll();
         
            $updatid=$_POST['imageid'];
            
           
            if(!empty($imagit)){
                
                    $uploadOk = 1;  
                     if (move_uploaded_file($_FILES["gallery_image"]["tmp_name"], $target_dir.$target_file)) {
    
                }
            }
           
              $connection = \Yii::$app->db;
              $pid=$_POST['pageid'];
                $sql = "SELECT ukraine.gallery_title, ukraine_gallery.id, ukraine_gallery.pageid,ukraine_gallery.image
                FROM ukraine
                INNER JOIN ukraine_gallery
                ON ukraine.id=ukraine_gallery.pageid WHERE ukraine.id = $pid";
                $command = $connection->createCommand($sql);
                $data = $command->queryAll();
                // echo "<pre>";print_r($_POST);die;
                return $this->redirect(['ukraine/all?id='.$_POST['pagetypeid'].'&page_type='.$page_type.'']);    
        }
        elseif($page_type=="section-3"){
            $model = Ukraine::find()->where(['id' => $_POST['pageid']])->one();
             $rename = rand(1, 100000);
              $t=time();
            $page_type=$_POST['page_type'];
            $updatid=$_POST['imageid'];
            $target_dir = "uploads/images/";
            $ext = pathinfo($_FILES["disclouser_file"]["name"], PATHINFO_EXTENSION);
          
            $target_file =$rename.$t.".".$ext;
              
            $uploadOk = 1;  
            if (move_uploaded_file($_FILES["disclouser_file"]["tmp_name"], $target_dir.$target_file)) {
            }
          
            // new data  
            $heading=$_POST["disclouser_heading"];
            $years=$_POST["disclouser_year"];
            $type=$_POST["disclouser_type"];
            $data=Yii::$app->db->createCommand("UPDATE ukraine_disclosure SET title = '".$heading."',years='".$years."', type='".$type."',file= '".$target_file."' WHERE id=$page_id")->execute();
             $connection = \Yii::$app->db;
              $pid=$_POST['pageid'];
                $sql = "SELECT ukraine.disclouser_title, ukraine_disclosure.id, ukraine_disclosure.pageid,ukraine_disclosure.title,ukraine_disclosure.years,ukraine_disclosure.type,ukraine_disclosure.file
                FROM ukraine
                INNER JOIN ukraine_disclosure
                ON ukraine.id=ukraine_disclosure.pageid WHERE ukraine.id = $pid";
                $command = $connection->createCommand($sql);
                $data = $command->queryAll();
                // echo "<pre>";print_r($data);die;
                return $this->redirect(['ukraine/all?id='.$_POST['pagetypeid'].'&page_type='.$page_type.'']);



        }
          elseif($page_type=="section-4"){
           $model['contact_title']=$_POST['Ukraine']['contact_title'];
           $model['company_name']=$_POST['Ukraine']['company_name'];
            $model['contact_address']=$_POST['Ukraine']['contact_address'];
           $model['map_link']=$_POST['Ukraine']['map_link'];
                       if(!empty($_POST['footer']) && !empty($_POST['header']) ){
                   $menufix="1,2";
                }
                elseif(!empty($_POST['footer'])){
                   $menufix='2,0';
                }
                elseif(!empty($_POST['header']) ){
                     $menufix='1,0';
                }
                else{
                    $menufix='0,0';
                }
         


            $model['section4_menu']=$menufix;
            // ffffff
        }
         elseif($page_type=="section-5"){
           $model['name']=$_POST['Ukraine']['name'];
           $model['phone']=$_POST['Ukraine']['phone'];
            $model['email']=$_POST['Ukraine']['email'];
        }
         $model->save(false);
         $data = Ukraine::find()->where(['page_id' => $page_id])->one();
        return $this->redirect(['ukraine/all?id='.$page_id.'&page_type='.$page_type.'']);
	
    }
    public function actionDelete($id,$image,$page_type){   
 

        if($page_type=="banner-section"){
            $data=Yii::$app->db->createCommand("DELETE FROM `ukraine_banner_image` WHERE id=$image")->execute();
              return $this->redirect(['ukraine/all?id='.$id.'&page_type='.$page_type.'']);
        }
        elseif($page_type=="section-2"){
                    $model = Ukraine::find()->where(['page_id' => $id])->one();
                $connection = \Yii::$app->db;
                $data=Yii::$app->db->createCommand("DELETE FROM `ukraine_gallery` WHERE id=$image")->execute();
        }
        elseif($page_type=="section-3"){
                    $model = Ukraine::find()->where(['page_id' => $id])->one();
                $connection = \Yii::$app->db;
                $data=Yii::$app->db->createCommand("DELETE FROM `ukraine_disclosure` WHERE id=$image")->execute();
        }
        $model->save(false);
         $data = Ukraine::find()->where(['page_id' => $id])->one();
        return $this->redirect(['ukraine/all?id='.$id.'&page_type='.$page_type.'']);
    }
      public function actionMenudelete($id){
            $data=Yii::$app->db->createCommand("DELETE FROM `ukraine_menu_link` WHERE id=$id")->execute();
          return $this->redirect(Yii::$app->request->referrer);
    }
	protected function findModel($id) {
        if (($model = Ukraine::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

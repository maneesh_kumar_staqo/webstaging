<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use backend\models\UpcomingProjects;

/**
 * Awards controller
 */
class UpcomingProjectsController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new UpcomingProjects();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	/**
     * Create new page layout.
     *
     * @return string
     */
    public function actionCreate($page_id='',$type='',$page=''){
		$model = new UpcomingProjects();
		if(!empty($_POST['UpcomingProjects'])){
			$model->attributes = $_POST['UpcomingProjects'];
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
            if(!empty($_FILES['box_image1']['name'])){
				$image_name = time().$_FILES['box_image1']['name'];
				$image_tmp_name = $_FILES['box_image1']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->box_image1 = $image_name;
			}
            if(!empty($_FILES['box_image2']['name'])){
				$image_name = time().$_FILES['box_image2']['name'];
				$image_tmp_name = $_FILES['box_image2']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->box_image2 = $image_name;
			}
            if(!empty($_FILES['box_image3']['name'])){
				$image_name = time().$_FILES['box_image3']['name'];
				$image_tmp_name = $_FILES['box_image3']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->box_image3 = $image_name;
			}
            if(!empty($_FILES['box_image4']['name'])){
				$image_name = time().$_FILES['box_image4']['name'];
				$image_tmp_name = $_FILES['box_image4']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->box_image4 = $image_name;
			}
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/'.$page.'/update?id='.$page_id.'&type='.$type]);
			}		
		}		
        return $this->render('create', [
				'model' => $model
		]);
    }
	
	
	// update page layout
	public function actionUpdate($id='', $page_id='', $type='', $page=''){
		$model = $this->findModel($id);
		if(!empty($_POST['UpcomingProjects'])){
			$model->attributes = $_POST['UpcomingProjects'];
			$model->image = !empty($_POST['uploaded_image']) ? $_POST['uploaded_image'] : '';
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}else{
               unset($model->image);
            }
            if(!empty($_FILES['box_image1']['name'])){
				$image_name = time().$_FILES['box_image1']['name'];
				$image_tmp_name = $_FILES['box_image1']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->box_image1 = $image_name;
			}else{
               unset($model->box_image1);
            }
            if(!empty($_FILES['box_image2']['name'])){
				$image_name = time().$_FILES['box_image2']['name'];
				$image_tmp_name = $_FILES['box_image2']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->box_image2 = $image_name;
			}else{
               unset($model->box_image2);
            }
            if(!empty($_FILES['box_image3']['name'])){
				$image_name = time().$_FILES['box_image3']['name'];
				$image_tmp_name = $_FILES['box_image3']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->box_image3 = $image_name;
			}else{
               unset($model->box_image3);
            }
            if(!empty($_FILES['box_image4']['name'])){
				$image_name = time().$_FILES['box_image4']['name'];
				$image_tmp_name = $_FILES['box_image4']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->box_image4 = $image_name;
			}else{
               unset($model->box_image4);
            }
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/'.$page.'/update?id='.$page_id.'&type='.$type]);
			}
		}
		return $this->render('update', [
				'model' => $model,
				'id' => $id
		]);
    }
	
	protected function findModel($id) {
        if (($model = UpcomingProjects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

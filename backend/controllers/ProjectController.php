<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use backend\models\Banners;
use backend\models\Health;
use backend\models\Pagelayout;
use backend\models\CommissionedProjects;


use backend\models\Awards;
use yii\helpers\ArrayHelper;
use backend\models\Category;
use backend\models\UpcomingProjects;
use backend\models\Coe;

/**
 * Project controller
 */
class ProjectController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'form', 'portfolio', 'portfolioimg', 'portfoliostate'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new CommissionedProjects();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	/**
     * Create new page layout.
     *
     * @return string
     */
    public function actionCreate($page_id=''){
		$model = new CommissionedProjects();
		if(!empty($_POST['Project'])){
			$model->attributes = $_POST['Project'];
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
			if(!empty($_FILES['image_pdf']['name'])){
				$image_name = time().$_FILES['image_pdf']['name'];
				$image_tmp_name = $_FILES['image_pdf']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image_pdf = $image_name;
			}
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/project/update?id='.$page_id.'&type=page-studies']);
			}		
		}		
        return $this->render('create', [
				'model' => $model
		]);
    }
	
	
	// update page layout
	public function actionForm($id='',$page_id=''){
		$model = $this->findModel($id);
		if(!empty($_POST['CommissionedProjects'])){
			$model->attributes = $_POST['CommissionedProjects'];
			$model->image = !empty($_POST['uploaded_image']) ? $_POST['uploaded_image'] : '';
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
			$model->image_pdf = !empty($_POST['uploaded_pdf']) ? $_POST['uploaded_pdf'] : '';
			if(!empty($_FILES['image_pdf']['name'])){
				$image_name = time().$_FILES['image_pdf']['name'];
				$image_tmp_name = $_FILES['image_pdf']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image_pdf = $image_name;
			}
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/project/update?id='.$page_id.'&type=page-studies']);
			}
		}
		return $this->render('form', [
				'model' => $model,
				'id' => $id
		]);
    }
	
	// For form 
	public function actionUpdate($id='',$type=''){
		$model = $this->findModelPagelayout($id);
		if(!empty($_POST['Pagelayout'])){ // for update pagelayout data
			$model->attributes = $_POST['Pagelayout'];
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/project/update?id='.$id.'&type=page-project']);
			}
		}
		
		// For page priority
		$modelPageLayout = new Pagelayout();
		$pageLayoutPriority = $modelPageLayout->getPriorityData($id);
		// echo "<pre>"; print_r($pageLayoutPriority); die;
		// For banners
		$bannersModel = new Banners();
		$bannersDataProvider = $bannersModel->getBannerData($id);
		
		// For contents
		$contentsModel = new Health();
		$contentsDataProvider = $contentsModel->getContentData($id);

		$CoeModel = new Coe();
		$CoeProvider = $CoeModel->getContentData($id);
		

		$UpcomingProjectsModel = new UpcomingProjects();
		$UpcomingProjects = $UpcomingProjectsModel->getUpcomingProjects();
		// For case studies
		$modelProject = new CommissionedProjects();
		$commissionedProjects = $modelProject->getCommissionedProjects($id);
		// For Awards
		$awardsModel2 = new Awards();
		$awardsDataProvider = $awardsModel2->getAwardsData($id);
		
		return $this->render('update', [
				'model' => $model,
				'UpcomingProjects' => $UpcomingProjects,
				'bannersDataProvider' => $bannersDataProvider,
				'contentsDataProvider' => $contentsDataProvider,
				'CoeProvider' => $CoeProvider,
				'commissionedProjects' => $commissionedProjects,
				'priority' => $pageLayoutPriority,
				'awardsDataProvider' => $awardsDataProvider,
				'type' => $type,
				'id' => $id
		]);
    }
	
	


	protected function findModel($id) {
        if (($model = CommissionedProjects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	protected function findModelPagelayout($id) {
        if (($model = Pagelayout::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

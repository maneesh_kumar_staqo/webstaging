<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use yii\helpers\ArrayHelper;
use backend\models\Banners;
use backend\models\Images;
use backend\models\Pagelayout;
use backend\models\Blogs;
use backend\models\Youtube;
use backend\models\HfeStatic;
use backend\models\Investor;
use backend\models\EsgHighlights;
use backend\models\SocialAction;


/**
 * Home controller
 */
class HomeController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'form'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    // For update
	public function actionUpdate($id='',$type=''){
		$model = $this->findModelPagelayout($id);
		if(!empty($_POST['Pagelayout'])){ // for update pagelayout data
			$model->attributes = $_POST['Pagelayout'];
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/focusareas/update?id='.$id.'&type=page-focus']);
			}
		}
		// For page priority
		$modelPageLayout = new Pagelayout();
		$pageLayoutPriority = $modelPageLayout->getPriorityData($id);
		
		// For banners
		$bannersModel = new Banners();
		$bannersDataProvider = $bannersModel->getBannerData($id);
		

		$hfestatic = new HfeStatic();
		$hfestaticDataProvider = $hfestatic->getStaticsData($id);

		$investor = new Investor();
		$investorDataProvider = $investor->getInvestorData($id);

		$highlight = new EsgHighlights();
		$highlightDataProvider = $highlight->getHightlightsData($id);


		$social = new SocialAction();
		$socialDataProvider = $social->getSocialData($id);
		
		// For Images
		$imageModel = new Images();
		$imageDataProvider = $imageModel->getImageData($id);
		
		// For Blogs
		$blogsModel = new Blogs();
		$blogsDataProvider = $blogsModel->getBlogsData($id);
		
		// For Video
		$youtubeModel = new Youtube();
		$youtubeDataProvider = $youtubeModel->getYoutubeData($id);
		
		return $this->render('update', [
				'model' => $model,
                	'bannersModel' => $bannersModel,
				'bannersDataProvider' => $bannersDataProvider,
				'hfestatic' => $hfestaticDataProvider,
				'investor'=>$investorDataProvider,
				'hightlights'=>$highlightDataProvider,
				'socialaction'=>$socialDataProvider,
				'bannersDataProvider' => $bannersDataProvider,
				'imageDataProvider' => $imageDataProvider,
				'blogsDataProvider' => $blogsDataProvider,
				'youtubeDataProvider' => $youtubeDataProvider,
				'priority' => $pageLayoutPriority,
				'type' => $type,
				'id' => $id
		]);
    }
	
	protected function findModelPagelayout($id) {
        if (($model = Pagelayout::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

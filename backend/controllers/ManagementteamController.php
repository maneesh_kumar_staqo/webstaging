<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use backend\models\Managementteam;

/**
 * Managementteam controller
 */
class ManagementteamController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new Managementteam();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	/**
     * Create new page layout.
     *
     * @return string
     */
    public function actionCreate($page_id=''){
		$model = new Managementteam();
		if(!empty($_POST['Managementteam'])){
			$model->attributes = $_POST['Managementteam'];
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'managementteam/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/pagelayout/update?id='.$page_id.'&type=management-team']);
			}		
		}		
        return $this->render('create', [
				'model' => $model
		]);
    }
	
	
	// update page layout
	public function actionUpdate($id='',$page_id=''){
		$model = $this->findModel($id);
		if(!empty($_POST['Managementteam'])){
			$model->attributes = $_POST['Managementteam'];
			$model->image = !empty($_POST['uploaded_image']) ? $_POST['uploaded_image'] : '';
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'managementteam/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/pagelayout/update?id='.$page_id.'&type=management-team']);
			}
		}
		return $this->render('update', [
				'model' => $model,
				'id' => $id
		]);
    }
	
	protected function findModel($id) {
        if (($model = Managementteam::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

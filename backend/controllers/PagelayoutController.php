<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use backend\models\Pagelayout;
use backend\models\Banners;
use backend\models\Directors;
use backend\models\Contents;
use backend\models\Leadership;
use backend\models\Managementteam;
use backend\models\Images;
use backend\models\Governance;
use backend\models\Focusareas;
use backend\models\OurValue;

/**
 * Pagelayout controller
 */
class PagelayoutController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'savepagepriority'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new Pagelayout();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	/**
     * Create new page layout.
     *
     * @return string
     */
    public function actionCreate(){
		$model = new Pagelayout();
		if(!empty($_POST['Pagelayout'])){
			$model->attributes = $_POST['Pagelayout'];
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect('page-layout');
			}		
		}		
        return $this->render('create', [
				'model' => $model
		]);
    }
	
	
	// update page layout
	public function actionUpdate($id,$type=''){
		$model = $this->findModel($id);
		if(!empty($_POST['Pagelayout'])){ // for update pagelayout data
			$model->attributes = $_POST['Pagelayout'];
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/pagelayout/update?id='.$id]);
			}
		}
		// For banners
		$bannersModel = new Banners();
		$bannersDataProvider = $bannersModel->getBannerData($id);
		
		// For contents
		$contentsModel = new Contents();
		$contentsDataProvider = $contentsModel->getContentData($id);
		
		// For Board of Directors
		$boardModel = new Directors();
		$boardDirectorsDataProvider = $boardModel->getDirectors($id);
		
		// For Management Team
		$ManagementModel = new Managementteam();
		$managementDataProvider = $ManagementModel->getManagementTeam($id);
		
		// For Images
		$imageModel = new Images();
		$imageDataProvider = $imageModel->getImageData($id);
		
		// For Governance
		$governanceModel = new Governance();
		$governanceDataProvider = $governanceModel->getGovernanceData($id);
		
		// For page priority
		$modelPageLayout = new Pagelayout();
		$pageLayoutPriority = $modelPageLayout->getPriorityData($id);
		
		// For page leadership
		$modelleadership = new Leadership();
		$pageLeadership = $modelleadership->getLeaders($id);

		$ourValueDataProvider = new OurValue();
		$ourValueDataProvider = $ourValueDataProvider->getOurValueData();


		// For page focus areas
		$modelfocus = new Focusareas();
		$pageFocusAreas = $modelfocus->getFocusareas($id);
		
		
        return $this->render('update', [
				'model' => $model,
				'bannersModel' => $bannersModel,
				'bannersDataProvider' => $bannersDataProvider,
				'contentsModel' => $contentsModel,
				'contentsDataProvider' => $contentsDataProvider,
				'boardDirectorsDataProvider' => $boardDirectorsDataProvider,
				'managementDataProvider' => $managementDataProvider,
				'imageDataProvider' => $imageDataProvider,
				'governanceDataProvider' => $governanceDataProvider,
				'priority' => $pageLayoutPriority,
				'ourValueDataProvider'=>$ourValueDataProvider,
				'pageLeadership' => $pageLeadership,
				'pageFocusAreas' => $pageFocusAreas,
				'id' => $id,
				'type' => $type
		]);
    }
	
	protected function findModel($id) {
        if (($model = Pagelayout::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionSavepagepriority(){
		$this->layout = false;
		$model = new Pagelayout();
		$model->SavePagePriority($_GET);
	}	

}

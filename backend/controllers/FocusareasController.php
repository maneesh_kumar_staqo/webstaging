<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use yii\helpers\ArrayHelper;
use backend\models\Banners;
use backend\models\Contents;
use backend\models\Pagelayout;
use backend\models\Focusareas;
use backend\models\Category;


/**
 * Focusareas controller
 */
class FocusareasController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'form'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new Focusareas();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	/**
     * Create new page layout.
     *
     * @return string
     */
    public function actionCreate($page_id=''){
		$model = new Focusareas();
		// for get category name
		$categoryData[""] = "Select Category";
		$data = ArrayHelper::map(Category::find()->where(['and', "type = 'focus_areas'"])->all(),'id','title');
		foreach($data as $key => $val){
			$categoryData[$key] = $val;	
		}
		// echo "<pre>"; print_r($categoryData); die;
		if(!empty($_POST['Focusareas'])){
			$model->attributes = $_POST['Focusareas'];
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'focusareas/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/focusareas/update?id='.$page_id.'&type=page-focusareas']);
			}		
		}		
        return $this->render('create', [
				'model' => $model,
				'categoryData' => $categoryData
		]);
    }
	
	
	// update page layout
	public function actionForm($id='',$page_id=''){
		$model = $this->findModel($id);
		// for get category name
		$categoryData[""] = "Select Category";
		$data = ArrayHelper::map(Category::find()->where(['and', "type = 'focus_areas'"])->all(),'id','title');
		foreach($data as $key => $val){
			$categoryData[$key] = $val;	
		}
		if(!empty($_POST['Focusareas'])){
			$model->attributes = $_POST['Focusareas'];
			$model->image = !empty($_POST['uploaded_image']) ? $_POST['uploaded_image'] : '';
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'focusareas/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/focusareas/update?id='.$page_id.'&type=page-focusareas']);
			}
		}
		return $this->render('form', [
				'model' => $model,
				'categoryData' => $categoryData,
				'id' => $id
		]);
    }
	
	// For form 
	public function actionUpdate($id='',$type=''){
		$model = $this->findModelPagelayout($id);
		if(!empty($_POST['Pagelayout'])){ // for update pagelayout data
			$model->attributes = $_POST['Pagelayout'];
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/focusareas/update?id='.$id.'&type=page-focus']);
			}
		}
		// For page priority
		$modelPageLayout = new Pagelayout();
		$pageLayoutPriority = $modelPageLayout->getPriorityData($id);
		
		// For banners
		$bannersModel = new Banners();
		$bannersDataProvider = $bannersModel->getBannerData($id);
		
		// For contents
		$contentsModel = new Contents();
		$contentsDataProvider = $contentsModel->getContentData($id);
		
		// For page focus areas
		$modelfocus = new Focusareas();
		$pageFocusAreas = $modelfocus->getFocusareas($id);
		
		return $this->render('update', [
				'model' => $model,
				'bannersDataProvider' => $bannersDataProvider,
				'contentsDataProvider' => $contentsDataProvider,
				'pageFocusAreas' => $pageFocusAreas,
				'priority' => $pageLayoutPriority,
				'type' => $type,
				'id' => $id
		]);
    }
	
	protected function findModel($id) {
        if (($model = Focusareas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	protected function findModelPagelayout($id) {
        if (($model = Pagelayout::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

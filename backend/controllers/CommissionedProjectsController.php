<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use backend\models\Banners;
use backend\models\Contents;
use backend\models\Pagelayout;
use backend\models\CommissionedProjects;
use backend\models\Images;
use backend\models\Images2;
use backend\models\Awards;
use backend\models\Portfolio;
use backend\models\Portfolioimg;
use backend\models\Portfoliostate;
use yii\helpers\ArrayHelper;
use backend\models\Category;
use backend\models\Investor;
use backend\models\Projectcontent;

/**
 * Project controller
 */
class CommissionedProjectsController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'form', 'portfolio', 'portfolioimg', 'portfoliostate'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new CommissionedProjects();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	/**
     * Create new page layout.
     *
     * @return string
     */
    public function actionCreate($page_id=''){
		$model = new CommissionedProjects();
		if(!empty($_POST['CommissionedProjects'])){
			$model->attributes = $_POST['CommissionedProjects'];
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}
			
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
            $model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/project/update?id='.$page_id.'&type=commissioned-projects']);
			}		
		}		
        return $this->render('create', [
				'model' => $model
		]);
    }
	
	
	
	// For form 
	public function actionUpdate($id='',$type='',$page_id=''){
		$model = $this->findModel($id);
	
		if(!empty($_POST['CommissionedProjects'])){
			$model->attributes = $_POST['CommissionedProjects'];
			$model->image = !empty($_POST['image']) ? $_POST['image'] : '';
			if(!empty($_FILES['image']['name'])){
				$image_name = time().$_FILES['image']['name'];
				$image_tmp_name = $_FILES['image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'project/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->image = $image_name;
			}else{
            	unset($model->image);
            }
      
			$model->page_id = $page_id;
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
		
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/project/update?id='.$page_id.'&type=commissioned-projects']);
			}		
		}
		return $this->render('update', [
				'model' => $model,
				'id' => $id
		]);
    }
		protected function findModel($id) {
        if (($model = CommissionedProjects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	

	
	
}

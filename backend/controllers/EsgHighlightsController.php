<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use backend\models\EsgHighlights;

/**
 * Category controller
 */
class EsgHighlightsController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new EsgHighlights();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	/**
     * Create new page layout.
     *
     * @return string
     */
    public function actionCreate(){
		$model = new EsgHighlights();
		if(!empty($_POST['EsgHighlights'])){
			$model->attributes = $_POST['EsgHighlights'];
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect('index');
			}		
		}		
        return $this->render('create', [
				'model' => $model
		]);
    }
	
	
	// update page layout
	public function actionUpdate($id=''){
		$model = $this->findModel($id);
		if(!empty($_POST['EsgHighlights'])){
			$model->attributes = $_POST['EsgHighlights'];
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect('index');
			}
		}
		return $this->render('update', [
				'model' => $model,
				'id' => $id
		]);
    }
	
	protected function findModel($id) {
        if (($model = EsgHighlights::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

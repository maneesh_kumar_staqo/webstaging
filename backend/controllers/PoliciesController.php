<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use yii\helpers\ArrayHelper;
use backend\models\Banners;
use backend\models\Pagelayout;
use backend\models\Policies;
use backend\models\Contents;


/**
 * Policies controller
 */
class PoliciesController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'form'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new Policies();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	/**
     * Create new page layout.
     *
     * @return string
     */
    public function actionCreate($page_id=''){
		$model = new Policies();
		if(!empty($_POST['Policies'])){
			$model->attributes = $_POST['Policies'];
			if(!empty($_FILES['pdf_image']['name'])){
				$image_name = time().$_FILES['pdf_image']['name'];
				$image_tmp_name = $_FILES['pdf_image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'policies/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->pdf_image = $image_name;
			}
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/policies/update?id='.$page_id.'&type=page-policies']);
			}		
		}		
        return $this->render('create', [
				'model' => $model
		]);
    }
	
	
	// update page layout
	public function actionForm($id='',$page_id=''){
		$model = $this->findModel($id);
		if(!empty($_POST['Policies'])){
			$model->attributes = $_POST['Policies'];
			$model->pdf_image = !empty($_POST['uploaded_image']) ? $_POST['uploaded_image'] : '';
			if(!empty($_FILES['pdf_image']['name'])){
				$image_name = time().$_FILES['pdf_image']['name'];
				$image_tmp_name = $_FILES['pdf_image']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'policies/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->pdf_image = $image_name;
			}
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/policies/update?id='.$page_id.'&type=page-policies']);
			}
		}
		return $this->render('form', [
				'model' => $model,
				'id' => $id
		]);
    }
	
	// For form 
	public function actionUpdate($id='',$type=''){
		$model = $this->findModelPagelayout($id);
		if(!empty($_POST['Pagelayout'])){ // for update pagelayout data
			$model->attributes = $_POST['Pagelayout'];
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/policies/update?id='.$id.'&type=page-policies']);
			}
		}
		// For page priority
		$modelPageLayout = new Pagelayout();
		$pageLayoutPriority = $modelPageLayout->getPriorityData($id);
		
		// For banners
		$bannersModel = new Banners();
		$bannersDataProvider = $bannersModel->getBannerData($id);
		
		// For contents
		$contentsModel = new Contents();
		$contentsDataProvider = $contentsModel->getContentData($id);
		
		// For page Policies
		$modelPolicies = new Policies();
		$pagePolicies = $modelPolicies->getPolicies($id);
		
		return $this->render('update', [
				'model' => $model,
				'bannersDataProvider' => $bannersDataProvider,
				'pagePolicies' => $pagePolicies,
				'contentsDataProvider' => $contentsDataProvider,
				'priority' => $pageLayoutPriority,
				'type' => $type,
				'id' => $id
		]);
    }
	
	protected function findModel($id) {
        if (($model = Policies::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	protected function findModelPagelayout($id) {
        if (($model = Pagelayout::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

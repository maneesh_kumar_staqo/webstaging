<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use yii\helpers\ArrayHelper;
use backend\models\Banners;
use backend\models\Contents;
use backend\models\Reasons;
use backend\models\Images;
use backend\models\Pagelayout;
use backend\models\Awards;
use backend\models\Benefits;
use backend\models\BusinessOffer;
use backend\models\Youtube;
use backend\models\Contactadd;
use backend\models\CommissionedProjects;



/**
 * Rooftop controller
 */
class RooftopController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'form'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    // For update
	public function actionUpdate($id='',$type=''){
		$model = $this->findModelPagelayout($id);
		if(!empty($_POST['Pagelayout'])){ // for update pagelayout data
			$model->attributes = $_POST['Pagelayout'];
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/rooftop/update?id='.$id]);
			}
		}
		// For page priority
		$modelPageLayout = new Pagelayout();
		$pageLayoutPriority = $modelPageLayout->getPriorityData($id);
		
		// For banners
		$bannersModel = new Banners();
		$bannersDataProvider = $bannersModel->getBannerData($id);
		
        $offerModel = new BusinessOffer();
		$offerDataProvider = $offerModel->getBusinessOffer();

        $reasonsModel = new Reasons();
		$ReasonsDataProvider = $reasonsModel->getReasonsData($id);

		// For Images
		$imageModel = new Images();
		$imageDataProvider = $imageModel->getImageData($id);
	
		// For Video Gallery
        	$modelContact = new Contactadd();
		$pageContact = $modelContact->getContact($id);



		$youtubeModel = new Youtube();
		$VideoDataProvider = $youtubeModel->getVideoGalleryData($id);
        		$modelProject = new CommissionedProjects();
		$commissionedProjects = $modelProject->getCommissionedProjects($id);
		
		return $this->render('update', [
				'model' => $model,
				'bannersDataProvider' => $bannersDataProvider,
                'ReasonsDataProvider' => $ReasonsDataProvider,
				'imageDataProvider' => $imageDataProvider,
				'VideoDataProvider' => $VideoDataProvider,
				'priority' => $pageLayoutPriority,
                'offerDataProvider'=>$offerDataProvider,
                'pageContact' => $pageContact,
                'commissionedProjects' => $commissionedProjects,
				'type' => $type,
				'id' => $id
		]);
    }
	
	protected function findModelPagelayout($id) {
        if (($model = Pagelayout::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

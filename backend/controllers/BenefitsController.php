<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Security;
use backend\models\Benefits;

/**
 * Benefits controller
 */
class BenefitsController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays page layout.
     *
     * @return string
     */
    public function actionIndex(){
		$searchModel = new Benefits();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider
		]);
    }
	
	/**
     * Create new page layout.
     *
     * @return string
     */
    public function actionCreate($page_id='', $type='', $page=''){
		$model = new Benefits();
		if(!empty($_POST['Benefits'])){
			$model->attributes = $_POST['Benefits'];
			$model->type = 'Benefits_Rooftop_Solar';
			if(!empty($_FILES['icon']['name'])){
				$image_name = time().$_FILES['icon']['name'];
				$image_tmp_name = $_FILES['icon']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'benefits/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->icon = $image_name;
			}
			$model->created_on = new \yii\db\Expression('now()');
			$model->created_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/'.$page.'/update?id='.$page_id.'&type='.$type]);
			}		
		}		
        return $this->render('create', [
				'model' => $model
		]);
    }
	
	
	// update page layout
	public function actionUpdate($id='', $page_id='', $page='', $type=''){
		$model = $this->findModel($id);
		if(!empty($_POST['Benefits'])){
			$model->attributes = $_POST['Benefits'];
			$model->type = 'Benefits_Rooftop_Solar';
			$model->icon = !empty($_POST['uploaded_image']) ? $_POST['uploaded_image'] : '';
			if(!empty($_FILES['icon']['name'])){
				$image_name = time().$_FILES['icon']['name'];
				$image_tmp_name = $_FILES['icon']['tmp_name'];
				$image_path = IMG_UPLOAD_URL.'benefits/';
				move_uploaded_file($image_tmp_name, $image_path. $image_name);
				$model->icon = $image_name;
			}
			$model->updated_on = new \yii\db\Expression('now()');
			$model->updated_by = Yii::$app->user->identity->id;
			$model->page_id = $page_id;
			if ($model->validate()) {
				$model->save();
				return $this->redirect(['/'.$page.'/update?id='.$page_id.'&type='.$type]);
			}
		}
		return $this->render('update', [
				'model' => $model,
				'id' => $id
		]);
    }
	
	protected function findModel($id) {
        if (($model = Benefits::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Blogs extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_blogs';
    }

    public function rules() {
        return [
            [['status','title', 'short_description','priority'], 'required'],
			[['page_url'], 'unique'],
            [['status','title', 'tags', 'description', 'category_id', 'team_name', 'publish_date', 'short_description', 'page_url','meta_title', 'meta_description', 'meta_keywords'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'tags' => 'Tags',
            'category_id' => 'Category Name',
            'image_link' => 'Image Link',
            'description' => 'Description',
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function search($params) {
        $query = Blogs::find()->orderBy(['created_on' => SORT_DESC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['like', 'title', $this->team_name]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }
	
	public function getBlogsData($id) {
        $params = '';
		$query = Blogs::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }

        return $dataProvider;
    }
	
	public function updateBlogs($blog_id){
		$connection = \Yii::$app->db;	
		$sql = "update hfe_blogs set is_home = 'No'";
		$command = $connection->createCommand($sql);
		$command->execute();
		
		$sql = "update hfe_blogs set is_home = 'Yes' where id = :id";
		$command = $connection->createCommand($sql);
		$command->bindParam(':id', $blog_id);
		$command->execute();
	}

}

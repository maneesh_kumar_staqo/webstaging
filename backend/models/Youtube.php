<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Youtube extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_youtube';
    }

    public function rules() {
        return [
            [['status', 'category_id'], 'required'],
            [['status', 'title', 'description', 'video_thumb_image', 'video_link'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'category_id' => 'Category Name',
            'image_link' => 'Image Link',
            'description' => 'Description',
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function search($params) {
        $query = Youtube::find()->orderBy(['created_on' => SORT_DESC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['like', 'status', $this->status]);
		
        return $dataProvider;
    }
	
	// For video gallery in rooftop page
	public function getVideoGalleryData($id) {
		$params = '';
		$query = Youtube::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['like', 'status', $this->status]);
		
        return $dataProvider;
    }
	
	public function getYoutubeData($id) {
        $params = '';
		$query = Youtube::find()->where(['and', 'page_id='.$id])->orderBy(['id' => SORT_ASC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }

        return $dataProvider;
    }
	
	public function updateVideo($video_id){
		$connection = \Yii::$app->db;	
		$sql = "update hfe_youtube set is_home = 'No'";
		$command = $connection->createCommand($sql);
		$command->execute();
		
		$sql = "update hfe_youtube set is_home = 'Yes' where id = :id";
		$command = $connection->createCommand($sql);
		$command->bindParam(':id', $video_id);
		$command->execute();
	}
	
	public function updateRooftopVideo($video_id, $type){
		$connection = \Yii::$app->db;	
		$sql = "update hfe_youtube set is_rooftop = :is_rooftop where id = :id";
		$command = $connection->createCommand($sql);
		$command->bindParam(':id', $video_id);
		$command->bindParam(':is_rooftop', $type);
		$command->execute();
	}

}

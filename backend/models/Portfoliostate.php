<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Portfoliostate extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_project_portfolio_state';
    }

    public function rules() {
        return [
            [['status', 'state_name', 'power_capacity', 'category_id'], 'required'],
            [['status', 'image', 'category_id', 'state_name', 'power_capacity', 'bottom_content'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'image' => 'Image',
            'category_id' => 'Category Name',
            'power_capacity' => 'Power Capacity (MW)',
            'state_name' => 'State Name',
            'bottom_content' => 'Bottom Content',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getPortfolioStateData() {
		$params = '';
        $query = Portfoliostate::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }	
	
	public function getState($category_id){
		$connection = \Yii::$app->db;
		$sql = "select * from hfe_project_portfolio_state where category_id = :category_id and status = 'Active'";
		$command = $connection->createCommand($sql);
		$command->bindParam(':category_id', $category_id);
		$data = $command->queryAll();
		return $data;
	}
	
}

<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Slider extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_career_slider';
    }

    public function rules() {
        return [
            [['status'], 'required'],
            [['status', 'image', 'priority'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getSlider() {
		$params = '';
        $query = Slider::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }
	
	
}

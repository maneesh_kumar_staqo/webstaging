<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Portfolioimg extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_project_portfolio_img';
    }

    public function rules() {
        return [
            [['status', 'category_id'], 'required'],
            [['status', 'image', 'category_id'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'image' => 'Image',
            'category_id' => 'Category Name',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getPortfolioImgData() {
		$params = '';
        $query = Portfolioimg::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }	
	
	public function getImagesData(){
		$connection = \Yii::$app->db;
		$sql = "select img.* from hfe_project_portfolio_img img join hfe_category cate on (cate.id = img.category_id) where img.status = 'Active'";
		$command = $connection->createCommand($sql);
		// $command->bindParam(':id', $blog_id);
		$data = $command->queryAll();
		return $data;
	}
	
	public function getContentImg($category_id){
		$connection = \Yii::$app->db;
		$sql = "select * from hfe_project_portfolio_img where category_id = :category_id and status = 'Active'";
		$command = $connection->createCommand($sql);
		$command->bindParam(':category_id', $category_id);
		$data = $command->queryAll();
		return $data;
	}

}

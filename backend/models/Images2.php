<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Images2 extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_page_images2';
    }

    public function rules() {
        return [
            [['status'], 'required'],
			[['status','title', 'description', 'image_position', 'priority', 'short_description'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'description' => 'Description',
            'short_description' => 'Short Description',
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getImageData($id) {
        $params = '';
        $query = Images2::find()->where(['and', 'page_id='.$id]);
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }

        return $dataProvider;
    }

}

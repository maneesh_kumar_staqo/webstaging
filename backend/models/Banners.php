<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Banners extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_banners';
    }

    public function rules() {
        return [
            [['status'], 'required'],
			[['status','title', 'description', 'priority', 'sub_title', 'mobile_image', 'image_link', 'title_color', 'pdf'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'image_link' => 'Image Link',
            'description' => 'Description',
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getBannerData($id) {
		$params = '';
        $query = Banners::find()->where(['and', 'page_id='.$id]);
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }

        return $dataProvider;
    }

}

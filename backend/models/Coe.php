<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Coe extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'coe';
    }

    public function rules() {
        return [
            [['status'], 'required'],
            [['status', 'title', 'description', 'image','mobile_image', 'sub_title'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'sub_title' => 'Sub Title',
            'description' => 'Description',
            'image' => 'Image',
            'mobile_image' => 'Mobile Image',
            'image_position' => 'Image Position',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getContentData($id) {
		$params = '';
        $query = Coe::find()->where(['and', 'page_id='.$id])->orderBy(['title' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }

}

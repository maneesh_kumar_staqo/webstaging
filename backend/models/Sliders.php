<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Sliders extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_sliders';
    }

    public function rules() {
        return [
            [['status'], 'required'],
			[['status','title', 'description'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getSliderData($id) {
        $params = '';
        $query = Sliders::find()->where(['and', 'page_id='.$id]);
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }

        return $dataProvider;
    }

}

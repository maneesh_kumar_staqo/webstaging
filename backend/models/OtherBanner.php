<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class OtherBanner extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'other_hfe_banners';
    }

    public function rules() {
        return [
            [['status','type'], 'required'],
			[['status','title', 'description', 'priority', 'sub_title', 'mobile_image', 'image_link', 'title_color', 'type'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'image_link' => 'Image Link',
            'description' => 'Description',
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

  public function search($params) {
		$query = OtherBanner::find();
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => ['pageSize' => 20],
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'page_title', $this->page_title]);
		$query->andFilterWhere(['like', 'status', $this->status]);
		
        return $dataProvider;
    }

}

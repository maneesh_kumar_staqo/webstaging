<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Bonholders extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_bonholders_information';
    }

    public function rules() {
        return [
            [['page_id','description', 'view_more','status'], 'required'],
            [['description', 'view_more','status'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getBonholdersData($id) {
		$params = '';
        $query = Bonholders::find()->where(['and', 'page_id='.$id]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'description', $this->description]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }

}

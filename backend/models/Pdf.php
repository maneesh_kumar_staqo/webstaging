<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Pdf extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_pdf';
    }

    public function rules() {
        return [
            [['pdf_name', 'pdf_link'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'pdf_name' => 'Pdf Name',
            'pdf_link' => 'Pdf Link',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function search($params) {
        $query = Pdf::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'pdf_name', $this->pdf_name]);
		$query->andFilterWhere(['like', 'pdf_link', $this->pdf_link]);
		
        return $dataProvider;
    }	
}

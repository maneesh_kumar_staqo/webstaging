<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class BusinessOfferTabdata extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_business_offer_tabdata';
    }

    public function rules() {
        return [
            [['status', 'heading'], 'required'],
            [['status', 'heading', 'image', 'priority','description'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'heading' => 'Heading',
            'image' => 'Image',
            'description'=>'Description',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

 	
    public function search($params) {
		$query = BusinessOfferTabdata::find();
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => ['pageSize' => 20],
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'username', $this->username]);
		$query->andFilterWhere(['like', 'email', $this->email]);
		$query->andFilterWhere(['like', 'status', $this->status]);
		
        return $dataProvider;
    }


}

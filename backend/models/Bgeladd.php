<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Bgeladd extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_bgel_address';
    }

    public function rules() {
        return [
            [['address', 'status'], 'required'],
            [['address', 'priority', 'status'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'status' => 'Status',
            'priority' => 'Priority',
            'address' => 'Address',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }
       public function getAddressData($id) {
		$params = '';
        $query = Bgeladd::find()->where(['and', 'page_id='.$id]);
     
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }

        return $dataProvider;
    }


    public function search($params) {
        $query = Bgeladd::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'address', $this->address]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		$query->andFilterWhere(['like', 'location', $this->location]);
		
        return $dataProvider;
    }	

  
}

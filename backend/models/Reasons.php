<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Reasons extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_rooftop_solar';
    }

    public function rules() {
        return [
            [['status', 'title'], 'required'],
            [['status', 'title', 'description', 'icon', 'priority', 'type'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'icon' => 'Icon',
            'description' => 'Description',
            'type' => 'Type',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getReasonsData($id) {
		$params = '';
		$type = 'Reasons_Choose_Us';
        $query = Reasons::find()->where(['and', 'page_id='.$id, 'type="'.$type.'"'])->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'name', $this->name]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }

}

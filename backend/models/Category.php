<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Category extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_category';
    }

    public function rules() {
        return [
            [['status', 'title'], 'required'],
            [['status', 'title', 'priority', 'type', 'link'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Category Name',
            'link' => 'Category Link',
            'type' => 'Type',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function search($params) {
        $query = Category::find()->orderBy(['priority' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['like', 'type', $this->type]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }

}

<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class EsgHighlights extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'esg_highlights';
    }

    public function rules() {
        return [
            [['status', 'title'], 'required'],
            [['status', 'title','heading', 'priority','description', ' esg_highlights', 'link','priority'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'heading' => 'Section Heading',
            'id' => 'Description',
            'title' => 'Title',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
            'priority'=>'Priority'
        ];
    }
     public function getHightlightsData($id) {
		$params = '';
        $query = EsgHighlights::find();
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }

        return $dataProvider;
    }


    public function search($params) {
        $query = EsgHighlights::find()->orderBy(['priority' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['like', 'type', $this->type]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }

}

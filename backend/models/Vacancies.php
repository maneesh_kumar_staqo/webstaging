<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Vacancies extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_career_vacancies';
    }

    public function rules() {
        return [
            [['status', 'title'], 'required'],
            [['status', 'title', 'pdf', 'short_description'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'status' => 'Status',
            'short_description' => 'Short Description',
            'pdf' => 'Pdf',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getVacancies() {
		$params = '';
        $query = Vacancies::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }
	
	
}

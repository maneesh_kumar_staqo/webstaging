<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class CommissionedProjects extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'commissioned_projects';
    }

    public function rules() {
        return [
            [['status', 'title','type','installation_type','location'], 'required'],
            [['status', 'title', 'description', 'priority', 'image','type','image_link','page_id','installation_type','location'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
            'priority' => 'Priority',
            'image_link'=>'Image Link',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getCommissionedProjects($id) {
        // echo $id;die;
		$params = '';
        $query = CommissionedProjects::find()->where(['and', 'page_id='.$id])->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }	

}

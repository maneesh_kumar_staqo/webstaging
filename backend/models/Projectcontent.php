<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Projectcontent extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_project_portfolio_content';
    }

    public function rules() {
        return [
            [['status'], 'required'],
			[['status','type', 'description'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'type' => 'Type',
            'description' => 'Description',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

	public function search($id, $params='') {
        $query = Projectcontent::find()->where(['and', 'page_id='.$id]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'type', $this->type]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }

}

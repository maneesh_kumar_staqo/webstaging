<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Ukraine extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'ukraine';
    }

    public function rules() {
        return [
            [['disclouser_title'], 'required'],
			[['about_title', 'about_desc','gallery_title','disclouser_title','company_name','contact_address','map_link','name','phone','email','created_on','updated_on','section1_menu','section2_menu','section3_menu','section4_menu'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'banner_text' => 'Banner Title',
            'banner_image' => 'Image',
            'about_title' => 'Section 1 Title',
            'about_desc' => ' Description',
            'gallery_title' => 'Section 2 Title',
            'gallery_image' => 'Image',
            'disclouser_title' => 'Section 3 Title',
            'disclouser_year' => 'Year',
            'disclouser_heading' => 'Title',
            'disclouser_type' => 'Type',
            'disclouser_file' => 'File',
            'company_name' => 'Company Name',
            'contact_title'=>'Section 4 Title',
			'contact_address' => 'Address',
			'map_link' => 'Map Links',
			'name' => 'Name',
			'phone' => 'Phone',
			'email'=>'email',
			'created_on' => 'Created On',
            'updated_on' => 'Updated On'
        ];
    }

      public function search($params) {
		$query = Ukraine::find();
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => ['pageSize' => 20],
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }
		
	
		$query->andFilterWhere(['like', 'name', $this->banner_text]);
		// $query->andFilterWhere(['like', 'designation', $this->designation]);
		// $query->andFilterWhere(['like', 'email', $this->email]);
        // $query->andFilterWhere(['like', 'seq', $this->seq]);
		
        return $dataProvider;
    }
   
	
	// Save page layout priority
	
	

	
}

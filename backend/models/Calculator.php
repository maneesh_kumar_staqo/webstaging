<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Calculator extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_calculator';
    }

    public function rules() {
        return [
            [['name', 'email_id', 'rooftop_area', 'phone'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'name' => 'Name',
            'email_id' => 'Email Id',
            'rooftop_area' => 'Rooftop Area',
            'phone' => 'Phone',
            'created_on' => 'Created On'
        ];
    }

    public function search($params) {
        $query = Calculator::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'name', $this->name]);
		$query->andFilterWhere(['like', 'email_id', $this->email_id]);
		$query->andFilterWhere(['like', 'phone', $this->phone]);
		
        return $dataProvider;
    }	
}

<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Portfolio extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_project_portfolio';
    }

    public function rules() {
        return [
            [['status', 'description', 'category_id'], 'required'],
            [['status', 'description', 'category_id'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'description' => 'Description',
            'category_id' => 'Category Name',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getPortfolioData() {
		$params = '';
        $query = Portfolio::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }	
	
	public function getContent($category_id){
		$connection = \Yii::$app->db;
		$sql = "select * from hfe_project_portfolio where category_id = :category_id and status = 'Active'";
		$command = $connection->createCommand($sql);
		$command->bindParam(':category_id', $category_id);
		$data = $command->queryAll();
		return $data;
	}
	
	public function getProjectContent($type){
		$connection = \Yii::$app->db;
		$sql = "select * from hfe_project_portfolio_content where type = :type and status = 'Active'";
		$command = $connection->createCommand($sql);
		$command->bindParam(':type', $type);
		$data = $command->queryAll();
		return $data;
	}

}

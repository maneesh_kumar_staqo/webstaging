<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Contactquery extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_contact_us_query';
    }

    public function rules() {
        return [
            [['name', 'email_id', 'type'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'name' => 'Name',
            'email_id' => 'Email Id',
            'type' => 'Type',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function search($params) {
        $query = Contactquery::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'name', $this->name]);
		$query->andFilterWhere(['like', 'email_id', $this->email_id]);
		$query->andFilterWhere(['like', 'type', $this->type]);
		
        return $dataProvider;
    }	
}

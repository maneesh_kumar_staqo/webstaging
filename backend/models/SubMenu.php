<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class SubMenu extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_submenu';
    }

    public function rules() {
        return [
            [['title', 'section_id','main_menu_id', 'status'], 'required'],
            [['title', 'section_id', 'status','external_link','menu_description','main_menu_id'], 'safe'],
        ];
    }

    public function attributeLabels() {
         return [
            'id' => 'Id',
            'status' => 'Status',
            'title' => 'Menu Name',
            'section_id' => 'Section Id',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function search($params) {
        // $query = SubMenu::find()->orderBy(['id' => SORT_ASC]);
        $query = SubMenu::find()->where(['and', 'main_menu_id='.$params['id']])->orderBy(['id' => SORT_ASC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->menu_name]);
		$query->andFilterWhere(['like', 'type', $this->type]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }	
}

<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Contact extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_contact_us';
    }

    public function rules() {
        return [
            [['status', 'title', 'category_id'], 'required'],
            [['status', 'title', 'priority', 'image', 'category_id', 'address1', 'address2', 'direction_link', 'direction_link2'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'image' => 'Image',
            'address1' => 'Registered office Address',
            'address2' => 'Corporate Office Address',
            'direction_link' => 'Registered Office Link',
            'direction_link2' => 'Corporate Office Link',
            'priority' => 'Priority',
            'category_id' => 'Category Name',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getContact($id) {
		$params = '';
        $query = Contact::find()->where(['and', 'page_id='.$id])->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }
	
	
}

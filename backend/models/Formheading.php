<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Formheading extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_page_heading';
    }

    public function rules() {
        return [
            [['heading', 'type', 'status'], 'required'],
            [['heading', 'type', 'status'], 'safe'],
        ];
    }

    public function attributeLabels() {
         return [
            'id' => 'Id',
            'status' => 'Status',
            'heading' => 'Heading',
            'type' => 'Type',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function search($params) {
        $query = Formheading::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'heading', $this->heading]);
		$query->andFilterWhere(['like', 'type', $this->type]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }	
}

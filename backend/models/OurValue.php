<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class OurValue extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_our_value';
    }

    public function rules() {
        return [
            [['status'], 'required'],
			[['status','title', 'description', 'priority', 'image_position', 'bg_color', 'link', 'image_pdf', 'short_description'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getImageData($id) {
        $params = '';
		if(!empty($_GET['imgtype'])){
			$imgtype = $_GET['imgtype'];
			$query = OurValue::find()->where(['and', 'page_id='.$id, 'type="'.$imgtype.'"']);
		}else{
			$query = OurValue::find()->where(['and', 'page_id='.$id])->andWhere(['IS', 'type', null]);	
		}
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }

        return $dataProvider;
    }
	
	public function getOurValueData() {
        $params = '';
		$query = OurValue::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }

        return $dataProvider;
    }

}

<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class SocialAction extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'social_action';
    }

    public function rules() {
        return [
            [['status'], 'required'],
			[['status','title','sec_heading', 'description', 'readmore_link', 'priority', 'sub_title', 'mobile_image', 'image','image_link'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'readmore_link' => 'Read More',
            'image_link' => 'Image Link',
            'description' => 'Description',
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getSocialData($id) {
		$params = '';
        $query = SocialAction::find();
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }

        return $dataProvider;
    }

}

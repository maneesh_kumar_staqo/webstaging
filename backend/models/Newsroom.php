<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Newsroom extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_news_room';
    }

    public function rules() {
        return [
            [['status','title', 'short_description'], 'required'],
            [['page_url'], 'unique'],
            [['status','title', 'description', 'tags', 'category_id', 'team_name', 'publish_date', 'short_description', 'page_url','meta_title', 'meta_description', 'meta_keywords'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'tags' => 'Tags',
            'category_id' => 'Category Name',
            'image_link' => 'Image Link',
            'description' => 'Description',
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function search($params) {
        $query = Newsroom::find()->orderBy(['publish_date' => SORT_DESC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }

}

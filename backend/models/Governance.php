<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Governance extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_corporate_governance';
    }

    public function rules() {
        return [
            [['status', 'title'], 'required'],
            [['status', 'title', 'description', 'image', 'sub_title', 'bg_color', 'priority'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'sub_title' => 'Sub Title',
            'description' => 'Description',
            'image' => 'Image',
            'image_position' => 'Image Position',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getGovernanceData($id) {
		$params = '';
        $query = Governance::find()->where(['and', 'page_id='.$id])->orderBy(['title' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }

}

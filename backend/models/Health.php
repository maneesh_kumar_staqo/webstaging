<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Health extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_health';
    }

    public function rules() {
        return [
            [['status'], 'required'],
            [['status', 'title', 'description', 'image', 'sub_title', 'bg_color','report'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'sub_title' => 'Sub Title',
            'description' => 'Description',
            'image' => 'Image',
            'image_position' => 'Image Position',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getContentData($id) {
		$params = '';
        $query = Health::find()->orderBy(['title' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }

}

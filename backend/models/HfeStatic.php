<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class HfeStatic extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_statics';
    }

    public function rules() {
        return [
            [['status'], 'required'],
			[['status','title', 'description', 'image', 'status', 'created_by', 'updated_by','priority'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'image' => 'Image',
            'description' => 'Description',
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getStaticsData($id) {
		$params = '';
        $query = HfeStatic::find();
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }

        return $dataProvider;
    }

}

<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Focusareas extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_focus_areas';
    }

    public function rules() {
        return [
            [['status', 'state_name', 'category_id'], 'required'],
            [['status', 'state_name', 'operational_beneficiary', 'priority', 'image', 'category_id'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'state_name' => 'State Name',
            'category_id' => 'Category Name',
            'operational_beneficiary' => 'Operational Beneficiary',
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getFocusareas($id) {
		$params = '';
        $query = Focusareas::find()->where(['and', 'page_id='.$id])->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'state_name', $this->state_name]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }
	
	
}

<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Careerquery extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_career_query';
    }

    public function rules() {
        return [
            [['name', 'email_id', 'apply_for', 'contact_no'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'name' => 'Name',
            'email_id' => 'Email Id',
            'apply_for' => 'Apply For',
            'contact_no' => 'Contact No.',
            'current_organisation' => 'Current Organisation',
            'current_designation' => 'Current Designation',
            'experience' => 'Experience',
            'resume' => 'Resume',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function search($params) {
        $query = Careerquery::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'name', $this->name]);
		$query->andFilterWhere(['like', 'email_id', $this->email_id]);
		$query->andFilterWhere(['like', 'apply_for', $this->apply_for]);
		$query->andFilterWhere(['like', 'contact_no', $this->contact_no]);
		
        return $dataProvider;
    }	
}

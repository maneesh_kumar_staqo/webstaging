<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Project extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_case_studies';
    }

    public function rules() {
        return [
            [['status', 'title'], 'required'],
            [['status', 'title', 'image_pdf', 'priority', 'image'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'image_pdf' => 'Image Pdf',
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getCaseStudies($id) {
		$params = '';
        $query = Project::find()->where(['and', 'page_id='.$id])->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }	

}

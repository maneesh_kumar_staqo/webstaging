<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Learn extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_career_learn_to_win';
    }

    public function rules() {
        return [
            [['status', 'title'], 'required'],
            [['status', 'title', 'description', 'image', 'image2', 'image3', 'image_short_description', 'image2_short_description', 'image3_short_description'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
            'image_short_description' => 'Image Short Description',
			'image2' => 'Image 2',
            'image2_short_description' => 'Image2 Short Description',
			'image3' => 'Image 3',
            'image3_short_description' => 'Image3 Short Description',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getLearn() {
		$params = '';
        $query = Learn::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }
	
	
}

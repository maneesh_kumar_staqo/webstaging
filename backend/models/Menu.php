<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Menu extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_menu';
    }

    public function rules() {
        return [
            [['title', 'type','page_url', 'status'], 'required'],
            [['title', 'type', 'status','page_url','menu_description'], 'safe'],
        ];
    }

    public function attributeLabels() {
         return [
            'id' => 'Id',
            'status' => 'Status',
            'title' => 'Menu Name',
            'type' => 'Type',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function search($params) {
        $query = Menu::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'menu_name', $this->menu_name]);
		$query->andFilterWhere(['like', 'type', $this->type]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }	
}

<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Videogallery extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_youtube';
    }

    public function rules() {
        return [
            [['status', 'title', 'category_id'], 'required'],
            [['status', 'title', 'priority', 'video_link', 'category_id', 'type'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'video_link' => 'Youtube Embed Code',
            'priority' => 'Priority',
            'category_id' => 'Category Name',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getVideogallery($id) {
		$params = '';
        $query = Videogallery::find()->where(['and', 'page_id='.$id])->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }
	
	
}

<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class BusinessOffer extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_business_offer';
    }

    public function rules() {
        return [
            [['status', 'heading'], 'required'],
            [['status', 'heading', 'image', 'priority', 'image','box_heading1','box_heading2','box_heading3','box_image1','box_image2','box_image3','box_description1','box_description2','box_description3'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'heading' => 'Heading',
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getBusinessOffer() {
		$params = '';
        $query = BusinessOffer::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }	
    	
    public function search($params) {
		$query = BusinessOffer::find();
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => ['pageSize' => 20],
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'username', $this->username]);
		$query->andFilterWhere(['like', 'email', $this->email]);
		$query->andFilterWhere(['like', 'status', $this->status]);
		
        return $dataProvider;
    }


}

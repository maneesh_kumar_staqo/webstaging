<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Directors extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_board_directors';
    }

    public function rules() {
        return [
            [['status', 'name', 'description', 'designation', 'type'], 'required'],
            [['status', 'name', 'description', 'designation', 'priority', 'type', 'image'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'name' => 'Name',
            'description' => 'Description',
            'designation' => 'Designation',
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getDirectors($id) {
		$params = '';
        $query = Directors::find()->where(['and', 'page_id='.$id])->orderBy(['designation' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'name', $this->name]);
		$query->andFilterWhere(['like', 'designation', $this->designation]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }

}

<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Disclosures extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_disclosures';
    }

    public function rules() {
        return [
            [['status','type'], 'required'],
            [['status', 'title', 'pdf', 'priority','month','years', 'category_id'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'category_id' => 'Category Name',
            'pdf' => 'Pdf',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getDisclosures($id,$params='') {
		$query = Disclosures::find()->where(['and', 'page_id='.$id])->orderBy(['id' => SORT_DESC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'years', $this->years]);
         $query->andFilterWhere(['like', 'month', $this->month]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }
	
	
}

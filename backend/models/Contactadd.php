<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Contactadd extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_contact_us_address';
    }

    public function rules() {
        return [
            [['location', 'email', 'status'], 'required'],
            [['email'], 'email'],
            [['location', 'email', 'title', 'phone','image', 'fax', 'priority', 'status'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'status' => 'Status',
            'email' => 'Email Id',
            'location' => 'Location',
            'title' => 'Title',
            'fax' => 'Fax',
            'phone' => 'Phone Number',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function search($params) {
        $query = Contactadd::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['like', 'email', $this->email]);
		$query->andFilterWhere(['like', 'phone', $this->phone]);
		$query->andFilterWhere(['like', 'fax', $this->fax]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		$query->andFilterWhere(['like', 'location', $this->location]);
		
        return $dataProvider;
    }
    public function getContact($id) {
		$params = '';
        $query = Contactadd::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }	
}

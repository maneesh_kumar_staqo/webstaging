<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Shareholders extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_investor_shareholders';
    }

    public function rules() {
        return [
            [['status', 'title','priority'], 'required'],
            [['status', 'title', 'page_id', 'image', 'priority'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'image' => 'Image',
            'image_position' => 'Image Position',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getShareholdersData($id) {
		$params = '';
        $query = Shareholders::find()->where(['and', 'page_id='.$id]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }

}

<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Pagelayout extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_page_layout';
    }

    public function rules() {
        return [
            [['status', 'page_name', 'page_url', 'page_type'], 'required'],
            [['page_url'], 'unique'],
            [['meta_title', 'meta_description', 'meta_keywords'], 'string', 'max' => 1000],
			[['status', 'page_name', 'page_url', 'page_type'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'page_name' => 'Page Name',
            'page_url' => 'Page Url',
            'page_type' => 'Page Type',
            'status' => 'Status',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function search($params) {
        $query = Pagelayout::find()->orderBy(['page_name' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'page_name', $this->page_name]);
		$query->andFilterWhere(['like', 'page_url', $this->page_url]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }
	
	// Save page layout priority
	public function savePagePriority($data){
		$page_id = !empty($data['page_id']) ? $data['page_id'] : '';
		$table_name = !empty($data['table_name']) ? $data['table_name'] : '';
		$page_type = !empty($data['page_type']) ? $data['page_type'] : '';
		$priority = !empty($data['priority']) ? $data['priority'] : '';
		$created_on = date("Y-m-d H:i:s");
		
		$connection = \Yii::$app->db;	
		$sql = "select id from hfe_page_priority where page_id = :page_id and table_name = :table_name and page_type = :page_type";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_id', $page_id);
		$command->bindParam(':table_name', $table_name);
		$command->bindParam(':page_type', $page_type);
		$data = $command->queryAll();
		if(!empty($data[0]['id'])){
			$sql = "update hfe_page_priority set priority = :priority, page_type = :page_type where id = :id";
			$command = $connection->createCommand($sql);
			$command->bindParam(':id', $data[0]['id']);
			$command->bindParam(':priority', $priority);
			$command->bindParam(':page_type', $page_type);
			$command->execute();
		}else{
			$sql = "insert into hfe_page_priority (page_id,table_name,page_type,priority,created_on) values (:page_id,:table_name,:page_type,:priority,:created_on)";
			$command = $connection->createCommand($sql);
			$command->bindParam(':page_id', $page_id);
			$command->bindParam(':table_name', $table_name);
			$command->bindParam(':page_type', $page_type);
			$command->bindParam(':priority', $priority);
			$command->bindParam(':created_on', $created_on);
			$command->execute();	
		}
	}
	
	// For get page priority
	public function getPriorityData($page_id){
		$connection = \Yii::$app->db;	
		$sql = "select * from hfe_page_priority where page_id = :page_id";
		$command = $connection->createCommand($sql);
		$command->bindParam(':page_id', $page_id);
		$data = $command->queryAll();
		$priorityArray = array();
		if(!empty($data)){
			foreach($data as $values){
				$priorityArray[$values['page_type']] = 	$values['priority'];
			}
		}
		return $priorityArray;
	}
	
}

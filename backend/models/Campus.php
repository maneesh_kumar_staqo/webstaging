<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Campus extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_career_campus_placements';
    }

    public function rules() {
        return [
            [['status', 'title'], 'required'],
            [['status', 'title', 'priority', 'short_description', 'description', 'image'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'title' => 'Title',
            'priority' => 'Priority',
            'status' => 'Status',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'image' => 'Photo',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getCampus() {
		$params = '';
        $query = Campus::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'title', $this->title]);
		$query->andFilterWhere(['=', 'status', $this->status]);
		
        return $dataProvider;
    }
	
	
}

<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Investor extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'investor_relation_logo';
    }

    public function rules() {
        return [
               [['status','priority'], 'required'],
			[['status', 'image', 'status', 'created_by', 'updated_by','priority'], 'safe']
        ];
    }

    public function attributeLabels() {
        return [
         'id' => 'Id',
           
            'image' => 'Image',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function getInvestorData($id) {
        $params = '';
		$query = Investor::find()->where(['and', 'page_id='.$id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }

        return $dataProvider;
    }
	
}

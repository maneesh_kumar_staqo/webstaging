<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use Yii;

class Rooftopquery extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'hfe_rooftop_solar_query';
    }

    public function rules() {
        return [
            [['name', 'email', 'phone', 'designation', 'company_name', 'city', 'monthly_electricity_consuption', 'approximate_space_available', 'message', 'average_monthly_bill', 'annual_turnover'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'Id',
            'name' => 'Name',
            'email' => 'Email',
            'designation' => 'Designation',
            'company_name' => 'Company Name',
            'phone' => 'Phone',
            'city' => 'City',
            'annual_turnover' => 'Annual Turnover',
            'average_monthly_bill' => 'Average Monthly Bill',
            'message' => 'Message',
            'monthly_electricity_consuption' => 'Monthly Electricity Consuption',
            'approximate_space_available' => 'Approximate Space Available',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On'
        ];
    }

    public function search($params) {
        $query = Rooftopquery::find()->orderBy(['id' => SORT_ASC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }
		
		$query->andFilterWhere(['like', 'name', $this->name]);
		$query->andFilterWhere(['like', 'email', $this->email]);
		$query->andFilterWhere(['like', 'designation', $this->designation]);
		$query->andFilterWhere(['like', 'company_name', $this->company_name]);
		$query->andFilterWhere(['like', 'phone', $this->phone]);
		$query->andFilterWhere(['like', 'city', $this->city]);
		
        return $dataProvider;
    }	
}

<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Menu';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <a href="<?php echo BASE_URL; ?>menu/create" title="Create Menu">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Title',
                                'attribute' => 'title',
                                'value' => 'title',
                            ],
                             [
                                'label' => 'Page Url',
                                'attribute' => 'page_url',
                                'value' => 'page_url',
                            ],
							[
                                'label' => 'Type',
                                'attribute' => 'type',
                                'value' => 'type'
                            ],
							[
                                'label' => 'Status',
                                'attribute' => 'status',
                                'value' => 'status'
                            ],
							[
                                'class' => 'yii\grid\ActionColumn',
                                'options' => ['style' => 'width:15px;'],
                                'template' => '{update}'
                            ],
                        ],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

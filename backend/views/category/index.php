<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;

$this->title = 'Category List';
?>
<div class="panel panel-default" >
	<div class="panel-heading">
		<?php echo $this->title; ?>
		<div class="menuLink">
			<a href="<?php echo BASE_URL; ?>category/create" title="Create Category">
				<i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
			</a>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			
			<?=
			GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'summary' => FALSE,
				'columns' => [

					[
						'label' => 'Category Name',
						'attribute' => 'title',
						'value' => 'title',
						'filter' => true,
					],
					// [
					// 	'label' => 'Type',
					// 	'attribute' => 'type',
					// 	'value' => function($model){
					// 		return ucwords(str_replace("_", " ", $model->type));
					// 	},
					// 	'filter' => true,
					// ],
					[
						'label' => 'Status',
						'attribute' => 'status',
						'value' => 'status',
						'filter' => true,
					],
					[
						'label' => 'Priority',
						'attribute' => 'priority',
						'value' => 'priority',
						'filter' => false,
					],
					[
						'class' => 'yii\grid\ActionColumn',
						'options' => ['style' => 'width:15px;'],
						'template' => '{update}'
					],
				],
			]);
			?>
			
		</div>
	</div>
</div>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$imgtype = !empty($_GET['imgtype']) ? $_GET['imgtype'] : '';
$type = !empty($_GET['type']) ? $_GET['type'] : '';
$this->title = 'Create Our Value';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?><?= $_GET['page']; ?>/update?id=<?= $_GET['page_id']; ?>&type=<?= $type; ?>&imgtype=<?= $imgtype; ?>" title="Images Our Value">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                                ]
                );
                ?>            

                    
                    <div class="form-group">
                        <?= $form->field($model, 'title')->textInput() ?>      
                    </div>
					<div class="form-group">
                        <?= $form->field($model, 'short_description')->textarea(['rows' => 4]); ?>
                    </div>
					<div class="form-group">
                        <?= $form->field($model, 'description')->textarea(['rows' => 5, 'class' => 'left_class_editor']); ?>
                    </div>
					<div class="form-group">
						<div style="width:50%; float:left;">
							<?= $form->field($model, 'link')->textInput() ?>      
						</div>
					
						<div style="width:50%;float:left;">
							<?= $form->field($model, 'priority')->textInput() ?>      
						</div>
					</div>
					<div class="form-group">	
						<div style="width:50%;float:left;">
                            <div class="form-group">
                                <div style="width:50%; float:left; margin-left:12px; margin-top:24px; margin-bottom:20px;">
                                    <label class="control-label" for="images-image">Image</label>
                                    <input type="file" name="image" />
                                </div>
                            
                            </div>
						</div>
						<div style="width:50%;float:left;">
							<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
						</div>
					</div>
					
					
                    <div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                        <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                        <?= Html::a('Cancel', ['/'.$_GET['page'].'/update?id='.$_GET['page_id'].'&type='.$_GET['type'].'&imgtype='.$imgtype], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

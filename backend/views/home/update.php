<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$bannerClass = "";
$contentClass = "";
$directorsClass = "";
$managementClass = "";
$governanceClass = "";
$imagesClass = "";
$leadershipClass = "";
$pageLayoutClass = "";
$hfestatics = "";
$investorrelation="";
$section4="";
$section5="";
$blogsClass = "";
$videoClass = "";
$pageLayoutClass ="";
if(!empty($type)){
	if($type == "page-banners"){
		$bannerClass = "active";
	}else if($type == "page-images"){
		$imagesClass = "active";
	}else if($type == "page-blogs"){
		$blogsClass = "active";
	}else if($type == "page-video"){
		$videoClass = "active";
	}else if($type == "page-focus"){
		$pageLayoutClass = "active";
	}
	if($type == "page-banners"){
		$bannerClass = "active";
	}else if($type == "page-contents"){
		$contentClass = "active";
	}else if($type == "board-directors"){
		$directorsClass = "active";
	}else if($type == "management-team"){
		$managementClass = "active";
	}else if($type == "page-governance"){
		$governanceClass = "active";
	}else if($type == "page-images"){
		$imagesClass = "active";
	}else if($type == "page-leadership"){
		$leadershipClass = "active";
	}else if($type == "page-layout"){
		$pageLayoutClass = "active";
	}
	else if($type == "investor-relation"){
		$investorrelation = "active";
	}
	else if($type == "hfe-statics"){
		$hfestatics = "active";
	}
	else if($type == "esg-highlights"){
		$section4 = "active";
	}
	else if($type == "social-action"){
		$section5 = "active";
	}
}else{
	$pageLayoutClass = "active"; 
}

$this->title = 'Update Page Layout';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="doctor-master-update">
    <div class="row">
        <div role="tabpanel">
            <div class="col-lg-2 col-md-offset-0" style="margin-left: -25px;margin-top: 5px">
                <!-- Nav tabs -->
                <ul class="left_tab nav-tabs nav-pills nav-stacked" role="tablist">
				<li role="presentation" class="<?= $pageLayoutClass; ?>"><a  href="<?= BASE_URL . 'home/update?id='.$id.'&type=page-layout' ?> ">Page Layout</a></li>
					<li role="presentation" class="<?= $bannerClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'home/update?id='.$id.'&type=page-banners' ?> ">Page Banners</a> <span><input type="text" id="pagePriority_banners" value="<?= !empty($priority['banners']) ? $priority['banners'] : ''; ?>" onchange="savePageLayout('banners','hfe_banners','<?= $id; ?>');"></span> </li>

					<li role="presentation" class="<?= $hfestatics; ?> hasTextbox"><a
							href="<?= BASE_URL . 'home/update?id='.$id.'&type=hfe-statics' ?> ">HFE Stactic</a> <span><input
								type="text" id="pagePriority_hfe_statics"
								value="<?= !empty($priority['hfe_statics']) ? $priority['hfe_statics'] : ''; ?>"
								onchange="savePageLayout('hfe_statics','hfe_statics','<?= $id; ?>');"></span> </li>



					<li role="presentation" class="<?= $investorrelation; ?> hasTextbox"><a
							href="<?= BASE_URL . 'home/update?id='.$id.'&type=investor-relation' ?> ">Investor Relation</a> <span><input
								type="text" id="pagePriority_investor_relation_logo"
								value="<?= !empty($priority['investor_relation_logo']) ? $priority['investor_relation_logo'] : ''; ?>"
								onchange="savePageLayout('investor_relation_logo','investor_relation_logo','<?= $id; ?>');"></span> </li>

					<li role="presentation" class="<?= $section4; ?> hasTextbox"><a
							href="<?= BASE_URL . 'home/update?id='.$id.'&type=esg-highlights' ?> ">ESG Highlights</a> <span><input
								type="text" id="pagePriority_esg-highlights"
								value="<?= !empty($priority['esg-highlights']) ? $priority['esg-highlights'] : ''; ?>"
								onchange="savePageLayout('esg-highlights','esg_highlights','<?= $id; ?>');"></span> </li>

					<li role="presentation" class="<?= $section5; ?> hasTextbox"><a
							href="<?= BASE_URL . 'home/update?id='.$id.'&type=social-action' ?> ">Social  Action</a> <span><input
								type="text" id="pagePriority_social-action"
								value="<?= !empty($priority['social-action']) ? $priority['social-action'] : ''; ?>"
								onchange="savePageLayout('social-action','social_action','<?= $id; ?>');"></span> </li>

					<li role="presentation" class="<?= $blogsClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'home/update?id='.$id.'&type=page-blogs' ?>">Blogs</a> <span><input type="text" id="pagePriority_blogs" value="<?= !empty($priority['blogs']) ? $priority['blogs'] : ''; ?>" onchange="savePageLayout('blogs','hfe_blogs','<?= $id; ?>');"></span></li>
					<li role="presentation" class="<?= $videoClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'home/update?id='.$id.'&type=page-video' ?>">Vlogs</a> <span><input type="text" id="pagePriority_video" value="<?= !empty($priority['video']) ? $priority['video'] : ''; ?>" onchange="savePageLayout('video','hfe_youtube','<?= $id; ?>');"></span></li>
				</ul>
            </div> 
            <div class="col-lg-10">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane <?= $pageLayoutClass; ?>">
                        <div class="panel panel-default">
							 <div class="panel-heading">
								<?php echo $this->title ?>
								<div class="menuLink">
									<a href="<?php echo BASE_URL; ?>page-layout" title="List">
										<i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
									</a>
								</div>
							</div>
							<div class="panel-body">
								<div class="row table-bordered">
									<?php
									$form = ActiveForm::begin([
												'id' => 'content_form',
												'options' => [
													'enctype' => 'multipart/form-data',
												],
													]
									);
									?>            


										<div class="form-group">
											<?= $form->field($model, 'page_name')->textInput(['onkeyup' => "getPageUrl($(this).val(), 'pageUrl')"]) ?>      
										</div>
										
										<div class="form-group">
											<?= $form->field($model, 'page_url')->textInput(["id" => "pageUrl"]) ?>      
										</div>
										<div class="form-group">
											<div style="width:50%;float:left;">
												<?= $form->field($model, 'page_type')->dropDownList(['' => 'Select Page Type', 'Home Page Layout' => 'Home Page Layout','About Page Layout' => 'About Page Layout','CSR Page Layout' => 'CSR Page Layout','Project Page Layout' => 'Project Page Layout']); ?>
											</div>
											 <div style="width:50%;float:left;">
												<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
											</div>
										</div>
										<div class="form-group">    
											<?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 1000]) ?>
										</div>

										<div class="form-group">    
											<?= $form->field($model, 'meta_title')->textInput(['maxlength' => 1000]) ?>
										</div>
										<div class="form-group">    
											<?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>
										</div>
										<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
											<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
											<?= Html::a('Cancel', ['/pagelayout/index'], ['class' => 'btn btn-danger']) ?>
										</div>
									</div>
								</div>
								<?php ActiveForm::end(); ?>
							</div>
                    </div>
                     <div role="tabpanel" class="tab-pane <?= $bannerClass; ?>">
                        <?php
                        echo $this->render('../banners/index', [
                            'dataProvider' => $bannersDataProvider,
                            'searchModel' => $bannersDataProvider,
                            'page' => 'pagelayout'
                        ])
                        ?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $hfestatics; ?>">
						<?php
					                        echo $this->render('../hfe-statics/index', [
					                            'dataProvider' => $hfestatic,
					                            'searchModel' => $hfestatic,
					                            'page' => 'pagelayout'
					                    ])
					      ?>
					</div>
					<div role="tabpanel" class="tab-pane <?= $investorrelation; ?>">
						<?php
					                        echo $this->render('../investor/index', [
					                            'dataProvider' => $investor,
					                            'searchModel' => $investor,
					                            'page' => 'pagelayout'
					                    ])
					      ?>
					</div>
					<div role="tabpanel" class="tab-pane <?= $section4; ?>">
						<?php
					                        echo $this->render('../esg-highlights/index', [
					                            'dataProvider' => $hightlights,
					                            'searchModel' => $hightlights,
					                            'page' => 'pagelayout'
					                    ])
					      ?>
					</div>
					<div role="tabpanel" class="tab-pane <?= $section5; ?>">
						<?php
					                        echo $this->render('../social-action/index', [
					                            'dataProvider' => $socialaction,
					                            'searchModel' => $socialaction,
					                            'page' => 'pagelayout'
					                    ])
					      ?>
					</div>
                    
					

					<div role="tabpanel" class="tab-pane <?= $imagesClass; ?>">
                        <?php
							echo $this->render('../images/index', [
								'dataProvider' => $imageDataProvider,
								'searchModel' => $imageDataProvider,
								'page' => 'pagelayout'
							])
						  ?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $blogsClass; ?>">
                        <?php
                        echo $this->render('../homeblogs/index', [
                            'dataProvider' => $blogsDataProvider,
                            'searchModel' => $blogsDataProvider,
							'page' => 'home'
                        ])
                        ?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $videoClass; ?>">
                        <?php
                        echo $this->render('../youtube/index', [
                            'dataProvider' => $youtubeDataProvider,
                            'searchModel' => $youtubeDataProvider,
							'page' => 'home'
                        ])
                        ?>
                    </div>
				</div>
            </div>
        </div>
    </div>
</div>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Create Projects';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
        <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>commissioned-projects/update?id=<?= $_GET['page_id']; ?>&type=commissioned-projects"
                    title="Commissioned Projects List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                                ]
                );
                ?>


                <div>
                    <div class="row">
                        <div class="col-md-12">

                            <?= $form->field($model, 'title')->textInput() ?>

                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label" for="project-title">Image</label>
                                <input type="file" name="image" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">

                            <?= $form->field($model, 'image_link')->textInput() ?>

                        </div>
                         <div class="col-md-4">
                            <?= $form->field($model, 'type')->dropDownList(['' => 'Select Type','Feature Project' => 'Feature Project', 'Commissioned Projects  ' => 'Commissioned Projects']); ?>
                        </div>
                         <div class="col-md-6">

                            <?= $form->field($model, 'location')->textInput() ?>

                        </div>
                         <div class="col-md-6">

                             <?= $form->field($model, 'installation_type')->dropDownList(['' => 'Select Installation Type','Utility Scale Solar' => 'Utility Scale Solar', 'Hybrid' => 'Hybrid','Utility Scale Wind'=>'Utility Scale Wind']); ?>

                        </div>
                        <div class="col-md-12">

                            <?= $form->field($model, 'description')->textarea(['rows' => 5, 'class' => 'left_class_editor']); ?>

                        </div>
                        <div class="col-md-6">
                                               
                                                    <?= $form->field($model, 'priority')->textInput(); ?>
                        </div>
                        <div class="col-md-6">
                        
<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
                        </div>
                    </div>
                </div>
     


                       
            
       

            </div>
            <div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                <?= Html::a('Cancel', ['/project/update?id='.$_GET['page_id'].'&type=commissioned-projects'], ['class' => 'btn btn-danger']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
</div>
</div>
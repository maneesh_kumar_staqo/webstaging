<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Update Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>commissioned-projects/update?id=<?= $_GET['page_id']; ?>&type=page-studies" title="Case Studies List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                             'enctype' => 'multipart/form-data',
						],
							]
                );
                ?>            

                    <div class="row">
                        <div class="col-md-12">

                            <?= $form->field($model, 'title')->textInput() ?>

                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                              
								
							
							<label class="control-label"  for="pagelayout-image">Image</label>
							<input type="file" name="image"  class="form-control"/>
							<input type="hidden" name="image" id="image<?= $model->id; ?>" value="<?= $model->image; ?>"/>
					
						<?php if(!empty($model->image)){ ?>
						<div  id="delete_img_<?= $model->id; ?>">
							<div style=""><img src="<?= BASE_URL; ?>img/close.png" style="cursor:pointer;" title="Delete Image" onClick="return deleteImg('<?= $model->id; ?>');"/></div>
							<img src="<?= BASE_URL ?>/uploads/project/<?= $model->image; ?>" height="50" width="50"/>
						</div>
						<?php } ?>

                            </div>
                        </div>
                        <div class="col-md-4">

                            <?= $form->field($model, 'image_link')->textInput() ?>

                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'type')->dropDownList(['' => 'Select Type','Feature Project' => 'Feature Project', 'Commissioned Projects' => 'Commissioned Projects']); ?>
                        </div>
                          <div class="col-md-6">

                            <?= $form->field($model, 'location')->textInput() ?>

                        </div>
                         <div class="col-md-6">

                            <?= $form->field($model, 'installation_type')->dropDownList(['' => 'Select Installation Type','Utility Scale Solar' => 'Utility Scale Solar', 'Hybrid' => 'Hybrid','Utility Scale Wind'=>'Utility Scale Wind']); ?>


                        </div>
                        <div class="col-md-12">

                            <?= $form->field($model, 'description')->textarea(['rows' => 5, 'class' => 'left_class_editor']); ?>

                        </div>
                        <div class="col-md-6">
                               <?= $form->field($model, 'priority')->textInput(); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
                        </div>
                    <div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                        <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                        <?= Html::a('Cancel', ['/project/update?id='.$_GET['page_id'].'&type=commissioned-projects'], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

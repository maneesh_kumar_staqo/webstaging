<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;

$this->title = 'Projects';
?>
<div class="panel panel-default" >
	<div class="panel-heading">
		<?php echo $this->title; ?>
		<div class="menuLink">
			<a href="<?php echo BASE_URL; ?>commissioned-projects/create?page_id=<?= $_GET['id']; ?>&type=page-studies" title="Create Case Studies">
				<i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
			</a>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			
			<?=
			GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'summary' => FALSE,
				'columns' => [

					[
						'label' => 'Title',
						'attribute' => 'title',
						'value' => 'title',
						'filter' => false,
					],

					[
						'label' => 'Image',
						'attribute' => 'image',
						'format' => 'raw',
						'value' => function($dataProvider){
							return '<img src="'.BASE_URL.'/uploads/project/'.$dataProvider->image.'" height="50" width="50"/>';
						},
						'filter' => false,
					],
					[
						'label' => 'Location',
						'attribute' => 'location',
						'value' => 'location',
						'filter' => false,
					],
					[
						'label' => 'Priority',
						'attribute' => 'priority',
						'value' => 'priority',
						'filter' => false,
					],
					[
						'label' => 'Status',
						'attribute' => 'status',
						'value' => 'status',
						'filter' => false,
					],
					[
						'class' => 'yii\grid\ActionColumn',
						'template' => '{update}',
						'buttons' => [
							'update' => function ($url, $searchModel) {
							return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
										'title' => Yii::t('yii', 'Update'),
							]);
						},
						],
						'urlCreator' => function ($action, $searchModel, $key, $index) {
						if ($action === 'update') {
							$url = \Yii::$app->urlManager->createAbsoluteUrl('commissioned-projects/update' . '?id=' . $searchModel->id . '&page_id=' . $_GET['id'] . '&type=commissioned-projects');
								return $url;
							}
						}
					],
					
					
				],
			]);
			?>
			
		</div>
	</div>
</div>
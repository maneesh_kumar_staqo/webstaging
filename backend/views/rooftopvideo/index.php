<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;

$type = !empty($_GET['type']) ? $_GET['type'] : '';
$page_id = !empty($_GET['id']) ? $_GET['id'] : '';
$page = !empty($_GET['page']) ? $_GET['page'] : '';
$this->title = 'Youtube Videos';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <a href="<?php echo BASE_URL; ?>youtube/create?page_id=<?= $page_id; ?>&page=<?= $page; ?>&type=<?= $type; ?>" title="Create Youtube Video">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Title',
                                'attribute' => 'title',
                                'value' => 'title',
								'filter' => false,
                            ],
							[
                                'label' => 'Video Thumb Image',
                                'attribute' => 'video_thumb_image',
								'format' => 'raw',
                                'value' => function($dataProvider){
									return '<img src="'.BASE_URL.'/uploads/youtube/'.$dataProvider->video_thumb_image.'" height="50" width="50"/>';
								},
								'filter' => false,
                            ],
							[
                                'label' => 'Video Link',
                                'attribute' => 'video_link',
                                'value' => 'video_link',
								'filter' => false,
                            ],
							[
                                'label' => 'Is Home',
                                'attribute' => 'is_home',
                                'value' => 'is_home',
								'filter' => false,
                            ],
							// [
                            //     'label' => 'Is Rooftop',
                            //     'attribute' => 'is_rooftop',
                            //     'format' => 'raw',
                            //     'value' => function($model){
							// 		$check = '';
							// 		if($model->is_rooftop == "Yes"){
							// 			$check = "checked='checked'";
							// 		}
							// 		return '<input type="checkbox" '.$check.' name="video" id="rooftop_'.$model->id.'" onclick="return updateVideo('.$model->id.')" style="cursor:pointer;">';
							// 	},
							// 	'filter' => false,
                            // ],
							[
                                'label' => 'Status',
                                'attribute' => 'status',
                                'value' => 'status',
								'filter' => false,
                            ],
                            [
								'class' => 'yii\grid\ActionColumn',
								'template' => '{update}',
								'buttons' => [
									'update' => function ($url, $searchModel) {
									return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
												'title' => Yii::t('yii', 'Update'),
									]);
								},
								],
								'urlCreator' => function ($action, $searchModel, $key, $index) {
								if ($action === 'update') {
									$type = !empty($_GET['type']) ? $_GET['type'] : '';
									$page_id = !empty($_GET['id']) ? $_GET['id'] : '';
									$url = \Yii::$app->urlManager->createAbsoluteUrl('youtube/update' . '?id=' . $searchModel->id . '&page_id=' . $page_id . '&type='.$type.'&page=rooftop');
										return $url;
									}
								}
							],
                        ],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function updateVideo(video_id){
	if($("#rooftop_" + video_id).is(":checked")){
		$.ajax({
			type: 'get',
			url: JS_BASE_URL + 'homevideo/updaterooftopvideo',
			data: {video_id : video_id, type : 'Yes'},
			success: function(data){
				alert("Updated successfully.");
			}
		});	
	}else{
		$.ajax({
			type: 'get',
			url: JS_BASE_URL + 'homevideo/updaterooftopvideo',
			data: {video_id : video_id, type : 'No'},
			success: function(data){
				alert("Updated successfully.");
			}
		});	
	
	}
}
</script>
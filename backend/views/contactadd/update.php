<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Update Board of Director';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>contactadd/index" title="Contact Us Address List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                             'enctype' => 'multipart/form-data',
						],
							]
                );
                ?>            

                    <div class="form-group">
                        <?= $form->field($model, 'location')->textInput() ?>      
                    </div>
					<div class="form-group">
                        <?= $form->field($model, 'title')->textInput() ?>      
                    </div>
					<div class="form-group">
                        <?= $form->field($model, 'phone')->textInput() ?>      
                    </div>
					<div class="form-group">
                        <?= $form->field($model, 'fax')->textInput() ?>      
                    </div>
					<div class="form-group">
                        <?= $form->field($model, 'email')->textInput() ?>      
                    </div>
                     <div class="form-group">
                             <div class="form-group"> 
								
							
							<label class="control-label"  for="pagelayout-image">Image</label>
							<input type="file" name="image"  class="form-control"/>
							<input type="hidden" name="image" id="image<?= $model->id; ?>" value="<?= $model->image; ?>"/>
					
						<?php if(!empty($model->image)){ ?>
						<div  id="delete_img_<?= $model->id; ?>">
							<div style=""><img src="<?= BASE_URL; ?>img/close.png" style="cursor:pointer;" title="Delete Image" onClick="return deleteImg('<?= $model->id; ?>');"/></div>
							<img src="<?= BASE_URL ?>/uploads/contact/<?= $model->image; ?>" height="50" width="50"/>
						</div>
						<?php } ?>
                        </div>
                            </div>

					<div class="form-group">
						<div style="width:50%;float:left;">
							<?= $form->field($model, 'priority')->textInput() ?>      
						</div>
						<div style="width:50%;float:left;">
							<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
						</div>
					</div>
                    <div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                        <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                        <?= Html::a('Cancel', ['/contactadd/index'], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

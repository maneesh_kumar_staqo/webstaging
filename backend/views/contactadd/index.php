<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Contact Us Address';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <a href="<?php echo BASE_URL; ?>contactadd/create" title="Create Contact Address">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Location',
                                'attribute' => 'location',
                                'value' => 'location',
                            ],
							[
                                'label' => 'Title',
                                'attribute' => 'title',
                                'value' => 'title',
                            ],
							[
                                'label' => 'Phone',
                                'attribute' => 'phone',
                                'value' => 'phone',
                            ],
							[
                                'label' => 'Fax',
                                'attribute' => 'fax',
                                'value' => 'fax'
                            ],
							[
                                'label' => 'Email Id',
                                'attribute' => 'email',
                                'value' => 'email'
                            ],
							[
                                'label' => 'Status',
                                'attribute' => 'status',
                                'value' => 'status'
                            ],
								[
						'class' => 'yii\grid\ActionColumn',
						'template' => '{update}',
						'buttons' => [
							'update' => function ($url, $searchModel) {
							return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
										'title' => Yii::t('yii', 'Update'),
							]);
						},
						],
						'urlCreator' => function ($action, $searchModel, $key, $index) {
						if ($action === 'update') {
							$url = \Yii::$app->urlManager->createAbsoluteUrl('contactadd/update' . '?id=' . $searchModel->id);
								return $url;
							}
						}
					],
                        ],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

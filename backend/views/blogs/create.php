<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Create Blog';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>blogs" title="Blogs List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                                ]
                );
                ?>            


                    <div class="form-group">
                        <?= $form->field($model, 'title')->textInput(['onkeyup' => "getPageUrl($(this).val(), 'pageUrl')"]) ?>      
                    </div>
					<div class="form-group">
                        <?= $form->field($model, 'page_url')->textInput(["id" => "pageUrl"]) ?>      
                    </div>
					<div class="form-group">
                        <?= $form->field($model, 'short_description')->textarea(['rows' => 3, 'class' => 'editor_short_desc']); ?>
                    </div>
					<div class="form-group">
                        <?= $form->field($model, 'description')->textarea(['rows' => 5, 'class' => 'right_class_editor']); ?>
                    </div>
					<div class="form-group">
                        <?= $form->field($model, 'tags')->textarea(['rows' => 3]); ?>
                    </div>
					<div class="form-group">
						<div style="width:50%;float:left;">
							<?= $form->field($model, 'team_name')->textInput() ?>
						</div>
						<div style="width:50%;float:left;">
							<?= $form->field($model, 'publish_date')->textInput(['placeholder' => "yyyy-mm-dd"]) ?>
						</div>
					</div>
					<div class="form-group">
						<div style="width:50%; float:left; margin-left:16px; margin-top:24px;">
							<input type="file" name="image"/>
							<input type="hidden" name="uploaded_image" value="<?= $model->image; ?>"/>
						</div>
						<div style="width:50%;float:left;">
							<?= $form->field($model, 'image_link')->textInput() ?>
						</div>
						<div style="width:33%;float:left;">
							<?= $form->field($model, 'category_id')->dropDownList($categoryData); ?>
						</div>
                         <div style="width:33%;float:left;">
							<?= $form->field($model, 'priority')->textInput() ?>
						</div>
						 <div style="width:33%;float:left;">
							<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
						</div>
                       
					</div>
					<div class="form-group">    
                        <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 1000]) ?>
                    </div>

                    <div class="form-group">    
                        <?= $form->field($model, 'meta_title')->textInput(['maxlength' => 1000]) ?>
                    </div>
                    <div class="form-group">    
                        <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>
                    </div>
                    <div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                        <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Update Banner';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?><?= $_GET['page']; ?>/update?id=<?= $_GET['page_id']; ?>&type=page-banners" title="Banners List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                             'enctype' => 'multipart/form-data',
						],
							]
                );
                ?>            

                    <div class="form-group">
						<div style="width:85%;float:left;">
							<?= $form->field($model, 'title')->textInput() ?>      
						</div>
						<div style="width:15%;float:left;">
						<?= $form->field($model, 'title_color')->textInput(['type' => 'color']) ?>   
						</div>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'sub_title')->textInput() ?>      
                    </div>
					<div class="form-group">
                         <?= $form->field($model, 'description')->textarea(['rows' => 4]); ?>
                    </div>
					<div class="form-group">
						<div style="width:40%;float:left;">
							<?= $form->field($model, 'image_link')->textInput() ?>
						</div>
						<div style="width:40%;float:left;">
							<?= $form->field($model, 'priority')->textInput() ?>
						</div>
						 <div style="width:20%;float:left;">
							<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
						</div>
					</div>
					<div class="form-group">
						<div style="width:20%; float:left; margin-left:12px; margin-top:10px;margin-bottom: 30px;">
							<label class="control-label" for="pagelayout-image">Image</label>
							<input type="file" name="image"/>
							<input type="hidden" name="uploaded_image" id="uploaded_image_<?= $model->id; ?>" value="<?= $model->image; ?>"/>
						</div>
						<?php if(!empty($model->image)){ ?>
						<div style="width:10%; float:left; margin-left:16px; margin-top:24px;" id="delete_img_<?= $model->id; ?>">
							<div style="position:absolute;margin-top:-11px;margin-left:42px;"><img src="<?= BASE_URL; ?>img/close.png" style="cursor:pointer;" title="Delete Image" onClick="return deleteImg('<?= $model->id; ?>');"/></div>
							<img src="<?= BASE_URL ?>/uploads/banners/<?= $model->image; ?>" height="50" width="50"/>
						</div>
						<?php } ?>
						<div style="width:20%;float:left;margin-bottom: 30px; margin-top:10px;">
							<label class="control-label" for="pagelayout-image">Mobile Image</label>
							<input type="file" name="mobile_image"/>
							<input type="hidden" name="uploaded_mobile_image" id="uploaded_image_1<?= $model->id; ?>" value="<?= $model->mobile_image; ?>"/>
						</div>
						<?php if(!empty($model->mobile_image)){ ?>
						<div style="width:10%; float:left; margin-left:16px; margin-top:24px;" id="delete_img_1<?= $model->id; ?>">
							<div style="position:absolute;margin-top:-11px;margin-left:42px;"><img src="<?= BASE_URL; ?>img/close.png" style="cursor:pointer;" title="Delete Image" onClick="return deleteImg('1<?= $model->id; ?>');"/></div>
							<img src="<?= BASE_URL ?>/uploads/banners/<?= $model->mobile_image; ?>" height="50" width="50"/>
						</div>
						<?php } ?>
						
					</div>
					<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                        <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                        <?= Html::a('Cancel', ['/'.$_GET['page'].'/update?id='.$_GET['page_id'].'&type=page-banners'], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

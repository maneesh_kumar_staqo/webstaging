<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Create Banner';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?><?= $_GET['page']; ?>/update?id=<?= $_GET['page_id']; ?>&type=page-banners" title="Banners List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                                ]
                );
                ?>            


                    <div class="form-group">
						<div style="width:85%;float:left;">
							<?= $form->field($model, 'title')->textInput() ?>      
						</div>
						<div style="width:15%;float:left;">
						<?= $form->field($model, 'title_color')->textInput(['type' => 'color']) ?>      
						</div>
                    </div>
					
					<div class="form-group">
                        <?= $form->field($model, 'sub_title')->textInput() ?>      
                    </div>
					
					<div class="form-group">
                        <?= $form->field($model, 'description')->textarea(['rows' => 4]); ?>
                    </div>
					<div class="form-group">	
						<div style="width:40%;float:left;">
							<?= $form->field($model, 'priority')->textInput() ?>
						</div>
						<div style="width:40%;float:left;">
							<?= $form->field($model, 'image_link')->textInput() ?>
						</div>
						<div style="width:20%;float:left;">
							<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
						</div>
					</div>
					<div class="form-group">
						<div style="width:40%; float:left; margin-left:16px; margin-bottom:24px; margin-top:10px;">
							<label class="control-label" for="pagelayout-image">Image</label>
							<input type="file" name="image"/>
						</div>
						<div style="width:20%;float:left; margin-top:10px;">
							<label class="control-label" for="pagelayout-image">Mobile Image</label>
							<input type="file" name="mobile_image"/>
						</div>
						
					</div>
					<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                        <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                        <?= Html::a('Cancel', ['/'.$_GET['page'].'/update?id='.$_GET['page_id'].'&type=page-banners'], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

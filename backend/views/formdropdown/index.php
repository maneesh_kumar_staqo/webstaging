<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Form Dropdown';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <a href="<?php echo BASE_URL; ?>formdropdown/create" title="Create Form Dropdown">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Name',
                                'attribute' => 'name',
                                'value' => 'name',
                            ],
							[
                                'label' => 'Priority',
                                'attribute' => 'priority',
                                'value' => 'priority'
                            ],
							[
                                'label' => 'Status',
                                'attribute' => 'status',
                                'value' => 'status'
                            ],
							[
                                'class' => 'yii\grid\ActionColumn',
                                'options' => ['style' => 'width:15px;'],
                                'template' => '{update}'
                            ],
                        ],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

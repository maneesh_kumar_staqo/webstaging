<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$bannerClass = "";
$imagesClass = "";


$cgovernance = "";
$leadershipClass = "";
$bgelClass = "";
$pageLayoutClass = "";
$shareholdersClass = "";
$bonholderClass = "";

if(!empty($type)){
	if($type == "page-banners"){
		$bannerClass = "active";
	}else if($type == "shareholders"){
		$shareholdersClass = "active";
	}else if($type == "corporate-governance"){
		$cgovernance = "active";
	}else if($type == "page-leadership"){
		$leadershipClass = "active";
	}else if($type == "page-investor-relations"){
		$pageLayoutClass = "active";
	}else if($type == "page-contents"){
		$bgelClass = "active";
	}else if($type == "bonholders"){
		$bonholderClass = "active";
	}
}else{
	$pageLayoutClass = "active"; 
}

$this->title = 'Update Page Layout';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="doctor-master-update">
    <div class="row">
        <div role="tabpanel">
            <div class="col-lg-3 col-md-offset-0" style="margin-left: -25px;margin-top: 5px">
                <!-- Nav tabs -->
                <ul class="left_tab nav-tabs nav-pills nav-stacked" role="tablist">
					<li role="presentation" class="<?= $pageLayoutClass; ?>"><a  href="<?= BASE_URL . 'investor-relations/update?id='.$id.'&type=page-investor-relations' ?> ">Page Layout</a></li>
					<li role="presentation" class="<?= $bannerClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'investor-relations/update?id='.$id.'&type=page-banners' ?> ">Page Banners</a> <span><input type="text" id="pagePriority_banner" value="<?= !empty($priority['banner']) ? $priority['banner'] : ''; ?>" onchange="savePageLayout('banner','hfe_banners','<?= $id; ?>');"></span> </li>
					

					<li role="presentation" class="<?= $cgovernance; ?> hasTextbox"><a  href="<?= BASE_URL . 'investor-relations/update?id='.$id.'&type=corporate-governance' ?> ">Corporate Governance</a> <span><input type="text" id="pagePriority_govcontents" value="<?= !empty($priority['govcontents']) ? $priority['govcontents'] : ''; ?>" onchange="savePageLayout('govcontents','hfe_corporate_governance','<?= $id; ?>');"></span> </li>

					<!--  -->
				
					<li role="presentation" class="<?= $shareholdersClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'investor-relations/update?id='.$id.'&type=shareholders' ?> ">Shareholders</a> <span><input type="text" id="pagePriority_shareholder" value="<?= !empty($priority['shareholder']) ? $priority['shareholder'] : ''; ?>" onchange="savePageLayout('shareholder','hfe_investor_shareholders','<?= $id; ?>');"></span> </li>

					<li role="presentation" class="<?= $leadershipClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'investor-relations/update?id='.$id.'&type=page-leadership' ?>">Voice Of HFE</a> <span><input type="text" id="pagePriority_leadership" value="<?= !empty($priority['leadership']) ? $priority['leadership'] : ''; ?>" onchange="savePageLayout('leadership','hfe_leadership','<?= $id; ?>');"></span></li>


					<li role="presentation" class="<?= $bgelClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'investor-relations/update?id='.$id.'&type=page-contents' ?>">BGEL Disclouser</a> <span><input type="text" id="pagePriority_contents" value="<?= !empty($priority['contents']) ? $priority['contents'] : ''; ?>" onchange="savePageLayout('contents','hfe_contents','<?= $id; ?>');"></span></li>

					<li role="presentation" class="<?= $bonholderClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'investor-relations/update?id='.$id.'&type=bonholders' ?>">Bondholder's Information</a> <span><input type="text" id="pagePriority_bonholders" value="<?= !empty($priority['bonholders']) ? $priority['bonholders'] : ''; ?>" onchange="savePageLayout('bonholders','hfe_bonholders_information','<?= $id; ?>');"></span></li>
				</ul>
            </div> 
            <div class="col-lg-9">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane <?= $pageLayoutClass; ?>">
                        <div class="panel panel-default">
							 <div class="panel-heading">
								<?php echo $this->title ?>
								<div class="menuLink">
									<a href="<?php echo BASE_URL; ?>page-layout" title="List">
										<i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
									</a>
								</div>
							</div>
							<div class="panel-body">
								<div class="row table-bordered">
									<?php
									$form = ActiveForm::begin([
												'id' => 'content_form',
												'options' => [
													'enctype' => 'multipart/form-data',
												],
													]
									);
									?>            


										<div class="form-group">
											<?= $form->field($model, 'page_name')->textInput(['onkeyup' => "getPageUrl($(this).val(), 'pageUrl')"]) ?>      
										</div>
										
										<div class="form-group">
											<?= $form->field($model, 'page_url')->textInput(["id" => "pageUrl"]) ?>      
										</div>
										<div class="form-group">
											<div style="width:50%;float:left;">
												<?= $form->field($model, 'page_type')->dropDownList(['' => 'Select Page Type', 'Home Page Layout' => 'Home Page Layout','About Page Layout' => 'About Page Layout','CSR Page Layout' => 'CSR Page Layout','Project Page Layout' => 'Project Page Layout','CI Solution' => 'CI Solution','Policies Layout' => 'Policies Layout','Contact Us' => 'Contact Us','Investor Relation' => 'Investor Relation']); ?>
											</div>
											 <div style="width:50%;float:left;">
												<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
											</div>
										</div>
										<div class="form-group">    
											<?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 1000]) ?>
										</div>

										<div class="form-group">    
											<?= $form->field($model, 'meta_title')->textInput(['maxlength' => 1000]) ?>
										</div>
										<div class="form-group">    
											<?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>
										</div>
										<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
											<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
											<?= Html::a('Cancel', ['/pagelayout/index'], ['class' => 'btn btn-danger']) ?>
										</div>
										
									</div>
								</div>
								<?php ActiveForm::end(); ?>
							</div>
                    </div>
                    <div role="tabpanel" class="tab-pane <?= $bannerClass; ?>">
                        <?php
                        echo $this->render('../banners/index', [
                            'dataProvider' => $bannersDataProvider,
                            'searchModel' => $bannersDataProvider,
							'page' => 'investor-relations'
                        ])
                        ?>
                    </div>
				
					<div role="tabpanel" class="tab-pane <?= $shareholdersClass; ?>">
						<?php
							echo $this->render('../shareholders/index', [
								'dataProvider' => $ShareholderDataProvider,
								'searchModel' => $ShareholderDataProvider,
								'page' => 'investor-relations'
							])
							?>
						</div>
				
					
                    
					   <div role="tabpanel" class="tab-pane <?= $cgovernance; ?>">
                        <?php
                        echo $this->render('../corporate-governance/index', [
                            'dataProvider' => $GovernanceDataProvider,
                            'searchModel' => $GovernanceDataProvider,
							'page' => 'investor-relations',
							// 'category' => 'rooftop_solar'
                        ])
                        ?>
                    </div>

                    <div role="tabpanel" class="tab-pane <?= $leadershipClass; ?>">
					   <?php
							echo $this->render('../leadership/index', [
								'dataProvider' => $pageLeadership,
								'searchModel' => $pageLeadership,
								'page' => 'investor-relations'
							])
						?>
                    </div>

                    <div role="tabpanel" class="tab-pane <?= $bonholderClass; ?>">
						<?php
							echo $this->render('../bonholders/index', [
								'dataProvider' => $BonholdersDataProvider,
								'searchModel' => $BonholdersDataProvider,
								'page' => 'investor-relations'
							])
							?>
						</div>

                    <div role="tabpanel" class="tab-pane <?= $bgelClass; ?>">
                        <?php
                        echo $this->render('../contents/index', [
                            'dataProvider' => $contentsDataProvider,
                            'searchModel' => $contentsDataProvider,
							'page' => 'investor-relations'
                        ])
                        ?>
                    </div>
					
					
					
				</div>
            </div>
        </div>
    </div>
</div>

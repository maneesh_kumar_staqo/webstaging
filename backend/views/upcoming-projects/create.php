<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$type = !empty($_GET['type']) ? $_GET['type'] : '';
$page = !empty($_GET['page']) ? $_GET['page'] : '';
$this->title = 'Create Upcoming Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?><?= $page; ?>/update?id=<?= $_GET['page_id']; ?>&type=<?= $type; ?>" title="Upcoming Projects List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                                ]
                );
                ?>            
                    <div class="row">
                        <div class="col-md-12">
                                <?= $form->field($model, 'heading')->textInput() ?>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                                <input type="file" name="image" class="form-control" />
                           </div>
                        </div>
                        <div class="col-md-6">
                        <?= $form->field($model, 'box_heading1')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Box Heading Image 1</label>
                            <input type="file" name="box_image1" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'box_description1')->textarea(['rows' => 4]); ?>
                         
                        </div>
                       
                        
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'box_heading2')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Box Heading Image 2</label>
                                <input type="file" name="box_image2" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'box_description2')->textarea(['rows' => 4]); ?>
                        
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'box_heading3')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Box Heading Image 3</label>
                                <input type="file" name="box_image3" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'box_description3')->textarea(['rows' => 4]); ?>
                    
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'box_heading4')->textInput() ?>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Box Heading Image 4</label>
                                <input type="file" name="box_image4" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'box_description4')->textarea(['rows' => 4]); ?>
                  
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                           
                            <?= $form->field($model, 'priority')->textInput() ?>
                    
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
                        </div>
                    </div>

                  
					
				
					
                    <div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                        <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                        <?= Html::a('Cancel', ['/'.$page.'/update?id='.$_GET['page_id'].'&type='.$type], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

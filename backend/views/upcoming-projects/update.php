<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$type = !empty($_GET['type']) ? $_GET['type'] : '';
$page = !empty($_GET['page']) ? $_GET['page'] : '';
$this->title = 'Update Upcoming Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?><?= $page; ?>/update?id=<?= $_GET['page_id']; ?>&type=<?= $type; ?>" title="Upcoming Projects List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                             'enctype' => 'multipart/form-data',
						],
							]
                );
                ?>            

                    <div class="row">
                        <div class="col-md-12">
                                <?= $form->field($model, 'heading')->textInput() ?>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                                <input type="file" name="image" class="form-control" />
                                <?php if(!empty($model->image)){ ?>
                                <div style="width:15%; float:left; margin-left:16px; margin-top:24px;" id="delete_img_<?= $model->id; ?>">
                                    <div style="position:absolute;margin-top:-11px;margin-left:42px;"><img src="<?= BASE_URL; ?>img/close.png"
                                            style="cursor:pointer;" title="Delete Image" onClick="return deleteImg('<?= $model->id; ?>');" /></div>
                                    <img src="<?= BASE_URL ?>/uploads/project/<?= $model->image; ?>" height="50" width="50" />
                                </div>
                                <?php } ?>
                           </div>
                        </div>
                        <div class="col-md-6">
                        <?= $form->field($model, 'box_heading1')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Box Heading Image 1</label>
                            <input type="file" name="box_image1" class="form-control" />
                            <?php if(!empty($model->box_image1)){ ?>
                            <div style="width:15%; float:left; margin-left:16px; margin-top:24px;" id="delete_img_<?= $model->id; ?>">
                                <div style="position:absolute;margin-top:-11px;margin-left:42px;"><img src="<?= BASE_URL; ?>img/close.png"
                                        style="cursor:pointer;" title="Delete Image" onClick="return deleteImg('<?= $model->id; ?>');" /></div>
                                <img src="<?= BASE_URL ?>/uploads/project/<?= $model->box_image1; ?>" height="50" width="50" />
                            </div>
                            <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'box_description1')->textarea(['rows' => 4]); ?>
                                 
                        
                        </div>
                       
                        
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'box_heading2')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Box Heading Image 2</label>
                                <input type="file" name="box_image2" class="form-control" />
                                <?php if(!empty($model->box_image2)){ ?>
                                <div style="width:15%; float:left; margin-left:16px; margin-top:24px;" id="delete_img_<?= $model->id; ?>">
                                    <div style="position:absolute;margin-top:-11px;margin-left:42px;"><img src="<?= BASE_URL; ?>img/close.png"
                                            style="cursor:pointer;" title="Delete Image" onClick="return deleteImg('<?= $model->id; ?>');" /></div>
                                    <img src="<?= BASE_URL ?>/uploads/project/<?= $model->box_image2; ?>" height="50" width="50" />
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-12">

                            <?= $form->field($model, 'box_description2')->textarea(['rows' => 4]); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'box_heading3')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Box Image 3</label>
                                <input type="file" name="box_image3" class="form-control" />
                                <?php if(!empty($model->box_image3)){ ?>
                                <div style="width:15%; float:left; margin-left:16px; margin-top:24px;" id="delete_img_<?= $model->id; ?>">
                                    <div style="position:absolute;margin-top:-11px;margin-left:42px;"><img src="<?= BASE_URL; ?>img/close.png"
                                            style="cursor:pointer;" title="Delete Image" onClick="return deleteImg('<?= $model->id; ?>');" /></div>
                                    <img src="<?= BASE_URL ?>/uploads/project/<?= $model->box_image3; ?>" height="50" width="50" />
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                  
                            <?= $form->field($model, 'box_description3')->textarea(['rows' => 4]); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'box_heading4')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Box Image 4</label>
                                <input type="file" name="box_image4" class="form-control" />
                                <?php if(!empty($model->box_image4)){ ?>
                                <div style="width:15%; float:left; margin-left:16px; margin-top:24px;" id="delete_img_<?= $model->id; ?>">
                                    <div style="position:absolute;margin-top:-11px;margin-left:42px;"><img src="<?= BASE_URL; ?>img/close.png"
                                            style="cursor:pointer;" title="Delete Image" onClick="return deleteImg('<?= $model->id; ?>');" /></div>
                                    <img src="<?= BASE_URL ?>/uploads/project/<?= $model->box_image4; ?>" height="50" width="50" />
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'box_description4')->textarea(['rows' => 4]); ?>
                          
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                           
                            <?= $form->field($model, 'priority')->textInput() ?>
                    
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
                        </div>
                    </div>


					
                    <div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                        <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                        <?= Html::a('Cancel', ['/'.$page.'/update?id='.$_GET['page_id'].'&type='.$type], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

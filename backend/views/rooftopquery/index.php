<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Rooftop Query';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <!--a href="<?php //echo BASE_URL; ?>page-layout-create" title="Create Page">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a -->
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Name',
                                'attribute' => 'name',
                                'value' => 'name',
                            ],
							[
                                'label' => 'Email',
                                'attribute' => 'email',
                                'value' => 'email',
                            ],
							[
                                'label' => 'Designation',
                                'attribute' => 'designation',
                                'value' => 'designation',
                            ],[
                                'label' => 'Company',
                                'attribute' => 'company_name',
                                'value' => 'company_name',
								'filter' => false,
                            ],
							[
                                'label' => 'Phone',
                                'attribute' => 'phone',
                                'value' => 'phone',
								'filter' => false,
                            ],
							[
                                'attribute' => 'city',
                                'value' => 'city',
								'filter' => false,
                            ],
							[
                                'attribute' => 'annual_turnover',
                                'value' => 'annual_turnover',
								'filter' => false,
                            ],
							[
                                'attribute' => 'average_monthly_bill',
                                'value' => 'average_monthly_bill',
								'filter' => false,
                            ],
							[
                                'attribute' => 'monthly_electricity_consuption',
                                'value' => 'monthly_electricity_consuption',
								'filter' => false,
                            ],
							[
                                'attribute' => 'approximate_space_available',
                                'value' => 'approximate_space_available',
								'filter' => false,
                            ],
							[
                                'label' => 'Message',
                                'attribute' => 'message',
                                'value' => 'message',
								'filter' => false,
                            ],
							[
                                'label' => 'Date',
                                'attribute' => 'created_on',
                                'value' => 'created_on',
								'filter' => false,
                            ],
                        ],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

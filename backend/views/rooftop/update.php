<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$bannerClass = "";
$imagesClass = "";



$commissionproject = "";
$clientsClass = "";

$BusinessClass = "";
$galleryClass = "";
$knowmoreClass = "";
$pageLayoutClass = "";
$reasonsClass = "";
$contactClass = "";

if(!empty($type)){
	if($type == "page-banners"){
		$bannerClass = "active";
	}else if($type == "rooftop-reasons"){
		$reasonsClass = "active";
	}else if($type == "rooftop-clients"){
		$clientsClass = "active";
	}else if($type == "commissioned-projects"){
		$commissionproject = "active";
	}else if($type == "business-offer"){
		$BusinessClass = "active";
	}else if($type == "rooftop-gallery"){
		$galleryClass = "active";
	}else if($type == "page-contact"){
		$contactClass = "active";
	}else if($type == "rooftop-knowmore"){
		$knowmoreClass = "active";
	}else if($type == "page-rooftop"){
		$pageLayoutClass = "active";
	}
}else{
	$pageLayoutClass = "active"; 
}

$this->title = 'Update Page Layout';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="doctor-master-update">
    <div class="row">
        <div role="tabpanel">
            <div class="col-lg-3 col-md-offset-0" style="margin-left: -25px;margin-top: 5px">
                <!-- Nav tabs -->
                <ul class="left_tab nav-tabs nav-pills nav-stacked" role="tablist">
					<li role="presentation" class="<?= $pageLayoutClass; ?>"><a  href="<?= BASE_URL . 'rooftop/update?id='.$id.'&type=page-rooftop' ?> ">Page Layout</a></li>
					<li role="presentation" class="<?= $bannerClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'rooftop/update?id='.$id.'&type=page-banners' ?> ">Page Banners</a> <span><input type="text" id="pagePriority_rooftop_banners" value="<?= !empty($priority['rooftop_banners']) ? $priority['rooftop_banners'] : ''; ?>" onchange="savePageLayout('rooftop_banners','hfe_banners','<?= $id; ?>');"></span> </li>
					

					<li role="presentation" class="<?= $BusinessClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'rooftop/update?id='.$id.'&type=business-offer' ?> ">Business Offer</a> <span><input type="text" id="pagePriority_BusinessClass" value="<?= !empty($priority['BusinessClass']) ? $priority['BusinessClass'] : ''; ?>" onchange="savePageLayout('BusinessClass','hfe_business_offer','<?= $id; ?>');"></span> </li>

					<!--  -->
					<li role="presentation" class="<?= $clientsClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'rooftop/update?id='.$id.'&type=rooftop-clients&imgtype=our_client' ?>">Our Clients</a> <span><input type="text" id="pagePriority_rooftop_clients" value="<?= !empty($priority['rooftop_clients']) ? $priority['rooftop_clients'] : ''; ?>" onchange="savePageLayout('rooftop_clients','hfe_page_images','<?= $id; ?>');"></span></li>
				
				
					<li role="presentation" class="<?= $commissionproject; ?> hasTextbox"><a  href="<?= BASE_URL . 'rooftop/update?id='.$id.'&type=commissioned-projects' ?>">Featured Project</a> <span><input type="text" id="pagePriority_feature_project" value="<?= !empty($priority['feature_project']) ? $priority['feature_project'] : ''; ?>" onchange="savePageLayout('feature_project','commissioned_projects','<?= $id; ?>');"></span></li>
					<li role="presentation" class="<?= $reasonsClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'rooftop/update?id='.$id.'&type=rooftop-reasons' ?> ">Why   Partner   with HFE</a> <span><input type="text" id="pagePriority_rooftop_reasons" value="<?= !empty($priority['rooftop_reasons']) ? $priority['rooftop_reasons'] : ''; ?>" onchange="savePageLayout('rooftop_reasons','hfe_rooftop_solar','<?= $id; ?>');"></span> </li>
					
					<li role="presentation" class="<?= $galleryClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'rooftop/update?id='.$id.'&type=rooftop-gallery' ?>">Video Gallery</a> <span><input type="text" id="pagePriority_rooftop_gallery" value="<?= !empty($priority['rooftop_gallery']) ? $priority['rooftop_gallery'] : ''; ?>" onchange="savePageLayout('rooftop_gallery','hfe_youtube','<?= $id; ?>');"></span></li>
					<li role="presentation" class="<?= $knowmoreClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'rooftop/update?id='.$id.'&type=rooftop-knowmore&imgtype=know_more' ?>">Know More</a> <span><input type="text" id="pagePriority_rooftop_knowmore" value="<?= !empty($priority['rooftop_knowmore']) ? $priority['rooftop_knowmore'] : ''; ?>" onchange="savePageLayout('rooftop_knowmore','hfe_page_images','<?= $id; ?>');"></span></li>
					   <li role="presentation" class="<?= $contactClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'rooftop/update?id='.$id.'&type=page-contact' ?>">Contact Us</a> <span><input type="text" id="pagePriority_contact" value="<?= !empty($priority['contact']) ? $priority['contact'] : ''; ?>" onchange="savePageLayout('contact','hfe_contact_us_address','<?= $id; ?>');"></span></li>

				</ul>
            </div> 
            <div class="col-lg-9">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane <?= $pageLayoutClass; ?>">
                        <div class="panel panel-default">
							 <div class="panel-heading">
								<?php echo $this->title ?>
								<div class="menuLink">
									<a href="<?php echo BASE_URL; ?>page-layout" title="List">
										<i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
									</a>
								</div>
							</div>
							<div class="panel-body">
								<div class="row table-bordered">
									<?php
									$form = ActiveForm::begin([
												'id' => 'content_form',
												'options' => [
													'enctype' => 'multipart/form-data',
												],
													]
									);
									?>            


										<div class="form-group">
											<?= $form->field($model, 'page_name')->textInput(['onkeyup' => "getPageUrl($(this).val(), 'pageUrl')"]) ?>      
										</div>
										
										<div class="form-group">
											<?= $form->field($model, 'page_url')->textInput(["id" => "pageUrl"]) ?>      
										</div>
										<div class="form-group">
											<div style="width:50%;float:left;">
												<?= $form->field($model, 'page_type')->dropDownList(['' => 'Select Page Type', 'Home Page Layout' => 'Home Page Layout','About Page Layout' => 'About Page Layout','CSR Page Layout' => 'CSR Page Layout','Project Page Layout' => 'Project Page Layout','CI Solution' => 'CI Solution','Policies Layout' => 'Policies Layout','Contact Us' => 'Contact Us']); ?>
											</div>
											 <div style="width:50%;float:left;">
												<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
											</div>
										</div>
										<div class="form-group">    
											<?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 1000]) ?>
										</div>

										<div class="form-group">    
											<?= $form->field($model, 'meta_title')->textInput(['maxlength' => 1000]) ?>
										</div>
										<div class="form-group">    
											<?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>
										</div>
										<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
											<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
											<?= Html::a('Cancel', ['/pagelayout/index'], ['class' => 'btn btn-danger']) ?>
										</div>
										
									</div>
								</div>
								<?php ActiveForm::end(); ?>
							</div>
                    </div>
                    <div role="tabpanel" class="tab-pane <?= $bannerClass; ?>">
                        <?php
                        echo $this->render('../banners/index', [
                            'dataProvider' => $bannersDataProvider,
                            'searchModel' => $bannersDataProvider,
							'page' => 'rooftop'
                        ])
                        ?>
                    </div>
				
					<div role="tabpanel" class="tab-pane <?= $clientsClass; ?>">
                        <?php
                        echo $this->render('../images/index', [
                            'dataProvider' => $imageDataProvider,
                            'searchModel' => $imageDataProvider,
							'page' => 'rooftop',
							'imgtype' => 'our_client'
                        ])
                        ?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $commissionproject; ?>">
                        <?php
							echo $this->render('../commissioned-projects/index', [
								'dataProvider' => $commissionedProjects,
								'searchModel' => $commissionedProjects
							])
						  ?>
                    </div>
								<div role="tabpanel" class="tab-pane <?= $reasonsClass; ?>">
												<?php
												echo $this->render('../reasons/index', [
													'dataProvider' => $ReasonsDataProvider,
													'searchModel' => $ReasonsDataProvider,
													'page' => 'rooftop'
												])
												?>
											</div>
				
					
                    <div role="tabpanel" class="tab-pane <?= $galleryClass; ?>">
                        <?php
                        echo $this->render('../rooftopvideo/index', [
                            'dataProvider' => $VideoDataProvider,
                            'searchModel' => $VideoDataProvider,
							'page' => 'rooftop',
							'category' => 'rooftop_solar'
                        ])
                        ?>
                    </div>
					   <div role="tabpanel" class="tab-pane <?= $BusinessClass; ?>">
                        <?php
                        echo $this->render('../business-offer/index', [
                            'dataProvider' => $offerDataProvider,
                            'searchModel' => $offerDataProvider,
							'page' => 'rooftop',
							'category' => 'rooftop_solar'
                        ])
                        ?>
                    </div>
					
					<div role="tabpanel" class="tab-pane <?= $knowmoreClass; ?>">
                        <?php
                        echo $this->render('../images/index', [
                            'dataProvider' => $imageDataProvider,
                            'searchModel' => $imageDataProvider,
							'page' => 'rooftop',
							'imgtype' => 'know_more'
                        ])
                        ?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $contactClass; ?>">
                        <?php
							echo $this->render('../contactadd/index', [
								'dataProvider' => $pageContact,
								'searchModel' => $pageContact
							])
						  ?>
                    </div>
				</div>
            </div>
        </div>
    </div>
</div>

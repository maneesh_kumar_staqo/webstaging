<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Career Page Query';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <!--a href="<?php //echo BASE_URL; ?>page-layout-create" title="Create Page">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a -->
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Name',
                                'attribute' => 'name',
                                'value' => 'name',
                            ],
							[
                                'label' => 'Email Id',
                                'attribute' => 'email_id',
                                'value' => 'email_id',
                            ],
							[
                                'label' => 'Contact No.',
                                'attribute' => 'contact_no',
                                'value' => 'contact_no',
                            ],
							[
                                'label' => 'Current Organisation',
                                'attribute' => 'current_organisation',
                                'value' => 'current_organisation',
								'filter' => false,
                            ],
							[
                                'label' => 'Current Designation',
                                'attribute' => 'current_designation',
                                'value' => 'current_designation',
								'filter' => false,
                            ],
							[
                                'label' => 'Experience',
                                'attribute' => 'experience',
                                'value' => 'experience',
								'filter' => false,
                            ],
							[
                                'label' => 'Apply For',
                                'attribute' => 'apply_for',
                                'value' => 'apply_for',
								'filter' => false,
                            ],
							[
                                'label' => 'Resume',
                                'attribute' => 'resume',
								'format' => 'raw',
                                'value' => function($model){
									if(!empty($model->resume)){
										return '<a href="'.BASE_URL.'uploads/career/'.$model->resume.'" target="_blank" title="Resume"><i class="fa fa-file fa-fw" style="font-size: 18px !important;"></i>Download</a>';
									}else{
										return "N/A";
									}
								},
								'filter' => false,
                            ],
						],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

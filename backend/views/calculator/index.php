<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Calculator';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <!--a href="<?php //echo BASE_URL; ?>page-layout-create" title="Create Page">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a -->
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Name',
                                'attribute' => 'name',
                                'value' => 'name',
                            ],
							[
                                'label' => 'Email Id',
                                'attribute' => 'email_id',
                                'value' => 'email_id',
                            ],
							[
                                'label' => 'Phone',
                                'attribute' => 'phone',
                                'value' => 'phone',
                            ],
							[
                                'label' => 'Rooftop Area',
                                'attribute' => 'rooftop_area',
                                'value' => 'rooftop_area',
								'filter' => false,
                            ],
							[
                                'label' => 'Date',
                                'attribute' => 'created_on',
                                'value' => 'created_on',
								'filter' => false,
                            ],
                        ],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Update Simulation Contact Us';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1 mainDiv">
    <div class="panel-default table-bordered">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>simulation-contactus" title="Simulation Contact Us List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
			<div class="form-container">
                <?php
                $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data',]]);
                ?>  
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<?= $form->field($model, 'banner_text')->textInput() ?>      
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?= $form->field($model, 'banner_image')->textInput() ?>      
							</div>
						</div>
					</div>
				
				
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<?= $form->field($model, 'about_title')->textInput() ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?= $form->field($model, 'about_desc')->textInput() ?>
							</div>
						</div>
						
                    </div>
                    <div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<?= $form->field($model, 'gallery_title')->textInput() ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								 <?= $form->field($model, 'gallery_image')->fileInput(['multiple' => true])->label('Profile Image') ?>

							</div>
						</div>
						
                    </div>
                    
                     <div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<?= $form->field($model, 'gallery_title')->textInput() ?>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								 <?= $form->field($model, 'gallery_image')->fileInput(['multiple' => true])->label('Profile Image') ?>

							</div>
						</div>
						
					</div>
			
				
					
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<div class="form-group">
									<?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
									<?= Html::a('Cancel', ['/simulation-contactus'], ['class' => 'btn btn-danger']) ?>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<script>
getContent();
getPhoto();
</script>

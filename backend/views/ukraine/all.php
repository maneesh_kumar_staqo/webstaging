<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$bannerClass = "";
$contentClass = "";
$directorsClass = "";
$managementClass = "";
$governanceClass = "";
$imagesClass = "";
$leadershipClass = "";
$pageLayoutClass = "";
$type=$_REQUEST['page_type'];


$this->title = 'Update Page Layout';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="doctor-master-update">
    <div class="row">
        <div role="tabpanel">
            <div class="col-lg-2 col-md-offset-0" style="margin-left: -25px;margin-top: 5px">
                <!-- Nav tabs -->
                <ul class="left_tab nav-tabs nav-pills nav-stacked" role="tablist">
                				<li role="presentation" class="<?php if($type=="menu-section"){echo "active";}?>">
					<a  href="<?= BASE_URL .'ukraine/all/?id='.$_REQUEST['id'].'&page_type=menu-section'; ?>">Menu Section</a></li>
					<li role="presentation" class="<?php if($type=="banner-section"){echo "active";}?>">
					<a  href="<?= BASE_URL .'ukraine/all/?id='.$_REQUEST['id'].'&page_type=banner-section'; ?>">Banner Section</a></li>
					<li role="presentation" class="<?php if($type=="section-1"){echo "active";}?> hasTextbox"><a  href="<?= BASE_URL .'ukraine/all/?id='.$_REQUEST['id'].'&page_type=section-1'; ?>">Section 1</a>  </li>
					<li role="presentation" class="<?php if($type=="section-2"){echo "active";}?>">
					<a  href="<?= BASE_URL .'ukraine/all/?id='.$_REQUEST['id'].'&page_type=section-2'; ?>">Section 2</a></li>
					<li role="presentation" class="<?php if($type=="section-3"){echo "active" ;}?>">
						<a href="<?= BASE_URL .'ukraine/all/?id='.$_REQUEST['id'].'&page_type=section-3'; ?>">Section 3
						</a>
					</li>
					<li role="presentation" class="<?php if($type=="section-4"){echo "active" ;}?>">
						<a href="<?= BASE_URL .'ukraine/all/?id='.$_REQUEST['id'].'&page_type=section-4'; ?>">Section 4
						</a>
					</li>
					<li role="presentation" class="<?php if($type=="section-5"){echo "active" ;}?>">
						<a href="<?= BASE_URL .'ukraine/all/?id='.$_REQUEST['id'].'&page_type=section-5'; ?>">Section 5
						</a>
					</li>
               
				</ul>
            </div> 
            <div class="col-lg-10">
				
				
                <!-- Tab panes -->
                <div class="tab-content">
						<?php if($type=='banner-section'){?>
                    <div role="tabpanel" class="tab-pane <?php if($type=="banner-section"){echo "active";}?>">
                        <div class="panel panel-default">
							 <div class="panel-heading">
								Banner Section
								<div class="menuLink">
									<!-- <a href="<?php echo BASE_URL; ?>page-layout" title="List">
										<i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
									</a> -->
								</div>
							</div>
							<div class="panel-body">
								<div class="row table-bordered">
												
									<?php
									$form = ActiveForm::begin([
												'id' => 'content_form',
												'action'=>''.BASE_URL.'ukraine/create?id='.$modeldata[0]['page_id'].'',
												'options' => [
													'enctype' => 'multipart/form-data',
												],
													]
									);
									?>            
										<div class="form-group">
											<div class="form-group">
											<label for="">Banner Title</label>
												<input type="text" class="form-control"  name="banner_text" id="">
											</div>
										</div>
										<div class="form-group">
											<div class="form-group">
											<label for="">Sequence</label>
												<input type="text" class="form-control"  name="sequence" id="">
											</div>
										</div>
										<div class="form-group">
											<div class="form-group">
											<label for="">Image</label>
												<input type="file" class="form-control"  name="banner_image" id="">
											</div>
										</div>
											<input type="hidden" value="<?= $_REQUEST['page_type']?>" name="page_type" id="">
										
										<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
											<?= Html::submitButton($model->isNewRecord ? 'Update' : 'Update', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
								
										</div>
									</div>
								</div>
								<?php ActiveForm::end(); ?>

								<div>
									<table class="table table-striped">
										<thead>
											<tr>
												<th>S.no</th>
												<th>Banner Text</th>
												<th>Image</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php 
										if($type=='banner-section'){
											if(!empty($data)){
								
								
													foreach($data as $key=> $value){?>
												<tr>
													<td><?= $key+1; ?></td>
													<td><?= $value['banner_text'] ?></td>
													<td><img src="<?= BASE_URL .'uploads/images/'.$value['banner_image'] ?>" alt="" style="width:50px;"></td>
													<td><a href="<?= BASE_URL .'ukraine/edit/?id='.$_REQUEST['id'].'&image='.$value['id'].'&page_type=banner-section'; ?>" class="btn btn-info"><i class="fa fa-pencil"></i></a> <a href="<?= BASE_URL .'ukraine/delete/?id='.$_REQUEST['id'].'&image='.$value['id'].'&page_type=banner-section'; ?>" class="btn btn-danger"><i
																class="fa fa-trash"></i></a></td>
												</tr>
											<?php } } } 
											?>
											
										</tbody>
									</table>
								</div>
							</div>
                    </div>
					<?php }elseif($type=='menu-section'){?>
							<div role="tabpanel" class="tab-pane <?php if($type=="menu-section"){echo "active" ;}?>">
							<div class="panel panel-default">
								<div class="panel-heading">
									Menu Section
									<div class="menuLink">
										<!-- <a href="<?php echo BASE_URL; ?>page-layout" title="List">
																<i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
															</a> -->
									</div>
								</div>
								<div class="panel-body">
									<div class="row table-bordered">
									<form action="<?= BASE_URL ?>ukraine/createmenu" method="post">
										<div class="form-group">
											<input type="hidden" name="_csrf-backend" value="<?=Yii::$app->request->getCsrfToken()?>" />

											<div class="form-group">
												<label for="">Menu Title</label>
												<input type="text" class="form-control" name="name" id="">
											</div>
										</div>
										<div class="form-group">
											<div class="form-group">
												<label for="">Page Section</label>
												<select name="link" id="" class="form-control">
													<option value="banner">Banner</option>
													<option value="about">About</option>
													<option value="query">for general queries</option>
													<option value="contact">Contact</option>
													<option value="photo">Photo</option>
													<option value="Disclosures">Disclosures</option>
													<option value="contact">Contact</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<div class="form-group">
												<label for="">Sequence</label>
												<input type="text" name="seq" class="form-control" name="">
											</div>
										</div>
										<div class="form-group">
											<div class="form-group">
												<label for="">Visble in</label>
												<select name="visible_in" id="" class="form-control">
													<option value="1">header</option>
													<option value="2">Footer</option>
													<option value="3">Header and Footer</option>
												</select>
											</div>
										</div>
										<input type="hidden" value="<?= $modeldata[0]['id']?>" name="pagetableid" id="">
						
										<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
										<button class="btn btn-primary">Submit</button>
						
										</div>
										</form>
									</div>
								</div>
			
						
								<div>
									<table class="table table-striped">
										<thead>
											<tr>
												<th>S.no</th>
												<th>Name</th>
												<th>Link</th>
												<th>Sequence</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php 
																if($type=='menu-section'){
																	if(!empty($data)){
																			foreach($data as $key=> $value){?>
											<tr>
												<td>
													<?= $key+1; ?>
												</td>
												<td>
													<?= $value['name'] ?>
												</td>
												<td>
													<?= $value['link'] ?>
												</td>
												<td>
													<?= $value['status']; ?>
												</td>
												<td><a href="<?= BASE_URL .'ukraine/editmenu?id='.$value['id'].'&page_type=menu-section&pageid='.$modeldata[0]['page_id']; ?>"
														class="btn btn-info"><i class="fa fa-pencil"></i></a> <a
														href="<?= BASE_URL .'ukraine/menudelete?id='.$value['id']; ?>"
														class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
											</tr>
											<?php } } } 
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					<?php } ?>

					<?php if($type=='section-1'){?>
                    <div role="tabpanel" class="tab-pane <?php if($type=="section-1"){echo "active";}?>">
						<div class="panel panel-default">
							<div class="panel-heading">
								Section 1
								<div class="menuLink">
									<!-- <a href="<?php echo BASE_URL; ?>page-layout" title="List">
										<i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
									</a> -->
								</div>
							</div>
							<div class="panel-body">
								<div class="row table-bordered">
									<?php
								$form = ActiveForm::begin([
											'id' => 'content_form',
											'action'=>''.BASE_URL.'ukraine/update?page_id='.$data[0]['page_id'],
											'options' => [
												'enctype' => 'multipart/form-data',
											],
												]
								);

								?>
						
								
										<div class="form-group">
											<div class="form-group">
											<div class="row">
											<?php 
													if(!empty($data[0]['section1_menu'])){
														$expitem=explode(',',$data[0]['section1_menu']);
														
												?>
												<div class="col-md-6">
												
		<input type="hidden" <?php if($expitem[0]=='1'){ echo "checked"; } ?> name="header" id="header" style="    margin-right: 10px;
		margin-left: 2px;"> <input type="hidden"   name="footer" id="footer" <?php if($expitem[1]=='2'){ echo "checked"; } ?>>
													</div>
												</div>
												<?php } ?>
											</div>
													
									</div>
									<div class="form-group">
										<input type="hidden" name="page_type" value="<?= $_REQUEST['page_type'];?>" id="">
										<?= $form->field($model, 'about_title')->textInput(['onkeyup' => "getPageUrl()",'value'=>$data[0]['about_title']]) ?>
									</div>
						
									<div class="form-group">
										<?= $form->field($model, 'about_desc')->textarea(['rows' => 4, 'class' => 'left_class_editor','value'=>$data[0]['about_desc']]); ?>
									</div>
									<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
										<?= Html::submitButton($model->isNewRecord ? 'Update' : 'Update', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
									</div>
								</div>
							</div>
							<?php ActiveForm::end(); ?>
						
						</div>
					</div>
					<?php } ?>
					<?php if($type=='section-2'){
						
						?>
					<div role="tabpanel" class="tab-pane <?php if($type=="section-2"){echo "active" ;}?>">
						<div class="panel panel-default">
							<div class="panel-heading">
								Section 2 (Gallery)
								<div class="menuLink">
								</div>
							</div>
							<div class="panel-body">
								<div class="row table-bordered">
									<?php
							
								
								
								$form = ActiveForm::begin([
											'id' => 'content_form',
											'action'=>''.BASE_URL.'ukraine/create?id='.$modeldata[0]['id'].'',
											'options' => [
												'enctype' => 'multipart/form-data',
											],
												]
								);
								?>
								
								<div class="form-group">
											<div class="form-group">
											<div class="row">
											<?php 
									
													if(!empty($modeldata[0]['section2_menu'])){
														
														$expitem=explode(',',$modeldata[0]['section2_menu']);
														$header=$expitem[0];
														$footer=$expitem[1];
												?>
												<div class="col-md-6">
												
															<input type="hidden" <?php if($expitem[0]=='1'){ echo "checked"; } ?> name="header" id="header" style="    margin-right: 10px;
															margin-left: 2px;"> <input type="hidden"   name="footer" id="footer" <?php if($expitem[1]=='2'){ echo "checked"; } ?>>
													</div>
												</div>
												<?php }
												
												else { ?>
														<div class="col-md-6">
															<input type="hidden"  name="header" id="header" style="    margin-right: 10px;
															margin-left: 2px;"> <input type="hidden"   name="footer" id="footer" >
													</div>
												</div>


												<?php }
												?>
											</div>
													
									</div>
								
									<input type="hidden" value="<?= $_REQUEST['page_type']?>" name="page_type" id="">
									<div class="form-group">
										<?= $form->field($model, 'gallery_title')->textInput(['onkeyup' => "getPageUrl()",'value'=>$modeldata[0]['gallery_title']]) ?>
									</div>
									<div class="form-group">
									<div class="form-group">
									<label for="">Sequence</label>
									<input type="text" name="sequence" id="" class="form-control">

									</div>
										
									</div>
									<div class="form-group">
									<div class="form-group">
									<label for="">Image</label>
									<input type="file" class="form-control file_size" name="gallery_image" id="" accept="image/x-png,image/gif,image/jpeg">
									</div>
										
									</div>
									<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
										<?= Html::submitButton($model->isNewRecord ? 'Update' : 'Update', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
										
									</div>
								</div>
							</div>
							<?php ActiveForm::end(); ?>
					
							<div>
								<table class="table table-striped">
									<thead>
										<tr>
											<th>S.no</th>
											<th>Image</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php 		

										foreach($data as $galkey=> $galitem){?>
										<tr>
											<td>
												<?= $galkey+1 ?>
											</td>
											
											<td><img src="<?= BASE_URL .'uploads/images/'.$galitem['image'] ?>" alt=""
													style="width:50px;"></td>
											<td><a href="<?= BASE_URL .'ukraine/edit/?id='.$_REQUEST['id'].'&image='.$galitem['id'].'&page_type=section-2'; ?>"
													class="btn btn-info"><i class="fa fa-pencil"></i></a> <a
													href="<?= BASE_URL .'ukraine/delete/?id='.$_REQUEST['id'].'&image='.$galitem['id'].'&page_type=section-2'; ?>"
													class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
										</tr>
										<?php } 
																?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<?php } ?>
					<?php if($type=="section-3"){?>
					<div role="tabpanel" class="tab-pane <?php if($type=="section-3"){echo "active" ;}?>">
						<div class="panel panel-default">
							<div class="panel-heading">
								Section 3 (Disclosure)

								<div class="menuLink">
									<!-- <a href="<?php echo BASE_URL; ?>page-layout" title="List">
										<i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
									</a> -->
								</div>
							</div>
							<div class="panel-body">
								<div class="row table-bordered">
									<?php
													$form = ActiveForm::begin([
																'id' => 'content_form',
																'action'=>''.BASE_URL.'ukraine/create?id='.$modeldata[0]['id'].'',
																'options' => [
																	'enctype' => 'multipart/form-data',
																],
																]
													);
													?>
													<div class="form-group">
											<div class="form-group">
											<div class="row">
											<?php 
									
													if(!empty($modeldata[0]['section3_menu'])){
														
														$expitem=explode(',',$modeldata[0]['section3_menu']);
														$header=$expitem[0];
														$footer=$expitem[1];
												?>
												<div class="col-md-6">
												
															<input type="hidden" <?php if($expitem[0]=='1'){ echo "checked"; } ?> name="header" id="header" style="    margin-right: 10px;
															margin-left: 2px;"> <input type="hidden"   name="footer" id="footer" <?php if($expitem[1]=='2'){ echo "checked"; } ?>>
													</div>
												</div>
												<?php }
												
												else { ?>
														<div class="col-md-6">
															<input type="hidden"  name="header" id="header" style="    margin-right: 10px;
															margin-left: 2px;"><input type="hidden"   name="footer" id="footer" >
													</div>
												</div>


												<?php }
												?>
											</div>




									<input type="hidden" value="<?= $_REQUEST['page_type']?>" name="page_type" id="">
									<div class="form-group">
										<?= $form->field($model, 'disclouser_title')->textInput(['onkeyup' => "getPageUrl()",'value'=>$modeldata[0]['disclouser_title']]) ?>
									</div>
									<div class="form-group">
									<div class="form-group">
									<label for="">Type</label>
										<select class="form-control" name="disclouser_type">
										<option value="Annual / Quarterly">Annual / Quarterly</option>
										<option value="Monthly">Monthly</option>
										</select>
										</div>
									</div>
									<div class="form-group">
									<div class="form-group">
									<label for="">Years</label>
									<input type="text" name="disclouser_year" id="" class="form-control">

									</div>
										
									</div>
									
									<div class="form-group">
									<div class="form-group">
									<label for="">Title</label>
									<input type="text" name="disclouser_heading" class="form-control" id=""></div>
										
									</div>
									
					
									<div class="form-group">
									<div class="form-group">
									<label for="">File</label>
									<input type="file" name="disclouser_file" class="form-control file_size" id="" accept="application/pdf,application/vnd.ms-excel,image/jpeg">
									</div>
										
									</div>
									<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
										<?= Html::submitButton($model->isNewRecord ? 'Update' : 'Update', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
										
									</div>
								</div>
							</div>
							<?php ActiveForm::end(); ?>
					
							<div>
								<table class="table table-striped">
									<thead>
										<tr>
											<th>S.no</th>
											<th>Title</th>
											<th>Disclosure Year</th>
											<th>Files</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										foreach($data as $key=> $discitem){?>
											<tr>
											<td>
												<?= $key+1 ?>
											</td>
											<td>
												<?= $discitem['title']; ?>
											</td>
											<td>
												<?= $discitem['years']; ?>
											</td>
					
											<td><a href="<?= BASE_URL .'uploads/images/'.$discitem['file'] ?>" target="_blank">View </a></td>
											<td><a href="<?= BASE_URL .'ukraine/edit/?id='.$_REQUEST['id'].'&image='.$discitem['id'].'&page_type=section-3'; ?>"
													class="btn btn-info"><i class="fa fa-pencil"></i></a> <a
													href="<?= BASE_URL .'ukraine/delete/?id='.$_REQUEST['id'].'&image='.$discitem['id'].'&page_type=section-3'; ?>"
													class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
										</tr>
										<?php }

										?>
					
									</tbody>
								</table>
							</div>
						</div>
					</div>
						<?php } ?>
					<?php if($type=="section-4"){?>
					<div role="tabpanel" class="tab-pane <?php if($type=="section-4"){echo "active" ;}?>">
						<div class="panel panel-default">
							<div class="panel-heading">
								Section 4

								<div class="menuLink">
									<!-- <a href="<?php echo BASE_URL; ?>page-layout" title="List">
										<i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
									</a> -->
								</div>
							</div>
							<div class="panel-body">
								<div class="row table-bordered">
									<?php
													$form = ActiveForm::begin([
																'id' => 'content_form',
																'action'=>''.BASE_URL.'ukraine/update?page_id='.$data[0]['page_id'],
																'options' => [
																	'enctype' => 'multipart/form-data',
																],
																	]
													);
													?>
									<input type="hidden" value="<?= $_REQUEST['page_type']?>" name="page_type" id="">
									<div class="form-group">
											<div class="form-group">
											<div class="row">
											<?php 
											
									
													if(!empty($data[0]['section4_menu'])){
														
														$expitem=explode(',',$data[0]['section4_menu']);
														$header=$expitem[0];
														$footer=$expitem[1];
												?>
												<div class="col-md-6">
												
															<input type="hidden" <?php if($expitem[0]=='1'){ echo "checked"; } ?> name="header" id="header" style="    margin-right: 10px;
															margin-left: 2px;"> <input type="hidden"   name="footer" id="footer" <?php if($expitem[1]=='2'){ echo "checked"; } ?>>
													</div>
												</div>
												<?php }
												
												else { ?>
														<div class="col-md-6">
															<input type="hidden"  name="header" id="header" style="    margin-right: 10px;
															margin-left: 2px;"> <input type="hidden"   name="footer" id="footer" ></p>
													</div>
												</div>


												<?php }
												?>
											</div>



									<div class="form-group">
										<?= $form->field($model, 'contact_title')->textInput(['onkeyup' => "getPageUrl()",'value'=>$data[0]['contact_title']]) ?>
									</div>
									<div class="form-group">
										<?= $form->field($model, 'company_name')->textInput(['onkeyup' => "getPageUrl()",'required'=>true,'value'=>$data[0]['company_name']]) ?>
									</div>
									<div class="form-group">
										<?= $form->field($model, 'contact_address')->textarea(['onkeyup' => "getPageUrl()",'rows'=>5,'required'=>true,'value'=>$data[0]['contact_address']]) ?>
									</div>
									
					
									<div class="form-group">
										<?= $form->field($model, 'map_link')->textInput(['onkeyup' => "getPageUrl()",'required'=>true,'value'=>$data[0]['map_link']]) ?>
									</div>
									<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
										<?= Html::submitButton($model->isNewRecord ? 'Update' : 'Update', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
								
									</div>
								</div>
							</div>
							<?php ActiveForm::end(); ?>
				
						</div>
					</div>
							<?php } ?>
									<?php if($type=="section-5"){?>
					<div role="tabpanel" class="tab-pane <?php if($type=="section-5"){echo "active" ;}?>">
						<div class="panel panel-default">
							<div class="panel-heading">
								Section 5 

								<div class="menuLink">
									<!-- <a href="<?php echo BASE_URL; ?>page-layout" title="List">
										<i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
									</a> -->
								</div>
							</div>
							<div class="panel-body">
								<div class="row table-bordered">
									<?php
													$form = ActiveForm::begin([
																'id' => 'content_form',
																'action'=>''.BASE_URL.'ukraine/update?page_id='.$data[0]['page_id'],
																'options' => [
																	'enctype' => 'multipart/form-data',
																],
																	]
													);
													?>
									<input type="hidden" value="<?= $_REQUEST['page_type']?>" name="page_type" id="">
									<div class="form-group">
										<?= $form->field($model, 'name')->textInput(['onkeyup' => "getPageUrl()",'value'=>$data[0]['name']]) ?>
									</div>
									<div class="form-group">
										<?= $form->field($model, 'phone')->textInput(['onkeyup' => "getPageUrl()",'required'=>true,'value'=>$data[0]['phone']]) ?>
									</div>
									<div class="form-group">
										<?= $form->field($model, 'email')->textInput(['onkeyup' => "getPageUrl()",'required'=>true,'value'=>$data[0]['email']]) ?>
									</div>
									
									<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
										<?= Html::submitButton($model->isNewRecord ? 'Update' : 'Update', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
									</div>
								</div>
							</div>
							<?php ActiveForm::end(); ?>
				
						</div>
					</div>
						<?php } ?>
                 
				</div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
$(".file_size").on("change", function (e) {

    var count=1;
    var files = e.currentTarget.files; // puts all files into an array
	var filesize=files[0].size;
	if(filesize > 10318072){
		this.value = "";
		alert(filesize+" file size is too big " );
	}
	
});

</script>
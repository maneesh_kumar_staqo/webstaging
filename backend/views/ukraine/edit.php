<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$bannerClass = "";
$contentClass = "";
$directorsClass = "";
$managementClass = "";
$governanceClass = "";
$imagesClass = "";
$leadershipClass = "";
$pageLayoutClass = "";
$type=$_REQUEST['page_type'];


$this->title = 'Update Page Layout';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="doctor-master-update">
    <div class="row">
        <div role="tabpanel">
         
            <div class="col-lg-10 col-lg-offset-1">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane <?php if($type=="banner-section"){echo "active";}?>">
                        <div class="panel panel-default">
							 <div class="panel-heading">
								Banner Section
							
							</div>
							<?php
							if($type=="banner-section"){?>
								<div class="panel-body">
									<div class="row table-bordered">
										<form id="content_form" action="<?= BASE_URL ?>ukraine/update?page_id=<?= $data[0]['id']; ?>" method="post" enctype="multipart/form-data">
											<input type="hidden" name="_csrf-backend"
												value="bGgEh8iRgm3KMFU1xLXMDR3xeGMCVaFPPEQGEcPOkQYqAWrwrdzhWa5oHwedx69eV6kAITRhkXoMEUFfoYngNw==">
											<div class="form-group">
												<div class="form-group field-ukraine-banner_text required">
													<label class="control-label" for="ukraine-banner_text">Banner Title</label>
													<input type="text" id="ukraine-banner_text" class="form-control" name="banner_text"
														onkeyup="getPageUrl()" value="<?= $data[0]['banner_text']; ?>" aria-required="true">
								
													<div class="help-block"></div>
												</div>
											</div>
										<div class="form-group">
										<input type="hidden" name="pageid" value="<?= $data[0]['pageid']?>">
												<div class="form-group field-ukraine-banner_text required">
													<label class="control-label" for="ukraine-banner_text">Sequence</label>
													<input type="text" id="ukraine-banner_text" class="form-control" name="sequence"
														onkeyup="getPageUrl()" value="<?= $data[0]['seq']; ?>" aria-required="true">
								
													<div class="help-block"></div>
												</div>
											</div>
											<input type="hidden" name="imageid" value="<?= $data[0]['id'] ?>" id="">
											<input type="hidden" name="pagetypeid" value="<?= $data[0]['pageid']?>">

											<div class="form-group">
												<div class="form-group field-file required">
													<label class="control-label" for="file">Image</label>
													<input type="hidden" name="page_type" value="<?= $page_type?>">
													<input type="file" id="file" class="form-control file_size" name="banner_image" aria-required="true"
														value="<?= $data[0]['banner_image'] ?>">
													<input type="hidden" value="<?= $data[0]['banner_image'] ?>" name="imagest" id="">
													<div class="help-block"></div>
												</div>
											</div>
												<div class="form-group">
													<div class="form-group field-file required">
											<div class="text-right">
												<a href="<?= BASE_URL .'uploads/images/'.$data[0]['banner_image'] ?>" target="_blank"><img src="<?= BASE_URL .'uploads/images/'.$data[0]['banner_image'] ?>" alt="" style="width:300px;"></a>
											</div>
											</div>

											</div>
											<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
												<button type="submit" class="btn btn-primary" name="create-button">Create</button> <a
													class="btn btn-danger" href="/hero-backup/backend/web/ukraine/index">Cancel</a>
											</div>
										</form>
									</div>
								</div>

							<?php } ?>
							
								

							</div>
                    </div>
						<?php if($type=="section-1"){?>
                    <div role="tabpanel" class="tab-pane <?php if($type=="section-1"){echo "active";}?>">
						<div class="panel panel-default">
							<div class="panel-heading">
								<?php echo $this->title ?>
								<div class="menuLink">
									
								</div>
							</div>
							<div class="panel-body">
								
							</div>
				
						
							
						</div>
					</div>
						<?php } ?>
					<?php if($type=="section-2"){
					
						?>
					 <div role="tabpanel" class="tab-pane <?php if($type=="section-2"){echo "active";}?>">
						<div class="panel panel-default">
							<div class="panel-heading">
								Edit
								
							</div>
							<div class="panel-body">
				
								<div class="row table-bordered">
									<form id="content_form" action="<?= BASE_URL ?>ukraine/update?page_id=<?= $data[0]['id']?>" method="post"
										enctype="multipart/form-data">
										<input type="hidden" name="_csrf-backend" value="<?=Yii::$app->request->getCsrfToken()?>" />
										<div class="form-group">
											<div class="form-group field-ukraine-gallery_title">
												<input type="hidden" name="page_type" value="<?= $page_type?>">
												<input type="hidden" name="pageid" value="<?= $data[0]['pageid']?>">
												<input type="hidden" name="pagetypeid" value="<?= $data[0]['page_id']?>">
												<input type="hidden" name="imageid" value="<?= $data[0]['image'] ?>" id="">
												
												
								
												<div class="help-block"></div>
											</div>
										</div>
											<div class="form-group">
												<div class="form-group">
													<label for="">Sequence</label>
													<input type="text" name="sequence" id="" value="<?= $data[0]['seq']?>" class="form-control">
											
												</div>
											
											</div>
								
										<div class="form-group">
											<div class="form-group field-file required">
												<label class="control-label" for="file">Image</label>
												<input type="file" id="file"
													class="form-control file_size" name="gallery_image" value="<?= $data[0]['image']?>" aria-required="true" accept="image/x-png,image/gif,image/jpeg">

												<div class="help-block"></div>
											</div>
										</div>
											<div class="form-group">
												<div class="form-group field-file required">
													<div class="text-right">
														<a href="<?= BASE_URL .'uploads/images/'.$data[0]['image'] ?>" target="_blank"><img
																src="<?= BASE_URL .'uploads/images/'.$data[0]['image'] ?>" alt="" style="width:300px;"></a>
													</div>
												</div>
											
											</div>

										<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
											<button type="submit" class="btn btn-primary" name="create-button">Update</button> 
										</div>
									</form>
								</div>
							</div>
				
						
							
						</div>
					</div>
					<?php } ?>
					<?php 
					if($type=="section-3"){?>
							<div role="tabpanel" class="tab-pane <?php if($type=="section-3"){echo "active" ;}?>">
								<div class="panel panel-default">
									<div class="panel-heading">
										Section 3 (Disclosure)

										
									</div>
									<div class="panel-body">
							
										<div class="row table-bordered">
											<form id="content_form" action="<?= BASE_URL ?>ukraine/update?page_id=<?= $data[0]['id']?>" method="post"
												enctype="multipart/form-data">
												<input type="hidden" name="_csrf-backend" value="<?=Yii::$app->request->getCsrfToken()?>" />
												<div class="form-group">
													<div class="form-group field-ukraine-disclouser_title required">
										
													<input type="hidden" name="page_type" value="<?= $page_type?>">
												<input type="hidden" name="pageid" value="<?= $data[0]['pageid']?>">
												<input type="hidden" name="pagetypeid" value="<?= $data[0]['page_id']?>">
													<input type="hidden" name="imageid" value="<?= $data[0]['file'] ?>" id="">
											
							
														<div class="help-block"></div>
													</div>
												</div>
												<div class="form-group">
												<div class="form-group">
												<label for="">Type</label>
													<select class="form-control" name="disclouser_type">
													<option value="Annual / Quarterly" <?php if($data[0]['type']=="Annual / Quarterly"){ echo "selected"; } ?>>Annual / Quarterly</option>
													<option value="Monthly" <?php if($data[0]['type']=="Monthly"){ echo "selected"; } ?>>Monthly</option>
													</select>
													</div>
												</div>
							
												<div class="form-group">
													<div class="form-group field-ukraine-disclouser_year required">
														<label class="control-label" for="ukraine-disclouser_year">Year</label>
														<input type="text" id="ukraine-disclouser_year" class="form-control" name="disclouser_year"
															onkeyup="getPageUrl()" required="" aria-required="true"
															value="<?= $data[0]['years']; ?>">
													</div>
												</div>
												<div class="form-group">
													<div class="form-group field-ukraine-disclouser_heading">
														<label class="control-label" for="ukraine-disclouser_heading">Title</label>
														<input type="text" id="ukraine-disclouser_heading" class="form-control"
															name="disclouser_heading" onkeyup="getPageUrl()" required=""
															value="<?= $data[0]['title']; ?>">
													</div>
												</div>
												<div class="form-group">
													<div class="form-group field-file required">
														<label class="control-label" for="file">File</label>
														<input type="file" id="file" class="form-control file_size" name="disclouser_file" 
															aria-required="true" value="<?= $data[0]['file']; ?>" accept="application/pdf,application/vnd.ms-excel">
													</div>
												</div>
											
												<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
													<button type="submit" class="btn btn-primary" name="create-button">Create</button> 
												</div>
											</form>
										</div>
									</div>
							
								</div>
							</div>
					<?php }
					?>
					<?php 
					if($type=="menu-section"){
					
						?>
							 <div role="tabpanel" class="tab-pane <?php if($type=="menu-section"){echo "active";}?>">
						<div class="panel panel-default">
							<div class="panel-heading">
								Edit
								
							</div>
							<div class="panel-body">
				
								<div class="row table-bordered">
									<form id="content_form" action="<?= BASE_URL ?>ukraine/updatemenu?id=<?= $data[0]['id']?>" method="post"
										enctype="multipart/form-data">
										<input type="hidden" name="_csrf-backend" value="<?=Yii::$app->request->getCsrfToken()?>" />
										<div class="form-group">
											<div class="form-group field-ukraine-gallery_title">
												<input type="hidden" name="page_type" value="<?= $page_type?>">
												<input type="hidden" name="pagetypeid" value="<?= $_REQUEST['pageid']?>">
												<div class="help-block"></div>
											</div>
										</div>
											<div class="form-group">
												<div class="form-group">
													<label for="">Name</label>
													<input type="text" name="name" id="" value="<?= $data[0]['name']?>" class="form-control">
												</div>
											</div>
										
										<div class="form-group">
											<div class="form-group">
												<label for="">Page Section</label>
												<select name="link" id="" class="form-control">
													<option value="banner" <?php if($data[0]['link']=="banner"){echo "selected"; } ?>>Banner</option>
													<option value="about" <?php if($data[0]['link']=="about"){echo "selected"; } ?>>About</option>
													<option value="photo" <?php if($data[0]['link']=="photo"){echo "selected"; } ?>>Photo</option>
													<option value="Disclosures" <?php if($data[0]['link']=="Disclosures"){echo "selected"; } ?>>Disclosures</option>
													<option value="contact" <?php if($data[0]['link']=="contact"){echo "selected"; } ?>>Contact</option>
													<option value="query" <?php if($data[0]['link']=="query"){echo "selected"; } ?>>for general queries</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<div class="form-group">
												<label for="">Visble in</label>
												<select name="visible_in" id="" class="form-control">
													<option value="1" <?php if($data[0]['visible_in']=="1"){echo "selected"; } ?>>header</option>
													<option value="2" <?php if($data[0]['visible_in']=="2"){echo "selected"; } ?>>Footer</option>
													<option value="3" <?php if($data[0]['visible_in']=="3"){echo "selected"; } ?>>Header and Footer</option>
												</select>
											</div>
										</div>
											<div class="form-group">
												<div class="form-group ">
													<label for="">Sequence</label>
														<input type="text"  name="seq" value="<?= $data[0]['status']; ?>" class="form-control">
												</div>
											
											</div>

										<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
											<button type="submit" class="btn btn-primary" name="create-button">Update</button> 
										</div>
									</form>
								</div>
							</div>
				
						
							
						</div>
					</div>
					<?php }
					?>
				
                 
				</div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
	$(".file_size").on("change", function (e) {

		var count = 1;
		var files = e.currentTarget.files; // puts all files into an array
		var filesize = files[0].size;
		if (filesize > 10318072) {
			this.value = "";
			alert(filesize + " file size is too big ");
		}
	});
</script>
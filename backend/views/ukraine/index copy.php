<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use backend\models\Utility;

$this->title = 'Ukraine';
?>
<div class="panel panel-default mainDiv">
	<div class="panel-heading">
		<?php echo $this->title; ?>
		<div class="menuLink">
			<a href="<?php echo BASE_URL; ?>simulation-contactus/create" title="Create Ukraine">
				<i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
			</a>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			
			<?=
			GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'summary' => FALSE,
				'columns' => [
					[
						'attribute' => 'name',
						'value' => 'banner_text',
						'filter' => true,
					],
					// [
					// 	'attribute' => 'designation',
					// 	'value' => 'designation',
					// 	'filter' => true,
					// ],
					// [
					// 	'attribute' => 'email',
					// 	'value' => 'email',
					// 	'filter' => true,
					// ],
					// [
					// 	'attribute' => 'seq',
					// 	'value' => 'seq',
					// 	'filter' => true,
					// ],
					
					// [
					// 	'attribute' => 'status',
					// 	'value' => 'status',
					// 	'filter' => true,
					// ],
					[
						'class' => 'yii\grid\ActionColumn',
						'template' => '{update}',
					],
					
					
				],
			]);
			?>
			
		</div>
	</div>
</div>
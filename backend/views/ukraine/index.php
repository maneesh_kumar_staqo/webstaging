<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use backend\models\Utility;
$user_type  = \Yii::$app->user->identity->user_type ;

$this->title = 'Ukraine';
?>
<div class="panel panel-default mainDiv">
	<div class="panel-heading">
		<?php echo $this->title; ?>
		<div class="menuLink">
			
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">

			<div id="w0" class="grid-view">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th><a href="" data-sort="banner_text">Page
									Name</a></th>
							
							</th>
						
						
							<th class="action-column">Edit</th>
						</tr>
						
					</thead>
					<tbody>
						<?php 
							if($user_type=="ukraine-gea"){ ?>
								<tr data-key="1">
							<td>GEA Solar </td>
							
							<td><a href="<?= BASE_URL ?>ukraine/all?id=1&page_type=banner-section";"
									title="Update"><span class="glyphicon glyphicon-pencil"></span></a></td>
						</tr>
							<?php }elseif($user_type=="ukraine-greenway"){?>
								<tr data-key="1">
							<td>Greenwaysolar </td>
						
							<td><a href="<?= BASE_URL ?>ukraine/all?id=2&page_type=banner-section";" title="Update"><span class="glyphicon glyphicon-pencil"></span></a></td>
						</tr>
							<?php }else{ ?>
								<tr data-key="1">
							<td>GEA Solar </td>
							
							<td><a href="<?= BASE_URL ?>ukraine/all?id=1&page_type=banner-section";"
									title="Update"><span class="glyphicon glyphicon-pencil"></span></a></td>
						</tr>
						<tr data-key="1">
							<td>Greenwaysolar </td>
						
							<td><a href="<?= BASE_URL ?>ukraine/all?id=2&page_type=banner-section";" title="Update"><span class="glyphicon glyphicon-pencil"></span></a></td>
						</tr>


							<?php } ?>
						
						
					</tbody>
				</table>
			</div>

		</div>
	</div>
</div>
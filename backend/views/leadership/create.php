<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Create Leadership';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>pagelayout/update?id=<?= $_GET['page_id']; ?>&type=page-directors" title="Leadership List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                                ]
                );
                ?>            


                     <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'name')->textInput() ?>
                        </div>
                        <div class="col-md-12">
                        <?= $form->field($model, 'designation')->textInput() ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'description')->textarea(['rows' => 4,'class'=>'left_class_editor']); ?>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="file" name="image" />
                                <input type="hidden" name="uploaded_image" value="<?= $model->image; ?>" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'type')->dropDownList(['Leadership' => 'Leadership', 'Leadership Vision' => 'Leadership Vision']); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'image_position')->dropDownList(['' => 'Select Image Position','Left' => 'Left', 'Right' => 'Right']); ?>
                        </div>
                        
                        <div class="col-md-6">
                            <?= $form->field($model, 'priority')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                                <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                                <?= Html::a('Cancel', ['/pagelayout/update?id='.$_GET['page_id'].'&type=page-directors'], ['class' => 'btn btn-danger']) ?>
                            </div>
                        </div>
                 </div>
					
					
                 
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Create Tracking';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?><?= $_GET['page']; ?>/update?id=<?= $_GET['page_id']; ?>&type=<?= $_GET['type']; ?>" title="Tracking List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                                ]
                );
                ?>            


                    <div class="form-group">
                        <?= $form->field($model, 'title')->textInput() ?>      
                    </div>
					<div class="form-group">
                        <?= $form->field($model, 'description')->textarea(['rows' => 5, 'class' => 'left_class_editor']); ?>      
                    </div>
					<div class="form-group">
						<div style="float:left;width:50%">
							<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>     
						</div>
						<div style="width:25%;float:left;">
							<?= $form->field($model, 'type')->dropDownList(["" => "Select Type","rooftop_contents" => "Rooftop Contents","rooftop_reasons" => "Rooftop Reasons","rooftop_benefits" => "Rooftop Benefits","rooftop_solar" => "Rooftop Solar","rooftop_clients" => "Rooftop Clients","rooftop_awards" => "Rooftop Awards","rooftop_appreciation" => "Rooftop Appreciation","rooftop_tracking" => "Rooftop Tracking","rooftop_gallery" => "Rooftop Gallery","rooftop_knowmore" => "Rooftop Knowmore" ]); ?>
						</div>
						<div style="float:left;width:25%">
							<?= $form->field($model, 'priority')->textInput() ?>      
						</div>
                    </div>
					<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                        <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                        <?= Html::a('Cancel', ['/'.$_GET['page'].'/update?id='.$_GET['page_id'].'&type='.$_GET['type']], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

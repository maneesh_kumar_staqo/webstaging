<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;

$type = !empty($_GET['type']) ? $_GET['type'] : '';
$this->title = 'Project Content List';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <a href="<?php echo BASE_URL; ?>projectcontent/create?page_id=<?= $_GET['id']; ?>&type=<?= $type; ?>" title="Create Project Content">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Description',
                                'attribute' => 'description',
                                'value' => 'description',
								'filter' => false,
                            ],
							[
                                'label' => 'Type',
                                'attribute' => 'type',
                                'value' => 'type',
								'filter' => true,
                            ],
							[
                                'label' => 'Status',
                                'attribute' => 'status',
                                'value' => 'status',
								'filter' => true,
                            ],
                            [
								'class' => 'yii\grid\ActionColumn',
								'template' => '{update}',
								'buttons' => [
									'update' => function ($url, $searchModel) {
									return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
												'title' => Yii::t('yii', 'Update'),
									]);
								},
								],
								'urlCreator' => function ($action, $searchModel, $key, $index) {
								if ($action === 'update') {
									$page = 'pagelayout';
									$type = 'other_content';
									if($_GET['id'] == 3){
										$page = 'project';
									}
									if(!empty($_GET['type'])){
										$type = $_GET['type'];
									}
									
									$url = \Yii::$app->urlManager->createAbsoluteUrl('projectcontent/update' . '?id=' . $searchModel->id . '&page_id=' . $_GET['id'] . '&type='.$type.'&page=project');
										return $url;
									}
								}
							],
                        ],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

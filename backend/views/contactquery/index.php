<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Contact Us Query';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <!--a href="<?php //echo BASE_URL; ?>page-layout-create" title="Create Page">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a -->
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Name',
                                'attribute' => 'name',
                                'value' => 'name',
                            ],
							[
                                'label' => 'Email Id',
                                'attribute' => 'email_id',
                                'value' => 'email_id',
                            ],
							[
                                'label' => 'Type',
                                'attribute' => 'type',
                                'value' => 'type',
                            ],
							[
                                'label' => 'Message',
                                'attribute' => 'message',
                                'value' => 'message',
								'filter' => false,
                            ],
                        ],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

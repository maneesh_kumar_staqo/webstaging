<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'PDF';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <a href="<?php echo BASE_URL; ?>pdf/create" title="Create Pdf">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Pdf Name',
                                'attribute' => 'pdf_name',
                                'value' => 'pdf_name',
                            ],
							[
                                'label' => 'Pdf Link',
                                'attribute' => 'pdf_link',
                                'value' => 'pdf_link',
                            ],
						],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

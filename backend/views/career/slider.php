<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;
use backend\models\Category;

$this->title = 'Slider';
?>
<div class="panel panel-default" >
	<div class="panel-heading">
		<?php echo $this->title; ?>
		<div class="menuLink">
			<a href="<?php echo BASE_URL; ?>career/slider?page_id=<?= $_GET['id']; ?>&type=page-slider" title="Create Slider">
				<i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
			</a>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			
			<?=
			GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'summary' => FALSE,
				'columns' => [
					[
						'label' => 'Image',
						'attribute' => 'image',
						'format' => 'raw',
						'value' => function($model){
							return '<img src="'.BASE_URL.'uploads/career/'.$model->image.'" height="50" width"50"/>';
						},
						'filter' => false,
					],
					[
						'label' => 'Priority',
						'attribute' => 'priority',
						'value' => 'priority',
						'filter' => false,
					],
					[
						'label' => 'Status',
						'attribute' => 'status',
						'value' => 'status',
						'filter' => false,
					],
					[
						'class' => 'yii\grid\ActionColumn',
						'template' => '{update}',
						'buttons' => [
							'update' => function ($url, $searchModel) {
							return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
										'title' => Yii::t('yii', 'Update'),
							]);
						},
						],
						'urlCreator' => function ($action, $searchModel, $key, $index) {
						if ($action === 'update') {
							$url = \Yii::$app->urlManager->createAbsoluteUrl('career/slider' . '?id=' . $searchModel->id . '&page_id=' . $_GET['id'] . '&type=page-slider');
								return $url;
							}
						}
					],
					
					
				],
			]);
			?>
			
		</div>
	</div>
</div>
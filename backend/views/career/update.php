<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$bannerClass = "";
$contentClass = "";
$wemakeClass = "";
$pageLayoutClass = "";
$sliderClass = "";

$learnClass = "";
$campusClass = "";
$vacanciesClass = "";
if(!empty($type)){
	if($type == "page-banners"){
		$bannerClass = "active";
	}else if($type == "page-contents"){
		$contentClass = "active";
	}else if($type == "page-wemake"){
		$wemakeClass = "active";
	}else if($type == "page-career"){
		$pageLayoutClass = "active";
	}else if($type == "page-slider"){
		$sliderClass = "active";
	}else if($type == "page-learn"){
		$learnClass = "active";
	}else if($type == "page-campus"){
		$campusClass = "active";
	}else if($type == "page-vacancies"){
		$vacanciesClass = "active";
	}
}else{
	$pageLayoutClass = "active"; 
}

$this->title = 'Update Page Layout';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="doctor-master-update">
    <div class="row">
        <div role="tabpanel">
            <div class="col-lg-2 col-md-offset-0" style="margin-left: -25px;margin-top: 5px">
                <!-- Nav tabs -->
                <ul class="left_tab nav-tabs nav-pills nav-stacked" role="tablist">
					<li role="presentation" class="<?= $pageLayoutClass; ?>"><a  href="<?= BASE_URL . 'career/update?id='.$id.'&type=page-career' ?> ">Page Layout</a></li>
					<li role="presentation" class="<?= $bannerClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'career/update?id='.$id.'&type=page-banners' ?> ">Page Banners</a> <span><input type="text" id="pagePriority_banners" value="<?= !empty($priority['banners']) ? $priority['banners'] : ''; ?>" onchange="savePageLayout('banners','hfe_banners','<?= $id; ?>');"></span> </li>
                    <li role="presentation" class="<?= $contentClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'career/update?id='.$id.'&type=page-contents' ?>">Page Contents</a> <span><input type="text" id="pagePriority_contents" value="<?= !empty($priority['contents']) ? $priority['contents'] : ''; ?>" onchange="savePageLayout('contents','hfe_contents','<?= $id; ?>');"></span></li>
					<li role="presentation" class="<?= $wemakeClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'career/update?id='.$id.'&type=page-wemake' ?>">We Make</a> <span><input type="text" id="pagePriority_wemake" value="<?= !empty($priority['wemake']) ? $priority['wemake'] : ''; ?>" onchange="savePageLayout('wemake','hfe_career_we_make','<?= $id; ?>');"></span></li>
					<li role="presentation" class="<?= $sliderClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'career/update?id='.$id.'&type=page-slider' ?>">Slider</a> <span><input type="text" id="pagePriority_slider" value="<?= !empty($priority['slider']) ? $priority['slider'] : ''; ?>" onchange="savePageLayout('slider','hfe_career_slider','<?= $id; ?>');"></span></li>
					
					<li role="presentation" class="<?= $learnClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'career/update?id='.$id.'&type=page-learn' ?>">Learn to Win</a> <span><input type="text" id="pagePriority_learn" value="<?= !empty($priority['learn']) ? $priority['learn'] : ''; ?>" onchange="savePageLayout('learn','hfe_career_learn_to_win','<?= $id; ?>');"></span></li>
					<li role="presentation" class="<?= $campusClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'career/update?id='.$id.'&type=page-campus' ?>">Camp Placement</a> <span><input type="text" id="pagePriority_campus" value="<?= !empty($priority['campus']) ? $priority['campus'] : ''; ?>" onchange="savePageLayout('campus','hfe_career_campus_placements','<?= $id; ?>');"></span></li>
					<li role="presentation" class="<?= $vacanciesClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'career/update?id='.$id.'&type=page-vacancies' ?>">Vacancies</a> <span><input type="text" id="pagePriority_vacancies" value="<?= !empty($priority['vacancies']) ? $priority['vacancies'] : ''; ?>" onchange="savePageLayout('vacancies','hfe_career_vacancies','<?= $id; ?>');"></span></li>
				</ul>
            </div> 
            <div class="col-lg-10">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane <?= $pageLayoutClass; ?>">
                        <div class="panel panel-default">
							 <div class="panel-heading">
								<?php echo $this->title ?>
								<div class="menuLink">
									<a href="<?php echo BASE_URL; ?>page-layout" title="List">
										<i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
									</a>
								</div>
							</div>
							<div class="panel-body">
								<div class="row table-bordered">
									<?php
									$form = ActiveForm::begin([
												'id' => 'content_form',
												'options' => [
													'enctype' => 'multipart/form-data',
												],
													]
									);
									?>            


										<div class="form-group">
											<?= $form->field($model, 'page_name')->textInput(['onkeyup' => "getPageUrl($(this).val(), 'pageUrl')"]) ?>      
										</div>
										
										<div class="form-group">
											<?= $form->field($model, 'page_url')->textInput(["id" => "pageUrl"]) ?>      
										</div>
										<div class="form-group">
											<div style="width:50%;float:left;">
												<?= $form->field($model, 'page_type')->dropDownList(['' => 'Select Page Type', 'Home Page Layout' => 'Home Page Layout','About Page Layout' => 'About Page Layout','CSR Page Layout' => 'CSR Page Layout','Project Page Layout' => 'Project Page Layout','Rooftop Solar Layout' => 'Rooftop Solar Layout','Policies Layout' => 'Policies Layout','Contact Us' => 'Contact Us', 'Video Gallery' => 'Video Gallery', 'Disclosures' => 'Disclosures', 'Career Page Layout' => 'Career Page Layout']); ?>
											</div>
											 <div style="width:50%;float:left;">
												<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
											</div>
										</div>
										<div class="form-group">    
											<?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 1000]) ?>
										</div>

										<div class="form-group">    
											<?= $form->field($model, 'meta_title')->textInput(['maxlength' => 1000]) ?>
										</div>
										<div class="form-group">    
											<?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>
										</div>
										<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
											<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
											<?= Html::a('Cancel', ['/pagelayout/index'], ['class' => 'btn btn-danger']) ?>
										</div>
									</div>
								</div>
								<?php ActiveForm::end(); ?>
							</div>
                    </div>
                    <div role="tabpanel" class="tab-pane <?= $bannerClass; ?>">
                        <?php
                        echo $this->render('../banners/index', [
                            'dataProvider' => $bannersDataProvider,
                            'searchModel' => $bannersDataProvider,
							'page' => 'career'
                        ])
                        ?>
                    </div>
                    <div role="tabpanel" class="tab-pane <?= $contentClass; ?>">
                        <?php
                        echo $this->render('../contents/index', [
                            'dataProvider' => $contentsDataProvider,
                            'searchModel' => $contentsDataProvider,
							'page' => 'career'
                        ])
                        ?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $wemakeClass; ?>">
                        <?php
							echo $this->render('../career/index', [
								'dataProvider' => $pageWemake,
								'searchModel' => $pageWemake
							])
						  ?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $sliderClass; ?>">
                        <?php
							echo $this->render('../career/slider', [
								'dataProvider' => $pageSlider,
								'searchModel' => $pageSlider
							])
						  ?>
                    </div>
					
					<div role="tabpanel" class="tab-pane <?= $learnClass; ?>">
                        <?php
							echo $this->render('../career/learn_to_win', [
								'dataProvider' => $pageLearn,
								'searchModel' => $pageLearn
							])
						  ?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $campusClass; ?>">
                        <?php
							echo $this->render('../career/campus_placement', [
								'dataProvider' => $pageCampus,
								'searchModel' => $pageCampus
							])
						  ?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $vacanciesClass; ?>">
                        <?php
							echo $this->render('../career/vacancies', [
								'dataProvider' => $pageVacancies,
								'searchModel' => $pageVacancies
							])
						  ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

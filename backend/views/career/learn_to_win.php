<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;
use backend\models\Category;

$this->title = 'Learn to Win';
?>
<div class="panel panel-default" >
	<div class="panel-heading">
		<?php echo $this->title; ?>
		<div class="menuLink">
			<a href="<?php echo BASE_URL; ?>career/learn?page_id=<?= $_GET['id']; ?>&type=page-learn" title="Create learn to Win">
				<i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
			</a>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			
			<?=
			GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'summary' => FALSE,
				'columns' => [
					[
						'label' => 'Title',
						'attribute' => 'title',
						'value' => 'title',
						'filter' => false,
					],
					[
						'label' => 'Image',
						'attribute' => 'image',
						'format' => 'raw',
						'value' => function($model){
							return '<img src="'.BASE_URL.'uploads/career/'.$model->image.'" height="50" width"50"/>';
						},
						'filter' => false,
					],
					[
						'label' => 'Image 2',
						'attribute' => 'image2',
						'format' => 'raw',
						'value' => function($model){
							return '<img src="'.BASE_URL.'uploads/career/'.$model->image2.'" height="50" width"50"/>';
						},
						'filter' => false,
					],
					[
						'label' => 'Image 3',
						'attribute' => 'image3',
						'format' => 'raw',
						'value' => function($model){
							return '<img src="'.BASE_URL.'uploads/career/'.$model->image3.'" height="50" width"50"/>';
						},
						'filter' => false,
					],
					[
						'label' => 'Status',
						'attribute' => 'status',
						'value' => 'status',
						'filter' => false,
					],
					[
						'class' => 'yii\grid\ActionColumn',
						'template' => '{update}',
						'buttons' => [
							'update' => function ($url, $searchModel) {
							return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
										'title' => Yii::t('yii', 'Update'),
							]);
						},
						],
						'urlCreator' => function ($action, $searchModel, $key, $index) {
						if ($action === 'update') {
							$url = \Yii::$app->urlManager->createAbsoluteUrl('career/learn' . '?id=' . $searchModel->id . '&page_id=' . $_GET['id'] . '&type=page-learn');
								return $url;
							}
						}
					],
					
					
				],
			]);
			?>
			
		</div>
	</div>
</div>
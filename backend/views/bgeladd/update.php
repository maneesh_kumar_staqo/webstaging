<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Update Bgel Address';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>bgeladd/index" title="Bgel Address List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                             'enctype' => 'multipart/form-data',
						],
							]
                );
                ?>            

                    <div class="form-group">
                        <?= $form->field($model, 'address')->textarea(['rows' => 5, 'class' => 'left_class_editor']); ?>
                    </div>
					<div class="form-group">
						<div style="width:50%;float:left;">
							<?= $form->field($model, 'priority')->textInput() ?>      
						</div>
						<div style="width:50%;float:left;">
							<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
						</div>
					</div>
                    <div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                        <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                        <?= Html::a('Cancel', ['/bgeladd/index'], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Bgel Address';
$page = 'disclosures';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
             

                    <a href="<?php echo BASE_URL; ?>bgeladd/create?page_id=<?= $_GET['id']; ?>&type=page-address&page=<?= $page; ?>" title="Bgel Address">
				<i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
			</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Address',
                                'attribute' => 'address',
                                'value' => 'address'
                            ],
							[
                                'label' => 'Status',
                                'attribute' => 'status',
                                'value' => 'status'
                            ],
							// [
                            //     'class' => 'yii\grid\ActionColumn',
                            //     'options' => ['style' => 'width:15px;'],
                            //     'template' => '{update}'
                            // ],
                            [
						'class' => 'yii\grid\ActionColumn',
						'template' => '{update}',
						'buttons' => [
							'update' => function ($url, $searchModel) {
							return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
										'title' => Yii::t('yii', 'Update'),
							]);
						},
						],
						'urlCreator' => function ($action, $searchModel, $key, $index) {
						if ($action === 'update') {
                         
							
						
                            $type= "page-contents";
							if(!empty($_GET['type'])){
								$type= $_GET['type'];
							}
                            $page = 'disclosures';
							$url = \Yii::$app->urlManager->createAbsoluteUrl('bgeladd/update' . '?id=' . $searchModel->id . '&page_id=' . $_GET['id'] . '&type='.$type.'&page='.$page);
								return $url;
							}
						}
					],
                        ],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

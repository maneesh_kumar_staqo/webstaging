<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$type = !empty($_GET['type']) ? $_GET['type'] : '';
$page = !empty($_GET['page']) ? $_GET['page'] : '';
$this->title = 'Create Business Offer Tabdata';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>business-offer-tabdata" title="Business Offer">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                                ]
                );
                ?>            
                    <div class="row">
                        <div class="col-md-12">
                                <?= $form->field($model, 'heading')->textInput() ?>
                        </div>
                       
                        <div class="col-md-12">
                  
                                <?= $form->field($model, 'description')->textarea(['rows' => 5, 'class' => 'left_class_editor']); ?>
                  
                      
                        </div>
                      
                       
                       
                        
                    </div>
                    
                   
                    <div class="row">
                        <div class="col-md-6">
                           
                            <?= $form->field($model, 'priority')->textInput() ?>
                    
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
                        </div>
                    </div>

                  
					
				
					
                    <div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                        <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                        <?= Html::a('Cancel', ['/'.$page.'/update'], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

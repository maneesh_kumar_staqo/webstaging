<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Update Investor Relations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>project/update?id=<?= $_GET['id']; ?>&type=<?= $_GET['type']; ?>" title="Investor Relations List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                             'enctype' => 'multipart/form-data',
						],
							]
                );
                ?>            

                    <div class="col-md-6">
                       <div class="form-group">
						<div style="width:100%; float:left; margin-left:12px; margin-top:10px;margin-bottom: 30px;">
							<label class="control-label" for="pagelayout-image">Image</label>
							<input type="file" name="image"/>
							<input type="hidden" name="uploaded_image" id="uploaded_image_<?= $model->id; ?>" value="<?= $model->image; ?>"/>
						</div>
						<?php if(!empty($model->image)){ ?>
						<div style="width:100%; float:left; margin-left:16px; margin-top:24px;" id="delete_img_<?= $model->id; ?>">
							<div style="position:absolute;margin-top:-11px;margin-left:42px;"><img src="<?= BASE_URL; ?>img/close.png" style="cursor:pointer;" title="Delete Image" onClick="return deleteImg('<?= $model->id; ?>');"/></div>
							<img src="<?= BASE_URL ?>/uploads/investor/<?= $model->image; ?>" height="50" width="50"/>
						</div>
						<?php } ?>
						
					
					</div>
                    </div>
                    <div class="col-md-6">
                          
                                <?= $form->field($model, 'priority')->textInput() ?>
                          
                    </div>
					 <div class="col-md-6">
				
						<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
					
            </div>
             <div class="col-md-12">
					<div class="form-group" >
                        <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                        <?= Html::a('Cancel', ['/project/update?id='.$_GET['page_id'].'&type='.$_GET['type']], ['class' => 'btn btn-danger']) ?>
                    </div>
            </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

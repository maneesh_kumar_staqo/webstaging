<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;

$type = !empty($_GET['type']) ? $_GET['type'] : '';
$this->title = 'Investor Relations List';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <a href="<?php echo BASE_URL; ?>investor/create?page_id=<?= $_GET['id']; ?>&type=<?= $type; ?>&page=<?= $page; ?>" title="Create Investor Relations">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                           [
						'label' => 'Image',
						'attribute' => 'image',
						'format' => 'raw',
						'value' => function($dataProvider){
							if(!empty($dataProvider->image)){
								return '<img src="'.BASE_URL.'/uploads/investor/'.$dataProvider->image.'" height="50" width="50"/>';
							}
						},
						'filter' => false,
					],
							[
                                'label' => 'Status',
                                'attribute' => 'status',
                                'value' => 'status',
								'filter' => false,
                            ],
                            [
								'class' => 'yii\grid\ActionColumn',
								'template' => '{update}',
								'buttons' => [
									'update' => function ($url, $searchModel) {
									return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
												'title' => Yii::t('yii', 'Update'),
									]);
								},
								],
								'urlCreator' => function ($action, $searchModel, $key, $index) {
								if ($action === 'update') {
									$page = 'pagelayout';
									if($_GET['id'] == 3){
										$page = 'project';
									}
									$type = 'page-images';
									$imgtype = '';
									if(!empty($_GET['type'])){
										$type = $_GET['type'];
									}if(!empty($_GET['imgtype'])){
										$imgtype = $_GET['imgtype'];
									}
									
									$url = \Yii::$app->urlManager->createAbsoluteUrl('investor/update' . '?id=' . $searchModel->id . '&page_id=' . $_GET['id'] . '&type='.$type.'&page='.$page);
										return $url;
									}
								}
							],
                        ],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

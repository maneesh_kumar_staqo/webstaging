<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$type = !empty($_GET['type']) ? $_GET['type'] : '';
$this->title = 'Create Investor Relations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>project/update?id=<?= $_GET['page_id']; ?>&type=<?= $type; ?>" title="Investor Relations List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                                ]
                );
                ?>            

                    <div class="col-md-6">
                        <div class="form-group">
                        
                                <label class="control-label" for="pagelayout-image">Image</label>
                                <input type="file" name="image" />
                          
                        </div>
                    </div>
                    <div class="col-md-6">
                          
                                <?= $form->field($model, 'priority')->textInput() ?>
                          
                    </div>
                    <div class="col-md-6">
                          
                                <?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
                           
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                            <?= Html::a('Cancel', ['/project/update?id='.$_GET['page_id'].'&type='.$_GET['type']], ['class' => 'btn btn-danger']) ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

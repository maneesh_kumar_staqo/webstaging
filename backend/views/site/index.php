<?php
use yii\helpers\Html;
use common\models\User;
use common\models\Utility;
$user_type  = \Yii::$app->user->identity->user_type ;

$this->title = 'Dashboard';

?>
<div class="col-lg-12">
<div class="panel panel-default">
<div class="panel-heading">
	<?php echo $this->title; ?>
</div>
<div class="panel-body">
	<div class="table-responsive">
		<div class="row table-bordered">
			
			<?php
		
				
			if($user_type!="ukraine-gea" && $user_type!="ukraine-greenway"){?>
					<div class="col-lg-3 col-md-6">
				<div class="panel panel-green">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-file fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div>All Page Layout</div>
							</div>
						</div>
					</div>
					<a href="<?php echo BASE_URL . 'page-layout' ?>">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-6">
				<div class="panel panel-success">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-newspaper-o fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div>Blogs</div>
							</div>
						</div>
					</div>
					<a href="<?php echo BASE_URL . 'blogs' ?>">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-6">
				<div class="panel panel-yellow">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-video-camera fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div>Youtube Videos</div>
							</div>
						</div>
					</div>
					<a href="<?php echo BASE_URL . 'youtube' ?>">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-6">
				<div class="panel panel-green">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-building fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div>Change Password</div>
							</div>
						</div>
					</div>
					<a href="<?php echo BASE_URL . 'change-password' ?>">
						<div class="panel-footer">
							<span class="pull-left">
								View Details
							</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
				<?php }else{?>


	<div class="col-lg-3 col-md-6">
				<div class="panel panel-yellow">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-file fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div>Ukraine Page</div>
							</div>
						</div>
					</div>
					<a href="<?php echo BASE_URL . 'ukraine' ?>">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>

			</div>
				<?php }
				
				?>
			
		</div>
	</div>
</div>



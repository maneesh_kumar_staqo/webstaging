<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

//$this->title = 'Login';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-4 col-md-offset-4">
	<div class="login-panel panel-default table-bordered">
		<div class="panel-heading">
			<h3 class="panel-title">Please Sign In</h3>
		</div>
		<div class="panel-body table-bordered">
			<?php
			$form = ActiveForm::begin([
						'id' => 'login-form',
			]);
			?>
			<fieldset>
				<div class="form-group">
					<?= $form->field($model, 'username') ?>
				</div>
				<div class="form-group">
					<?= $form->field($model, 'password')->passwordInput() ?>
				</div>
				<div class="checkbox">
					<label>
						<?= $form->field($model, 'rememberMe')->checkbox() ?>
					</label>
				</div>
				<div class="form-group" style="margin-top:20px;">
					<?= Html::submitButton('Login', ['class' => 'btn btn-lg btn-primary btn-block', 'name' => 'login-button']) ?>
				</div>
			</fieldset>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>

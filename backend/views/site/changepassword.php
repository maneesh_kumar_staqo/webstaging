<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\User $model
 */
$this->title = 'Change Password' ;
$this->params['breadcrumbs'][] = 'Change Password';
?>
<div class="col-lg-5 col-md-offset-3">
    <div class="panel panel-default">
		<div class="panel-heading">
			<?php echo $this->title ?>
		</div>
		<div class="panel-body">
			<div class="table-responsive table-bordered">
				<?php if (!empty($error)) { ?>
					<div class="alert alert-info">
						<?= $error ?>
					</div>
				<?php } ?>
				<?php $form = ActiveForm::begin(); ?>
				<fieldset>
					<div class='form-group'>
						<?= $form->field($model, 'old_password_hash')->passwordInput(['maxlength' => 32]) ?>
					</div>
					<div class='form-group'>
						<?= $form->field($model, 'new_password_hash')->passwordInput(['maxlength' => 32]) ?>
					</div>
					<div class='form-group'>
						<?= $form->field($model, 'confirm_password_hash')->passwordInput(['maxlength' => 32]) ?>
					</div>
					<div class="form-group">
						<?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'style' => 'margin-left:10px;']) ?>
					</div>
					<?php ActiveForm::end(); ?>
			</div>
		</div>
    </div>
</div>


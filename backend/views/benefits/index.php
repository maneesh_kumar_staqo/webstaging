<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;

$type = !empty($_GET['type']) ? $_GET['type'] : '';
$this->title = 'Benefits List';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <a href="<?php echo BASE_URL; ?>benefits/create?page_id=<?= $_GET['id']; ?>&page=<?= $page; ?>&type=<?= $type; ?>" title="Create Benefits">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Title',
                                'attribute' => 'title',
                                'value' => 'title',
								'filter' => false,
                            ],
							[
                                'label' => 'Priority',
                                'attribute' => 'priority',
                                'value' => 'priority',
								'filter' => false,
                            ],
							[
                                'label' => 'Icon',
                                'attribute' => 'icon',
								'format' => 'raw',
                                'value' => function($dataProvider){
									return '<img src="'.BASE_URL.'/uploads/benefits/'.$dataProvider->icon.'" height="50" width="50"/>';
								},
								'filter' => false,
                            ],
							[
                                'label' => 'Status',
                                'attribute' => 'status',
                                'value' => 'status',
								'filter' => false,
                            ],
                            [
								'class' => 'yii\grid\ActionColumn',
								'template' => '{update}',
								'buttons' => [
									'update' => function ($url, $searchModel) {
									return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
												'title' => Yii::t('yii', 'Update'),
									]);
								},
								],
								'urlCreator' => function ($action, $searchModel, $key, $index) {
								if ($action === 'update') {
									$type = !empty($_GET['type']) ? $_GET['type'] : '';
									$url = \Yii::$app->urlManager->createAbsoluteUrl('benefits/update' . '?id=' . $searchModel->id . '&page_id=' . $_GET['id'] . '&type='.$type.'&page=rooftop');
										return $url;
									}
								}
							],
                        ],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;

$this->title = 'Newsroom';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <a href="<?php echo BASE_URL; ?>newsroom/create" title="Create Newsroom">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Title',
                                'attribute' => 'title',
                                'value' => 'title',
								'filter' => true,
                            ],
							[
                                'label' => 'Team Name',
                                'attribute' => 'team_name',
                                'value' => 'team_name',
								'filter' => true,
                            ],
							[
                                'label' => 'Image',
                                'attribute' => 'image',
								'format' => 'raw',
                                'value' => function($dataProvider){
									if(!empty($dataProvider->image)){
										return '<img src="'.BASE_URL.'/uploads/newsroom/'.$dataProvider->image.'" height="50" width="50"/>';
									}
								},
								'filter' => false,
                            ],
							[
                                'label' => 'Status',
                                'attribute' => 'status',
                                'value' => 'status',
								'filter' => true,
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'options' => ['style' => 'width:15px;'],
                                'template' => '{update}'
                            ],
                        ],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

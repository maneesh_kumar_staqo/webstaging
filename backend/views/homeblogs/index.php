<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;

$this->title = 'Blogs';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <!--a href="#" title="Create Blog">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a-->
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Title',
                                'attribute' => 'title',
                                'value' => 'title',
								'filter' => false,
                            ],
							[
                                'label' => 'Team Name',
                                'attribute' => 'team_name',
                                'value' => 'team_name',
								'filter' => false,
                            ],
							[
                                'label' => 'Image',
                                'attribute' => 'image',
								'format' => 'raw',
                                'value' => function($dataProvider){
									return '<img src="'.BASE_URL.'/uploads/blogs/'.$dataProvider->image.'" height="50" width="50"/>';
								},
								'filter' => false,
                            ],
							[
                                'label' => 'Status',
                                'attribute' => 'status',
                                'value' => 'status',
								'filter' => false,
                            ],
                            [
                                'label' => 'Is Home',
                                'attribute' => 'is_home',
                                'format' => 'raw',
                                'value' => function($model){
									$check = '';
									if($model->is_home == "Yes"){
										$check = "checked='checked'";
									}
									return '<input type="radio" '.$check.' name="blogs" id="blogs_'.$model->id.'" onclick="return updateBlogs('.$model->id.')" style="cursor:pointer;">';
								},
								'filter' => false,
							],
                        ],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function updateBlogs(blog_id){
	$.ajax({
		type: 'get',
		url: JS_BASE_URL + 'homeblogs/updateblogs',
		data: {blog_id : blog_id},
		success: function(data){
			alert("Updated successfully.");
		}
	});
}
</script>
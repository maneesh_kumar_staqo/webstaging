<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Create Focus Areas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>focusareas/update?id=<?= $_GET['page_id']; ?>&type=page-focusareas" title="Focus Areas List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                                ]
                );
                ?>            


                    <div class="form-group">
                        <?= $form->field($model, 'state_name')->textInput() ?>      
                    </div>
					<div class="form-group">
                        <?= $form->field($model, 'operational_beneficiary')->textInput() ?>      
                    </div>
					<div class="form-group">
						<div style="width:30%; float:left; margin-left:16px; margin-top:24px;">
							<input type="file" name="image"/>
							<input type="hidden" name="uploaded_image" value="<?= $model->image; ?>"/>
						</div>
						<div style="width:30%;float:left;">
							<?= $form->field($model, 'category_id')->dropDownList($categoryData); ?>
						</div>
						<div style="width:30%;float:left;">
							<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
						</div>
					</div>
                    <div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                        <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                        <?= Html::a('Cancel', ['/focusareas/update?id='.$_GET['page_id'].'&type=page-focusareas'], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

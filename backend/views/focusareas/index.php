<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;
use backend\models\Category;

$this->title = 'Focus Areas';
?>
<div class="panel panel-default" >
	<div class="panel-heading">
		<?php echo $this->title; ?>
		<div class="menuLink">
			<a href="<?php echo BASE_URL; ?>focusareas/create?page_id=<?= $_GET['id']; ?>&type=page-focusareas" title="Create Focus Areas">
				<i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
			</a>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			
			<?=
			GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'summary' => FALSE,
				'columns' => [

					[
						'label' => 'State Name',
						'attribute' => 'state_name',
						'value' => 'state_name',
						'filter' => false,
					],
					[
						'label' => 'Operational Beneficiary',
						'attribute' => 'operational_beneficiary',
						'value' => 'operational_beneficiary',
						'filter' => false,
					],
					[
						'label' => 'category Name',
						'attribute' => 'category_id',
						'value' => function($model){
							$data = Category::find()->select('title')->where(['id'=>$model->category_id])->one();
							if(!empty($data['title'])){
								return $data['title'];
							}
						},
						'filter' => false,
					],
					[
						'label' => 'Image',
						'attribute' => 'image',
						'format' => 'raw',
						'value' => function($dataProvider){
							return '<img src="'.BASE_URL.'/uploads/focusareas/'.$dataProvider->image.'" height="50" width="50"/>';
						},
						'filter' => false,
					],
					[
						'label' => 'Status',
						'attribute' => 'status',
						'value' => 'status',
						'filter' => false,
					],
					[
						'class' => 'yii\grid\ActionColumn',
						'template' => '{update}',
						'buttons' => [
							'update' => function ($url, $searchModel) {
							return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
										'title' => Yii::t('yii', 'Update'),
							]);
						},
						],
						'urlCreator' => function ($action, $searchModel, $key, $index) {
						if ($action === 'update') {
							$url = \Yii::$app->urlManager->createAbsoluteUrl('focusareas/form' . '?id=' . $searchModel->id . '&page_id=' . $_GET['id'] . '&type=page-focusareas');
								return $url;
							}
						}
					],
					
					
				],
			]);
			?>
			
		</div>
	</div>
</div>
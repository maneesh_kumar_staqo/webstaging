<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$bannerClass = "";
$contentClass = "";
$focusClass = "";
$pageLayoutClass = "";
if(!empty($type)){
	if($type == "page-banners"){
		$bannerClass = "active";
	}else if($type == "page-contents"){
		$contentClass = "active";
	}else if($type == "page-focusareas"){
		$focusClass = "active";
	}else if($type == "page-focus"){
		$pageLayoutClass = "active";
	}
}else{
	$pageLayoutClass = "active"; 
}

$this->title = 'Update Page Layout';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="doctor-master-update">
    <div class="row">
        <div role="tabpanel">
            <div class="col-lg-2 col-md-offset-0" style="margin-left: -25px;margin-top: 5px">
                <!-- Nav tabs -->
                <ul class="left_tab nav-tabs nav-pills nav-stacked" role="tablist">
					<li role="presentation" class="<?= $pageLayoutClass; ?>"><a  href="<?= BASE_URL . 'focusareas/update?id='.$id.'&type=page-focus' ?> ">Page Layout</a></li>
					<li role="presentation" class="<?= $bannerClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'focusareas/update?id='.$id.'&type=page-banners' ?> ">Page Banners</a> <span><input type="text" id="pagePriority_banners" value="<?= !empty($priority['banners']) ? $priority['banners'] : ''; ?>" onchange="savePageLayout('banners','hfe_banners','<?= $id; ?>');"></span> </li>
                    <li role="presentation" class="<?= $contentClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'focusareas/update?id='.$id.'&type=page-contents' ?>">Page Contents</a> <span><input type="text" id="pagePriority_contents" value="<?= !empty($priority['contents']) ? $priority['contents'] : ''; ?>" onchange="savePageLayout('contents','hfe_contents','<?= $id; ?>');"></span></li>
					<li role="presentation" class="<?= $focusClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'focusareas/update?id='.$id.'&type=page-focusareas' ?>">Focus Areas</a> <span><input type="text" id="pagePriority_focusareas" value="<?= !empty($priority['focusareas']) ? $priority['focusareas'] : ''; ?>" onchange="savePageLayout('focusareas','hfe_focus_areas','<?= $id; ?>');"></span></li>
				</ul>
            </div> 
            <div class="col-lg-10">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane <?= $pageLayoutClass; ?>">
                        <div class="panel panel-default">
							 <div class="panel-heading">
								<?php echo $this->title ?>
								<div class="menuLink">
									<a href="<?php echo BASE_URL; ?>page-layout" title="List">
										<i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
									</a>
								</div>
							</div>
							<div class="panel-body">
								<div class="row table-bordered">
									<?php
									$form = ActiveForm::begin([
												'id' => 'content_form',
												'options' => [
													'enctype' => 'multipart/form-data',
												],
													]
									);
									?>            


										<div class="form-group">
											<?= $form->field($model, 'page_name')->textInput(['onkeyup' => "getPageUrl($(this).val(), 'pageUrl')"]) ?>      
										</div>
										
										<div class="form-group">
											<?= $form->field($model, 'page_url')->textInput(["id" => "pageUrl"]) ?>      
										</div>
										<div class="form-group">
											<div style="width:50%;float:left;">
												<?= $form->field($model, 'page_type')->dropDownList(['' => 'Select Page Type', 'Home Page Layout' => 'Home Page Layout','About Page Layout' => 'About Page Layout','CSR Page Layout' => 'CSR Page Layout','Project Page Layout' => 'Project Page Layout','Rooftop Solar Layout' => 'Rooftop Solar Layout','Policies Layout' => 'Policies Layout','Contact Us' => 'Contact Us', 'Video Gallery' => 'Video Gallery']); ?>
											</div>
											 <div style="width:50%;float:left;">
												<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
											</div>
										</div>
										<div class="form-group">    
											<?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 1000]) ?>
										</div>

										<div class="form-group">    
											<?= $form->field($model, 'meta_title')->textInput(['maxlength' => 1000]) ?>
										</div>
										<div class="form-group">    
											<?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>
										</div>
										<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
											<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
											<?= Html::a('Cancel', ['/pagelayout/index'], ['class' => 'btn btn-danger']) ?>
										</div>
									</div>
								</div>
								<?php ActiveForm::end(); ?>
							</div>
                    </div>
                    <div role="tabpanel" class="tab-pane <?= $bannerClass; ?>">
                        <?php
                        echo $this->render('../banners/index', [
                            'dataProvider' => $bannersDataProvider,
                            'searchModel' => $bannersDataProvider,
							'page' => 'focusareas'
                        ])
                        ?>
                    </div>
                    <div role="tabpanel" class="tab-pane <?= $contentClass; ?>">
                        <?php
                        echo $this->render('../contents/index', [
                            'dataProvider' => $contentsDataProvider,
                            'searchModel' => $contentsDataProvider,
							'page' => 'focusareas'
                        ])
                        ?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $focusClass; ?>">
                        <?php
							echo $this->render('../focusareas/index', [
								'dataProvider' => $pageFocusAreas,
								'searchModel' => $pageFocusAreas
							])
						  ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

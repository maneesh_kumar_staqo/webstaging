<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
ini_set('memory_limit', '-1');
?>
<?php $this->beginPage() ?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex">

        <title><?= Html::encode($this->title) ?> - HFE</title>

        <?php include(dirname(__FILE__) . "/../common/allcssjs.php"); ?>
        <?= Html::csrfMetaTags() ?>
        <link rel="shortcut icon" href="<?php echo BASE_URL; ?>/favicon.ico" type="image/x-icon" />
    </head>
    <body class="body">
        <div id="main-upload-image-div-bg" style="display: none;"></div>
        <div id="main-upload-image-div-gallery" style="display: none;"></div>
        <div class="msgLoading">
            <div align="center" class="loadingImg"><img src="<?= BASE_URL ?>images/loader.gif" width="30" height="30" /></div>
        </div>
        <?php $this->beginBody() ?>
        <div id="wrapper">
            <?php if (Yii::$app->controller->action->id != 'mediaimg') { ?>
                <nav class="navbar navbar-default navbar-static-top header-menu" role="navigation" style="margin-bottom: 0">
                    <?php include(dirname(__FILE__) . "/../common/header.php"); ?>
                </nav>
            <?php } ?>
            <?php if (Yii::$app->controller->id != 'featured') { ?>
                <div id="page-wrapper" style="margin: 0 0 0 0;">
                <?php } else { ?>
                    <div id="page-wrapper">
                        <?php
                    }
                    ?>
                    <div class="row">
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <br/>
            <hr/>
            <p style="text-align: center">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>
            <br/>
        </footer>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

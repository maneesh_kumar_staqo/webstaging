<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Create Menu';
$this->params['breadcrumbs'][] = $this->title;
$id = $_GET['id'];
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
        <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>menu" title="Menu List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                                ]
                );
                ?>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= $form->field($model, 'title')->textInput(['class'=>'form-control']) ?>
                            <input type="hidden" name="mainid" value="<?= $id ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= $form->field($model, 'external_link')->textInput(['class'=>'form-control']) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= $form->field($model, 'menu_description')->textarea(['rows' => 5, 'class' => 'left_class_editor']); ?>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                             <?= $form->field($model, 'section_id')->textInput(['class'=>'form-control',"id" => "pageUrl"]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                            <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                            <?= Html::a('Cancel', ['/menu'], ['class' => 'btn btn-danger']) ?>
                        </div>
                    </div>
                </div>

                
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
</div>
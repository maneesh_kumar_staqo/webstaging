<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Menu';
$id = $_GET['id'];

?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <a href="<?php echo BASE_URL; ?>sub-menu/create?id=<?= $id ?>" title="Create Menu">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Title',
                                'attribute' => 'title',
                                'value' => 'title',
                            ],
                           
							[
                                'label' => 'Section',
                                'attribute' => 'section_id',
                                'value' => 'section_id'
                            ],
							[
                                'label' => 'Status',
                                'attribute' => 'status',
                                'value' => 'status'
                            ],
								[
						'class' => 'yii\grid\ActionColumn',
						'template' => '{update}',
						'buttons' => [
							'update' => function ($url, $searchModel) {
							return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
										'title' => Yii::t('yii', 'Update'),
							]);
						},
						],
						'urlCreator' => function ($action, $searchModel, $key, $index) {
						if ($action === 'update') {
							$url = \Yii::$app->urlManager->createAbsoluteUrl('sub-menu/update' . '?id=' . $searchModel->id . '&menuid=' . $_GET['id']);
								return $url;
							}
						}
					],
                        ],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

<?php
use common\models\User;
use common\models\Utility;
$current_user = \Yii::$app->user->identity->id;
$user_type  = \Yii::$app->user->identity->user_type ;
?>
<?php if($current_user != '') { 

	?>

<ul class="nav navbar-top-links navbar-right navbar-collapse menu-heading">
	<?php 
		if($user_type!="ukraine-gea" && $user_type!="ukraine-greenway"){?>
			<li style="float:left;"><a href="<?= BASE_URL; ?>"><i class="fa fa-home fa-fw"></i> Home</a></li>
	<li>
		<a href="<?= BASE_URL . 'page-layout' ?>"><i class="fa fa-file fa-fw"></i> All Pages Layout</a>
	</li>
	<!-- <li>
		<a href="<?= BASE_URL . 'category' ?>"><i class="fa fa-newspaper-o fa-fw"></i> Category</a>
	</li> -->
	<li>
		<a href="<?= BASE_URL . 'blogs' ?>"><i class="fa fa-newspaper-o fa-fw"></i> Blogs</a>
	</li>
	<li>
		<a href="<?= BASE_URL . 'newsroom' ?>"><i class="fa fa-newspaper-o fa-fw"></i> Media Release & Media Coverage </a>
	</li>
	<li>
		<a href="<?= BASE_URL . 'youtube' ?>"><i class="fa fa-video-camera fa-fw"></i> Video Gallery Page</a>
	</li>
	<li class="dropdown">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			<i class="fa fa-bullhorn fa-fw"></i> Queries & Form &nbsp;<i class="fa fa-caret-down"></i>
		</a>
		<ul class="dropdown-menu dropdown-user">
			<li>
				<a href="<?= BASE_URL . 'contactquery'; ?>">
					<i class="fa fa-arrow-right fa-fw"></i>  Contact Us Query
				</a>
			</li>
			<li class="divider" ></li>
			<li>
				<a href="<?php echo BASE_URL . 'rooftopquery'; ?>">
					<i class="fa fa-arrow-right fa-fw"></i>  Rooftop Query
				</a>
			</li>
			<li class="divider" ></li>
			<li>
				<a href="<?php echo BASE_URL . 'careerquery'; ?>">
					<i class="fa fa-arrow-right fa-fw"></i>  Career Query
				</a>
			</li>
			<li class="divider" ></li>
			<li>
				<a href="<?php echo BASE_URL . 'calculator'; ?>">
					<i class="fa fa-arrow-right fa-fw"></i>  Solar Calculator Query
				</a>
			</li>
		</ul>	
	</li>
	<li class="dropdown">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			<i class="fa fa-file fa-fw"></i> Others &nbsp;<i class="fa fa-caret-down"></i>
		</a>
		<ul class="dropdown-menu dropdown-user">
			<li>
				<a href="<?= BASE_URL . 'category'; ?>">
					<i class="fa fa-arrow-right fa-fw"></i> Category
				</a>
			</li>
			<li class="divider" ></li>

			<li>
				<a href="<?= BASE_URL . 'other-banners'; ?>">
					<i class="fa fa-arrow-right fa-fw"></i> Other Page Banner
				</a>
			</li>
			<li class="divider" ></li>
			<li>
				<a href="<?= BASE_URL . 'contactadd'; ?>">
					<i class="fa fa-arrow-right fa-fw"></i>  Contact Us Address
				</a>
			</li>
			<li class="divider" ></li>
			<li>
				<a href="<?= BASE_URL . 'bgeladd'; ?>">
					<i class="fa fa-arrow-right fa-fw"></i>  Bgel Address
				</a>
			</li>
			<li class="divider" ></li>
			<li>
				<a href="<?= BASE_URL . 'formdropdown'; ?>">
					<i class="fa fa-arrow-right fa-fw"></i>  Form Dropdown Values
				</a>
			</li>
			<li class="divider" ></li>
			<li>
				<a href="<?= BASE_URL . 'formheading'; ?>">
					<i class="fa fa-arrow-right fa-fw"></i>  Form Heading Values
				</a>
			</li>
			<li class="divider" ></li>
			<li>
				<a href="<?= BASE_URL . 'menu'; ?>">
					<i class="fa fa-arrow-right fa-fw"></i>  Header & Footer Menu
				</a>
			</li>
			<li class="divider" ></li>
			<li>
				<a href="<?= BASE_URL . 'pdf'; ?>">
					<i class="fa fa-arrow-right fa-fw"></i>  Upload PDF
				</a>
			</li>
		</ul>
	</li>
    <li class="dropdown">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			<i class="fa fa-user fa-fw"></i> <?= Yii::$app->user->identity->username; ?> &nbsp;<i class="fa fa-caret-down"></i>
		</a>
		<ul class="dropdown-menu dropdown-user">
			<li>
				<a href="<?= BASE_URL . 'change-password'; ?>">
					<i class="fa fa-gear fa-fw"></i>  Change Password
				</a>
			</li>
			<li class="divider" ></li>
			<li>
				<a href="<?php echo BASE_URL . 'logout'; ?>">
					<i class="fa fa-sign-out fa-fw"></i>  Logout
				</a>
			</li>
		</ul>
	</li>
		<?php }
		else{?>
			<li style="float:left;"><a href="<?= BASE_URL; ?>"><i class="fa fa-home fa-fw"></i> Home</a></li>
			  <li class="dropdown">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			<i class="fa fa-user fa-fw"></i> <?= Yii::$app->user->identity->username; ?> &nbsp;<i class="fa fa-caret-down"></i>
		</a>
		<ul class="dropdown-menu dropdown-user">
			<li>
				<a href="<?= BASE_URL . 'change-password'; ?>">
					<i class="fa fa-gear fa-fw"></i>  Change Password
				</a>
			</li>
			<li class="divider" ></li>
			<li>
				<a href="<?php echo BASE_URL . 'logout'; ?>">
					<i class="fa fa-sign-out fa-fw"></i>  Logout
				</a>
			</li>
		</ul>
	</li>
		<?php }
		?>
	
</ul>
<?php } ?>

<!--All CSS Start-->
<link href="<?= BASE_URL; ?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?= BASE_URL; ?>css/admin.css" rel="stylesheet">
<link href="<?= BASE_URL; ?>css/jquery-ui.css" rel="stylesheet">
<link href="<?= BASE_URL; ?>css/jquery-autocomplete-ui.css" rel="stylesheet">
<link href="<?= BASE_URL; ?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?= BASE_URL; ?>css/jquery.datetimepicker.css" rel="stylesheet" type="text/css"/>
<!--All CSS End-->

<!--All JS Start-->
<script src="<?= BASE_URL; ?>js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="<?= BASE_URL; ?>js/bootstrap.min.js" type="text/javascript" ></script>
<script src="<?= BASE_URL; ?>js/jquery-min.js" type="text/javascript"></script>
<script src="<?= BASE_URL; ?>js/tinymce/tinymce.min.js"></script>
<script src="<?= BASE_URL; ?>js/plugins/metisMenu/metisMenu.min.js" type="text/javascript" ></script>
<script src="<?= BASE_URL; ?>js/make.copy.js"></script>
<script src="<?= BASE_URL; ?>js/jquery-ui.js"></script>
<script src="<?= BASE_URL; ?>js/jquery-autocomplete-ui.js"></script>
<script src="<?= BASE_URL; ?>js/jquery.datetimepicker.js" type="text/javascript"></script>
<script src="<?= BASE_URL; ?>js/application.js" type="text/javascript"></script>
<script type="text/javascript">

var JS_BASE_URL = '<?= BASE_URL ?>';
	
tinymce.init({
  // toolbar: 'imageupload',
  selector: ".right_class_editor",
  height: 300,
   plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
  toolbar1: "mybutton | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link code | imageupload",
  image_advtab: true,
  menubar: false,
  setup: function(editor) {
	initImageUpload(editor);
  }
});

function initImageUpload(editor) {
  // create input and insert in the DOM
  var inp = $('<input id="tinymce-uploader" type="file" name="pic" accept="image/*" style="display:none">');
  $(editor.getElement()).parent().append(inp);

  // add the image upload button to the editor toolbar
  editor.addButton('imageupload', {
	text: 'Upload Image',
	icon: false,
	onclick: function(e) { // when toolbar button is clicked, open file select modal
	  inp.trigger('click');
	}
  });

  // when a file is selected, upload it to the server
  inp.on("change", function(e){
	uploadFile($(this), editor);
  });
}

function uploadFile(inp, editor) {
  var input = inp.get(0);
  var data = new FormData();
  data.append('image[file]', input.files[0]);

  $.ajax({
	url: JS_BASE_URL + 'blogs/images',
	type: 'POST',
	data: data,
	processData: false, // Don't process the files
	contentType: false, // Set content type to false as jQuery will tell the server its a query string request
	success: function(data, textStatus, jqXHR) {
	  editor.insertContent('<img class="content-img img-fluid" src="' + data + '"/>');
	},
	error: function(jqXHR, textStatus, errorThrown) {
	  if(jqXHR.responseText) {
		errors = JSON.parse(jqXHR.responseText).errors
		alert('Error uploading image: ' + errors.join(", ") + '. Make sure the file is an image and has extension jpg/jpeg/png.');
	  }
	}
  });
}
	
	
	// tinymce.init({
    // selector: ".right_class_editor",
    // toolbar: "mybutton",
	// height: 300,
	// toolbar1: "mybutton | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link code | imageupload",
	// image_advtab: true,
    // menubar: false,
    // setup: function(editor) {
        // editor.addButton('mybutton', {
            // text:"IMAGE",
            // icon: false,
            // onclick: function(e) {
                // console.log($(e.target));
                // if($(e.target).prop("tagName") == 'BUTTON'){
                    // console.log($(e.target).parent().parent().find('input').attr('id'));
                    // if($(e.target).parent().parent().find('input').attr('id') != 'tinymce-uploader') {
                        // $(e.target).parent().parent().append('<input id="tinymce-uploader" type="file" name="pic" accept="image/*" style="display:none">');
                    // }
                    // $('#tinymce-uploader').trigger('click');
                    // $('#tinymce-uploader').change(function(){
                        // var input, file, fr, img;

                        // if (typeof window.FileReader !== 'function') {
                            // write("The file API isn't supported on this browser yet.");
                            // return;
                        // }

                        // input = document.getElementById('tinymce-uploader');
                        // if (!input) {
                            // write("Um, couldn't find the imgfile element.");
                        // } else if (!input.files) {
                            // write("This browser doesn't seem to support the `files` property of file inputs.");
                        // } else if (!input.files[0]) {
                            // write("Please select a file before clicking 'Load'");
                        // } else {
                            // file = input.files[0];
                            // fr = new FileReader();
                            // fr.onload = createImage;
                            // fr.readAsDataURL(file);
                        // }

                        // function createImage() {
                            // img = new Image();
                            // img.src = fr.result;
                            // editor.insertContent('<img src="'+img.src+'"/>');
                        // }
                    // });

                // }

                // if($(e.target).prop("tagName") == 'DIV'){
                    // if($(e.target).parent().find('input').attr('id') != 'tinymce-uploader') {
                        // console.log($(e.target).parent().find('input').attr('id'));                                
                        // $(e.target).parent().append('<input id="tinymce-uploader" type="file" name="pic" accept="image/*" style="display:none">');
                    // }
                    // $('#tinymce-uploader').trigger('click');
                    // $('#tinymce-uploader').change(function(){
                        // var input, file, fr, img;

                        // if (typeof window.FileReader !== 'function') {
                            // write("The file API isn't supported on this browser yet.");
                            // return;
                        // }

                        // input = document.getElementById('tinymce-uploader');
                        // if (!input) {
                            // write("Um, couldn't find the imgfile element.");
                        // } else if (!input.files) {
                            // write("This browser doesn't seem to support the `files` property of file inputs.");
                        // } else if (!input.files[0]) {
                            // write("Please select a file before clicking 'Load'");
                        // } else {
                            // file = input.files[0];
                            // fr = new FileReader();
                            // fr.onload = createImage;
                            // fr.readAsDataURL(file);
                        // }

                        // function createImage() {
                            // img = new Image();
                            // img.src = fr.result;
                             // editor.insertContent('<img src="'+img.src+'"/>');
                        // }
                    // });
                // }

                // if($(e.target).prop("tagName") == 'I'){
                    // console.log($(e.target).parent().parent().parent().find('input').attr('id')); if($(e.target).parent().parent().parent().find('input').attr('id') != 'tinymce-uploader') {               $(e.target).parent().parent().parent().append('<input id="tinymce-uploader" type="file" name="pic" accept="image/*" style="display:none">');
                                                                                           // }
                    // $('#tinymce-uploader').trigger('click');
                    // $('#tinymce-uploader').change(function(){
                        // var input, file, fr, img;

                        // if (typeof window.FileReader !== 'function') {
                            // write("The file API isn't supported on this browser yet.");
                            // return;
                        // }

                        // input = document.getElementById('tinymce-uploader');
                        // if (!input) {
                            // write("Um, couldn't find the imgfile element.");
                        // } else if (!input.files) {
                            // write("This browser doesn't seem to support the `files` property of file inputs.");
                        // } else if (!input.files[0]) {
                            // write("Please select a file before clicking 'Load'");
                        // } else {
                            // file = input.files[0];
                            // fr = new FileReader();
                            // fr.onload = createImage;
                            // fr.readAsDataURL(file);
                        // }

                        // function createImage() {
                            // img = new Image();
                            // img.src = fr.result;
                             // editor.insertContent('<img src="'+img.src+'"/>');
                        // }
                    // });
                // }

            // }
        // });
    // }
// });
	
	
	
    // tinymce.init({
        // selector: ".right_class_editor",
        // theme: "modern",
        // height: 300,
        // plugins: [
            // "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            // "searchreplace wordcount visualblocks visualchars code fullscreen",
            // "insertdatetime media nonbreaking save table contextmenu directionality",
            // "emoticons template paste textcolor colorpicker textpattern"
        // ],
        // body_class: "right_class",
        // toolbar1: "mybutton | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link code | imageupload",
        // image_advtab: true,
        // menubar: true,
        // setup: function(editor) {
            // editor.addButton('mybutton', {
                // text: 'Upload Image',
                // icon: false,
                // onclick: function() {
                    // $('#main-upload-image-div-bg').show();
                    // $('#main-upload-image-div').show();
                    // $('#uploadFile').val('');
                    // mediaImage();
                    // $('#widthHeight').show();
                // }
            // });
        // }
    // });
    
    tinymce.init({
        selector: ".left_class_editor",
        theme: "modern",
        height: 300,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "mybutton | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link code | imageupload",
        image_advtab: true,
        menubar: false,
        
        // setup: function(editor) {
            // editor.addButton('mybutton', {
                // text: 'Upload Image',
                // icon: false,
                // onclick: function() {
                    // $('#main-upload-image-div-bg').show();
                    // $('#uploadImgBtn').html("<input type='button' namr='upload' value='Upload' onclick='return uploadImg();'>");
                    // $('#main-upload-image-div').show();
                    // $('#uploadFile').val('');
                    // mediaImage();
                    // $('#widthHeight').show();
                // }
            // });
        // }

    });
	
	tinymce.init({
        selector: ".editor_short_desc",
        theme: "modern",
        height: 150,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "mybutton | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link code",
        image_advtab: true,
        menubar: false,
        // setup: function(editor) {
            // editor.addButton('mybutton', {
                // text: 'Upload Image',
                // icon: false,
                // onclick: function() {
                    // $('#main-upload-image-div-bg').show();
                    // $('#uploadImgBtn').html("<input type='button' namr='upload' value='Upload' onclick='return uploadImg();'>");
                    // $('#main-upload-image-div').show();
                    // $('#uploadFile').val('');
                    // mediaImage();
                    // $('#widthHeight').show();
                // }
            // });
        // }

    });
</script>
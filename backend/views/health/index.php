<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;

$this->title = 'Health Safety & Environment';
?>
<div class="panel panel-default" >
	<div class="panel-heading">
		<?php echo $this->title; ?>
		<div class="menuLink">
			<a href="<?php echo BASE_URL; ?>health/create?page_id=<?= $_GET['id']; ?>&type=page-contents&page=project" title="Create Health Safety & Environment">
				<i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
			</a>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			
			<?=
			GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'summary' => FALSE,
				'columns' => [

					[
						'label' => 'Title',
						'attribute' => 'title',
						'value' => 'title',
						'filter' => false,
					],
					[
						'label' => 'Sub Title',
						'attribute' => 'sub_title',
						'value' => 'sub_title',
						'filter' => false,
					],
					[
						'label' => 'Image',
						'attribute' => 'image',
						'format' => 'raw',
						'value' => function($dataProvider){
							if(!empty($dataProvider->image)){
								return '<img src="'.BASE_URL.'/uploads/contents/'.$dataProvider->image.'" height="50" width="50"/>';
							}
						},
						'filter' => false,
					],
					[
						'label' => 'Status',
						'attribute' => 'status',
						'value' => 'status',
						'filter' => false,
					],
					[
						'class' => 'yii\grid\ActionColumn',
						'template' => '{update}',
						'buttons' => [
							'update' => function ($url, $searchModel) {
							return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
										'title' => Yii::t('yii', 'Update'),
							]);
						},
						],
						'urlCreator' => function ($action, $searchModel, $key, $index) {
						if ($action === 'update') {
							$page = 'pagelayout';
							if($_GET['id'] == 2){
								$page = 'focusareas';
							}if($_GET['id'] == 3){
								$page = 'project';
							}if($_GET['id'] == 5){
								$page = 'rooftop';
							}if($_GET['id'] == 11){
								$page = 'career';
							}if($_GET['id'] == 9){
								$page = 'disclosures';
							}
							$type= "page-contents";
							if(!empty($_GET['type'])){
								$type= $_GET['type'];
							}
							$url = \Yii::$app->urlManager->createAbsoluteUrl('health/update' . '?id=' . $searchModel->id . '&page_id=' . $_GET['id'] . '&type='.$type.'&page='.$page);
								return $url;
							}
						}
					],
				],
			]);
			?>
			
		</div>
	</div>
</div>

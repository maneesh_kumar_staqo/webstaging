<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Update Health Safety & Environment';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?><?= $_GET['page']; ?>/update?id=<?= $_GET['page_id']; ?>&type=page-contents" title="Health Safety & Environment List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                             'enctype' => 'multipart/form-data',
						],
							]
                );
                ?>            

                     <div class="row">

                    <div class="col-md-12">
                        <?= $form->field($model, 'title')->textInput() ?>      
                    </div>
                    
					<div class="col-md-12">
                        <?= $form->field($model, 'sub_title')->textarea(['rows' => 5, 'class' => 'left_class_editor']); ?>
                       
                  
                    </div>
					<div class="col-md-12">
                        <?= $form->field($model, 'description')->textarea(['rows' => 5, 'class' => 'left_class_editor']); ?>
              
                    </div>
                    <div class="col-md-4">
                    <div class="form-group"">
                        <label for="">Image</label>
                        <input type="file" name="image" class="form-control" />
                        <input type="hidden" name="uploaded_image" id="uploaded_image_<?= $model->id; ?>" value="<?= $model->image; ?>" />
                    </div>
                    <?php if(!empty($model->image)){ ?>
                    <div style="width:15%; float:left; margin-left:16px; margin-top:24px;" id="delete_img_<?= $model->id; ?>">
                        <div style="position:absolute;margin-top:-11px;margin-left:42px;"><img src="<?= BASE_URL; ?>img/close.png"
                                style="cursor:pointer;" title="Delete Image" onClick="return deleteImg('<?= $model->id; ?>');" /></div>
                        <img src="<?= BASE_URL ?>/uploads/contents/<?= $model->image; ?>" height="50" width="50" />
                    </div>
                    <?php } ?>
                    
                    </div>
                    <div class="col-md-4">
                        <div class="form-group"">
                            <label for="">Report</label>
                            <input type="file" name="report" class="form-control" />
                           
                        </div>
                        <?php if(!empty($model->report)){ ?>
                        <div style="width:15%; float:left; margin-left:16px; margin-top:24px;" id="delete_img_<?= $model->id; ?>">
                            <div style="position:absolute;margin-top:-11px;margin-left:42px;"><img src="<?= BASE_URL; ?>img/close.png"
                                    style="cursor:pointer;" title="Delete Image" onClick="return deleteImg('<?= $model->id; ?>');" /></div>
                           <a href="<?= BASE_URL ?>/uploads/contents/<?= $model->report; ?>" target="_blank">View File</a>
                        </div>
                        <?php } ?>
                    
                    </div>
                    <div class="col-md-4">
                    
                    
                        <div class="form-group">
                            <?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
                        </div>
                    </div>
                    </div>
					
                    <div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                        <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                        <?= Html::a('Cancel', ['/'.$_GET['page'].'/update?id='.$_GET['page_id'].'&type=page-contents'], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

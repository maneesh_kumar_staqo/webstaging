<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;

$this->title = 'Banners';
?>
<div class="panel panel-default" >
	<div class="panel-heading">
		<?php echo $this->title; ?>
		<div class="menuLink">
			<a href="<?php echo BASE_URL; ?>other-banners/create" title="Create Banner">
				<i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
			</a>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			
			<?=
			GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'summary' => FALSE,
				'columns' => [

					[
						'label' => 'Title',
						'attribute' => 'title',
						'value' => 'title',
						'filter' => false,
					],
					[
						'label' => 'Page ',
						'attribute' => 'type',
						'value' => 'type',
						'filter' => false,
					],
					[
						'label' => 'Image',
						'attribute' => 'image',
						'format' => 'raw',
						'value' => function($dataProvider){
							if(!empty($dataProvider->image)){
								return '<img src="'.BASE_URL.'/uploads/banners/'.$dataProvider->image.'" height="50" width="50"/>';
							}
						},
						'filter' => false,
					],
					[
						'label' => 'Mobile Image',
						'attribute' => 'mobile_image',
						'format' => 'raw',
						'value' => function($dataProvider){
							if(!empty($dataProvider->mobile_image)){
								return '<img src="'.BASE_URL.'/uploads/banners/'.$dataProvider->mobile_image.'" height="50" width="50"/>';
							}
						},
						'filter' => false,
					],
					[
						'label' => 'Status',
						'attribute' => 'status',
						'value' => 'status',
						'filter' => false,
					],
					[
						'label' => 'Priority',
						'attribute' => 'priority',
						'value' => 'priority',
						'filter' => false,
					],
					[
						'class' => 'yii\grid\ActionColumn',
						'template' => '{update}',
						'buttons' => [
							'update' => function ($url, $searchModel) {
							return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
										'title' => Yii::t('yii', 'Update'),
							]);
						},
						],
						'urlCreator' => function ($action, $searchModel, $key, $index) {
						if ($action === 'update') {
							$page = '';
							
							$url = \Yii::$app->urlManager->createAbsoluteUrl('other-banners/update' . '?id=' . $searchModel->id);
								return $url;
							}
						}
					],
					
				],
			]);
			?>
			
		</div>
	</div>
</div>
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Update Reasons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>rooftop/update?id=<?= $_GET['page_id']; ?>&type=<?= $_GET['type']; ?>" title="Benefits List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                                ]
                );
                ?>            


                    <div class="form-group">
                        <?= $form->field($model, 'title')->textInput() ?>      
                    </div>
					<div class="form-group">
                        <?= $form->field($model, 'description')->textarea(['rows' => 5, 'class' => 'left_class_editor']); ?>      
                    </div>
					<div class="form-group">
						<div style="width:33%; float:left; margin-left:16px; margin-top:24px;">
							<input type="file" name="icon"/>
							<input type="hidden" name="uploaded_image" id="uploaded_image_<?= $model->id; ?>" value="<?= $model->icon; ?>"/>
						</div>
						<?php if(!empty($model->icon)){ ?>
						<div style="width:15%; float:left; margin-left:16px; margin-top:24px;" id="delete_img_<?= $model->id; ?>">
							<div style="position:absolute;margin-top:-11px;margin-left:42px;"><img src="<?= BASE_URL; ?>img/close.png" style="cursor:pointer;" title="Delete Image" onClick="return deleteImg('<?= $model->id; ?>');"/></div>
							<img src="<?= BASE_URL ?>/uploads/reasons/<?= $model->icon; ?>" height="50" width="50"/>
						</div>
						<?php } ?>
						<div style="width:20%;float:left;">
							<?= $form->field($model, 'priority')->textInput() ?>      
						</div>
						<div style="width:20%;float:left;">
							<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
						</div>
					</div>
                    <div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                        <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                        <?= Html::a('Cancel', ['/'.$_GET['page'].'/update?id='.$_GET['page_id'].'&type='.$_GET['type']], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

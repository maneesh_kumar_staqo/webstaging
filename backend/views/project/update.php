<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$bannerClass = "";

$commissionproject = "";
$pageLayoutClass = "";
$contentClass = "";
$awardsClass = "";
$upcomingprojects="";
$investor_image = "";
$other_content = "";
$coe = "";
if(!empty($type)){
	if($type == "page-banners"){
		$bannerClass = "active";
	}else if($type == "coe"){
		$coe = "active";
	}else if($type == "page-awards"){
		$awardsClass = "active";
	}else if($type == "commissioned-projects"){
		$commissionproject = "active";
	}else if($type == "page-project"){
		$pageLayoutClass = "active";
	}else if($type == "page-contents"){
		$contentClass = "active";
	}else if($type == "other_content"){
		$other_content = "active";
	}
	else if($type == "upcoming-projects"){
		$upcomingprojects = "active";
	}
	
}else{
	$pageLayoutClass = "active"; 
}

$this->title = 'Update Page Layout';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="doctor-master-update">
    <div class="row">
        <div role="tabpanel">
            <div class="col-lg-2 col-md-offset-0" style="margin-left: -25px;margin-top: 5px">
                <!-- Nav tabs -->
                <ul class="left_tab nav-tabs nav-pills nav-stacked" role="tablist">
					<li role="presentation" class="<?= $pageLayoutClass; ?>"><a  href="<?= BASE_URL . 'project/update?id='.$id.'&type=page-project' ?> ">Page Layout</a></li>
					<li role="presentation" class="<?= $bannerClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'project/update?id='.$id.'&type=page-banners' ?> ">Page Banners</a> <span><input type="text" id="pagePriority_banners_project" value="<?= !empty($priority['banners_project']) ? $priority['banners_project'] : ''; ?>" onchange="savePageLayout('banners_project','hfe_banners','<?= $id; ?>');"></span> </li>
                   
					<li role="presentation" class="<?= $commissionproject; ?> hasTextbox"><a  href="<?= BASE_URL . 'project/update?id='.$id.'&type=commissioned-projects' ?>">Commission Project</a> <span><input type="text" id="pagePriority_commissioned_projects" value="<?= !empty($priority['commissioned_projects']) ? $priority['commissioned_projects'] : ''; ?>" onchange="savePageLayout('commissioned_projects','commissioned_projects','<?= $id; ?>');"></span></li>
					 <li role="presentation" class="<?= $coe; ?> hasTextbox"><a  href="<?= BASE_URL . 'project/update?id='.$id.'&type=coe' ?>">COE</a> <span><input type="text" id="pagePriority_coe" value="<?= !empty($priority['coe']) ? $priority['coe'] : ''; ?>" onchange="savePageLayout('coe','coe','<?= $id; ?>');"></span></li>

					
					<li role="presentation" class="<?= $awardsClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'project/update?id='.$id.'&type=page-awards' ?>">Awards</a> <span><input type="text" id="pagePriority_awards_project" value="<?= !empty($priority['awards_project']) ? $priority['awards_project'] : ''; ?>" onchange="savePageLayout('awards_project','hfe_awards','<?= $id; ?>');"></span></li>

					  <li role="presentation" class="<?= $contentClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'project/update?id='.$id.'&type=page-contents' ?>">Health Safety & Environment</a> <span><input type="text" id="pagePriority_contents" value="<?= !empty($priority['contents']) ? $priority['contents'] : ''; ?>" onchange="savePageLayout('contents','hfe_health','<?= $id; ?>');"></span></li>

					<li role="presentation" class="<?= $upcomingprojects; ?> hasTextbox"><a  href="<?= BASE_URL . 'project/update?id='.$id.'&type=upcoming-projects' ?>">Upcoming Projects</a> <span><input type="text" id="pagePriority_upcoming-projects" value="<?= !empty($priority['upcoming-projects']) ? $priority['upcoming-projects'] : ''; ?>" onchange="savePageLayout('upcoming-projects','hfe_upcoming_projects','<?= $id; ?>');"></span></li>
				
				
				
				
				
				</ul>
            </div> 
            <div class="col-lg-10">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane <?= $pageLayoutClass; ?>">
                        <div class="panel panel-default">
							 <div class="panel-heading">
								<?php echo $this->title ?>
								<div class="menuLink">
									<a href="<?php echo BASE_URL; ?>page-layout" title="List">
										<i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
									</a>
								</div>
							</div>
							<div class="panel-body">
								<div class="row table-bordered">
									<?php
									$form = ActiveForm::begin([
												'id' => 'content_form',
												'options' => [
													'enctype' => 'multipart/form-data',
												],
													]
									);
									?>            


										<div class="form-group">
											<?= $form->field($model, 'page_name')->textInput(['onkeyup' => "getPageUrl($(this).val(), 'pageUrl')"]) ?>      
										</div>
										
										<div class="form-group">
											<?= $form->field($model, 'page_url')->textInput(["id" => "pageUrl"]) ?>      
										</div>
										<div class="form-group">
											<div style="width:50%;float:left;">
												<?= $form->field($model, 'page_type')->dropDownList(['' => 'Select Page Type', 'Home Page Layout' => 'Home Page Layout','About Page Layout' => 'About Page Layout','CSR Page Layout' => 'CSR Page Layout','Project Page Layout' => 'Project Page Layout','Rooftop Solar Layout' => 'Rooftop Solar Layout','Policies Layout' => 'Policies Layout','Contact Us' => 'Contact Us']); ?>
											</div>
											 <div style="width:50%;float:left;">
												<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
											</div>
										</div>
										<div class="form-group">    
											<?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 1000]) ?>
										</div>

										<div class="form-group">    
											<?= $form->field($model, 'meta_title')->textInput(['maxlength' => 1000]) ?>
										</div>
										<div class="form-group">    
											<?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>
										</div>
										<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
											<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
											<?= Html::a('Cancel', ['/pagelayout/index'], ['class' => 'btn btn-danger']) ?>
										</div>
									</div>
								</div>
								<?php ActiveForm::end(); ?>
							</div>
                    </div>
                    <div role="tabpanel" class="tab-pane <?= $bannerClass; ?>">
                        <?php
                        echo $this->render('../banners/index', [
                            'dataProvider' => $bannersDataProvider,
                            'searchModel' => $bannersDataProvider,
							'page' => 'project'
                        ])
                        ?>
                    </div>
                    
					<div role="tabpanel" class="tab-pane <?= $commissionproject; ?>">
                        <?php
							echo $this->render('../commissioned-projects/index', [
								'dataProvider' => $commissionedProjects,
								'searchModel' => $commissionedProjects
							])
						  ?>
                    </div>
					
					<div role="tabpanel" class="tab-pane <?= $awardsClass; ?>">
                        <?php
							echo $this->render('../awards/index', [
								'dataProvider' => $awardsDataProvider,
								'searchModel' => $awardsDataProvider,
								'page' => 'project'
							])
						  ?>
                    </div>
					
					<div role="tabpanel" class="tab-pane <?= $upcomingprojects; ?>">
                        <?php
							echo $this->render('../upcoming-projects/index', [
								'dataProvider' => $UpcomingProjects,
								'searchModel' => $UpcomingProjects,
								'page' => 'project'
							])
						  ?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $contentClass; ?>">
                        <?php
                        echo $this->render('../health/index', [
                            'dataProvider' => $contentsDataProvider,
                            'searchModel' => $contentsDataProvider,
							'page' => 'pagelayout'
                        ])
                        ?>
                    </div>
					
				
					  <div role="tabpanel" class="tab-pane <?= $coe; ?>">
                        <?php
                        echo $this->render('../coe/index', [
                            'dataProvider' => $CoeProvider,
                            'searchModel' => $CoeProvider,
							'page' => 'project'
                        ])
                        ?>
                    </div>
				
                </div>
            </div>
        </div>
    </div>
</div>

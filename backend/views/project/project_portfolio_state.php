<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;

$this->title = 'Project Portfolio State';
?>
<div class="panel panel-default" >
	<div class="panel-heading">
		<?php echo $this->title; ?>
		<div class="menuLink">
			<a href="<?php echo BASE_URL; ?>project/portfoliostate?page_id=<?= $_GET['id']; ?>&type=project_portfolio_state" title="Create Project Portfolio State">
				<i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
			</a>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			
			<?=
			GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'summary' => FALSE,
				'columns' => [

					[
						'label' => 'State Name',
						'attribute' => 'state_name',
						'value' => 'state_name',
						'filter' => false,
					],
					[
						'label' => 'Power Capacity',
						'attribute' => 'power_capacity',
						'value' => 'power_capacity',
						'filter' => false,
					],
					[
						'label' => 'category_id',
						'attribute' => 'category_id',
						'value' => 'category_id',
						'filter' => false,
					],
					[
						'label' => 'Status',
						'attribute' => 'status',
						'value' => 'status',
						'filter' => false,
					],
					[
						'class' => 'yii\grid\ActionColumn',
						'template' => '{update}',
						'buttons' => [
							'update' => function ($url, $searchModel) {
							return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
										'title' => Yii::t('yii', 'Update'),
							]);
						},
						],
						'urlCreator' => function ($action, $searchModel, $key, $index) {
						if ($action === 'update') {
							$url = \Yii::$app->urlManager->createAbsoluteUrl('project/portfoliostate' . '?id=' . $searchModel->id . '&page_id=' . $_GET['id'] . '&type=project_portfolio_state');
								return $url;
							}
						}
					],
					
					
				],
			]);
			?>
			
		</div>
	</div>
</div>
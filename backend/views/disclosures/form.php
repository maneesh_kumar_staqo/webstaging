<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Update Disclosures';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>disclosures/update?id=<?= $_GET['page_id']; ?>&type=page-disclosures" title="Disclosures List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                             'enctype' => 'multipart/form-data',
						],
							]
                );
                ?>            

                     <div class="row">
                        
                        <div class="col-md-6">
                            <?= $form->field($model, 'type')->dropDownList(['' => 'Select Status','Annual / Quarterly' => 'Annual / Quarterly', 'Monthly' => 'Monthly']); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'years')->textInput() ?>
                        </div>
                         <div class="col-md-6">
                            <?= $form->field($model, 'month')->dropDownList(['' => 'Select Month','1' => 'JANUARY', '2' => 'FEBRUARY','3' => 'MARCH','4' => 'APRIL','5' => 'MAY','6' => 'JUNE','7' => 'JULY','8' => 'AUGUST','9' => 'SEPTEMBER','10' => 'OCTOBER','11' => 'NOVEMBER','12' => 'DECEMBER']); ?>
                        </div>
                       
                        <div class="col-md-6">
                            <?= $form->field($model, 'title')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                                <div style="width:20%; float:left; margin-left:12px; margin-top:24px;">
                                    <input type="file" name="pdf" />
                                    <input type="hidden" name="uploaded_image" id="uploaded_image_<?= $model->id; ?>" value="<?= $model->pdf; ?>" />
                                </div>
                                <?php if(!empty($model->pdf)){ ?>
                                <div style="width:15%; float:left; margin-left:16px; margin-top:24px;" id="delete_img_<?= $model->id; ?>">
                                    <div style="position:absolute;margin-top:-11px;margin-left:42px;"><img src="<?= BASE_URL; ?>img/close.png"
                                            style="cursor:pointer;" title="Delete Image" onClick="return deleteImg('<?= $model->id; ?>');" /></div>
                                    <img src="<?= BASE_URL ?>img/pdf-icon.png" height="50" width="50" />
                                </div>
                                <?php } ?>
                            </div>
                            <!-- <div class="col-md-6">
                                <?= $form->field($model, 'category_id')->dropDownList($categoryData); ?>
                            </div> -->
                            <div class="col-md-6">
                                <?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                                    <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                                    <?= Html::a('Cancel', ['/disclosures/update?id='.$_GET['page_id'].'&type=page-disclosures'], ['class' => 'btn btn-danger']) ?>
                                </div>
                            </div>

            </div>
				
                   
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Create Disclosures';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>disclosures/update?id=<?= $_GET['page_id']; ?>&type=page-disclosures" title="Focus Disclosures">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                                ]
                );
                ?>            

                    <div class="row">
                        
                        <div class="col-md-6">
                           <?= $form->field($model, 'type')->dropDownList(['' => 'Select Status','Annual / Quarterly' => 'Annual / Quarterly', 'Monthly' => 'Monthly']); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'years')->textInput() ?>
                        </div>
                         <div class="col-md-6">
       
                                <?= $form->field($model, 'month')->dropDownList(['' => 'Select Month','1' => 'JANUARY', '2' => 'FEBRUARY','3' => 'MARCH','4' => 'APRIL','5' => 'MAY','6' => 'JUNE','7' => 'JULY','8' => 'AUGUST','9' => 'SEPTEMBER','10' => 'OCTOBER','11' => 'NOVEMBER','12' => 'DECEMBER']); ?>

                        </div>
                        
                        <div class="col-md-6">
                            <?= $form->field($model, 'title')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Upload PDF</label>
                                <input type="file" name="pdf" />
                            </div>
                        </div>
                        <!-- <div class="col-md-6">
                            <?= $form->field($model, 'category_id')->dropDownList($categoryData); ?>
                        </div> -->
                        <div class="col-md-6">
                            <?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
                        </div>
                      

                        <div class="col-md-12">
                            <div class="form-group" >
                                <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                                <?= Html::a('Cancel', ['/disclosures/update?id='.$_GET['page_id'].'&type=page-disclosures'], ['class' => 'btn btn-danger']) ?>
                            </div>
                        </div>
                    </div>
                   
				
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

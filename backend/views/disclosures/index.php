<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;
use backend\models\Category;

$this->title = 'Disclosures';
?>
<div class="panel panel-default" >
	<div class="panel-heading">
		<?php echo $this->title; ?>
		<div class="menuLink">
			<a href="<?php echo BASE_URL; ?>disclosures/create?page_id=<?= $_GET['id']; ?>&type=page-disclosures" title="Create Disclosures">
				<i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
			</a>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			
			<?=
			GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'summary' => FALSE,
				'columns' => [

					[
						'label' => 'title',
						'attribute' => 'title',
						'value' => 'title',
						'filter' => true,
					],
					[
						'label' => 'Year',
						'attribute' => 'years',
						'value' => 'years',
						'filter' => true,
					],
					
					// [
					// 	'label' => 'category Name',
					// 	'attribute' => 'category_id',
					// 	'value' => function($model){
					// 		$data = Category::find()->select('title')->where(['id'=>$model->category_id])->one();
					// 		if(!empty($data['title'])){
					// 			return $data['title'];
					// 		}
					// 	},
					// 	'filter' => false,
					// ],
					[
						'label' => 'Pdf',
						'attribute' => 'pdf',
						'format' => 'raw',
						'value' => function($dataProvider){
							return '<img src="'.BASE_URL.'img/pdf-icon.png" height="50" width="50"/>';
						},
						'filter' => false,
					],
					[
						'label' => 'Status',
						'attribute' => 'status',
						'value' => 'status',
						'filter' => true,
					],
					[
						'class' => 'yii\grid\ActionColumn',
						'template' => '{update}',
						'buttons' => [
							'update' => function ($url, $searchModel) {
							return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
										'title' => Yii::t('yii', 'Update'),
							]);
						},
						],
						'urlCreator' => function ($action, $searchModel, $key, $index) {
						if ($action === 'update') {
							$url = \Yii::$app->urlManager->createAbsoluteUrl('disclosures/form' . '?id=' . $searchModel->id . '&page_id=' . $_GET['id'] . '&type=page-disclosures');
								return $url;
							}
						}
					],
					
					
				],
			]);
			?>
			
		</div>
	</div>
</div>
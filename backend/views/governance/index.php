<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;

$this->title = 'Corporate Governance';
?>
<div class="panel panel-default" >
	<div class="panel-heading">
		<?php echo $this->title; ?>
		<div class="menuLink">
			<a href="<?php echo BASE_URL; ?>governance/create?page_id=<?= $_GET['id']; ?>&type=page-governance" title="Create Corporate Governance">
				<i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
			</a>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			
			<?=
			GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'summary' => FALSE,
				'columns' => [

					[
						'label' => 'Title',
						'attribute' => 'title',
						'value' => 'title',
						'filter' => false,
					],
					[
						'label' => 'Sub Title',
						'attribute' => 'sub_title',
						'value' => 'sub_title',
						'filter' => false,
					],
					[
						'label' => 'Priority',
						'attribute' => 'priority',
						'value' => 'priority',
						'filter' => false,
					],
					[
						'label' => 'BG Color',
						'attribute' => 'bg_color',
						'value' => 'bg_color',
						'filter' => false,
					],
					[
						'label' => 'Status',
						'attribute' => 'status',
						'value' => 'status',
						'filter' => false,
					],
					[
						'class' => 'yii\grid\ActionColumn',
						'template' => '{update}',
						'buttons' => [
							'update' => function ($url, $searchModel) {
							return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
										'title' => Yii::t('yii', 'Update'),
							]);
						},
						],
						'urlCreator' => function ($action, $searchModel, $key, $index) {
						if ($action === 'update') {
							$url = \Yii::$app->urlManager->createAbsoluteUrl('governance/update' . '?id=' . $searchModel->id . '&page_id=' . $_GET['id'] . '&type=page-governance');
								return $url;
							}
						}
					],
				],
			]);
			?>
			
		</div>
	</div>
</div>

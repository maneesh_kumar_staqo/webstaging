<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$bannerClass = "";
$contentClass = "";
$directorsClass = "";
$managementClass = "";
$governanceClass = "";
$imagesClass = "";
$leadershipClass = "";
$pageLayoutClass = "";
if(!empty($type)){
	if($type == "page-banners"){
		$bannerClass = "active";
	}else if($type == "page-contents"){
		$contentClass = "active";
	}else if($type == "board-directors"){
		$directorsClass = "active";
	}else if($type == "management-team"){
		$managementClass = "active";
	}
	
	else if($type == "page-governance"){
		$governanceClass = "active";
	}else if($type == "our-value"){
		$imagesClass = "active";
	}else if($type == "page-leadership"){
		$leadershipClass = "active";
	}else if($type == "page-layout"){
		$pageLayoutClass = "active";
	}
}else{
	$pageLayoutClass = "active"; 
}

$this->title = 'Update Page Layout';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="doctor-master-update">
    <div class="row">
        <div role="tabpanel">
            <div class="col-lg-2 col-md-offset-0" style="margin-left: -25px;margin-top: 5px">
                <!-- Nav tabs -->
                <ul class="left_tab nav-tabs nav-pills nav-stacked" role="tablist">
					<li role="presentation" class="<?= $pageLayoutClass; ?>"><a  href="<?= BASE_URL . 'pagelayout/update?id='.$id.'&type=page-layout' ?> ">Page Layout</a></li>
					<li role="presentation" class="<?= $bannerClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'pagelayout/update?id='.$id.'&type=page-banners' ?> ">Page Banners</a> <span><input type="text" id="pagePriority_banners" value="<?= !empty($priority['banners']) ? $priority['banners'] : ''; ?>" onchange="savePageLayout('banners','hfe_banners','<?= $id; ?>');"></span> </li>
                    <li role="presentation" class="<?= $contentClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'pagelayout/update?id='.$id.'&type=page-contents' ?>">Page Contents</a> <span><input type="text" id="pagePriority_contents" value="<?= !empty($priority['contents']) ? $priority['contents'] : ''; ?>" onchange="savePageLayout('contents','hfe_contents','<?= $id; ?>');"></span></li>
					<li role="presentation" class="<?= $leadershipClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'pagelayout/update?id='.$id.'&type=page-leadership' ?>">Leadership</a> <span><input type="text" id="pagePriority_leadership" value="<?= !empty($priority['leadership']) ? $priority['leadership'] : ''; ?>" onchange="savePageLayout('leadership','hfe_leadership','<?= $id; ?>');"></span></li>
					<li role="presentation" class="<?= $directorsClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'pagelayout/update?id='.$id.'&type=board-directors' ?>">Board of Directors</a> <span><input type="text" id="pagePriority_directors" value="<?= !empty($priority['directors']) ? $priority['directors'] : ''; ?>" onchange="savePageLayout('directors','hfe_board_directors','<?= $id; ?>');"></span></li>
					<!-- <li role="presentation" class="<?= $managementClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'pagelayout/update?id='.$id.'&type=management-team' ?>">Management Team</a> <span><input type="text" id="pagePriority_managementteam" value="<?= !empty($priority['managementteam']) ? $priority['managementteam'] : ''; ?>" onchange="savePageLayout('managementteam','hfe_management_team','<?= $id; ?>');"></span></li> -->
					<!-- <li role="presentation" class="<?= $governanceClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'pagelayout/update?id='.$id.'&type=page-governance' ?>">Corp. Governance</a> <span><input type="text" id="pagePriority_governance" value="<?= !empty($priority['governance']) ? $priority['governance'] : ''; ?>" onchange="savePageLayout('governance','hfe_corporate_governance','<?= $id; ?>');"></span></li> -->
                    <li role="presentation" class="<?= $imagesClass; ?> hasTextbox"><a  href="<?= BASE_URL . 'pagelayout/update?id='.$id.'&type=our-value' ?>">Our Value</a> <span><input type="text" id="pagePriority_images" value="<?= !empty($priority['images']) ? $priority['images'] : ''; ?>" onchange="savePageLayout('images','hfe_our_value','<?= $id; ?>');"></span></li>
				</ul>
            </div> 
            <div class="col-lg-10">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane <?= $pageLayoutClass; ?>">
                        <div class="panel panel-default">
							 <div class="panel-heading">
								<?php echo $this->title ?>
								<div class="menuLink">
									<a href="<?php echo BASE_URL; ?>page-layout" title="List">
										<i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
									</a>
								</div>
							</div>
							<div class="panel-body">
								<div class="row table-bordered">
									<?php
									$form = ActiveForm::begin([
												'id' => 'content_form',
												'options' => [
													'enctype' => 'multipart/form-data',
												],
													]
									);
									?>            


										<div class="form-group">
											<?= $form->field($model, 'page_name')->textInput(['onkeyup' => "getPageUrl($(this).val(), 'pageUrl')"]) ?>      
										</div>
										
										<div class="form-group">
											<?= $form->field($model, 'page_url')->textInput(["id" => "pageUrl"]) ?>      
										</div>
										<div class="form-group">
											<div style="width:50%;float:left;">
												<?= $form->field($model, 'page_type')->dropDownList(['' => 'Select Page Type', 'Home Page Layout' => 'Home Page Layout','About Page Layout' => 'About Page Layout','CSR Page Layout' => 'CSR Page Layout','Project Page Layout' => 'Project Page Layout','Rooftop Solar Layout' => 'Rooftop Solar Layout','Policies Layout' => 'Policies Layout','Contact Us' => 'Contact Us', 'Video Gallery' => 'Video Gallery','Disclosures' => 'Disclosures','Investor Relation' => 'Investor Relation']); ?>
											</div>
											 <div style="width:50%;float:left;">
												<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
											</div>
										</div>
										<div class="form-group">    
											<?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 1000]) ?>
										</div>

										<div class="form-group">    
											<?= $form->field($model, 'meta_title')->textInput(['maxlength' => 1000]) ?>
										</div>
										<div class="form-group">    
											<?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>
										</div>
										<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
											<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
											<?= Html::a('Cancel', ['index'], ['class' => 'btn btn-danger']) ?>
										</div>
									</div>
								</div>
								<?php ActiveForm::end(); ?>
							</div>
                    </div>
                    <div role="tabpanel" class="tab-pane <?= $bannerClass; ?>">
                        <?php
                        echo $this->render('../banners/index', [
                            'dataProvider' => $bannersDataProvider,
                            'searchModel' => $bannersDataProvider,
                            'page' => 'pagelayout'
                        ])
                        ?>
                    </div>
                    <div role="tabpanel" class="tab-pane <?= $contentClass; ?>">
                        <?php
                        echo $this->render('../contents/index', [
                            'dataProvider' => $contentsDataProvider,
                            'searchModel' => $contentsDataProvider,
							'page' => 'pagelayout'
                        ])
                        ?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $leadershipClass; ?>">
					   <?php
							echo $this->render('../leadership/index', [
								'dataProvider' => $pageLeadership,
								'searchModel' => $pageLeadership
							])
						?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $directorsClass; ?>">
					   <?php
							echo $this->render('../directors/index', [
								'dataProvider' => $boardDirectorsDataProvider,
								'searchModel' => $boardDirectorsDataProvider
							])
						?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $managementClass; ?>">
                       <?php
							echo $this->render('../managementteam/index', [
								'dataProvider' => $managementDataProvider,
								'searchModel' => $managementDataProvider
							])
						?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $governanceClass; ?>">
                         <?php
							echo $this->render('../governance/index', [
								'dataProvider' => $governanceDataProvider,
								'searchModel' => $governanceDataProvider
							])
						  ?>
                    </div>
					<div role="tabpanel" class="tab-pane <?= $imagesClass; ?>">
                        <?php
							echo $this->render('../our-value/index', [
								'dataProvider' => $ourValueDataProvider,
								'searchModel' => $ourValueDataProvider,
								'page' => 'pagelayout'
							])
						  ?>
                    </div>
				</div>
            </div>
        </div>
    </div>
</div>

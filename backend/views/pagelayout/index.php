<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\ArrayHelper;
use common\models\Utility;

$this->title = 'Page Layout';
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default" >
            <div class="panel-heading">
                <?php echo $this->title; ?>
                <div class="menuLink">
                    <a href="<?php echo BASE_URL; ?>page-layout-create" title="Create Page">
                        <i class="fa fa-plus-circle fa-fw" style="font-size: 18px !important;"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    
					<?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => FALSE,
                        'columns' => [

                            [
                                'label' => 'Page Name',
                                'attribute' => 'page_name',
                                'value' => 'page_name',
                            ],
							[
                                'label' => 'Page Url',
                                'attribute' => 'page_url',
                                'value' => 'page_url',
                            ],
							[
                                'label' => 'Page Type',
                                'attribute' => 'page_type',
                                'value' => 'page_type',
                            ],
                            [
                                'label' => 'Status',
                                'attribute' => 'status',
                                'value' => 'status',
                            ],
							[
								'class' => 'yii\grid\ActionColumn',
								'template' => '{update}',
								'buttons' => [
									'update' => function ($url, $searchModel) {
									return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
												'title' => Yii::t('yii', 'Update'),
									]);
								},
								],
								'urlCreator' => function ($action, $searchModel, $key, $index) {
									if ($searchModel->id == 2) {
										$url = \Yii::$app->urlManager->createAbsoluteUrl('focusareas/update' . '?id=' . $searchModel->id);
										return $url;
									}else if ($searchModel->id == 3) {
										$url = \Yii::$app->urlManager->createAbsoluteUrl('project/update' . '?id=' . $searchModel->id);
										return $url;
									}else if ($searchModel->id == 4) {
										$url = \Yii::$app->urlManager->createAbsoluteUrl('home/update' . '?id=' . $searchModel->id);
										return $url;
									}else if ($searchModel->id == 5) {
										$url = \Yii::$app->urlManager->createAbsoluteUrl('rooftop/update' . '?id=' . $searchModel->id);
										return $url;
									}else if ($searchModel->page_type == "Policies Layout") {
										$url = \Yii::$app->urlManager->createAbsoluteUrl('policies/update' . '?id=' . $searchModel->id);
										return $url;
									}else if ($searchModel->page_type == "Contact Us") {
										$url = \Yii::$app->urlManager->createAbsoluteUrl('contact/update' . '?id=' . $searchModel->id);
										return $url;
									}else if ($searchModel->page_type == "Video Gallery") {
										$url = \Yii::$app->urlManager->createAbsoluteUrl('videogallery/update' . '?id=' . $searchModel->id);
										return $url;
									}else if ($searchModel->page_type == "Disclosures") {
									
										$url = \Yii::$app->urlManager->createAbsoluteUrl('disclosures/update' . '?id=' . $searchModel->id);
										return $url;
									}else if ($searchModel->page_type == "Career Page Layout") {
										$url = \Yii::$app->urlManager->createAbsoluteUrl('career/update' . '?id=' . $searchModel->id);
										return $url;
									}else if ($searchModel->page_type == "Investor Relation") {
										$url = \Yii::$app->urlManager->createAbsoluteUrl('investor-relations/update' . '?id=' . $searchModel->id);
										return $url;
									}else{
										$url = \Yii::$app->urlManager->createAbsoluteUrl('pagelayout/update' . '?id=' . $searchModel->id);
										return $url;
									}
								}
							],
                        ],
                    ]);
                    ?>
					
                </div>
            </div>
        </div>
    </div>
</div>

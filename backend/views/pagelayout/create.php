<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $model->isNewRecord ? 'Create Page Layout' : 'Update Page Layout';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-10 col-lg-offset-1">
    <div class="panel panel-default">
         <div class="panel-heading">
            <?php echo $this->title ?>
            <div class="menuLink">
                <a href="<?php echo BASE_URL; ?>page-layout" title="List">
                    <i class="fa fa-list-ul fa-fw" style="font-size: 18px !important;"></i>
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row table-bordered">
                <?php
                $form = ActiveForm::begin([
                            'id' => 'content_form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                                ]
                );
                ?>            


                    <div class="form-group">
                        <?= $form->field($model, 'page_name')->textInput(['onkeyup' => "getPageUrl($(this).val(), 'pageUrl')"]) ?>      
                    </div>
					
					<div class="form-group">
                        <?= $form->field($model, 'page_url')->textInput(["id" => "pageUrl"]) ?>      
                    </div>
					<div class="form-group">
						<div style="width:50%;float:left;">
							<?= $form->field($model, 'page_type')->dropDownList(['' => 'Select Page Type', 'Home Page Layout' => 'Home Page Layout','About Page Layout' => 'About Page Layout','CSR Page Layout' => 'CSR Page Layout','Project Page Layout' => 'Project Page Layout','Rooftop Solar Layout' => 'Rooftop Solar Layout','Policies Layout' => 'Policies Layout','Contact Us' => 'Contact Us', 'Video Gallery' => 'Video Gallery', 'Disclosures' => 'Disclosures', 'Career Page Layout' => 'Career Page Layout','Investor Relation' => 'Investor Relation']); ?>
						</div>
						 <div style="width:50%;float:left;">
							<?= $form->field($model, 'status')->dropDownList(['' => 'Select Status','Active' => 'Active', 'Inactive' => 'Inactive']); ?>
						</div>
					</div>
                    <div class="form-group">    
                        <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 1000]) ?>
                    </div>

                    <div class="form-group">    
                        <?= $form->field($model, 'meta_title')->textInput(['maxlength' => 1000]) ?>
                    </div>
                    <div class="form-group">    
                        <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>
                    </div>
					<div class="form-group" style="margin-left: 20px;margin-top: 20px;">
                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-danger']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
